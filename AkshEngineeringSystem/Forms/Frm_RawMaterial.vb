﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
'Imports ELCF
Public Class Frm_RawMaterial
    Dim DA As New OleDbDataAdapter
    Dim DS As New DataSet
    Dim BS As New BindingSource
    Dim DADetails As New OleDbDataAdapter
    Dim Status As Integer = 0

    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetDataTable()
        SetQuery()
        SetBinding()
        'InitLookup()
        AddNew()
        ProductNameTextEdit.Focus()
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Function SetDataTable()
        DS.Tables.Add("RawMaterial")
        With DS.Tables("RawMaterial").Columns
            .Add("RawMaterialID", GetType(Integer))
            '.Add("CategoryID", GetType(Integer))
            .Add("ProductName", GetType(String))
            .Add("UOM", GetType(String))
            .Add("HSNNoOrSACNo", GetType(String))
            .Add("Price", GetType(Double))
            .Add("Company", GetType(String))
        End With

        With DS.Tables("RawMaterial").Columns("RawMaterialID")
            .AutoIncrement = True
            .AutoIncrementSeed = -1
            .AutoIncrementStep = -1
            .ReadOnly = True
            .Unique = True
        End With
        Return True
    End Function

    Function SetQuery()
        DA.SelectCommand = New OleDbCommand("Select RawMaterialID,ProductName,UOM,HSNNoOrSACNo,Price,Company From RawMaterial Where RawMaterialID=@RawMaterialID", ConStr)
        DA.SelectCommand.Parameters.Add("@RawMaterialID", OleDbType.Integer, 4, "RawMaterialID")

        DA.InsertCommand = New OleDbCommand("Insert Into RawMaterial (ProductName,UOM,HSNNoOrSACNo,Price,Company) Values (@ProductName,@UOM,@HSNNoOrSACNo,@Price,@Company)", ConStr)
        DA.InsertCommand.Parameters.Add("@ProductName", OleDbType.VarChar, 250, "ProductName")
        DA.InsertCommand.Parameters.Add("@UOM", OleDbType.VarChar, 50, "UOM")
        DA.InsertCommand.Parameters.Add("@HSNNoOrSACNo", OleDbType.VarChar, 50, "HSNNoOrSACNo")
        DA.InsertCommand.Parameters.Add("@Price", OleDbType.Double, 8, "Price")
        DA.InsertCommand.Parameters.Add("@Company", OleDbType.VarChar, 50, "Company")

        DA.UpdateCommand = New OleDbCommand("Update RawMaterial Set ProductName=@ProductName,UOM=@UMO,HSNNoOrSACNo=@HSNNoOrSACNo,Price=@Price Where RawMaterialID=@RawMaterialID", ConStr)
        DA.UpdateCommand.Parameters.Add("@ProductName", OleDbType.VarChar, 250, "ProductName")
        DA.UpdateCommand.Parameters.Add("@UOM", OleDbType.VarChar, 50, "UOM")
        DA.UpdateCommand.Parameters.Add("@HSNNoOrSACNo", OleDbType.VarChar, 50, "HSNNoOrSACNo")
        DA.UpdateCommand.Parameters.Add("@Price", OleDbType.Double, 8, "Price")
        DA.UpdateCommand.Parameters.Add("@RawMaterialID", OleDbType.Integer, 4, "RawMaterialID")


        DA.DeleteCommand = New OleDbCommand("Delete From RawMaterial Where RawMaterialID=@RawMaterialID", ConStr)
        DA.DeleteCommand.Parameters.Add("@RawMaterialID", OleDbType.Integer, 4, "RawMaterialID")
        Return True
    End Function

    Function SetBinding()
        BS.DataSource = DS.Tables("RawMaterial")
        'CategoryIDLookUpEdit.DataBindings.Add(New Binding("EditValue", BS, "CategoryID"))
        ProductNameTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ProductName"))
        UOMComboBoxEdit.DataBindings.Add(New Binding("EditValue", BS, "UOM"))
        HSNNoOrSACNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "HSNNoOrSACNo"))
        PriceTextEdit.DataBindings.Add(New Binding("EditValue", BS, "Price"))
        'OpenStockTextEdit.DataBindings.Add(New Binding("EditValue", BS, "OpeningStock"))
        'CurrentStockTextEdit.DataBindings.Add(New Binding("EditValue", BS, "CurrentStock"))
        'CloseStockTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ClosingStock"))
        'ReorderLevelTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ReorderLevel"))
        Return True
    End Function
    'Sub InitLookup()
    '    SetLookUp("SELECT CategoryID, CategoryName FROM Category", "Category", "CategoryID", "CategoryName", CategoryIDLookUpEdit, "Category")
    'End Sub

    Sub AddNew()
        BS.AddNew()
        ProductNameTextEdit.Focus()
    End Sub

    Private Sub NewBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles NewBarButtonItem.ItemClick
        BS.CancelEdit()
        DS.RejectChanges()
        AddNew()
        ProductNameTextEdit.Focus()
    End Sub

    Private Sub OpenBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles OpenBarButtonItem.ItemClick
        Dim ID = ShowRecord("Select * From RawMaterial Where Company ='" + PubCompanyName + "'", "RawMaterialID")
        If ID > 0 Then
            Try
                DS.Tables("RawMaterial").Clear()
            Catch
            End Try
            DA.SelectCommand.Parameters("@RawMaterialID").Value = ID
            DA.Fill(DS, "RawMaterial")
        End If
    End Sub

    Private Sub DeleteBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DeleteBarButtonItem.ItemClick
        Try
            Dim Delete = XtraMessageBox.Show("Are You Want To Delete This Record", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
            If Delete = 1 Then
                BS.RemoveCurrent()
                DA.Update(DS.Tables("RawMaterial"))
                AddNew()
            End If
        Catch ex As Exception
            BS.CancelEdit()
            DS.RejectChanges()
            XtraMessageBox.Show("Operation Failed :", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Function Validation() As Boolean
        If ProductNameTextEdit.EditValue Is DBNull.Value Then
            ProductNameTextEdit.Focus()
            Return False
        ElseIf HSNNoOrSACNoTextEdit.EditValue Is DBNull.Value Then
            HSNNoOrSACNoTextEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveSimpleButton.Click
        If Validation() Then
            BS.Current!Company = PubCompanyName
            BS.Current!UOM = UOMComboBoxEdit.Text
            BS.Current!ProductName = ProductNameTextEdit.Text
            BS.EndEdit()
            DA.Update(DS.Tables("RawMaterial"))
            AddNew()
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub ProductNameTextEdit_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ProductNameTextEdit.Validating
        If Not ProductNameTextEdit.EditValue Is DBNull.Value Then
            Try
                Dim CMD As New OleDbCommand("SELECT Count(RawMaterialID) FROM RawMaterial WHERE ProductName=@ProductName AND RawMaterialID<>@RawMaterialID", ConStr)
                CMD.Parameters.AddWithValue("@ProductName", ProductNameTextEdit.EditValue)
                CMD.Parameters.AddWithValue("@RawMaterialID", IIf(BS.Current!RawMaterialID Is DBNull.Value, -1, BS.Current!RawMaterialID))
                CnnOpen() : Dim Verify As Integer = Val(CMD.ExecuteScalar & "") : CnnClose()
                If Verify <> 0 Then
                    XtraMessageBox.Show("Value Exist! Enter Unique Value.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ProductNameTextEdit.Focus()
                    e.Cancel = True
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub

    'Private Sub OpenStockTextEdit_Leave(sender As Object, e As EventArgs)
    '    CurrentStockTextEdit.Text = OpenStockTextEdit.Text
    'End Sub

    'Private Sub HSNNoOrSACNoTextEdit_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles HSNNoOrSACNoTextEdit.Validating
    '    Dim iNumber As Integer
    '    If Not Integer.TryParse(HSNNoOrSACNoTextEdit.Text, iNumber) Then
    '        XtraMessageBox.Show("Not a Number! Please enter only Numerical Values.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        HSNNoOrSACNoTextEdit.Focus()
    '        e.Cancel = True
    '    End If
    'End Sub

    'Private Sub CategoryIDLookUpEdit_EditValueChanged(sender As Object, e As EventArgs) Handles CategoryIDLookUpEdit.EditValueChanged
    '    Try
    '        If (CategoryIDLookUpEdit.EditValue IsNot DBNull.Value) Then
    '            SetLookUp("Select SubCategoryID,SubCategoryName From SubCategory Where CategoryID = " + CategoryIDLookUpEdit.EditValue.ToString + "", "SubCategory", "SubCategoryID", "SubCategoryName", SubCategoryIDLookUpEdit, "Sub Category")
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
End Class