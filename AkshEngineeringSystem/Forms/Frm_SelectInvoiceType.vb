﻿Imports System.Globalization
Public Class Frm_SelectInvoiceType
    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Public Sub OpenForm(Form As DevExpress.XtraEditors.XtraForm, Optional Index As Boolean = False, Optional ByRef Index1 As Boolean = False)
        If Index1 = False Then Start_Waiting()
        Form.MdiParent = Frm_Main
        If Index = False Then Form.WindowState = FormWindowState.Maximized Else Form.WindowState = FormWindowState.Normal
        Form.Show()
        Form.BringToFront()
    End Sub

    Private Sub OkSimpleButton_Click(sender As Object, e As EventArgs) Handles OkSimpleButton.Click
        If InvoiceTypeComboBoxEdit.Text = "Sales Invoice" Then
            PubIGST = InterStateRadioButton.Checked
            If PubIGST Then
                PubInvoiceType = "Sales Invoice IGST"
            Else
                PubInvoiceType = "Sales Invoice"
            End If
            OpenForm(Frm_InvoicePL)
            With Frm_InvoicePL
                .InitLookup()
            End With
            End_Waiting()
            Me.Close()
        ElseIf InvoiceTypeComboBoxEdit.Text = "SEZ Sales Invoice" Then
            PubIGST = InterStateRadioButton.Checked
            PubWithLUT = WithLUTRadioButton.Checked
            If PubIGST Then
                PubInvoiceType = "SEZ Sales Invoice IGST"
            Else
                PubInvoiceType = "SEZ Sales Invoice"
            End If
            OpenForm(Frm_InvoicePL)
            With Frm_InvoicePL
                .InitLookup()
            End With
            End_Waiting()
            Me.Close()
        ElseIf InvoiceTypeComboBoxEdit.Text = "Export Sales Invoice" Then
            PubIGST = InterStateRadioButton.Checked
            PubWithLC = WithLCRadioButton.Checked
            If PubIGST Then
                PubInvoiceType = "Export Sales Invoice IGST"
            Else
                PubInvoiceType = "Export Sales Invoice"
            End If
            OpenForm(Frm_Invoice_Export)
            With Frm_Invoice_Export
                .InitLookup()
            End With
            End_Waiting()
            Me.Close()
        End If
        'OpenForm(Frm_InvoicePL)
        'With Frm_InvoicePL
        '    .InitLookup()
        'End With
        'End_Waiting()
        'Me.Close()
    End Sub
    Private Sub Frm_SelectInvoiceType_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InvoiceTypeComboBoxEdit.Text = "Sales Invoice"
    End Sub

    Private Sub InvoiceTypeComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles InvoiceTypeComboBoxEdit.SelectedIndexChanged
        If InvoiceTypeComboBoxEdit.Text = "SEZ Sales Invoice" Then
            LocalStateRadioButton.Enabled = True
            LocalStateRadioButton.Checked = True
            InterStateRadioButton.Checked = False
            WithLUTRadioButton.Enabled = True
            WithoutLUTRadioButton.Enabled = True
            WithLUTRadioButton.Checked = True
            WithLCRadioButton.Enabled = False
            WithoutLCRadioButton.Enabled = False
            PriceTypeComboBoxEdit.Enabled = False
            PriceTypeComboBoxEdit.SelectedIndex = -1
        ElseIf InvoiceTypeComboBoxEdit.Text = "Export Sales Invoice" Then
            LocalStateRadioButton.Enabled = False
            InterStateRadioButton.Enabled = True
            InterStateRadioButton.Checked = True
            WithoutLUTRadioButton.Enabled = False
            WithLUTRadioButton.Enabled = False
            WithLCRadioButton.Enabled = True
            WithoutLCRadioButton.Enabled = True
            WithLCRadioButton.Checked = True
            PriceTypeComboBoxEdit.Enabled = True
            PriceTypeComboBoxEdit.SelectedIndex = 0
        Else
            LocalStateRadioButton.Enabled = True
            LocalStateRadioButton.Checked = True
            InterStateRadioButton.Checked = False
            WithLCRadioButton.Enabled = False
            WithoutLCRadioButton.Enabled = False
            WithoutLUTRadioButton.Enabled = False
            WithLUTRadioButton.Enabled = False
            PriceTypeComboBoxEdit.Enabled = False
            PriceTypeComboBoxEdit.SelectedIndex = -1
        End If
    End Sub

    Private Sub PriceTypeComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles PriceTypeComboBoxEdit.SelectedIndexChanged
        If PriceTypeComboBoxEdit.Text = "Dollar ($)" Then
            PubCurrency = New CultureInfo("en-US")
        ElseIf PriceTypeComboBoxEdit.Text = "Euro (€)" Then
            PubCurrency = New CultureInfo("it-IT")
        ElseIf PriceTypeComboBoxEdit.Text = "Pound (£)" Then
            PubCurrency = New CultureInfo("en-GB")
        Else
            PubCurrency = New CultureInfo("hi-IN")
        End If
    End Sub
End Class