﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraReports.UI
Imports System.Data.OleDb
Public Class Frm_WorkOrderReport
    Dim DS As New DataSet

    Dim WorkOrderDA As New OleDbDataAdapter
    Dim WorkOrderBS As New BindingSource

    Dim PaymentDA As New OleDbDataAdapter
    Dim PaymentBS As New BindingSource

    Dim BillDA As New OleDbDataAdapter
    Dim BillBS As New BindingSource
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        JobNoComboBoxEdit.Focus()
        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM WorkOrder Where Company='" + PubCompanyName + "'", "WorkOrder", JobNoComboBoxEdit)
        SetLookUp("SELECT PartyID, PartyName FROM Party Where Company='" + PubCompanyName + "'", "Party", "PartyID", "PartyName", PartyLookUpEdit, "Party Name")
        SetLookUp("Select WorkOrderID, WorkOrderNo FROM WorkOrder Where Company='" + PubCompanyName + "'", "WorkOrder", "WorkOrderID", "WorkOrderNo", WorkOrderPOLookUpEdit, "Order No")
    End Sub

    Sub setGridData(condition As String)
        'WorkOrderDA.SelectCommand = New OleDbCommand("SELECT WorkOrderID,JobNoPrefix,JobNo,WorkOrderNo,WorkOrderDate,ReceiverName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,WoStatus FROM WorkOrder WHERE Company='" + PubCompanyName + "'" + condition, ConStr)    
        WorkOrderDA.SelectCommand = New OleDbCommand("SELECT WorkOrder.WorkOrderID,WorkOrder.JobNoPrefix,WorkOrder.JobNo,WorkOrder.WorkOrderNo,WorkOrder.WorkOrderDate,WorkOrder.ReceiverName,WorkOrder.TotalAmtBeforeTax,WorkOrder.AdvancePayment,WorkOrder.GrandTotalAmount,WorkOrder.WoStatus,WorkOrder.BillStatus,WorkOrder.InvoiceType,WorkOrderPayment.RemainAmount FROM WorkOrder INNER Join WorkOrderPayment On WorkOrder.WorkOrderID = WorkOrderPayment.WorkOrderID WHERE Company='" + PubCompanyName + "' AND WorkOrderPayment.PaymentID IN (SELECT max(PaymentID) FROM WorkOrderPayment GROUP BY WorkOrderID)" + condition, ConStr)
        WorkOrderDA.Fill(DS, "WorkOrder")

        PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,WorkOrderID,JobNo,WONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From WorkOrderPayment", ConStr)
        PaymentDA.Fill(DS, "WorkOrderPayment")

        BillDA.SelectCommand = New OleDbCommand("Select WorkOrderID,JobNo,InvoiceNo,InvoiceDate,WONo,ReceiverName,TotalAmtBeforeTax,TotalAmtAfterTax,GrandTotalAmount,WOBillType From WOBill", ConStr)
        BillDA.Fill(DS, "WOBill")
    End Sub

    Sub SetRelation()
        DS.Relations.Add(New DataRelation("Payments", DS.Tables("WorkOrder").Columns("WorkOrderID"), DS.Tables("WorkOrderPayment").Columns("WorkOrderID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DS.Tables("WorkOrder").Columns("WorkOrderID"), DS.Tables("WorkOrderPayment").Columns("WorkOrderID"))
        Try
            DS.Tables("WorkOrderPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        WorkOrderBS.DataSource = DS
        WorkOrderBS.DataMember = "WorkOrder"

        PaymentBS.DataSource = WorkOrderBS
        PaymentBS.DataMember = "Payments"

        DS.Relations.Add(New DataRelation("Bills", DS.Tables("WorkOrder").Columns("WorkOrderID"), DS.Tables("WOBill").Columns("WorkOrderID"), False))
        Dim FK_Bills As New Global.System.Data.ForeignKeyConstraint("FK_Order1", DS.Tables("WorkOrder").Columns("WorkOrderID"), DS.Tables("WOBill").Columns("WorkOrderID"))
        Try
            DS.Tables("WOBill").Constraints.Add(FK_Bills)
        Catch
        End Try

        With FK_Bills
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        WorkOrderBS.DataSource = DS
        WorkOrderBS.DataMember = "WorkOrder"

        BillBS.DataSource = WorkOrderBS
        BillBS.DataMember = "Bills"
    End Sub

    Sub setGrid()
        With WorkOrderGridView
            .Columns("WorkOrderID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("WorkOrderNo").Caption = "WO No."
            .Columns("WorkOrderNo").OptionsColumn.AllowFocus = False
            .Columns("WorkOrderNo").OptionsColumn.ReadOnly = True
            .Columns("WorkOrderDate").OptionsColumn.AllowFocus = False
            .Columns("WorkOrderDate").OptionsColumn.ReadOnly = True
            .Columns("ReceiverName").OptionsColumn.AllowFocus = False
            .Columns("ReceiverName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").Caption = "Advance"
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "WO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("WoStatus").OptionsColumn.AllowFocus = False
            .Columns("WoStatus").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").Caption = "Outstanding"
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True
            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("RemainAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
    End Sub

    Private Sub WorkOrderPOButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
        Dim condition As String = " AND"
        If JobNoComboBoxEdit.EditValue IsNot Nothing Then
            condition += " WorkOrder.JobNo = '" + JobNoComboBoxEdit.Text.Replace("A-", "") + "' AND"
        End If
        If PartyLookUpEdit.EditValue IsNot Nothing Then
            condition += " ReceiverName = '" + PartyLookUpEdit.Text + "' AND"
        End If
        If WorkOrderPOLookUpEdit.EditValue IsNot Nothing Then
            condition += " WorkOrderNo = '" + WorkOrderPOLookUpEdit.Text + "' AND"
        End If
        If FromDateEdit.EditValue IsNot Nothing Then
            If ToDateEdit.EditValue IsNot Nothing Then
                condition += " (WorkOrderDate BETWEEN #" & FromDateEdit.Text & "# AND #" & ToDateEdit.Text & "#) AND"
            End If
        End If
        If WOStatusComboBoxEdit.EditValue IsNot Nothing Then
            condition += " WoStatus = '" + WOStatusComboBoxEdit.Text + "' AND"
        End If
        condition = condition.Remove(condition.Length - 4)

        DS = New DataSet

        WorkOrderDA = New OleDbDataAdapter
        WorkOrderBS = New BindingSource

        PaymentDA = New OleDbDataAdapter
        PaymentBS = New BindingSource

        BillDA = New OleDbDataAdapter
        BillBS = New BindingSource

        setGridData(condition)
        SetRelation()
        WorkOrderGridControl.DataSource = WorkOrderBS
        setGrid()
    End Sub

    Private Sub ClearSimpleButton_Click(sender As Object, e As EventArgs) Handles ClearSimpleButton.Click
        JobNoComboBoxEdit.Text = Nothing
        FromDateEdit.EditValue = Nothing
        ToDateEdit.EditValue = Nothing
        PartyLookUpEdit.EditValue = Nothing
        WorkOrderPOLookUpEdit.EditValue = Nothing
        WOStatusComboBoxEdit.EditValue = Nothing
    End Sub

    Private Sub CloseSimpleButton_Click(sender As Object, e As EventArgs) Handles CloseSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub WorkOrderGridView_MasterRowExpanded(sender As Object, e As DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs) Handles WorkOrderGridView.MasterRowExpanded

        If e.RelationIndex = 0 Then
            Dim gridViewTests As GridView = CType(sender, GridView)
            Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            gridViewDefects.BeginUpdate()

            With gridViewDefects
                .Columns("PaymentID").Visible = False
                .Columns("WorkOrderID").Visible = False
                .Columns("JobNo").OptionsColumn.AllowFocus = False
                .Columns("JobNo").OptionsColumn.ReadOnly = True
                .Columns("WONo").OptionsColumn.AllowFocus = False
                .Columns("WONo").OptionsColumn.ReadOnly = True
                .Columns("CheckNo").Caption = "Cheque No."
                .Columns("CheckNo").OptionsColumn.AllowFocus = False
                .Columns("CheckNo").OptionsColumn.ReadOnly = True
                .Columns("TDS").OptionsColumn.AllowFocus = False
                .Columns("TDS").OptionsColumn.ReadOnly = True
                .Columns("ChkDate").OptionsColumn.AllowFocus = False
                .Columns("ChkDate").OptionsColumn.ReadOnly = True
                .Columns("PaidAmount").OptionsColumn.AllowFocus = False
                .Columns("PaidAmount").OptionsColumn.ReadOnly = True
                .Columns("RemainAmount").OptionsColumn.AllowFocus = False
                .Columns("RemainAmount").OptionsColumn.ReadOnly = True

                .OptionsView.ShowFooter = True
                .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End With
            gridViewDefects.EndUpdate()
        ElseIf e.RelationIndex = 1 Then
            Dim gridViewTests As GridView = CType(sender, GridView)
            Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            gridViewDefects.BeginUpdate()

            With gridViewDefects
                .Columns("WorkOrderID").Visible = False
                .Columns("JobNo").OptionsColumn.AllowFocus = False
                .Columns("JobNo").OptionsColumn.ReadOnly = True
                .Columns("InvoiceNo").OptionsColumn.AllowFocus = False
                .Columns("InvoiceNo").OptionsColumn.ReadOnly = True
                .Columns("InvoiceDate").OptionsColumn.AllowFocus = False
                .Columns("InvoiceDate").OptionsColumn.ReadOnly = True
                .Columns("WONo").OptionsColumn.AllowFocus = False
                .Columns("WONo").OptionsColumn.ReadOnly = True
                .Columns("ReceiverName").OptionsColumn.AllowFocus = False
                .Columns("ReceiverName").OptionsColumn.ReadOnly = True
                .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
                .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
                .Columns("TotalAmtAfterTax").OptionsColumn.AllowFocus = False
                .Columns("TotalAmtAfterTax").OptionsColumn.ReadOnly = True
                .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
                .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
                .Columns("WOBillType").OptionsColumn.AllowFocus = False
                .Columns("WOBillType").OptionsColumn.ReadOnly = True

                .OptionsView.ShowFooter = True
                .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End With
            gridViewDefects.EndUpdate()
        End If


    End Sub

    Private Sub ExportExcelSimpleButton_Click(sender As Object, e As EventArgs) Handles ExportExcelSimpleButton.Click
        'WorkOrderGridControl.ExportToXlsx("WorkOrderReport.xlsx")
        'Process.Start("WorkOrderReport.xlsx")
        Dim value As String
        Dim filename As String
        If WOStatusComboBoxEdit.EditValue IsNot Nothing Then
            If WOStatusComboBoxEdit.Text = "WO is closed" Then
                filename = "WorkOrderReport_WOClose_" + DateTime.Today.ToShortDateString + ".xlsx"
            ElseIf WOStatusComboBoxEdit.Text = "WO is open" Then
                filename = "WorkOrderReport_WOOpen_" + DateTime.Today.ToShortDateString + ".xlsx"
            End If
        Else
            filename = "WorkOrderReport_" + DateTime.Today.ToShortDateString + ".xlsx"
        End If
        value = "E:\Aksh Software\AkshEngineeringSystem\AkshEngineeringSystem\" + filename
        WorkOrderGridControl.ExportToXlsx(value)
        Process.Start(value)
    End Sub
End Class