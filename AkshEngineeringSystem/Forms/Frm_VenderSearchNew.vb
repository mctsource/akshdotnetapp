﻿Imports DevExpress.XtraEditors
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Grid
'Imports ELCF
Public Class Frm_VenderSearchNew
    Dim DS As New DataSet
    Dim BS As New BindingSource
    Dim BSDetails As New BindingSource
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetLookUp("SELECT PartyID, PartyName FROM Party Where Company='" + PubCompanyName + "'", "Party", "PartyID", "PartyName", PartyIDLookUpEdit, "Vendor Name")
    End Sub

    Function VendorPOValidation() As Boolean
        If PartyIDLookUpEdit.EditValue Is DBNull.Value Then
            PartyIDLookUpEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub VendorPOButton_Click(sender As Object, e As EventArgs) Handles VendorPOButton.Click
        If VendorPOValidation() Then
            DS = New DataSet
            BS = New BindingSource
            BSDetails = New BindingSource

            Dim VendorDA As New OleDbDataAdapter
            VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
            VendorDA.Fill(DS, "VendorPO")

            Dim InvoiceDA As New OleDbDataAdapter
            Dim InvoiceDT As New DataTable
            InvoiceDA.SelectCommand = New OleDbCommand("Select BillID,BillNo,JobNoPrefix,JobNo,PONo,TotalAmtBeforeTax,GrandTotalAmount From BillNew where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
            InvoiceDA.Fill(InvoiceDT)
            BillGridControl.DataSource = InvoiceDT

            Dim PaymentDA As New OleDbDataAdapter
            PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From VendorPayment", ConStr)
            PaymentDA.Fill(DS, "VendorPayment")

            SetRelation()
            OrderGridControl.DataSource = BS
            setGrid()

            'GridLookUpFunc("Select PartyID,PartyName FROM Party", "Party", "PartyID", "PartyName", OrderGridControl, OrderGridView, "PartyID", "Party Name")
        End If
    End Sub

    Sub SetRelation()
        DS.Relations.Add(New DataRelation("Payments", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPayment").Columns("PurchaseID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPayment").Columns("PurchaseID"))
        Try
            DS.Tables("VendorPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        BS.DataSource = DS
        BS.DataMember = "VendorPO"

        BSDetails.DataSource = BS
        BSDetails.DataMember = "Payments"
    End Sub

    Sub setGrid()
        With BillGridView
            .Columns("BillID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("BillNo").OptionsColumn.AllowFocus = False
            .Columns("BillNo").OptionsColumn.ReadOnly = True
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Bill Basic Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "Bill Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        BillGridView.BestFitColumns()

        With OrderGridView
            .Columns("PurchaseID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("OrderNo").Caption = "PO No."
            .Columns("OrderNo").OptionsColumn.AllowFocus = False
            .Columns("OrderNo").OptionsColumn.ReadOnly = True
            .Columns("OrderDate").OptionsColumn.AllowFocus = False
            .Columns("OrderDate").OptionsColumn.ReadOnly = True
            .Columns("ConsignorName").OptionsColumn.AllowFocus = False
            .Columns("ConsignorName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "PO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("POStatus").OptionsColumn.AllowFocus = False
            .Columns("POStatus").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        OrderGridView.BestFitColumns()
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub OrderGridView_MasterRowExpanded(sender As Object, e As DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs) Handles OrderGridView.MasterRowExpanded
        Dim gridViewTests As GridView = CType(sender, GridView)
        Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
        gridViewDefects.BeginUpdate()
        With gridViewDefects
            .Columns("PaymentID").Visible = False
            .Columns("PurchaseID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("TDS").OptionsColumn.AllowFocus = False
            .Columns("TDS").OptionsColumn.ReadOnly = True
            .Columns("CheckNo").Caption = "Cheque No."
            .Columns("CheckNo").OptionsColumn.AllowFocus = False
            .Columns("CheckNo").OptionsColumn.ReadOnly = True
            .Columns("ChkDate").OptionsColumn.AllowFocus = False
            .Columns("ChkDate").OptionsColumn.ReadOnly = True
            .Columns("PaidAmount").OptionsColumn.AllowFocus = False
            .Columns("PaidAmount").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        gridViewDefects.BestFitColumns()
        gridViewDefects.EndUpdate()
    End Sub
End Class