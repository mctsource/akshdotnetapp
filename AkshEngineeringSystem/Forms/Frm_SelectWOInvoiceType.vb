﻿Public Class Frm_SelectWOInvoiceType
    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Public Sub OpenForm(Form As DevExpress.XtraEditors.XtraForm, Optional Index As Boolean = False, Optional ByRef Index1 As Boolean = False)
        If Index1 = False Then Start_Waiting()
        Form.MdiParent = Frm_Main
        If Index = False Then Form.WindowState = FormWindowState.Maximized Else Form.WindowState = FormWindowState.Normal
        Form.Show()
        Form.BringToFront()
    End Sub

    Private Sub OkSimpleButton_Click(sender As Object, e As EventArgs) Handles OkSimpleButton.Click
        PubIGST = InterStateRadioButton.Checked
        'PubPrice = WithoutPriceRadioButton.Checked
        If PubIGST Then
            PubWOInvoiceType = "WO Bill IGST"
        Else
            PubWOInvoiceType = "WO Bill"
        End If
        OpenForm(Frm_WorkOrderBill)
        With Frm_WorkOrderBill
            .InitLookup()
        End With
        End_Waiting()
        Me.Close()
    End Sub
End Class