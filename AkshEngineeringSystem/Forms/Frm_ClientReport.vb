﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraReports.UI
Imports System.Data.OleDb
Public Class Frm_ClientReport
    Dim DS As New DataSet

    Dim ClientDA As New OleDbDataAdapter
    Dim ClientBS As New BindingSource

    Dim PaymentDA As New OleDbDataAdapter
    Dim PaymentBS As New BindingSource

    Dim BillDA As New OleDbDataAdapter
    Dim BillBS As New BindingSource
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        JobNoComboBoxEdit.Focus()
        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM ClientPO Where Company='" + PubCompanyName + "'", "ClientPO", JobNoComboBoxEdit)
        SetLookUp("SELECT PartyID, PartyName FROM Party Where Company='" + PubCompanyName + "'", "Party", "PartyID", "PartyName", PartyLookUpEdit, "Party Name")
        SetLookUp("SELECT SalesID, OrderNo FROM ClientPO Where Company='" + PubCompanyName + "'", "ClientPO", "SalesID", "OrderNo", ClientPOLookUpEdit, "PO No")
    End Sub

    Sub setGridData(condition As String)
        'ClientDA.SelectCommand = New OleDbCommand("SELECT  SalesID,JobNoPrefix,JobNo,OrderNo,OrderDate,ReceiverName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus,POType FROM ClientPO WHERE Company='" + PubCompanyName + "'" + condition, ConStr)
        ClientDA.SelectCommand = New OleDbCommand("SELECT ClientPO.SalesID,ClientPO.JobNoPrefix,ClientPO.JobNo,ClientPO.OrderNo,ClientPO.OrderDate,ClientPO.ReceiverName,ClientPO.TotalAmtBeforeTax,ClientPO.AdvancePayment,ClientPO.GrandTotalAmount,ClientPO.POStatus,ClientPO.BillStatus,ClientPO.POType,ClientPayment.RemainAmount FROM ClientPO INNER Join ClientPayment On ClientPO.SalesID = ClientPayment.SalesID WHERE Company='" + PubCompanyName + "' AND ClientPayment.PaymentID IN (SELECT max(PaymentID) FROM ClientPayment GROUP BY SalesID)" + condition, ConStr)
        ClientDA.Fill(DS, "ClientPO")

        PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,SalesID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From ClientPayment", ConStr)
        PaymentDA.Fill(DS, "ClientPayment")

        BillDA.SelectCommand = New OleDbCommand("Select SalesID,JobNo,InvoiceNo,InvoiceDate,OfferNo,ReceiverName,TotalAmtBeforeTax,TotalAmtAfterTax,GrandTotalAmount,InvoiceType From Invoice", ConStr)
        BillDA.Fill(DS, "Invoice")
    End Sub

    Sub SetRelation()
        DS.Relations.Add(New DataRelation("Payments", DS.Tables("ClientPO").Columns("SalesID"), DS.Tables("ClientPayment").Columns("SalesID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DS.Tables("ClientPO").Columns("SalesID"), DS.Tables("ClientPayment").Columns("SalesID"))
        Try
            DS.Tables("ClientPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        ClientBS.DataSource = DS
        ClientBS.DataMember = "ClientPO"

        PaymentBS.DataSource = ClientBS
        PaymentBS.DataMember = "Payments"

        DS.Relations.Add(New DataRelation("Bills", DS.Tables("ClientPO").Columns("SalesID"), DS.Tables("Invoice").Columns("SalesID"), False))
        Dim FK_Bills As New Global.System.Data.ForeignKeyConstraint("FK_Order1", DS.Tables("ClientPO").Columns("SalesID"), DS.Tables("Invoice").Columns("SalesID"))
        Try
            DS.Tables("Invoice").Constraints.Add(FK_Bills)
        Catch
        End Try

        With FK_Bills
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        ClientBS.DataSource = DS
        ClientBS.DataMember = "ClientPO"

        BillBS.DataSource = ClientBS
        BillBS.DataMember = "Bills"
    End Sub

    Sub setGrid()
        With ClientGridView
            .Columns("SalesID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("OrderNo").Caption = "PO No."
            .Columns("OrderNo").OptionsColumn.AllowFocus = False
            .Columns("OrderNo").OptionsColumn.ReadOnly = True
            .Columns("OrderDate").OptionsColumn.AllowFocus = False
            .Columns("OrderDate").OptionsColumn.ReadOnly = True
            .Columns("ReceiverName").OptionsColumn.AllowFocus = False
            .Columns("ReceiverName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").Caption = "Advance"
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "PO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("POStatus").OptionsColumn.AllowFocus = False
            .Columns("POStatus").OptionsColumn.ReadOnly = True
            .Columns("POType").OptionsColumn.AllowFocus = False
            .Columns("POType").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").Caption = "Outstanding"
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True
            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("RemainAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
    End Sub

    Private Sub ClientPOButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
        Dim condition As String = " AND"
        If JobNoComboBoxEdit.EditValue IsNot Nothing Then
            condition += " ClientPO.JobNo = '" + JobNoComboBoxEdit.Text.Replace("A-", "") + "' AND"
        End If
        If PartyLookUpEdit.EditValue IsNot Nothing Then
            condition += " ReceiverName = '" + PartyLookUpEdit.Text + "' AND"
        End If
        If ClientPOLookUpEdit.EditValue IsNot Nothing Then
            condition += " OrderNo = '" + ClientPOLookUpEdit.Text + "' AND"
        End If
        If FromDateEdit.EditValue IsNot Nothing Then
            If ToDateEdit.EditValue IsNot Nothing Then
                condition += " (OrderDate BETWEEN #" & FromDateEdit.Text & "# AND #" & ToDateEdit.Text & "#) AND"
            End If
        End If
        If POStatusComboBoxEdit.EditValue IsNot Nothing Then
            condition += " POStatus = '" + POStatusComboBoxEdit.Text + "' AND"
        End If
        condition = condition.Remove(condition.Length - 4)

        DS = New DataSet

        ClientDA = New OleDbDataAdapter
        ClientBS = New BindingSource

        PaymentDA = New OleDbDataAdapter
        PaymentBS = New BindingSource

        BillDA = New OleDbDataAdapter
        BillBS = New BindingSource

        setGridData(condition)
        SetRelation()
        ClientGridControl.DataSource = ClientBS
        setGrid()
    End Sub

    Private Sub ClearSimpleButton_Click(sender As Object, e As EventArgs) Handles ClearSimpleButton.Click
        JobNoComboBoxEdit.Text = Nothing
        FromDateEdit.EditValue = Nothing
        ToDateEdit.EditValue = Nothing
        PartyLookUpEdit.EditValue = Nothing
        ClientPOLookUpEdit.EditValue = Nothing
        POStatusComboBoxEdit.EditValue = Nothing
    End Sub

    Private Sub CloseSimpleButton_Click(sender As Object, e As EventArgs) Handles CloseSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub ClientGridView_MasterRowExpanded(sender As Object, e As DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs) Handles ClientGridView.MasterRowExpanded

        If e.RelationIndex = 0 Then
            Dim gridViewTests As GridView = CType(sender, GridView)
            Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            gridViewDefects.BeginUpdate()

            With gridViewDefects
                .Columns("PaymentID").Visible = False
                .Columns("SalesID").Visible = False
                .Columns("JobNo").OptionsColumn.AllowFocus = False
                .Columns("JobNo").OptionsColumn.ReadOnly = True
                .Columns("PONo").OptionsColumn.AllowFocus = False
                .Columns("PONo").OptionsColumn.ReadOnly = True
                .Columns("CheckNo").Caption = "Cheque No."
                .Columns("CheckNo").OptionsColumn.AllowFocus = False
                .Columns("CheckNo").OptionsColumn.ReadOnly = True
                .Columns("TDS").OptionsColumn.AllowFocus = False
                .Columns("TDS").OptionsColumn.ReadOnly = True
                .Columns("ChkDate").OptionsColumn.AllowFocus = False
                .Columns("ChkDate").OptionsColumn.ReadOnly = True
                .Columns("PaidAmount").OptionsColumn.AllowFocus = False
                .Columns("PaidAmount").OptionsColumn.ReadOnly = True
                .Columns("RemainAmount").OptionsColumn.AllowFocus = False
                .Columns("RemainAmount").OptionsColumn.ReadOnly = True

                .OptionsView.ShowFooter = True
                .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End With
            gridViewDefects.EndUpdate()
        ElseIf e.RelationIndex = 1 Then
            Dim gridViewTests As GridView = CType(sender, GridView)
            Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            gridViewDefects.BeginUpdate()

            With gridViewDefects
                .Columns("SalesID").Visible = False
                .Columns("JobNo").OptionsColumn.AllowFocus = False
                .Columns("JobNo").OptionsColumn.ReadOnly = True
                .Columns("InvoiceNo").OptionsColumn.AllowFocus = False
                .Columns("InvoiceNo").OptionsColumn.ReadOnly = True
                .Columns("InvoiceDate").OptionsColumn.AllowFocus = False
                .Columns("InvoiceDate").OptionsColumn.ReadOnly = True
                .Columns("OfferNo").OptionsColumn.AllowFocus = False
                .Columns("OfferNo").OptionsColumn.ReadOnly = True
                .Columns("ReceiverName").OptionsColumn.AllowFocus = False
                .Columns("ReceiverName").OptionsColumn.ReadOnly = True
                .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
                .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
                .Columns("TotalAmtAfterTax").OptionsColumn.AllowFocus = False
                .Columns("TotalAmtAfterTax").OptionsColumn.ReadOnly = True
                .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
                .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
                .Columns("InvoiceType").OptionsColumn.AllowFocus = False
                .Columns("InvoiceType").OptionsColumn.ReadOnly = True

                .OptionsView.ShowFooter = True
                .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End With
            gridViewDefects.EndUpdate()
        End If


    End Sub

    Private Sub ExportExcelSimpleButton_Click(sender As Object, e As EventArgs) Handles ExportExcelSimpleButton.Click
        'ClientGridControl.ExportToXlsx("ClientReport.xlsx")
        'Process.Start("ClientReport.xlsx")
        Dim value As String
        Dim filename As String
        If POStatusComboBoxEdit.EditValue IsNot Nothing Then
            If POStatusComboBoxEdit.Text = "PO is closed" Then
                filename = "ClientReport_POClose_" + DateTime.Today.ToShortDateString + ".xlsx"
            ElseIf POStatusComboBoxEdit.Text = "PO is open" Then
                filename = "ClientReport_POOpen_" + DateTime.Today.ToShortDateString + ".xlsx"
            End If
        Else
            filename = "ClientReport_" + DateTime.Today.ToShortDateString + ".xlsx"
        End If
        value = "E:\Aksh Software\AkshEngineeringSystem\AkshEngineeringSystem\" + filename
        ClientGridControl.ExportToXlsx(value)
        Process.Start(value)
    End Sub
End Class