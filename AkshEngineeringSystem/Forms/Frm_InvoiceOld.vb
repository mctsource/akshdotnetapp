﻿Imports System.Data.OleDb
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraReports.UI

Public Class Frm_InvoiceOld
    Dim DS As New DataSet
    Dim DA As New OleDbDataAdapter
    Dim BS As New BindingSource
    Dim DADetails As New OleDbDataAdapter
    Dim BSDetails As New BindingSource
    Dim Status As Integer = 0
    Dim NetAmtLabel As New LabelControl
    Private Sub FrmMasterConceptFrom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetDataTable()
        SetRelation()
        SetQuery()
        SetBinding()
        SetGrid()
        InitLookup()

        AddNew()

        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub
    Sub SetDataTable()
        'Invoice
        DS.Tables.Add("Invoice")
        With DS.Tables("Invoice").Columns
            .Add("InvoiceID", GetType(Integer))
            .Add("JobNoPrefix", GetType(String))
            .Add("JobNo", GetType(String))
            .Add("InvoicePrefix", GetType(String))
            .Add("InvoiceNo", GetType(String))
            .Add("InvoiceYear", GetType(String))
            .Add("FullInvoiceNo", GetType(String))
            .Add("InvoiceDate", GetType(Date))
            .Item("InvoiceDate").DefaultValue = Date.Today
            .Add("DCNo", GetType(String))
            .Add("OfferNo", GetType(String))
            .Add("PODate", GetType(Date))
            .Add("PartyID", GetType(Integer))
            .Add("Remarks", GetType(String))
            .Add("IsVATORSTAX", GetType(String))
            .Add("TotalAmount", GetType(Double))
            .Add("FreightAmount", GetType(Double))
            .Add("Transportation", GetType(Double))
            .Add("SubTotal", GetType(Double))
            .Add("VAT", GetType(Double))
            .Add("VATAmount", GetType(Double))
            .Add("AddTax", GetType(Double))
            .Add("AddTaxAmount", GetType(Double))
            .Add("SerTax", GetType(Double))
            .Add("SerTaxAmount", GetType(Double))
            .Add("KrushiKalyan", GetType(Double))
            .Add("SwachataAbhiyan", GetType(Double))
            .Add("CST", GetType(Double))
            .Add("CSTAmount", GetType(Double))
            .Add("Discount", GetType(Double))
            .Add("DiscountAmount", GetType(Double))
            .Add("GrandTotalAmount", GetType(Double))
            .Add("TotalInWords", GetType(String))
            .Add("Company", GetType(String))
            .Item("Company").DefaultValue = PubCompanyName
            .Add("UserID", GetType(Integer))
            .Item("UserID").DefaultValue = PubUserID
        End With

        With DS.Tables("Invoice").Columns("InvoiceID")
            .AutoIncrement = True
            .AutoIncrementSeed = -1
            .AutoIncrementStep = -1
            .ReadOnly = True
            .Unique = True
        End With

        'Invoice Detail
        DS.Tables.Add("InvoiceDetail")
        With DS.Tables("InvoiceDetail").Columns
            .Add("InvoiceDetailID", GetType(Integer))
            .Add("InvoiceID", GetType(Integer))
            .Add("ItemName", GetType(String))
            .Add("Qty", GetType(Integer))
            .Add("Rate", GetType(Double))
            .Add("Amount", GetType(Double))
        End With

        With DS.Tables("InvoiceDetail").Columns("InvoiceDetailID")
            .AutoIncrement = True
            .AutoIncrementSeed = -1
            .AutoIncrementStep = -1
            .ReadOnly = True
            .Unique = True
        End With
    End Sub

    Sub SetRelation()
        DS.Relations.Add(New DataRelation("RelationInvoice", DS.Tables("Invoice").Columns("InvoiceID"), DS.Tables("InvoiceDetail").Columns("InvoiceID"), False))
        Dim FK_InvoiceDetail As New Global.System.Data.ForeignKeyConstraint("FK_Invoice", DS.Tables("Invoice").Columns("InvoiceID"), DS.Tables("InvoiceDetail").Columns("InvoiceID"))
        DS.Tables("InvoiceDetail").Constraints.Add(FK_InvoiceDetail)
        With FK_InvoiceDetail
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        BS.DataSource = DS
        BS.DataMember = "Invoice"

        BSDetails.DataSource = BS
        BSDetails.DataMember = "RelationInvoice"

        BillDetailGridControl.DataSource = BSDetails
    End Sub

    Sub SetQuery()
        ' Invoice ......................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
        DA.SelectCommand = New OleDbCommand("Select InvoiceID,JobNoPrefix,JobNo,InvoicePrefix,InvoiceNo,InvoiceYear,FullInvoiceNo,InvoiceDate,DCNo,OfferNo,PODate,PartyID,Remarks,IsVATORSTAX,TotalAmount,FreightAmount,Transportation,SubTotal,VAT,VATAmount,AddTax,AddTaxAmount,SerTax,SerTaxAmount,KrushiKalyan,SwachataAbhiyan,CST,CSTAmount,Discount,DiscountAmount,GrandTotalAmount,TotalInWords From Invoice Where InvoiceID=@InvoiceID", ConStr)
        DA.SelectCommand.Parameters.Add("@InvoiceID", OleDbType.Integer, 4, "InvoiceID")

        DA.InsertCommand = New OleDbCommand("Insert Into Invoice (JobNoPrefix,JobNo,InvoicePrefix,InvoiceNo,InvoiceYear,FullInvoiceNo,InvoiceDate,DCNo,OfferNo,PODate,PartyID,Remarks,IsVATORSTAX,TotalAmount,FreightAmount,Transportation,SubTotal,VAT,VATAmount,AddTax,AddTaxAmount,SerTax,SerTaxAmount,KrushiKalyan,SwachataAbhiyan,CST,CSTAmount,Discount,DiscountAmount,GrandTotalAmount,TotalInWords,Company,UserID) Values (@JobNoPrefix,@JobNo,@InvoicePrefix,@InvoiceNo,@InvoiceYear,@FullInvoiceNo,@InvoiceDate,@DCNo,@OfferNo,@PODate,@PartyID,@Remarks,@IsVATORSTAX,@TotalAmount,@FreightAmount,@Transportation,@SubTotal,@VAT,@VATAmount,@AddTax,@AddTaxAmount,@SerTax,@SerTaxAmount,@KrushiKalyan,@SwachataAbhiyan,@CST,@CSTAmount,@Discount,@DiscountAmount,@GrandTotalAmount,@TotalInWords,@Company,@UserID)", ConStr)
        DA.InsertCommand.Parameters.Add("@JobNoPrefix", OleDbType.VarChar, 50, "JobNoPrefix")
        DA.InsertCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        DA.InsertCommand.Parameters.Add("@InvoicePrefix", OleDbType.VarChar, 50, "InvoicePrefix")
        DA.InsertCommand.Parameters.Add("@InvoiceNo", OleDbType.VarChar, 50, "InvoiceNo")
        DA.InsertCommand.Parameters.Add("@InvoiceYear", OleDbType.VarChar, 50, "InvoiceYear")
        DA.InsertCommand.Parameters.Add("@FullInvoiceNo", OleDbType.VarChar, 50, "FullInvoiceNo")
        DA.InsertCommand.Parameters.Add("@InvoiceDate", OleDbType.Date, 3, "InvoiceDate")
        DA.InsertCommand.Parameters.Add("@DCNo", OleDbType.VarChar, 50, "DCNo")
        DA.InsertCommand.Parameters.Add("@OfferNo", OleDbType.VarChar, 50, "OfferNo")
        DA.InsertCommand.Parameters.Add("@PODate", OleDbType.Date, 3, "PODate")
        DA.InsertCommand.Parameters.Add("@PartyID", OleDbType.Integer, 4, "PartyID")
        DA.InsertCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 5000, "Remarks")
        DA.InsertCommand.Parameters.Add("@IsVATORSTAX", OleDbType.VarChar, 50, "IsVATORSTAX")
        DA.InsertCommand.Parameters.Add("@TotalAmount", OleDbType.Double, 8, "TotalAmount")
        DA.InsertCommand.Parameters.Add("@FreightAmount", OleDbType.Double, 8, "FreightAmount")
        DA.InsertCommand.Parameters.Add("@Transportation", OleDbType.Double, 8, "Transportation")
        DA.InsertCommand.Parameters.Add("@SubTotal", OleDbType.Double, 8, "SubTotal")
        DA.InsertCommand.Parameters.Add("@VAT", OleDbType.Double, 8, "VAT")
        DA.InsertCommand.Parameters.Add("@VATAmount", OleDbType.Double, 8, "VATAmount")
        DA.InsertCommand.Parameters.Add("@AddTax", OleDbType.Double, 8, "AddTax")
        DA.InsertCommand.Parameters.Add("@AddTaxAmount", OleDbType.Double, 8, "AddTaxAmount")
        DA.InsertCommand.Parameters.Add("@SerTax", OleDbType.Double, 8, "SerTax")
        DA.InsertCommand.Parameters.Add("@SerTaxAmount", OleDbType.Double, 8, "SerTaxAmount")
        DA.InsertCommand.Parameters.Add("@KrushiKalyan", OleDbType.Double, 8, "KrushiKalyan")
        DA.InsertCommand.Parameters.Add("@SwachataAbhiyan", OleDbType.Double, 8, "SwachataAbhiyan")
        DA.InsertCommand.Parameters.Add("@CST", OleDbType.Double, 8, "CST")
        DA.InsertCommand.Parameters.Add("@CSTAmount", OleDbType.Double, 8, "CSTAmount")
        DA.InsertCommand.Parameters.Add("@Discount", OleDbType.Double, 8, "Discount")
        DA.InsertCommand.Parameters.Add("@DiscountAmount", OleDbType.Double, 8, "DiscountAmount")
        DA.InsertCommand.Parameters.Add("@GrandTotalAmount", OleDbType.Double, 8, "GrandTotalAmount")
        DA.InsertCommand.Parameters.Add("@TotalInWords", OleDbType.VarChar, 150, "TotalInWords")
        DA.InsertCommand.Parameters.Add("@Company", OleDbType.VarChar, 50, "Company")
        DA.InsertCommand.Parameters.Add("@UserID", OleDbType.Integer, 4, "UserID")

        DA.UpdateCommand = New OleDbCommand("Update Invoice Set JobNoPrefix=@JobNoPrefix,JobNo=@JobNo,InvoicePrefix=@InvoicePrefix,InvoiceNo=@InvoiceNo,InvoiceYear=@InvoiceYear,FullInvoiceNo=@FullInvoiceNo,InvoiceDate=@InvoiceDate,DCNo=@DCNo,OfferNo=@OfferNo,PODate=@PODate,PartyID=@PartyID,Remarks=@Remarks,IsVATORSTAX=@IsVATORSTAX,TotalAmount=@TotalAmount,FreightAmount=@FreightAmount,Transportation=@Transportation,SubTotal=@SubTotal,VAT=@VAT,VATAmount=@VATAmount,AddTax=@AddTax,AddTaxAmount=@AddTaxAmount,SerTax=@SerTax,SerTaxAmount=@SerTaxAmount,KrushiKalyan=@KrushiKalyan,SwachataAbhiyan=@SwachataAbhiyan,CST=@CST,CSTAmount=@CSTAmount,Discount=@Discount,DiscountAmount=@DiscountAmount,GrandTotalAmount=@GrandTotalAmount,TotalInWords=@TotalInWords Where InvoiceID=@InvoiceID", ConStr)
        DA.UpdateCommand.Parameters.Add("@JobNoPrefix", OleDbType.VarChar, 50, "JobNoPrefix")
        DA.UpdateCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        DA.UpdateCommand.Parameters.Add("@InvoicePrefix", OleDbType.VarChar, 50, "InvoicePrefix")
        DA.UpdateCommand.Parameters.Add("@InvoiceNo", OleDbType.VarChar, 50, "InvoiceNo")
        DA.UpdateCommand.Parameters.Add("@InvoiceYear", OleDbType.VarChar, 50, "InvoiceYear")
        DA.UpdateCommand.Parameters.Add("@FullInvoiceNo", OleDbType.VarChar, 50, "FullInvoiceNo")
        DA.UpdateCommand.Parameters.Add("@InvoiceDate", OleDbType.Date, 3, "InvoiceDate")
        DA.UpdateCommand.Parameters.Add("@DCNo", OleDbType.VarChar, 50, "DCNo")
        DA.UpdateCommand.Parameters.Add("@OfferNo", OleDbType.VarChar, 50, "OfferNo")
        DA.UpdateCommand.Parameters.Add("@PODate", OleDbType.Date, 3, "PODate")
        DA.UpdateCommand.Parameters.Add("@PartyID", OleDbType.Integer, 4, "PartyID")
        DA.UpdateCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 5000, "Remarks")
        DA.UpdateCommand.Parameters.Add("@IsVATORSTAX", OleDbType.VarChar, 50, "IsVATORSTAX")
        DA.UpdateCommand.Parameters.Add("@TotalAmount", OleDbType.Double, 8, "TotalAmount")
        DA.UpdateCommand.Parameters.Add("@FreightAmount", OleDbType.Double, 8, "FreightAmount")
        DA.UpdateCommand.Parameters.Add("@Transportation", OleDbType.Double, 8, "Transportation")
        DA.UpdateCommand.Parameters.Add("@SubTotal", OleDbType.Double, 8, "SubTotal")
        DA.UpdateCommand.Parameters.Add("@VAT", OleDbType.Double, 8, "VAT")
        DA.UpdateCommand.Parameters.Add("@VATAmount", OleDbType.Double, 8, "VATAmount")
        DA.UpdateCommand.Parameters.Add("@AddTax", OleDbType.Double, 8, "AddTax")
        DA.UpdateCommand.Parameters.Add("@AddTaxAmount", OleDbType.Double, 8, "AddTaxAmount")
        DA.UpdateCommand.Parameters.Add("@SerTax", OleDbType.Double, 8, "SerTax")
        DA.UpdateCommand.Parameters.Add("@SerTaxAmount", OleDbType.Double, 8, "SerTaxAmount")
        DA.UpdateCommand.Parameters.Add("@KrushiKalyan", OleDbType.Double, 8, "KrushiKalyan")
        DA.UpdateCommand.Parameters.Add("@SwachataAbhiyan", OleDbType.Double, 8, "SwachataAbhiyan")
        DA.UpdateCommand.Parameters.Add("@CST", OleDbType.Double, 8, "CST")
        DA.UpdateCommand.Parameters.Add("@CSTAmount", OleDbType.Double, 8, "CSTAmount")
        DA.UpdateCommand.Parameters.Add("@Discount", OleDbType.Double, 8, "Discount")
        DA.UpdateCommand.Parameters.Add("@DiscountAmount", OleDbType.Double, 8, "DiscountAmount")
        DA.UpdateCommand.Parameters.Add("@GrandTotalAmount", OleDbType.Double, 8, "GrandTotalAmount")
        DA.UpdateCommand.Parameters.Add("@TotalInWords", OleDbType.VarChar, 150, "TotalInWords")
        DA.UpdateCommand.Parameters.Add("@InvoiceID", OleDbType.Integer, 4, "InvoiceID")

        DA.DeleteCommand = New OleDbCommand("Delete From Invoice Where InvoiceID=@InvoiceID", ConStr)
        DA.DeleteCommand.Parameters.Add("@InvoiceID", OleDbType.Integer, 4, "InvoiceID")

        ' Invoice Detail ..............................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................

        DADetails.SelectCommand = New OleDbCommand("Select InvoiceDetailID,InvoiceID,ItemName,Qty,Rate,Amount From InvoiceDetail Where InvoiceID=@InvoiceID", ConStr)
        DADetails.SelectCommand.Parameters.Add("@InvoiceID", OleDbType.Integer, 4, "InvoiceID")

        DADetails.InsertCommand = New OleDbCommand("Insert Into InvoiceDetail (InvoiceID,ItemName,Qty,Rate,Amount) Values (@InvoiceID,@ItemName,@Qty,@Rate,@Amount)", ConStr)
        DADetails.InsertCommand.Parameters.Add("@InvoiceID", OleDbType.Integer, 4, "InvoiceID")
        DADetails.InsertCommand.Parameters.Add("@ItemName", OleDbType.VarChar, 15000, "ItemName")
        DADetails.InsertCommand.Parameters.Add("@Qty", OleDbType.Integer, 4, "Qty")
        DADetails.InsertCommand.Parameters.Add("@Rate", OleDbType.Double, 8, "Rate")
        DADetails.InsertCommand.Parameters.Add("@Amount", OleDbType.Double, 8, "Amount")

        DADetails.UpdateCommand = New OleDbCommand("Update InvoiceDetail Set InvoiceID=@InvoiceID,ItemName=@ItemName,Qty=@Qty,Rate=@Rate,Amount=@Amount Where InvoiceDetailID=@InvoiceDetailID", ConStr)
        DADetails.UpdateCommand.Parameters.Add("@InvoiceID", OleDbType.Integer, 4, "InvoiceID")
        DADetails.UpdateCommand.Parameters.Add("@ItemName", OleDbType.VarChar, 15000, "ItemName")
        DADetails.UpdateCommand.Parameters.Add("@Qty", OleDbType.Integer, 4, "Qty")
        DADetails.UpdateCommand.Parameters.Add("@Rate", OleDbType.Double, 8, "Rate")
        DADetails.UpdateCommand.Parameters.Add("@Amount", OleDbType.Double, 8, "Amount")
        DADetails.UpdateCommand.Parameters.Add("@InvoiceDetailID", OleDbType.Integer, 4, "InvoiceDetailID")

        DADetails.DeleteCommand = New OleDbCommand("Delete From InvoiceDetail Where InvoiceDetailID=@InvoiceDetailID", ConStr)
        DADetails.DeleteCommand.Parameters.Add("@InvoiceDetailID", OleDbType.Integer, 4, "InvoiceDetailID")

    End Sub

    Sub SetBinding()
        JobNoPrefixTextEdit.DataBindings.Add(New Binding("EditValue", BS, "JobNoPrefix"))
        JobNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "JobNo"))
        DateDateEdit.DataBindings.Add(New Binding("EditValue", BS, "InvoiceDate"))
        InvoicePrefixTextEdit.DataBindings.Add(New Binding("EditValue", BS, "InvoicePrefix"))
        InvoiceNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "InvoiceNo"))
        InvoiceYearTextEdit.DataBindings.Add(New Binding("EditValue", BS, "InvoiceYear"))
        PartyIDLookUpEdit.DataBindings.Add(New Binding("EditValue", BS, "PartyID"))
        DCNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "DCNo"))
        OfferNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "OfferNo"))
        PODateDateEdit.DataBindings.Add(New Binding("EditValue", BS, "PODate"))
        TotalAmtTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalAmount"))
        FreightAmountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "FreightAmount"))
        TransportationTextEdit.DataBindings.Add(New Binding("EditValue", BS, "Transportation"))
        SubTotalTextEdit.DataBindings.Add(New Binding("EditValue", BS, "SubTotal"))
        SerTaxTextEdit.DataBindings.Add(New Binding("EditValue", BS, "SerTax"))
        SerTaxAmountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "SerTaxAmount"))
        KrushiKalyanTextEdit.DataBindings.Add(New Binding("EditValue", BS, "KrushiKalyan"))
        SwachataAbhiyanTextEdit.DataBindings.Add(New Binding("EditValue", BS, "SwachataAbhiyan"))
        CSTTextEdit.DataBindings.Add(New Binding("EditValue", BS, "CST"))
        CSTAmountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "CSTAmount"))
        DiscountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "Discount"))
        DiscountAmountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "DiscountAmount"))
        VATTextEdit.DataBindings.Add(New Binding("EditValue", BS, "VAT"))
        VATAmountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "VATAmount"))
        AddTextEdit.DataBindings.Add(New Binding("EditValue", BS, "AddTax"))
        AddAmountTextEdit.DataBindings.Add(New Binding("EditValue", BS, "AddTaxAmount"))
        NetAmtLabel.DataBindings.Add(New Binding("Text", BS, "GrandTotalAmount"))
        RemarksTextEdit.DataBindings.Add(New Binding("EditValue", BS, "Remarks"))
        IsVATORCSTComboBoxEdit.DataBindings.Add(New Binding("EditValue", BS, "IsVATORSTAX"))
        TotalInWordsTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalInWords"))
    End Sub

    Sub InitLookup()
        SetLookUp("SELECT PartyID, PartyName FROM Party Where Company='" + PubCompanyName + "'", "Party", "PartyID", "PartyName", PartyIDLookUpEdit, "Party Name")
        ' GridComboBoxFunc("SELECT DISTINCT ItemName FROM InvoiceDetail", "InvoiceDetail", BillDetailGridControl, BillDetailGridView, "ItemName", "Item Name")
    End Sub

    Sub SetGrid()
        With BillDetailGridView
            .Columns("InvoiceDetailID").Visible = False
            .Columns("InvoiceID").Visible = False
            .Columns("Amount").OptionsColumn.AllowFocus = False
            .Columns("Amount").OptionsColumn.ReadOnly = True
            .Columns("ItemName").Width = 500

            .OptionsView.ShowFooter = True
            .Columns("Amount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        BillDetailGridView.BestFitColumns()
        BillDetailGridView.FocusedColumn = BillDetailGridView.Columns(2)

        ' SetCurrencyFormat("Rate", "c", BillDetailGridView, BillDetailGridControl)
        'SetCurrencyFormat("Amount", "c", BillDetailGridView, BillDetailGridControl)
    End Sub

    Function Validation() As Boolean
        If JobNoTextEdit.EditValue Is DBNull.Value Then
            JobNoTextEdit.Focus()
            Return False
        ElseIf InvoiceNoTextEdit.EditValue Is DBNull.Value Then
            InvoiceNoTextEdit.Focus()
            Return False
        ElseIf InvoiceYearTextEdit.EditValue Is DBNull.Value Then
            InvoiceYearTextEdit.Focus()
            Return False
        ElseIf DateDateEdit.EditValue Is DBNull.Value Then
            DateDateEdit.Focus()
            Return False
        ElseIf PartyIDLookUpEdit.EditValue Is DBNull.Value Then
            PartyIDLookUpEdit.Focus()
            Return False
        ElseIf OfferNoTextEdit.EditValue Is DBNull.Value Then
            OfferNoTextEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Sub AddNew()
        BS.AddNew()
        BS.Current!InvoicePrefix = "AKSH/Invoice/"
        BS.Current!InvoiceYear = "/" + DateTime.Now.ToString("yy") + "-" + DateTime.Now.AddYears(1).ToString("yy")
        BS.Current!JobNoPrefix = "A-"
        BS.Current!IsVATORSTAX = "Service Tax"
        BS.Current!InvoiceDate = Date.Today
        BS.Current!TotalAmount = 0
        BS.Current!FreightAmount = 0
        BS.Current!Transportation = 0
        BS.Current!SubTotal = 0
        BS.Current!SerTax = 0
        BS.Current!SerTaxAmount = 0
        BS.Current!KrushiKalyan = 0
        BS.Current!SwachataAbhiyan = 0
        BS.Current!CST = 0
        BS.Current!CSTAmount = 0
        BS.Current!VAT = 0
        BS.Current!VATAmount = 0
        BS.Current!AddTax = 0
        BS.Current!AddTaxAmount = 0
        BS.Current!Discount = 0
        BS.Current!DiscountAmount = 0
        BS.Current!GrandTotalAmount = 0
        BS.EndEdit()

        VATTextEdit.Enabled = False
        VATTextEdit.TabStop = False
        AddTextEdit.Enabled = False
        AddTextEdit.TabStop = False
        CSTTextEdit.Enabled = False
        CSTTextEdit.TabStop = False
        SerTaxTextEdit.Enabled = True
        SerTaxTextEdit.TabStop = True

        Status = 0
        Dim bz As New CultureInfo("hi-IN")
        Dim GTotal As Double = BS.Current!GrandTotalAmount
        NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)
        JobNoTextEdit.Focus()
        InitLookup()
    End Sub

    Private Sub NewBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles NewBarButtonItem.ItemClick
        BS.CancelEdit()
        BSDetails.CancelEdit()
        DS.RejectChanges()
        AddNew()
    End Sub

    Private Sub OpenBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles OpenBarButtonItem.ItemClick
        Dim ID = ShowRecord("SELECT * FROM Invoice Where Company='" + PubCompanyName + "' ORDER BY InvoiceDate DESC", "InvoiceID")
        If ID > 0 Then
            Try
                DS.Tables("InvoiceDetail").Clear()
                DS.Tables("Invoice").Clear()
            Catch
            End Try
            DA.SelectCommand.Parameters("@InvoiceID").Value = ID
            DA.Fill(DS, "Invoice")
            Status = 1
            DADetails.SelectCommand.Parameters("@InvoiceID").Value = ID
            DADetails.Fill(DS, "InvoiceDetail")
            Dim bz As New CultureInfo("hi-IN")
            Dim GTotal As Double = BS.Current!GrandTotalAmount
            NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)
        End If
    End Sub

    Private Sub DeleteBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DeleteBarButtonItem.ItemClick
        Try
            Dim Delete = XtraMessageBox.Show("Are You Want To Delete This Record ?" + vbNewLine + "If You Delete This Record then Related Items Record also Delete..", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
            If Delete = 1 Then
                For Each row In DS.Tables("InvoiceDetail").Select("InvoiceID =" & BS.Current!InvoiceID)
                    BSDetails.RemoveCurrent()
                    DADetails.Update(DS.Tables("InvoiceDetail"))
                Next

                BS.RemoveCurrent()
                DA.Update(DS.Tables("Invoice"))

                AddNew()
            End If
        Catch ex As Exception
            BS.CancelEdit()
            DS.RejectChanges()
            XtraMessageBox.Show("This Record Is Used In Another Record : Operation Failed :", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveSimpleButton.Click
        If Validation() Then
            If BSDetails.Count = 0 Then
                XtraMessageBox.Show("Please Enter Invoice Items", "Info")
            Else
                BS.Current!FullInvoiceNo = BS.Current!InvoicePrefix + BS.Current!InvoiceNo + BS.Current!InvoiceYear
                BS.EndEdit()
                DA.Update(DS.Tables("Invoice"))
                If Status = 0 Then
                    Dim NewCMD As New OleDbCommand("SELECT MAX(InvoiceID) FROM Invoice", ConStr)
                    CnnOpen() : Dim CurrentID = Val(NewCMD.ExecuteScalar & "") : CnnClose()
                    BSDetails.MoveFirst()

                    For item As Integer = 0 To BSDetails.Count - 1
                        Dim InsertCMD As New OleDbCommand("Insert Into InvoiceDetail (InvoiceID,ItemName,Qty,Rate,Amount) Values (@InvoiceID,@ItemName,@Qty,@Rate,@Amount)", ConStr)
                        InsertCMD.Parameters.AddWithValue("@InvoiceID", CurrentID)
                        InsertCMD.Parameters.AddWithValue("@ItemName", BSDetails.Current!ItemName)
                        InsertCMD.Parameters.AddWithValue("@Qty", BSDetails.Current!Qty)
                        InsertCMD.Parameters.AddWithValue("@Rate", BSDetails.Current!Rate)
                        InsertCMD.Parameters.AddWithValue("@Amount", BSDetails.Current!Amount)

                        CnnOpen() : InsertCMD.ExecuteNonQuery() : CnnClose()

                        BSDetails.MoveNext()

                    Next
                ElseIf Status = 1 Then
                    BSDetails.EndEdit()
                    DADetails.Update(DS.Tables("InvoiceDetail"))
                End If
                AddNew()
            End If
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub BillDetailsGridControl_Leave(sender As Object, e As EventArgs) Handles BillDetailGridControl.Leave
        BS.Current!TotalAmount = BillDetailGridView.Columns("Amount").SummaryItem.SummaryValue
        BS.Current!SubTotal = BS.Current!TotalAmount
        BS.EndEdit()
        Calc()
    End Sub

    Private Sub BillDetailGridView_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles BillDetailGridView.CellValueChanged
        Try
            If e.Column.FieldName = "Rate" Or e.Column.FieldName = "Qty" Then
                BSDetails.Current!Amount = IIf(BSDetails.Current!Rate Is DBNull.Value, 0, BSDetails.Current!Rate) * IIf(BSDetails.Current!Qty Is DBNull.Value, 0, BSDetails.Current!Qty)
                BSDetails.EndEdit()
            End If
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub BillDetailGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles BillDetailGridView.KeyDown
        If e.KeyCode = Keys.Delete Then
            BSDetails.RemoveCurrent()
            BSDetails.EndEdit()
        End If
    End Sub

    Private Sub BillDetailGridView_LostFocus(sender As Object, e As EventArgs) Handles BillDetailGridView.LostFocus
        Try
            BS.Current!GrandTotalAmount = Math.Round((IIf(BS.Current!TotalAmount Is DBNull.Value, 0, BS.Current!TotalAmount))) ' + IIf(BS.Current!IntAmt Is DBNull.Value, 0, BS.Current!IntAmt)))
            BS.EndEdit()
            Calc()
            Dim bz As New CultureInfo("hi-IN")
            Dim GTotal As Double = BS.Current!GrandTotalAmount
            NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub BillDetailGridView_FocusedColumnChanged(sender As Object, e As FocusedColumnChangedEventArgs) Handles BillDetailGridView.FocusedColumnChanged
        If e.FocusedColumn.FieldName = "Rate" Then
            If BillDetailGridView.GetFocusedRowCellDisplayText("Qty") = "" Then
                BillDetailGridControl.Focus()
                SendKeys.Send("{TAB}")
                SendKeys.Send("{TAB}")
            End If
        End If
    End Sub

    Dim ciUSA As CultureInfo = New CultureInfo("hi-IN")

    Private Sub BillDetailGridView_CustomColumnDisplayText(ByVal sender As Object, ByVal e As CustomColumnDisplayTextEventArgs) Handles BillDetailGridView.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "Rate" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim Rate As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", Rate)
            End If
            If e.Column.FieldName = "Amount" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim Amt As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", Amt)
            End If
        Catch
        End Try
    End Sub

    Private Sub FreightTextEdit_LostFocus(sender As Object, e As EventArgs) Handles VATTextEdit.LostFocus, AddTextEdit.LostFocus, SerTaxTextEdit.LostFocus, CSTTextEdit.LostFocus, FreightAmountTextEdit.LostFocus, DiscountTextEdit.LostFocus, TransportationTextEdit.LostFocus
        Try
            Calc()
            BS.EndEdit()
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub Calc()
        BS.Current!DiscountAmount = IIf(BS.Current!TotalAmount Is DBNull.Value, 0, BS.Current!TotalAmount) * IIf(BS.Current!Discount Is DBNull.Value, 0, BS.Current!Discount) / 100
        BS.Current!SubTotal = (IIf(BS.Current!TotalAmount Is DBNull.Value, 0, BS.Current!TotalAmount) - IIf(BS.Current!DiscountAmount Is DBNull.Value, 0, BS.Current!DiscountAmount)) + IIf(BS.Current!FreightAmount Is DBNull.Value, 0, BS.Current!FreightAmount) + IIf(BS.Current!Transportation Is DBNull.Value, 0, BS.Current!Transportation)
        Dim GTA As Double = BS.Current!SubTotal
        If IsVATORCSTComboBoxEdit.Text = "VAT" Then
            BS.Current!SerTax = 0
            BS.Current!SerTaxAmount = 0
            BS.Current!CST = 0
            BS.Current!CSTAmount = 0
            BS.Current!KrushiKalyan = 0
            BS.Current!SwachataAbhiyan = 0

            'VAT Calculation
            BS.Current!VATAmount = GTA * IIf(BS.Current!VAT Is DBNull.Value, 0, BS.Current!VAT) / 100
            Dim GTA1 As Double = GTA + IIf(BS.Current!VATAmount Is DBNull.Value, 0, BS.Current!VATAmount)
            'AddTax Calculation
            BS.Current!AddTaxAmount = GTA * IIf(BS.Current!AddTax Is DBNull.Value, 0, BS.Current!AddTax) / 100
            Dim GTA2 As Double = GTA1 + IIf(BS.Current!AddTaxAmount Is DBNull.Value, 0, BS.Current!AddTaxAmount)

            BS.Current!GrandTotalAmount = GTA2
        ElseIf IsVATORCSTComboBoxEdit.Text = "Service Tax" Then
            BS.Current!VAT = 0
            BS.Current!VATAmount = 0
            BS.Current!AddTax = 0
            BS.Current!AddTaxAmount = 0
            BS.Current!CST = 0
            BS.Current!CSTAmount = 0

            'Ser. Tax Calculation
            BS.Current!SerTaxAmount = GTA * IIf(BS.Current!SerTax Is DBNull.Value, 0, BS.Current!SerTax) / 100
            BS.Current!KrushiKalyan = GTA * 0.5 / 100
            BS.Current!SwachataAbhiyan = GTA * 0.5 / 100

            Dim GTA3 As Double = GTA + (IIf(BS.Current!SerTaxAmount Is DBNull.Value, 0, BS.Current!SerTaxAmount) + IIf(BS.Current!KrushiKalyan Is DBNull.Value, 0, BS.Current!KrushiKalyan) + IIf(BS.Current!SwachataAbhiyan Is DBNull.Value, 0, BS.Current!SwachataAbhiyan))

            BS.Current!GrandTotalAmount = GTA3
        ElseIf IsVATORCSTComboBoxEdit.Text = "CST" Then
            BS.Current!VAT = 0
            BS.Current!VATAmount = 0
            BS.Current!AddTax = 0
            BS.Current!AddTaxAmount = 0
            BS.Current!SerTax = 0
            BS.Current!SerTaxAmount = 0
            BS.Current!KrushiKalyan = 0
            BS.Current!SwachataAbhiyan = 0

            'CST Calculation
            BS.Current!CSTAmount = GTA * IIf(BS.Current!CST Is DBNull.Value, 0, BS.Current!CST) / 100
            Dim GTA4 As Double = GTA + IIf(BS.Current!CSTAmount Is DBNull.Value, 0, BS.Current!CSTAmount)

            BS.Current!GrandTotalAmount = GTA4
        End If

        Dim bz As New CultureInfo("hi-IN")
        Dim GTotal As Double = BS.Current!GrandTotalAmount
        NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)
        BS.Current!TotalInWords = AmtInWord(Convert.ToDecimal(GTotal.ToString("c", bz).Remove(0, 1)))
    End Sub

    Private Sub PrintBarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PrintBarButtonItem1.ItemClick
        Try
            If BSDetails.Count <> 0 Then
                If IsVATORCSTComboBoxEdit.Text = "VAT" Then
                    Dim Rpt As New XR_InvoiceTax
                    Rpt.Invoice.Value = BS.Current!InvoiceID
                    Rpt.Invoice.Visible = False
                    Rpt.FillDataSource()
                    Rpt.XrLabel14.Text = "Original Copy"
                    Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                    End_Waiting()

                    Rpt.ShowRibbonPreviewDialog()
                ElseIf IsVATORCSTComboBoxEdit.Text = "Service Tax" Then
                    Dim Rpt As New XR_Invoice
                    Rpt.Invoice.Value = BS.Current!InvoiceID
                    Rpt.Invoice.Visible = False
                    Rpt.FillDataSource()
                    Rpt.XrLabel14.Text = "Original Copy"
                    Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                    End_Waiting()

                    Rpt.ShowRibbonPreviewDialog()
                ElseIf IsVATORCSTComboBoxEdit.Text = "CST" Then
                    Dim Rpt As New XR_InvoiceRetail
                    Rpt.Invoice.Value = BS.Current!InvoiceID
                    Rpt.Invoice.Visible = False
                    Rpt.FillDataSource()
                    Rpt.XrLabel14.Text = "Original Copy"
                    Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                    End_Waiting()

                    Rpt.ShowRibbonPreviewDialog()
                End If
            Else
                XtraMessageBox.Show("Please Enter Details")
            End If
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub IsVATORCSTComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles IsVATORCSTComboBoxEdit.SelectedIndexChanged
        If IsVATORCSTComboBoxEdit.Text = "VAT" Then
            VATTextEdit.Enabled = True
            VATTextEdit.TabStop = True
            AddTextEdit.Enabled = True
            AddTextEdit.TabStop = True

            SerTaxTextEdit.Enabled = False
            SerTaxTextEdit.TabStop = False
            CSTTextEdit.Enabled = False
            CSTTextEdit.TabStop = False
        ElseIf IsVATORCSTComboBoxEdit.Text = "Service Tax" Then
            VATTextEdit.Enabled = False
            VATTextEdit.TabStop = False
            AddTextEdit.Enabled = False
            AddTextEdit.TabStop = False
            CSTTextEdit.Enabled = False
            CSTTextEdit.TabStop = False

            SerTaxTextEdit.Enabled = True
            SerTaxTextEdit.TabStop = True
        ElseIf IsVATORCSTComboBoxEdit.Text = "CST" Then
            VATTextEdit.Enabled = False
            VATTextEdit.TabStop = False
            AddTextEdit.Enabled = False
            AddTextEdit.TabStop = False
            SerTaxTextEdit.Enabled = False
            SerTaxTextEdit.TabStop = False

            CSTTextEdit.Enabled = True
            CSTTextEdit.TabStop = True
        End If
    End Sub
End Class