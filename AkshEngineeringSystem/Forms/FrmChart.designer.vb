﻿<CompilerServices.DesignerGenerated()>
Partial Class FrmChart
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmChart))
        Dim XyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Me.FilterPanel = New DevExpress.XtraEditors.PanelControl()
        Me.FilterCloseSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.FilterClearButton = New DevExpress.XtraEditors.SimpleButton()
        Me.FilterResultButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Filter1Panel = New DevExpress.XtraEditors.PanelControl()
        Me.ToDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.FromDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.FilterButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ChartControl1 = New DevExpress.XtraCharts.ChartControl()
        CType(Me.FilterPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FilterPanel.SuspendLayout()
        CType(Me.Filter1Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Filter1Panel.SuspendLayout()
        CType(Me.ToDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FromDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FromDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FilterPanel
        '
        Me.FilterPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FilterPanel.Controls.Add(Me.FilterCloseSimpleButton)
        Me.FilterPanel.Controls.Add(Me.FilterClearButton)
        Me.FilterPanel.Controls.Add(Me.FilterResultButton)
        Me.FilterPanel.Controls.Add(Me.Filter1Panel)
        Me.FilterPanel.Location = New System.Drawing.Point(499, 4)
        Me.FilterPanel.Name = "FilterPanel"
        Me.FilterPanel.Size = New System.Drawing.Size(227, 98)
        Me.FilterPanel.TabIndex = 4
        Me.FilterPanel.Visible = False
        '
        'FilterCloseSimpleButton
        '
        Me.FilterCloseSimpleButton.Location = New System.Drawing.Point(155, 70)
        Me.FilterCloseSimpleButton.Name = "FilterCloseSimpleButton"
        Me.FilterCloseSimpleButton.Size = New System.Drawing.Size(68, 23)
        Me.FilterCloseSimpleButton.TabIndex = 5
        Me.FilterCloseSimpleButton.Text = "&Hide"
        '
        'FilterClearButton
        '
        Me.FilterClearButton.Location = New System.Drawing.Point(80, 70)
        Me.FilterClearButton.Name = "FilterClearButton"
        Me.FilterClearButton.Size = New System.Drawing.Size(68, 23)
        Me.FilterClearButton.TabIndex = 5
        Me.FilterClearButton.Text = "&Clear"
        '
        'FilterResultButton
        '
        Me.FilterResultButton.Location = New System.Drawing.Point(5, 70)
        Me.FilterResultButton.Name = "FilterResultButton"
        Me.FilterResultButton.Size = New System.Drawing.Size(68, 23)
        Me.FilterResultButton.TabIndex = 5
        Me.FilterResultButton.Text = "&Show"
        '
        'Filter1Panel
        '
        Me.Filter1Panel.Controls.Add(Me.ToDateDateEdit)
        Me.Filter1Panel.Controls.Add(Me.FromDateDateEdit)
        Me.Filter1Panel.Controls.Add(Me.LabelControl5)
        Me.Filter1Panel.Controls.Add(Me.LabelControl6)
        Me.Filter1Panel.Location = New System.Drawing.Point(5, 5)
        Me.Filter1Panel.Name = "Filter1Panel"
        Me.Filter1Panel.Size = New System.Drawing.Size(218, 59)
        Me.Filter1Panel.TabIndex = 4
        '
        'ToDateDateEdit
        '
        Me.ToDateDateEdit.EditValue = Nothing
        Me.ToDateDateEdit.Location = New System.Drawing.Point(58, 31)
        Me.ToDateDateEdit.Name = "ToDateDateEdit"
        Me.ToDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ToDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ToDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yy"
        Me.ToDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.ToDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yy"
        Me.ToDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.ToDateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.ToDateDateEdit.Properties.Mask.EditMask = "dd-MM-yy"
        Me.ToDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.ToDateDateEdit.Size = New System.Drawing.Size(155, 20)
        Me.ToDateDateEdit.TabIndex = 3
        '
        'FromDateDateEdit
        '
        Me.FromDateDateEdit.EditValue = Nothing
        Me.FromDateDateEdit.Location = New System.Drawing.Point(58, 5)
        Me.FromDateDateEdit.Name = "FromDateDateEdit"
        Me.FromDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.FromDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.FromDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yy"
        Me.FromDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.FromDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yy"
        Me.FromDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.FromDateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.FromDateDateEdit.Properties.Mask.EditMask = "dd-MM-yy"
        Me.FromDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.FromDateDateEdit.Size = New System.Drawing.Size(155, 20)
        Me.FromDateDateEdit.TabIndex = 3
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(6, 34)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl5.TabIndex = 2
        Me.LabelControl5.Text = "To :"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(6, 8)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl6.TabIndex = 2
        Me.LabelControl6.Text = "From :"
        '
        'FilterButton
        '
        Me.FilterButton.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.FilterButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FilterButton.Image = CType(resources.GetObject("FilterButton.Image"), System.Drawing.Image)
        Me.FilterButton.Location = New System.Drawing.Point(702, 4)
        Me.FilterButton.Name = "FilterButton"
        Me.FilterButton.Size = New System.Drawing.Size(24, 24)
        Me.FilterButton.TabIndex = 5
        Me.FilterButton.ToolTip = "Chart Filter "
        '
        'ChartControl1
        '
        XyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
        XyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
        Me.ChartControl1.Diagram = XyDiagram1
        Me.ChartControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControl1.Legend.MarkerVisible = False
        Me.ChartControl1.Legend.TextVisible = False
        Me.ChartControl1.Location = New System.Drawing.Point(0, 0)
        Me.ChartControl1.Name = "ChartControl1"
        Series1.ArgumentDataMember = "BillDate"
        Series1.Name = "Series 1"
        Series1.ValueDataMembersSerializable = "Amount"
        Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
        Me.ChartControl1.Size = New System.Drawing.Size(730, 418)
        Me.ChartControl1.TabIndex = 6
        '
        'FrmChart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(730, 418)
        Me.Controls.Add(Me.FilterPanel)
        Me.Controls.Add(Me.FilterButton)
        Me.Controls.Add(Me.ChartControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmChart"
        Me.Text = "Chart"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.FilterPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FilterPanel.ResumeLayout(False)
        CType(Me.Filter1Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Filter1Panel.ResumeLayout(False)
        Me.Filter1Panel.PerformLayout()
        CType(Me.ToDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FromDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FromDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraCharts.ChartControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraCharts.ChartControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents FilterPanel As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents Filter1Panel As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
    Friend WithEvents ToDateDateEdit As DevExpress.XtraEditors.DateEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
    Friend WithEvents FromDateDateEdit As DevExpress.XtraEditors.DateEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents FilterButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents FilterCloseSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents FilterResultButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents FilterClearButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ChartControl1 As DevExpress.XtraCharts.ChartControl
End Class
