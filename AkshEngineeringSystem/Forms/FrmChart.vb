﻿Imports System.Data.SqlClient

Public Class FrmChart

    Private Sub FrmChart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DefaultView()
    End Sub

    Sub DefaultView()
        'Dim startdt As Date = Today.AddDays(-7)
        'Dim enddt As Date = Today.Date
        'Me.DsChart.D_Bill.Clear()
        'Dim ada As New SqlDataAdapter(String.Format("SELECT BillDate, SUM(NetAmt) AS Amount FROM D_Bill Where BillDate >= '" + Format(startdt, "MM-dd-yyyy") + "' AND BillDate <= '" + Format(enddt, "MM-dd-yyyy") + "'  GROUP BY BillDate"), CnnStrMiniStore)
        'ada.Fill(DsChart.D_Bill)
    End Sub

    Private Sub FilterButton_Click(sender As Object, e As EventArgs) Handles FilterButton.Click
        FilterPanel.Visible = True
        FromDateDateEdit.Focus()
    End Sub

    Private Sub FilterResultButton_Click(sender As Object, e As EventArgs) Handles FilterResultButton.Click
        Dim Query As String = "SELECT BillDate, SUM(NetAmt) AS Amount FROM D_Bill"

        'Dim FilterSTR As String = " WHERE D_Bill.CompanyID=" + PubCompanyID.ToString
        Dim FilterSTR As String = " WHERE "

        'If Not PartyIDLookUpEdit.EditValue Is DBNull.Value And Not PartyIDLookUpEdit.EditValue = Nothing Then
        '    FilterSTR = String.Format("{0} AND D_Transaction.PartyID={1}", FilterSTR, PartyIDLookUpEdit.EditValue)
        'End If

        If Not FromDateDateEdit.EditValue Is DBNull.Value Then
            If Not FromDateDateEdit.EditValue = Nothing Then
                FilterSTR = String.Format("{0}  D_Bill.BillDate >='{1}'", FilterSTR, Format(FromDateDateEdit.EditValue, "MM-dd-yyyy"))
            End If
        End If

        If Not ToDateDateEdit.EditValue Is DBNull.Value Then
            If Not ToDateDateEdit.EditValue = Nothing Then
                FilterSTR = String.Format("{0} AND D_Bill.BillDate <='{1}'", FilterSTR, Format(ToDateDateEdit.EditValue, "MM-dd-yyyy"))
            End If
        End If

        If FilterSTR <> " WHERE " Then
            Query = Query + FilterSTR
        End If

        Query = String.Format("{0} GROUP BY BillDate", Query)

        'Me.DsChart.D_Bill.Clear()
        'Dim ada As New SqlDataAdapter(Query, CnnStrMiniStore)
        'ada.Fill(DsChart.D_Bill)

        FilterPanel.Visible = False
        ChartControl1.Focus()
    End Sub

    Private Sub FilterClearButton_Click(sender As Object, e As EventArgs) Handles FilterClearButton.Click
        DefaultView()
        FilterPanel.Visible = False
    End Sub

    Private Sub FilterCloseSimpleButton_Click(sender As Object, e As EventArgs) Handles FilterCloseSimpleButton.Click
        FilterPanel.Visible = False
    End Sub
End Class
