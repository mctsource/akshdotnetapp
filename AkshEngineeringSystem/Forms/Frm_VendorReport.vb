﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraReports.UI
Imports System.Data.OleDb
Public Class Frm_VendorReport
    Dim DS As New DataSet

    Dim VendorDA As New OleDbDataAdapter
    Dim VendorBS As New BindingSource

    Dim PaymentDA As New OleDbDataAdapter
    Dim PaymentBS As New BindingSource

    Dim BillDA As New OleDbDataAdapter
    Dim BillBS As New BindingSource
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        JobNoComboBoxEdit.Focus()
        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM VendorPO Where Company='" + PubCompanyName + "'", "VenderPO", JobNoComboBoxEdit)
        SetLookUp("SELECT PartyID, PartyName FROM Party Where Company='" + PubCompanyName + "'", "Party", "PartyID", "PartyName", PartyLookUpEdit, "Party Name")
        SetLookUp("Select PurchaseID, OrderNo FROM VendorPO Where Company='" + PubCompanyName + "'", "VendorPO", "PurchaseID", "OrderNo", VendorPOLookUpEdit, "PO No")
    End Sub

    Sub setGridData(condition As String)
        'VendorDA.SelectCommand = New OleDbCommand("SELECT PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus,BillStatus,POType FROM VendorPO WHERE Company='" + PubCompanyName + "'" + condition, ConStr)
        VendorDA.SelectCommand = New OleDbCommand("SELECT VendorPO.PurchaseID,VendorPO.JobNoPrefix,VendorPO.JobNo,VendorPO.OrderNo,VendorPO.OrderDate,VendorPO.ConsignorName,VendorPO.TotalAmtBeforeTax,VendorPO.AdvancePayment,VendorPO.GrandTotalAmount,VendorPO.POStatus,VendorPO.BillStatus,VendorPO.POType,VendorPayment.RemainAmount FROM VendorPO INNER Join VendorPayment On VendorPO.PurchaseID = VendorPayment.PurchaseID WHERE Company='" + PubCompanyName + "' AND VendorPayment.PaymentID IN (SELECT max(PaymentID) FROM VendorPayment GROUP BY PurchaseID)" + condition, ConStr)
        VendorDA.Fill(DS, "VendorPO")

        PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From VendorPayment", ConStr)
        PaymentDA.Fill(DS, "VendorPayment")

        BillDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNo,BillNo,BillDate,PONo,ConsignorName,TotalAmtBeforeTax,TotalAmtAfterTax,GrandTotalAmount,BillType From BillNew", ConStr)
        BillDA.Fill(DS, "BillNew")
    End Sub

    Sub SetRelation()
        DS.Relations.Add(New DataRelation("Payments", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPayment").Columns("PurchaseID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPayment").Columns("PurchaseID"))
        Try
            DS.Tables("VendorPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        VendorBS.DataSource = DS
        VendorBS.DataMember = "VendorPO"

        PaymentBS.DataSource = VendorBS
        PaymentBS.DataMember = "Payments"

        DS.Relations.Add(New DataRelation("Bills", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("BillNew").Columns("PurchaseID"), False))
        Dim FK_Bills As New Global.System.Data.ForeignKeyConstraint("FK_Order1", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("BillNew").Columns("PurchaseID"))
        Try
            DS.Tables("BillNew").Constraints.Add(FK_Bills)
        Catch
        End Try

        With FK_Bills
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        VendorBS.DataSource = DS
        VendorBS.DataMember = "VendorPO"

        BillBS.DataSource = VendorBS
        BillBS.DataMember = "Bills"
    End Sub

    Sub setGrid()
        With VendorGridView
            .Columns("PurchaseID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("OrderNo").Caption = "PO No."
            .Columns("OrderNo").OptionsColumn.AllowFocus = False
            .Columns("OrderNo").OptionsColumn.ReadOnly = True
            .Columns("OrderDate").OptionsColumn.AllowFocus = False
            .Columns("OrderDate").OptionsColumn.ReadOnly = True
            .Columns("ConsignorName").OptionsColumn.AllowFocus = False
            .Columns("ConsignorName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").Caption = "Advance"
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "PO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("POStatus").OptionsColumn.AllowFocus = False
            .Columns("POStatus").OptionsColumn.ReadOnly = True
            .Columns("BillStatus").OptionsColumn.AllowFocus = False
            .Columns("BillStatus").OptionsColumn.ReadOnly = True
            .Columns("POType").OptionsColumn.AllowFocus = False
            .Columns("POType").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").Caption = "Outstanding"
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True
            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("RemainAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
    End Sub

    Private Sub VendorPOButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
        Dim condition As String = " AND"
        If JobNoComboBoxEdit.EditValue IsNot Nothing Then
            condition += " VendorPO.JobNo = '" + JobNoComboBoxEdit.Text.Replace("A-", "") + "' AND"
        End If
        If PartyLookUpEdit.EditValue IsNot Nothing Then
            condition += " ConsignorName = '" + PartyLookUpEdit.Text + "' AND"
        End If
        If VendorPOLookUpEdit.EditValue IsNot Nothing Then
            condition += " OrderNo = '" + VendorPOLookUpEdit.Text + "' AND"
        End If
        If FromDateEdit.EditValue IsNot Nothing Then
            If ToDateEdit.EditValue IsNot Nothing Then
                condition += " (OrderDate BETWEEN #" & FromDateEdit.Text & "# AND #" & ToDateEdit.Text & "#) AND"
                'condition += "(OrderDate BETWEEN " #" & FromDateDateEdit.Text & "# And #" & ToDateDateEdit.Text & "#)
            End If
        End If
        If POStatusComboBoxEdit.EditValue IsNot Nothing Then
            condition += " POStatus = '" + POStatusComboBoxEdit.Text + "' AND"
        End If
        condition = condition.Remove(condition.Length - 4)

        DS = New DataSet

        VendorDA = New OleDbDataAdapter
        VendorBS = New BindingSource

        PaymentDA = New OleDbDataAdapter
        PaymentBS = New BindingSource

        BillDA = New OleDbDataAdapter
        BillBS = New BindingSource

        setGridData(condition)
        SetRelation()
        VendorGridControl.DataSource = VendorBS
        setGrid()
    End Sub

    Private Sub ClearSimpleButton_Click(sender As Object, e As EventArgs) Handles ClearSimpleButton.Click
        JobNoComboBoxEdit.Text = Nothing
        FromDateEdit.EditValue = Nothing
        ToDateEdit.EditValue = Nothing
        PartyLookUpEdit.EditValue = Nothing
        VendorPOLookUpEdit.EditValue = Nothing
        POStatusComboBoxEdit.EditValue = Nothing
    End Sub

    Private Sub CloseSimpleButton_Click(sender As Object, e As EventArgs) Handles CloseSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub VendorGridView_MasterRowExpanded(sender As Object, e As DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs) Handles VendorGridView.MasterRowExpanded

        If e.RelationIndex = 0 Then
            Dim gridViewTests As GridView = CType(sender, GridView)
            Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            gridViewDefects.BeginUpdate()

            With gridViewDefects
                .Columns("PaymentID").Visible = False
                .Columns("PurchaseID").Visible = False
                .Columns("JobNo").OptionsColumn.AllowFocus = False
                .Columns("JobNo").OptionsColumn.ReadOnly = True
                .Columns("PONo").OptionsColumn.AllowFocus = False
                .Columns("PONo").OptionsColumn.ReadOnly = True
                .Columns("CheckNo").Caption = "Cheque No."
                .Columns("CheckNo").OptionsColumn.AllowFocus = False
                .Columns("CheckNo").OptionsColumn.ReadOnly = True
                .Columns("TDS").OptionsColumn.AllowFocus = False
                .Columns("TDS").OptionsColumn.ReadOnly = True
                .Columns("ChkDate").OptionsColumn.AllowFocus = False
                .Columns("ChkDate").OptionsColumn.ReadOnly = True
                .Columns("PaidAmount").OptionsColumn.AllowFocus = False
                .Columns("PaidAmount").OptionsColumn.ReadOnly = True
                .Columns("RemainAmount").OptionsColumn.AllowFocus = False
                .Columns("RemainAmount").OptionsColumn.ReadOnly = True

                .OptionsView.ShowFooter = True
                .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End With
            gridViewDefects.EndUpdate()
        ElseIf e.RelationIndex = 1 Then
            Dim gridViewTests As GridView = CType(sender, GridView)
            Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
            gridViewDefects.BeginUpdate()

            With gridViewDefects
                .Columns("PurchaseID").Visible = False
                .Columns("JobNo").OptionsColumn.AllowFocus = False
                .Columns("JobNo").OptionsColumn.ReadOnly = True
                .Columns("BillNo").OptionsColumn.AllowFocus = False
                .Columns("BillNo").OptionsColumn.ReadOnly = True
                .Columns("BillDate").OptionsColumn.AllowFocus = False
                .Columns("BillDate").OptionsColumn.ReadOnly = True
                .Columns("PONo").OptionsColumn.AllowFocus = False
                .Columns("PONo").OptionsColumn.ReadOnly = True
                .Columns("ConsignorName").OptionsColumn.AllowFocus = False
                .Columns("ConsignorName").OptionsColumn.ReadOnly = True
                .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
                .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
                .Columns("TotalAmtAfterTax").OptionsColumn.AllowFocus = False
                .Columns("TotalAmtAfterTax").OptionsColumn.ReadOnly = True
                .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
                .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
                .Columns("BillType").OptionsColumn.AllowFocus = False
                .Columns("BillType").OptionsColumn.ReadOnly = True

                .OptionsView.ShowFooter = True
                .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            End With
            gridViewDefects.EndUpdate()
        End If

    End Sub

    Private Sub ExportExcelSimpleButton_Click(sender As Object, e As EventArgs) Handles ExportExcelSimpleButton.Click
        Dim value As String
        Dim filename As String
        If POStatusComboBoxEdit.EditValue IsNot Nothing Then
            If POStatusComboBoxEdit.Text = "PO is closed" Then
                filename = "VendorReport_POClose_" + DateTime.Today.ToShortDateString + ".xlsx"
            ElseIf POStatusComboBoxEdit.Text = "PO is open" Then
                filename = "VendorReport_POOpen_" + DateTime.Today.ToShortDateString + ".xlsx"
            End If
        Else
            filename = "VendorReport_" + DateTime.Today.ToShortDateString + ".xlsx"
        End If
            value = "E:\Aksh Software\AkshEngineeringSystem\AkshEngineeringSystem\" + filename
        VendorGridControl.ExportToXlsx(value)
        Process.Start(value)
    End Sub

End Class