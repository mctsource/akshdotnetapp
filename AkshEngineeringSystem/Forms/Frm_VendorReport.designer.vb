﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_VendorReport
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_VendorReport))
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.POStatusComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ExportExcelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.JobNoComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CloseSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ClearSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.VendorGridControl = New DevExpress.XtraGrid.GridControl()
        Me.VendorGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ToDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.FromDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.PartyLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.BTALabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.RemainAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PaidAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.BillIDLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.SearchButton = New DevExpress.XtraEditors.SimpleButton()
        Me.VendorPOLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.VendorPONewTableAdapter = New AkshEngineeringSystem.DS_VendorPONewTableAdapters.VendorPONewTableAdapter()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.POStatusComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VendorGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VendorGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FromDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FromDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PartyLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VendorPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.POStatusComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl5)
        Me.PanelCtrl.Controls.Add(Me.ExportExcelSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.JobNoComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.CloseSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ClearSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.VendorGridControl)
        Me.PanelCtrl.Controls.Add(Me.ToDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl4)
        Me.PanelCtrl.Controls.Add(Me.FromDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl3)
        Me.PanelCtrl.Controls.Add(Me.PartyLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Controls.Add(Me.BTALabelControl)
        Me.PanelCtrl.Controls.Add(Me.RemainAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PaidAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.BillIDLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.SearchButton)
        Me.PanelCtrl.Controls.Add(Me.VendorPOLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelCtrl.Location = New System.Drawing.Point(2, 2)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(874, 515)
        Me.PanelCtrl.TabIndex = 0
        '
        'POStatusComboBoxEdit
        '
        Me.POStatusComboBoxEdit.Location = New System.Drawing.Point(395, 45)
        Me.POStatusComboBoxEdit.MenuManager = Me.BarManager1
        Me.POStatusComboBoxEdit.Name = "POStatusComboBoxEdit"
        Me.POStatusComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.POStatusComboBoxEdit.Properties.Items.AddRange(New Object() {"PO is closed", "PO is open"})
        Me.POStatusComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.POStatusComboBoxEdit.Size = New System.Drawing.Size(188, 20)
        Me.POStatusComboBoxEdit.TabIndex = 4
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem})
        Me.BarManager1.MaxItemId = 4
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.Caption = "New"
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.ImageOptions.Image = CType(resources.GetObject("NewBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.NewBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("NewBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.Caption = "Open"
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.ImageOptions.Image = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.OpenBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.ImageOptions.Image = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.DeleteBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(902, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 562)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(902, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 531)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(902, 31)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 531)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.Caption = "Print"
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(319, 48)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl5.TabIndex = 172
        Me.LabelControl5.Text = "PO Status"
        '
        'ExportExcelSimpleButton
        '
        Me.ExportExcelSimpleButton.Location = New System.Drawing.Point(788, 74)
        Me.ExportExcelSimpleButton.Name = "ExportExcelSimpleButton"
        Me.ExportExcelSimpleButton.Size = New System.Drawing.Size(78, 27)
        Me.ExportExcelSimpleButton.TabIndex = 170
        Me.ExportExcelSimpleButton.Text = "&Export Excel"
        '
        'JobNoComboBoxEdit
        '
        Me.JobNoComboBoxEdit.Location = New System.Drawing.Point(82, 12)
        Me.JobNoComboBoxEdit.MenuManager = Me.BarManager1
        Me.JobNoComboBoxEdit.Name = "JobNoComboBoxEdit"
        Me.JobNoComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.JobNoComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.JobNoComboBoxEdit.Size = New System.Drawing.Size(214, 20)
        Me.JobNoComboBoxEdit.TabIndex = 0
        '
        'CloseSimpleButton
        '
        Me.CloseSimpleButton.ImageOptions.Image = CType(resources.GetObject("CloseSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.CloseSimpleButton.Location = New System.Drawing.Point(697, 74)
        Me.CloseSimpleButton.Name = "CloseSimpleButton"
        Me.CloseSimpleButton.Size = New System.Drawing.Size(82, 27)
        Me.CloseSimpleButton.TabIndex = 7
        Me.CloseSimpleButton.Text = "Close"
        '
        'ClearSimpleButton
        '
        Me.ClearSimpleButton.ImageOptions.Image = CType(resources.GetObject("ClearSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.ClearSimpleButton.Location = New System.Drawing.Point(697, 41)
        Me.ClearSimpleButton.Name = "ClearSimpleButton"
        Me.ClearSimpleButton.Size = New System.Drawing.Size(82, 27)
        Me.ClearSimpleButton.TabIndex = 6
        Me.ClearSimpleButton.Text = "Clear"
        '
        'VendorGridControl
        '
        Me.VendorGridControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.VendorGridControl.Location = New System.Drawing.Point(8, 109)
        Me.VendorGridControl.MainView = Me.VendorGridView
        Me.VendorGridControl.MenuManager = Me.BarManager1
        Me.VendorGridControl.Name = "VendorGridControl"
        Me.VendorGridControl.Size = New System.Drawing.Size(858, 401)
        Me.VendorGridControl.TabIndex = 8
        Me.VendorGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VendorGridView})
        '
        'VendorGridView
        '
        Me.VendorGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.VendorGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.VendorGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.VendorGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.VendorGridView.GridControl = Me.VendorGridControl
        Me.VendorGridView.Name = "VendorGridView"
        Me.VendorGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.VendorGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.VendorGridView.OptionsNavigation.UseTabKey = False
        Me.VendorGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.VendorGridView.OptionsView.EnableAppearanceOddRow = True
        Me.VendorGridView.OptionsView.RowAutoHeight = True
        Me.VendorGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.VendorGridView.OptionsView.ShowGroupPanel = False
        Me.VendorGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.VendorGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'ToDateEdit
        '
        Me.ToDateEdit.EditValue = Nothing
        Me.ToDateEdit.Location = New System.Drawing.Point(501, 12)
        Me.ToDateEdit.MenuManager = Me.BarManager1
        Me.ToDateEdit.Name = "ToDateEdit"
        Me.ToDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ToDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ToDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.ToDateEdit.Size = New System.Drawing.Size(82, 20)
        Me.ToDateEdit.TabIndex = 2
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(483, 15)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(12, 13)
        Me.LabelControl4.TabIndex = 104
        Me.LabelControl4.Text = "To"
        '
        'FromDateEdit
        '
        Me.FromDateEdit.EditValue = Nothing
        Me.FromDateEdit.Location = New System.Drawing.Point(395, 12)
        Me.FromDateEdit.MenuManager = Me.BarManager1
        Me.FromDateEdit.Name = "FromDateEdit"
        Me.FromDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.FromDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.FromDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.FromDateEdit.Size = New System.Drawing.Size(82, 20)
        Me.FromDateEdit.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(319, 15)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl3.TabIndex = 102
        Me.LabelControl3.Text = "Date From"
        '
        'PartyLookUpEdit
        '
        Me.PartyLookUpEdit.Location = New System.Drawing.Point(82, 45)
        Me.PartyLookUpEdit.Name = "PartyLookUpEdit"
        Me.PartyLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PartyLookUpEdit.Properties.NullText = ""
        Me.PartyLookUpEdit.Size = New System.Drawing.Size(214, 20)
        Me.PartyLookUpEdit.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(6, 48)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl2.TabIndex = 101
        Me.LabelControl2.Text = "Party Name"
        '
        'BTALabelControl
        '
        Me.BTALabelControl.Location = New System.Drawing.Point(-487, -206)
        Me.BTALabelControl.Name = "BTALabelControl"
        Me.BTALabelControl.Size = New System.Drawing.Size(0, 13)
        Me.BTALabelControl.TabIndex = 98
        '
        'RemainAmountLabelControl
        '
        Me.RemainAmountLabelControl.Location = New System.Drawing.Point(-499, 237)
        Me.RemainAmountLabelControl.Name = "RemainAmountLabelControl"
        Me.RemainAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.RemainAmountLabelControl.TabIndex = 97
        '
        'PaidAmountLabelControl
        '
        Me.PaidAmountLabelControl.Location = New System.Drawing.Point(-499, -221)
        Me.PaidAmountLabelControl.Name = "PaidAmountLabelControl"
        Me.PaidAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.PaidAmountLabelControl.TabIndex = 96
        '
        'BillIDLabelControl
        '
        Me.BillIDLabelControl.Location = New System.Drawing.Point(-499, -202)
        Me.BillIDLabelControl.Name = "BillIDLabelControl"
        Me.BillIDLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.BillIDLabelControl.TabIndex = 95
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(6, 15)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl8.TabIndex = 22
        Me.LabelControl8.Text = "Job No."
        '
        'SearchButton
        '
        Me.SearchButton.ImageOptions.Image = CType(resources.GetObject("SearchButton.ImageOptions.Image"), System.Drawing.Image)
        Me.SearchButton.Location = New System.Drawing.Point(697, 8)
        Me.SearchButton.Name = "SearchButton"
        Me.SearchButton.Size = New System.Drawing.Size(82, 27)
        Me.SearchButton.TabIndex = 5
        Me.SearchButton.Text = "Search"
        '
        'VendorPOLookUpEdit
        '
        Me.VendorPOLookUpEdit.Location = New System.Drawing.Point(82, 78)
        Me.VendorPOLookUpEdit.Name = "VendorPOLookUpEdit"
        Me.VendorPOLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.VendorPOLookUpEdit.Properties.NullText = ""
        Me.VendorPOLookUpEdit.Size = New System.Drawing.Size(214, 20)
        Me.VendorPOLookUpEdit.TabIndex = 5
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(6, 81)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Vendor PO No."
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(12, 37)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(878, 519)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'VendorPONewTableAdapter
        '
        Me.VendorPONewTableAdapter.ClearBeforeFill = True
        '
        'Frm_VendorReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 562)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_VendorReport"
        Me.Text = "Vendor Report"
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.POStatusComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VendorGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VendorGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FromDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FromDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PartyLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VendorPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SearchButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RemainAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PaidAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BillIDLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BTALabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PartyLookUpEdit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents FromDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents VendorGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents VendorGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CloseSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ClearSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents JobNoComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents VendorPONewTableAdapter As DS_VendorPONewTableAdapters.VendorPONewTableAdapter
    Friend WithEvents ExportExcelSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents POStatusComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents VendorPOLookUpEdit As DevExpress.XtraEditors.LookUpEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
End Class
