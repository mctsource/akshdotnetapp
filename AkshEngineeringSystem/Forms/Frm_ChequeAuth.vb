﻿Imports System.Data.OleDb
Imports DevExpress.XtraReports.UI
Public Class Frm_ChequeAuth
    Dim PaymentID As String
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PasswordTextEdit.Focus()
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub
    Function Validation() As Boolean
        If PasswordTextEdit.EditValue Is Nothing Then
            PasswordTextEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles OkSimpleButton.Click
        If Validation() Then
            Dim CMD As New OleDbCommand("SELECT UserID FROM LoginUser Where UserName='AUTHORITY' AND Pwd='" + PasswordTextEdit.Text + "'", ConStr)
            CnnOpen() : Dim Varify As Integer = CMD.ExecuteScalar : CnnClose()

            If Varify <> 0 Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Close()
            Else
                ErrorMsgLabelControl.Text = "Password Invalid."
            End If
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
End Class