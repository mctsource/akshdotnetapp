﻿Imports System.Data.OleDb
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraReports.UI
Public Class Frm_VendorPO

    Dim DS As New DataSet
    Dim DA As New OleDbDataAdapter
    Dim BS As New BindingSource
    Dim DADetails As New OleDbDataAdapter
    Dim BSDetails As New BindingSource
    Dim Status As Integer = 0
    Dim NetAmtLabel As New LabelControl
    Dim poCount As String = ""
    Dim len As Integer = 0
    Dim CurrentInvoiceNo As Integer = 0
    Private Sub FrmMasterConceptFrom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetDataTable()
        SetRelation()
        SetQuery()
        SetBinding()
        SetGrid()
        InitLookup()
        CreatedByTextEdit.Text = CurrentUserName.ToString()
        ModifiedByTextEdit.Text = CurrentUserName.ToString()

        AddNew()
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub SetDataTable()
        'Vendor PO
        DS.Tables.Add("VendorPO")
        With DS.Tables("VendorPO").Columns
            .Add("PurchaseID", GetType(Integer))
            .Add("JobNoPrefix", GetType(String))
            .Add("JobNo", GetType(String))
            .Add("OrderNo", GetType(String))
            .Add("OrderDate", GetType(Date))
            .Item("OrderDate").DefaultValue = Date.Today
            .Add("ModeOfDelivery", GetType(String))
            .Add("PlaceOfOrder", GetType(String))
            .Add("QuotationRef", GetType(String))
            .Add("Destination", GetType(String))
            .Add("Transporter", GetType(String))
            .Add("ConsignorName", GetType(String))
            .Add("ConsignorAddress", GetType(String))
            .Add("ConsignorGSTIN", GetType(String))
            .Add("ConsignorState", GetType(String))
            .Add("ConsignorStateCode", GetType(String))
            .Add("ConsignorPanNo", GetType(String))
            .Add("TotalAmtBeforeTax", GetType(Double))
            .Add("PackingCharge", GetType(Double))
            .Add("TransCharge", GetType(Double))
            .Add("PCGSTRate", GetType(Double))
            .Add("PCGSTAmt", GetType(Double))
            .Add("PSGSTRate", GetType(Double))
            .Add("PSGSTAmt", GetType(Double))
            .Add("PIGSTRate", GetType(Double))
            .Add("PIGSTAmt", GetType(Double))
            .Add("TCSRate", GetType(Double))
            .Add("TCSAmt", GetType(Double))
            .Add("CGST", GetType(Double))
            .Add("SGST", GetType(Double))
            .Add("IGST", GetType(Double))
            .Add("TotalGSTTax", GetType(Double))
            .Add("TotalAmtAfterTax", GetType(Double))
            .Add("TotalAmt", GetType(Double))
            .Add("GSTReverseCharge", GetType(Double))
            .Add("TotalInWords", GetType(String))
            .Add("TaxInWords", GetType(String))
            .Add("Remarks1", GetType(String))
            .Add("Remarks2", GetType(String))
            .Add("PaymentTerms", GetType(String))
            .Add("AdvancePayment", GetType(Double))
            .Add("GrandTotalAmount", GetType(Double))
            .Add("RoundOff", GetType(Double))
            .Add("CreatedBy", GetType(String))
            .Add("ModifiedBy", GetType(String))
            .Add("Company", GetType(String))
            .Add("POType", GetType(String))
            .Add("POStatus", GetType(String))
            .Item("POStatus").DefaultValue = "PO is open"
            .Add("BillStatus", GetType(String))
            .Item("BillStatus").DefaultValue = "Bill Cycle is open"
        End With

        With DS.Tables("VendorPO").Columns("PurchaseID")
            .AutoIncrement = True
            .AutoIncrementSeed = -1
            .AutoIncrementStep = -1
            .ReadOnly = True
            .Unique = True
        End With

        'VendorPO Detail
        DS.Tables.Add("VendorPODetail")
        With DS.Tables("VendorPODetail").Columns
            .Add("PurchaseDetailID", GetType(Integer))
            .Add("PurchaseID", GetType(Integer))
            .Add("ProductName", GetType(String))
            .Add("Description", GetType(String))
            .Add("HSNACS", GetType(String))
            .Add("UOM", GetType(String))
            .Add("Qty", GetType(Double))
            .Add("Rate", GetType(Double))
            .Add("Amount", GetType(Double))
            .Add("Discount", GetType(Double))
            .Add("DiscountVal", GetType(Double))
            .Add("TaxableValue", GetType(Double))
            .Add("CGSTRate", GetType(Double))
            .Add("CGSTAmt", GetType(Double))
            .Add("SGSTRate", GetType(Double))
            .Add("SGSTAmt", GetType(Double))
            .Add("IGSTRate", GetType(Double))
            .Add("IGSTAmt", GetType(Double))
            .Add("TotalAmount", GetType(Double))
        End With

        With DS.Tables("VendorPODetail").Columns("PurchaseDetailID")
            .AutoIncrement = True
            .AutoIncrementSeed = -1
            .AutoIncrementStep = -1
            .ReadOnly = True
            .Unique = True
        End With
    End Sub

    Sub SetRelation()
        DS.Relations.Add(New DataRelation("RelationVendorPO", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPODetail").Columns("PurchaseID"), False))
        Dim FK_VendorPODetail As New Global.System.Data.ForeignKeyConstraint("FK_VendorPO", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPODetail").Columns("PurchaseID"))
        DS.Tables("VendorPODetail").Constraints.Add(FK_VendorPODetail)
        With FK_VendorPODetail
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        BS.DataSource = DS
        BS.DataMember = "VendorPO"

        BSDetails.DataSource = BS
        BSDetails.DataMember = "RelationVendorPO"

        InvoiceDetailGridControl.DataSource = BSDetails
    End Sub

    Sub SetQuery()
        ' VendorPO ......................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
        DA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ModeOfDelivery,PlaceOfOrder,QuotationRef,Destination,Transporter,ConsignorName,ConsignorAddress,ConsignorGSTIN,ConsignorState,ConsignorStateCode,ConsignorPanNo,TotalAmtBeforeTax,PackingCharge,TransCharge,PCGSTRate,PCGSTAmt,PSGSTRate,PSGSTAmt,PIGSTRate,PIGSTAmt,TCSRate,TCSAmt,CGST,SGST,IGST,TotalGSTTax,TotalAmtAfterTax,TotalAmt,GSTReverseCharge,TotalInWords,TaxInWords,Remarks1,Remarks2,PaymentTerms,AdvancePayment,GrandTotalAmount,RoundOff,CreatedBy,ModifiedBy,Company,POType From VendorPO Where PurchaseID=@PurchaseID", ConStr)
        DA.SelectCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")

        DA.InsertCommand = New OleDbCommand("Insert Into VendorPO (JobNoPrefix,JobNo,OrderNo,OrderDate,ModeOfDelivery,PlaceOfOrder,QuotationRef,Destination,Transporter,ConsignorName,ConsignorAddress,ConsignorGSTIN,ConsignorState,ConsignorStateCode,ConsignorPanNo,TotalAmtBeforeTax,PackingCharge,TransCharge,PCGSTRate,PCGSTAmt,PSGSTRate,PSGSTAmt,PIGSTRate,PIGSTAmt,TCSRate,TCSAmt,CGST,SGST,IGST,TotalGSTTax,TotalAmtAfterTax,TotalAmt,GSTReverseCharge,TotalInWords,TaxInWords,Remarks1,Remarks2,PaymentTerms,AdvancePayment,GrandTotalAmount,RoundOff,CreatedBy,ModifiedBy,Company,POType,POStatus,BillStatus) Values (@JobNoPrefix,@JobNo,@OrderNo,@OrderDate,@ModeOfDelivery,@PlaceOfOrder,@QuotationRef,@Destination,@Transporter,@ConsignorName,@ConsignorAddress,@ConsignorGSTIN,@ConsignorState,@ConsignorStateCode,@ConsignorPanNo,@TotalAmtBeforeTax,@PackingCharge,@TransCharge,@PCGSTRate,@PCGSTAmt,@PSGSTRate,@PSGSTAmt,@PIGSTRate,@PIGSTAmt,@TCSRate,@TCSAmt,@CGST,@SGST,@IGST,@TotalGSTTax,@TotalAmtAfterTax,@TotalAmt,@GSTReverseCharge,@TotalInWords,@TaxInWords,@Remarks1,@Remarks2,@PaymentTerms,@AdvancePayment,@GrandTotalAmount,@RoundOff,@CreatedBy,@ModifiedBy,@Company,@POType,@POStatus,@BillStatus)", ConStr)
        DA.InsertCommand.Parameters.Add("@JobNoPrefix", OleDbType.VarChar, 50, "JobNoPrefix")
        DA.InsertCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        DA.InsertCommand.Parameters.Add("@OrderNo", OleDbType.VarChar, 50, "OrderNo")
        DA.InsertCommand.Parameters.Add("@OrderDate", OleDbType.Date, 3, "OrderDate")
        DA.InsertCommand.Parameters.Add("@ModeOfDelivery", OleDbType.VarChar, 50, "ModeOfDelivery")
        DA.InsertCommand.Parameters.Add("@PlaceOfOrder", OleDbType.VarChar, 50, "PlaceOfOrder")
        DA.InsertCommand.Parameters.Add("@QuotationRef", OleDbType.VarChar, 50, "QuotationRef")
        DA.InsertCommand.Parameters.Add("@Destination", OleDbType.VarChar, 50, "Destination")
        DA.InsertCommand.Parameters.Add("@Transporter", OleDbType.VarChar, 50, "Transporter")
        DA.InsertCommand.Parameters.Add("@ConsignorName", OleDbType.VarChar, 50, "ConsignorName")
        DA.InsertCommand.Parameters.Add("@ConsignorAddress", OleDbType.VarChar, 200, "ConsignorAddress")
        DA.InsertCommand.Parameters.Add("@ConsignorGSTIN", OleDbType.VarChar, 50, "ConsignorGSTIN")
        DA.InsertCommand.Parameters.Add("@ConsignorState", OleDbType.VarChar, 50, "ConsignorState")
        DA.InsertCommand.Parameters.Add("@ConsignorStateCode", OleDbType.VarChar, 50, "ConsignorStateCode")
        DA.InsertCommand.Parameters.Add("@ConsignorPanNo", OleDbType.VarChar, 50, "ConsignorPanNo")
        DA.InsertCommand.Parameters.Add("@TotalAmtBeforeTax", OleDbType.Double, 8, "TotalAmtBeforeTax")
        DA.InsertCommand.Parameters.Add("@PackingCharge", OleDbType.Double, 8, "PackingCharge")
        DA.InsertCommand.Parameters.Add("@TransCharge", OleDbType.Double, 8, "TransCharge")
        DA.InsertCommand.Parameters.Add("@PCGSTRate", OleDbType.Double, 8, "PCGSTRate")
        DA.InsertCommand.Parameters.Add("@PCGSTAmt", OleDbType.Double, 8, "PCGSTAmt")
        DA.InsertCommand.Parameters.Add("@PSGSTRate", OleDbType.Double, 8, "PSGSTRate")
        DA.InsertCommand.Parameters.Add("@PSGSTAmt", OleDbType.Double, 8, "PSGSTAmt")
        DA.InsertCommand.Parameters.Add("@PIGSTRate", OleDbType.Double, 8, "PIGSTRate")
        DA.InsertCommand.Parameters.Add("@PIGSTAmt", OleDbType.Double, 8, "PIGSTAmt")
        DA.InsertCommand.Parameters.Add("@TCSRate", OleDbType.Double, 8, "TCSRate")
        DA.InsertCommand.Parameters.Add("@TCSAmt", OleDbType.Double, 8, "TCSAmt")
        DA.InsertCommand.Parameters.Add("@CGST", OleDbType.Double, 8, "CGST")
        DA.InsertCommand.Parameters.Add("@SGST", OleDbType.Double, 8, "SGST")
        DA.InsertCommand.Parameters.Add("@IGST", OleDbType.Double, 8, "IGST")
        DA.InsertCommand.Parameters.Add("@TotalGSTTax", OleDbType.Double, 8, "TotalGSTTax")
        DA.InsertCommand.Parameters.Add("@TotalAmtAfterTax", OleDbType.Double, 8, "TotalAmtAfterTax")
        DA.InsertCommand.Parameters.Add("@TotalAmt", OleDbType.Double, 8, "TotalAmt")
        DA.InsertCommand.Parameters.Add("@GSTReverseCharge", OleDbType.Double, 8, "GSTReverseCharge")
        DA.InsertCommand.Parameters.Add("@TotalInWords", OleDbType.VarChar, 150, "TotalInWords")
        DA.InsertCommand.Parameters.Add("@TaxInWords", OleDbType.VarChar, 150, "TaxInWords")
        DA.InsertCommand.Parameters.Add("@Remarks1", OleDbType.VarChar, 1500, "Remarks1")
        DA.InsertCommand.Parameters.Add("@Remarks2", OleDbType.VarChar, 1500, "Remarks2")
        DA.InsertCommand.Parameters.Add("@PaymentTerms", OleDbType.VarChar, 150, "PaymentTerms")
        DA.InsertCommand.Parameters.Add("@AdvancePayment", OleDbType.Double, 8, "AdvancePayment")
        DA.InsertCommand.Parameters.Add("@GrandTotalAmount", OleDbType.Double, 8, "GrandTotalAmount")
        DA.InsertCommand.Parameters.Add("@RoundOff", OleDbType.Double, 8, "RoundOff")
        DA.InsertCommand.Parameters.Add("@CreatedBy", OleDbType.VarChar, 50, "CreatedBy")
        DA.InsertCommand.Parameters.Add("@ModifiedBy", OleDbType.VarChar, 50, "ModifiedBy")
        DA.InsertCommand.Parameters.Add("@Company", OleDbType.VarChar, 50, "Company")
        DA.InsertCommand.Parameters.Add("@POType", OleDbType.VarChar, 50, "POType")
        DA.InsertCommand.Parameters.Add("@POStatus", OleDbType.VarChar, 50, "POStatus")
        DA.InsertCommand.Parameters.Add("@BillStatus", OleDbType.VarChar, 50, "BillStatus")

        DA.UpdateCommand = New OleDbCommand("Update VendorPO Set JobNoPrefix=@JobNoPrefix,JobNo=@JobNo,OrderNo=@OrderNo,OrderDate=@OrderDate,ModeOfDelivery=@ModeOfDelivery,PlaceOfOrder=@PlaceOfOrder,QuotationRef=@QuotationRef,Destination=@Destination,Transporter=@Transporter,ConsignorName=@ConsignorName,ConsignorAddress=@ConsignorAddress,ConsignorGSTIN=@ConsignorGSTIN,ConsignorState=@ConsignorState,ConsignorStateCode=@ConsignorStateCode,ConsignorPanNo=@ConsignorPanNo,TotalAmtBeforeTax=@TotalAmtBeforeTax,PackingCharge=@PackingCharge,TransCharge=@TransCharge,PCGSTRate=@PCGSTRate,PCGSTAmt=@PCGSTAmt,PSGSTRate=@PSGSTRate,PSGSTAmt=@PSGSTAmt,PIGSTRate=@PIGSTRate,PIGSTAmt=@PIGSTAmt,TCSRate=@TCSRate,TCSAmt=@TCSAmt,CGST=@CGST,SGST=@SGST,IGST=@IGST,TotalGSTTax=@TotalGSTTax,TotalAmtAfterTax=@TotalAmtAfterTax,TotalAmt=@TotalAmt,GSTReverseCharge=@GSTReverseCharge,TotalInWords=@TotalInWords,TaxInWords=@TaxInWords,Remarks1=@Remarks1,Remarks2=@Remarks2,PaymentTerms=@PaymentTerms,AdvancePayment=@AdvancePayment,GrandTotalAmount=@GrandTotalAmount,RoundOff=@RoundOff,ModifiedBy=@ModifiedBy Where PurchaseID=@PurchaseID", ConStr)
        DA.UpdateCommand.Parameters.Add("@JobNoPrefix", OleDbType.VarChar, 50, "JobNoPrefix")
        DA.UpdateCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        DA.UpdateCommand.Parameters.Add("@OrderNo", OleDbType.VarChar, 50, "OrderNo")
        DA.UpdateCommand.Parameters.Add("@OrderDate", OleDbType.Date, 3, "OrderDate")
        DA.UpdateCommand.Parameters.Add("@ModeOfDelivery", OleDbType.VarChar, 50, "ModeOfDelivery")
        DA.UpdateCommand.Parameters.Add("@PlaceOfOrder", OleDbType.VarChar, 50, "PlaceOfOrder")
        DA.UpdateCommand.Parameters.Add("@QuotationRef", OleDbType.VarChar, 50, "QuotationRef")
        DA.UpdateCommand.Parameters.Add("@Destination", OleDbType.VarChar, 50, "Destination")
        DA.UpdateCommand.Parameters.Add("@Transporter", OleDbType.VarChar, 50, "Transporter")
        DA.UpdateCommand.Parameters.Add("@ConsignorName", OleDbType.VarChar, 50, "ConsignorName")
        DA.UpdateCommand.Parameters.Add("@ConsignorAddress", OleDbType.VarChar, 200, "ConsignorAddress")
        DA.UpdateCommand.Parameters.Add("@ConsignorGSTIN", OleDbType.VarChar, 50, "ConsignorGSTIN")
        DA.UpdateCommand.Parameters.Add("@ConsignorState", OleDbType.VarChar, 50, "ConsignorState")
        DA.UpdateCommand.Parameters.Add("@ConsignorStateCode", OleDbType.VarChar, 50, "ConsignorStateCode")
        DA.UpdateCommand.Parameters.Add("@ConsignorPanNo", OleDbType.VarChar, 50, "ConsignorPanNo")
        DA.UpdateCommand.Parameters.Add("@TotalAmtBeforeTax", OleDbType.Double, 8, "TotalAmtBeforeTax")
        DA.UpdateCommand.Parameters.Add("@PackingCharge", OleDbType.Double, 8, "PackingCharge")
        DA.UpdateCommand.Parameters.Add("@TransCharge", OleDbType.Double, 8, "TransCharge")
        DA.UpdateCommand.Parameters.Add("@PCGSTRate", OleDbType.Double, 8, "PCGSTRate")
        DA.UpdateCommand.Parameters.Add("@PCGSTAmt", OleDbType.Double, 8, "PCGSTAmt")
        DA.UpdateCommand.Parameters.Add("@PSGSTRate", OleDbType.Double, 8, "PSGSTRate")
        DA.UpdateCommand.Parameters.Add("@PSGSTAmt", OleDbType.Double, 8, "PSGSTAmt")
        DA.UpdateCommand.Parameters.Add("@PIGSTRate", OleDbType.Double, 8, "PIGSTRate")
        DA.UpdateCommand.Parameters.Add("@PIGSTAmt", OleDbType.Double, 8, "PIGSTAmt")
        DA.UpdateCommand.Parameters.Add("@TCSRate", OleDbType.Double, 8, "TCSRate")
        DA.UpdateCommand.Parameters.Add("@TCSAmt", OleDbType.Double, 8, "TCSAmt")
        DA.UpdateCommand.Parameters.Add("@CGST", OleDbType.Double, 8, "CGST")
        DA.UpdateCommand.Parameters.Add("@SGST", OleDbType.Double, 8, "SGST")
        DA.UpdateCommand.Parameters.Add("@IGST", OleDbType.Double, 8, "IGST")
        DA.UpdateCommand.Parameters.Add("@TotalGSTTax", OleDbType.Double, 8, "TotalGSTTax")
        DA.UpdateCommand.Parameters.Add("@TotalAmtAfterTax", OleDbType.Double, 8, "TotalAmtAfterTax")
        DA.UpdateCommand.Parameters.Add("@TotalAmt", OleDbType.Double, 8, "TotalAmt")
        DA.UpdateCommand.Parameters.Add("@GSTReverseCharge", OleDbType.Double, 8, "GSTReverseCharge")
        DA.UpdateCommand.Parameters.Add("@TotalInWords", OleDbType.VarChar, 150, "TotalInWords")
        DA.UpdateCommand.Parameters.Add("@TaxInWords", OleDbType.VarChar, 150, "TaxInWords")
        DA.UpdateCommand.Parameters.Add("@Remarks1", OleDbType.VarChar, 1500, "Remarks1")
        DA.UpdateCommand.Parameters.Add("@Remarks2", OleDbType.VarChar, 1500, "Remarks2")
        DA.UpdateCommand.Parameters.Add("@PaymentTerms", OleDbType.VarChar, 150, "PaymentTerms")
        DA.UpdateCommand.Parameters.Add("@AdvancePayment", OleDbType.Double, 8, "AdvancePayment")
        DA.UpdateCommand.Parameters.Add("@GrandTotalAmount", OleDbType.Double, 8, "GrandTotalAmount")
        DA.UpdateCommand.Parameters.Add("@RoundOff", OleDbType.Double, 8, "RoundOff")
        DA.UpdateCommand.Parameters.Add("@ModifiedBy", OleDbType.VarChar, 50, "ModifiedBy")
        DA.UpdateCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")

        DA.DeleteCommand = New OleDbCommand("Delete From VendorPO Where PurchaseID=@PurchaseID", ConStr)
        DA.DeleteCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")

        ' VendorPO Detail ..............................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................

        DADetails.SelectCommand = New OleDbCommand("Select PurchaseDetailID,PurchaseID,ProductName,Description,HSNACS,UOM,Qty,Rate,Amount,Discount,DiscountVal,TaxableValue,CGSTRate,CGSTAmt,SGSTRate,SGSTAmt,IGSTRate,IGSTAmt,TotalAmount From VendorPODetail Where PurchaseID=@PurchaseID", ConStr)
        DADetails.SelectCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")

        DADetails.InsertCommand = New OleDbCommand("Insert Into VendorPODetail (PurchaseID,ProductName,Description,HSNACS,UOM,Qty,Rate,Amount,Discount,DiscountVal,TaxableValue,CGSTRate,CGSTAmt,SGSTRate,SGSTAmt,IGSTRate,IGSTAmt,TotalAmount) Values (@PurchaseID,@ProductName,@Description,@HSNACS,@UOM,@Qty,@Rate,@Amount,@Discount,@DiscountVal,@TaxableValue,@CGSTRate,@CGSTAmt,@SGSTRate,@SGSTAmt,@IGSTRate,@IGSTAmt,@TotalAmount)", ConStr)
        DADetails.InsertCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")
        DADetails.InsertCommand.Parameters.Add("@ProductName", OleDbType.VarChar, 5000, "ProductName")
        DADetails.InsertCommand.Parameters.Add("@Description", OleDbType.VarChar, 10000, "Description")
        DADetails.InsertCommand.Parameters.Add("@HSNACS", OleDbType.VarChar, 50, "HSNACS")
        DADetails.InsertCommand.Parameters.Add("@UOM", OleDbType.VarChar, 50, "UOM")
        DADetails.InsertCommand.Parameters.Add("@Qty", OleDbType.Double, 8, "Qty")
        DADetails.InsertCommand.Parameters.Add("@Rate", OleDbType.Double, 8, "Rate")
        DADetails.InsertCommand.Parameters.Add("@Amount", OleDbType.Double, 8, "Amount")
        DADetails.InsertCommand.Parameters.Add("@Discount", OleDbType.Double, 8, "Discount")
        DADetails.InsertCommand.Parameters.Add("@DiscountVal", OleDbType.Double, 8, "DiscountVal")
        DADetails.InsertCommand.Parameters.Add("@TaxableValue", OleDbType.Double, 8, "TaxableValue")
        DADetails.InsertCommand.Parameters.Add("@CGSTRate", OleDbType.Double, 8, "CGSTRate")
        DADetails.InsertCommand.Parameters.Add("@CGSTAmt", OleDbType.Double, 8, "CGSTAmt")
        DADetails.InsertCommand.Parameters.Add("@SGSTRate", OleDbType.Double, 8, "SGSTRate")
        DADetails.InsertCommand.Parameters.Add("@SGSTAmt", OleDbType.Double, 8, "SGSTAmt")
        DADetails.InsertCommand.Parameters.Add("@IGSTRate", OleDbType.Double, 8, "IGSTRate")
        DADetails.InsertCommand.Parameters.Add("@IGSTAmt", OleDbType.Double, 8, "IGSTAmt")
        DADetails.InsertCommand.Parameters.Add("@TotalAmount", OleDbType.Double, 8, "TotalAmount")

        DADetails.UpdateCommand = New OleDbCommand("Update VendorPODetail Set PurchaseID=@PurchaseID,ProductName=@ProductName,Description=@Description,HSNACS=@HSNACS,UOM=@UOM,Qty=@Qty,Rate=@Rate,Amount=@Amount,Discount=@Discount,DiscountVal=@DiscountVal,TaxableValue=@TaxableValue,CGSTRate=@CGSTRate,CGSTAmt=@CGSTAmt,SGSTRate=@SGSTRate,SGSTAmt=@SGSTAmt,IGSTRate=@IGSTRate,IGSTAmt=@IGSTAmt,TotalAmount=@TotalAmount Where PurchaseDetailID=@PurchaseDetailID", ConStr)
        DADetails.UpdateCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")
        DADetails.UpdateCommand.Parameters.Add("@ProductName", OleDbType.VarChar, 5000, "ProductName")
        DADetails.UpdateCommand.Parameters.Add("@Description", OleDbType.VarChar, 10000, "Description")
        DADetails.UpdateCommand.Parameters.Add("@HSNACS", OleDbType.VarChar, 50, "HSNACS")
        DADetails.UpdateCommand.Parameters.Add("@UOM", OleDbType.VarChar, 50, "UOM")
        DADetails.UpdateCommand.Parameters.Add("@Qty", OleDbType.Double, 8, "Qty")
        DADetails.UpdateCommand.Parameters.Add("@Rate", OleDbType.Double, 8, "Rate")
        DADetails.UpdateCommand.Parameters.Add("@Amount", OleDbType.Double, 8, "Amount")
        DADetails.UpdateCommand.Parameters.Add("@Discount", OleDbType.Double, 8, "Discount")
        DADetails.UpdateCommand.Parameters.Add("@DiscountVal", OleDbType.Double, 8, "DiscountVal")
        DADetails.UpdateCommand.Parameters.Add("@TaxableValue", OleDbType.Double, 8, "TaxableValue")
        DADetails.UpdateCommand.Parameters.Add("@CGSTRate", OleDbType.Double, 8, "CGSTRate")
        DADetails.UpdateCommand.Parameters.Add("@CGSTAmt", OleDbType.Double, 8, "CGSTAmt")
        DADetails.UpdateCommand.Parameters.Add("@SGSTRate", OleDbType.Double, 8, "SGSTRate")
        DADetails.UpdateCommand.Parameters.Add("@SGSTAmt", OleDbType.Double, 8, "SGSTAmt")
        DADetails.UpdateCommand.Parameters.Add("@IGSTRate", OleDbType.Double, 8, "IGSTRate")
        DADetails.UpdateCommand.Parameters.Add("@IGSTAmt", OleDbType.Double, 8, "IGSTAmt")
        DADetails.UpdateCommand.Parameters.Add("@TotalAmount", OleDbType.Double, 8, "TotalAmount")
        DADetails.UpdateCommand.Parameters.Add("@PurchaseDetailID", OleDbType.Integer, 4, "PurchaseDetailID")

        DADetails.DeleteCommand = New OleDbCommand("Delete From VendorPODetail Where PurchaseDetailID=@PurchaseDetailID", ConStr)
        DADetails.DeleteCommand.Parameters.Add("@PurchaseDetailID", OleDbType.Integer, 4, "PurchaseDetailID")
    End Sub

    Sub SetBinding()
        JobPrefixTextEdit.DataBindings.Add(New Binding("EditValue", BS, "JobNoPrefix"))
        JobNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "JobNo"))
        DateDateEdit.DataBindings.Add(New Binding("EditValue", BS, "OrderDate"))
        OrderNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "OrderNo"))
        ModeOfDeliveryTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ModeOfDelivery"))
        PlaceOfOrderTextEdit.DataBindings.Add(New Binding("EditValue", BS, "PlaceOfOrder"))
        QuotationRefTextEdit.DataBindings.Add(New Binding("EditValue", BS, "QuotationRef"))
        DestinationTextEdit.DataBindings.Add(New Binding("EditValue", BS, "Destination"))
        TransporterTextEdit.DataBindings.Add(New Binding("EditValue", BS, "Transporter"))
        ConsignorNameComboBoxEdit.DataBindings.Add(New Binding("EditValue", BS, "ConsignorName"))
        ConsignorAddressTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ConsignorAddress"))
        ConsignorGSTINTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ConsignorGSTIN"))
        ConsignorStateTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ConsignorState"))
        ConsignorStateCodeTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ConsignorStateCode"))
        ConsignorPANNoTextEdit.DataBindings.Add(New Binding("EditValue", BS, "ConsignorPanNo"))
        TotalAmtBeforeTaxTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalAmtBeforeTax"))
        PackingTextEdit.DataBindings.Add(New Binding("EditValue", BS, "PackingCharge"))
        TransTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TransCharge"))
        PCGSTRateTextEdit.DataBindings.Add(New Binding("EditValue", BS, "PCGSTRate"))
        PCGSTAmt.DataBindings.Add(New Binding("EditValue", BS, "PCGSTAmt"))
        PSGSTRateTextEdit.DataBindings.Add(New Binding("EditValue", BS, "PSGSTRate"))
        PSGSTAmt.DataBindings.Add(New Binding("EditValue", BS, "PSGSTAmt"))
        PIGSTRateTextEdit.DataBindings.Add(New Binding("EditValue", BS, "PIGSTRate"))
        PIGSTAmt.DataBindings.Add(New Binding("EditValue", BS, "PIGSTAmt"))
        TCSRateTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TCSRate"))
        TotalTCSTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TCSAmt"))
        TotalGSTTaxTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalGSTTax"))
        TotalAmtAfterTaxTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalAmtAfterTax"))
        TotalAmtTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalAmt"))
        GSTReverseChargeTextEdit.DataBindings.Add(New Binding("EditValue", BS, "GSTReverseCharge"))
        TotalInWordsTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TotalInWords"))
        TaxInWordsTextEdit.DataBindings.Add(New Binding("EditValue", BS, "TaxInWords"))
        Remarks1TextEdit.DataBindings.Add(New Binding("EditValue", BS, "Remarks1"))
        Remarks2TextEdit.DataBindings.Add(New Binding("EditValue", BS, "Remarks2"))
        PaymentTermsTextEdit.DataBindings.Add(New Binding("EditValue", BS, "PaymentTerms"))
        AdvancePaymentTextEdit.DataBindings.Add(New Binding("EditValue", BS, "AdvancePayment"))
        NetAmtLabel.DataBindings.Add(New Binding("Text", BS, "GrandTotalAmount"))
        CreatedByTextEdit.DataBindings.Add(New Binding("Text", BS, "CreatedBy"))
        ModifiedByTextEdit.DataBindings.Add(New Binding("Text", BS, "ModifiedBy"))
    End Sub

    Sub InitLookup()
        If PubIGST = False Then
            SetGridCommboBox("SELECT DISTINCT PartyName FROM Party Where StateCode = 24 ", "Party", ConsignorNameComboBoxEdit)
        ElseIf PubIGST = True Then
            SetGridCommboBox("SELECT DISTINCT PartyName FROM Party Where StateCode <> 24 ", "Party", ConsignorNameComboBoxEdit)
        End If
        'SetGridCommboBox("SELECT DISTINCT PartyName FROM Party", "Party", ReceiverNameComboBoxEdit)
        'SetGridCommboBox("SELECT DISTINCT PartyName FROM Party", "Party", ConsigneeNameComboBoxEdit)
        GridComboBoxFunc("Select DISTINCT ProductName FROM Product", "Product", InvoiceDetailGridControl, InvoiceDetailGridView, "ProductName", "Product")
        GridMemoEditFunc(InvoiceDetailGridControl, InvoiceDetailGridView, "Description")
        'GridButtonEditFunc(InvoiceDetailGridControl, InvoiceDetailGridView, "AddProduct")
    End Sub

    Sub SetGrid()
        With InvoiceDetailGridView
            .Columns("PurchaseDetailID").Visible = False
            .Columns("PurchaseID").Visible = False
            .Columns("Amount").OptionsColumn.AllowFocus = False
            .Columns("Amount").OptionsColumn.ReadOnly = True
            .Columns("TaxableValue").OptionsColumn.AllowFocus = False
            .Columns("TaxableValue").OptionsColumn.ReadOnly = True
            .Columns("CGSTAmt").OptionsColumn.AllowFocus = False
            .Columns("CGSTAmt").OptionsColumn.ReadOnly = True
            .Columns("SGSTAmt").OptionsColumn.AllowFocus = False
            .Columns("SGSTAmt").OptionsColumn.ReadOnly = True
            .Columns("IGSTAmt").OptionsColumn.AllowFocus = False
            .Columns("IGSTAmt").OptionsColumn.ReadOnly = True
            .Columns("TotalAmount").Visible = False
            .Columns("TotalAmount").OptionsColumn.AllowFocus = False
            .Columns("TotalAmount").OptionsColumn.ReadOnly = True
            .Columns("ProductName").Width = 200
            .Columns("ProductName").Caption = "Product"
            .Columns("Description").Caption = "Description"
            .Columns("Description").Width = 200
            .Columns("HSNACS").Caption = "HSN"
            .Columns("Discount").Caption = "Dis.(%)"
            .Columns("DiscountVal").Caption = "Dis.(Value)"
            .Columns("CGSTRate").Caption = "CGST(%)"
            .Columns("SGSTRate").Caption = "SGST(%)"
            .Columns("IGSTRate").Caption = "IGST(%)"

            .OptionsView.ShowFooter = True
            .Columns("TaxableValue").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '.Columns("CGSTAmt").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '.Columns("SGSTAmt").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '.Columns("IGSTAmt").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '.Columns("TotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

            .Columns("Qty").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("DiscountVal").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        InvoiceDetailGridView.BestFitColumns()
        InvoiceDetailGridView.FocusedColumn = InvoiceDetailGridView.Columns(2)
        BSDetails.ResetBindings(True)


        If PubIGST = False Then
            InvoiceDetailGridView.Columns("CGSTRate").Visible = False
            InvoiceDetailGridView.Columns("CGSTAmt").Visible = False
            InvoiceDetailGridView.Columns("SGSTRate").Visible = False
            InvoiceDetailGridView.Columns("SGSTAmt").Visible = False
            InvoiceDetailGridView.Columns("IGSTRate").Visible = False
            InvoiceDetailGridView.Columns("IGSTAmt").Visible = False
        ElseIf PubIGST = True Then
            InvoiceDetailGridView.Columns("CGSTRate").Visible = False
            InvoiceDetailGridView.Columns("CGSTAmt").Visible = False
            InvoiceDetailGridView.Columns("SGSTRate").Visible = False
            InvoiceDetailGridView.Columns("SGSTAmt").Visible = False
            InvoiceDetailGridView.Columns("IGSTRate").Visible = False
            InvoiceDetailGridView.Columns("IGSTAmt").Visible = False
        End If
        'SetCurrencyFormat("Rate", "c", BillDetailGridView, BillDetailGridControl)
        'SetCurrencyFormat("Amount", "c", BillDetailGridView, BillDetailGridControl)
    End Sub

    Function Validation() As Boolean
        If JobNoTextEdit.EditValue Is DBNull.Value Then
            JobNoTextEdit.Focus()
            Return False
        ElseIf OrderNoTextEdit.EditValue Is DBNull.Value Then
            OrderNoTextEdit.Focus()
            Return False
        ElseIf DateDateEdit.EditValue Is DBNull.Value Then
            DateDateEdit.Focus()
            Return False
        ElseIf ConsignorNameComboBoxEdit.EditValue Is DBNull.Value Then
            ConsignorNameComboBoxEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Sub AddNew()
        BS.AddNew()
        BS.Current!CreatedBy = CurrentUserName
        BS.Current!ModifiedBy = CurrentUserName
        BS.Current!Company = PubCompanyName
        BS.Current!POType = PubPOType
        BS.Current!JobNoPrefix = "A-"
        BS.Current!OrderNo = GenerateNo()
        BS.Current!OrderDate = Date.Today
        BS.Current!PackingCharge = 0
        BS.Current!TransCharge = 0
        BS.Current!PCGSTRate = 0
        BS.Current!PCGSTAmt = 0
        BS.Current!PSGSTRate = 0
        BS.Current!PSGSTAmt = 0
        BS.Current!PIGSTRate = 0
        BS.Current!PIGSTAmt = 0
        BS.Current!TCSRate = 0
        BS.Current!TCSAmt = 0
        BS.Current!TotalAmtBeforeTax = 0
        BS.Current!CGST = 0
        BS.Current!SGST = 0
        BS.Current!IGST = 0
        BS.Current!TotalGSTTax = 0
        BS.Current!TotalAmtAfterTax = 0
        BS.Current!TotalAmt = 0
        BS.Current!GSTReverseCharge = 0
        BS.Current!RoundOff = 0
        BS.Current!AdvancePayment = 0
        BS.Current!GrandTotalAmount = 0
        BS.EndEdit()

        If PubIGST = False Then
            PIGSTRateTextEdit.Properties.ReadOnly = True
            PIGSTRateTextEdit.TabStop = False
        ElseIf PubIGST = True Then
            PCGSTRateTextEdit.Properties.ReadOnly = True
            PCGSTRateTextEdit.TabStop = False
            PSGSTRateTextEdit.Properties.ReadOnly = True
            PSGSTRateTextEdit.TabStop = False
        End If

        Status = 0
        Dim bz As New CultureInfo("hi-IN")
        Dim GTotal As Double = BS.Current!GrandTotalAmount
        NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)

        DateDateEdit.Focus()
        InitLookup()
    End Sub
    Function GetCurrentYear() As String
        Dim DACompany As New OleDbDataAdapter("Select CompanyID,FinancialYear From Company Where Name ='" + PubCompanyName + "'", ConStr)
        Dim DTCompany As New DataTable
        DACompany.Fill(DTCompany)

        Dim FinancialYear As String = DTCompany.Rows(0).Item("FinancialYear").ToString
        Return FinancialYear
    End Function
    Function GenerateNo() As String
        Dim DAFinance As New OleDbDataAdapter("Select FinancialYearID,VendorPOCount From FinancialSettings Where Company ='" + PubCompanyName + "' AND FinancialYear = '" + GetCurrentYear() + "'", ConStr)
        Dim DTFinance As New DataTable
        DAFinance.Fill(DTFinance)

        Dim PONo As String = DTFinance.Rows(0).Item("VendorPOCount").ToString
        len = PONo.Length()

        Dim VendorPOPrefix As String = "Aksh"

        poCount = PONo

        Dim newPO As String = VendorPOPrefix + "/" + poCount + "/" + GetCurrentYear()
        Return newPO
    End Function

    Private Sub NewBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles NewBarButtonItem.ItemClick
        BS.CancelEdit()
        BSDetails.CancelEdit()
        DS.RejectChanges()
        AddNew()
    End Sub
    Private Sub OpenBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles OpenBarButtonItem.ItemClick
        Dim DAFinance As New OleDbDataAdapter("Select StartDate,EndDate From Company Where name ='" + PubCompanyName + "'", ConStr)
        Dim DTFinance As New DataTable
        DAFinance.Fill(DTFinance)

        Dim StartDate As String = DTFinance.Rows(0).Item("StartDate").ToString()
        Dim SubStart As DateTime = Convert.ToDateTime(StartDate.Substring(0, 9))
        Dim EndDate As String = DTFinance.Rows(0).Item("EndDate").ToString()
        Dim SubEnd As DateTime = Convert.ToDateTime(EndDate.Substring(0, 9))

        Dim ID = ShowRecord("SELECT * FROM VendorPO Where POType='" + PubPOType + "' AND (VendorPO.OrderDate between #" & SubStart & "# And #" & SubEnd & "#) AND Company ='" + PubCompanyName + "' ORDER BY OrderDate DESC", "PurchaseID")
        'Dim ID = ShowRecord("SELECT * FROM ServiceInvoice Where InvoiceType='" + PubInvoiceType + "' AND Company = '" + PubCompanyName + "' ORDER BY InvoiceDate DESC", "ServiceInvoiceID")
        If ID > 0 Then
            Try
                DS.Tables("VendorPODetail").Clear()
                DS.Tables("VendorPO").Clear()
            Catch
            End Try
            DA.SelectCommand.Parameters("@PurchaseID").Value = ID
            DA.Fill(DS, "VendorPO")
            Status = 1
            DADetails.SelectCommand.Parameters("@PurchaseID").Value = ID
            DADetails.Fill(DS, "VendorPODetail")
            Dim bz As New CultureInfo("hi-IN")
            Dim GTotal As Double = BS.Current!GrandTotalAmount
            NetAmtLabelControl.Text = "Grand Amt:  " + GTotal.ToString("c", bz)

            CurrentInvoiceNo = ID
        End If
    End Sub

    Sub SetCurrentData()
        Dim ID = CurrentInvoiceNo
        If ID > 0 Then
            Try
                DS.Tables("VendorPODetail").Clear()
                DS.Tables("VendorPO").Clear()
            Catch
            End Try
            DA.SelectCommand.Parameters("@PurchaseID").Value = ID
            DA.Fill(DS, "VendorPO")
            Status = 1
            DADetails.SelectCommand.Parameters("@PurchaseID").Value = ID
            DADetails.Fill(DS, "VendorPODetail")

            Dim bz As New CultureInfo("hi-IN")
            Dim GTotal As Double = BS.Current!GrandTotalAmount
            NetAmtLabelControl.Text = "Grand Amt:  " + GTotal.ToString("c", bz)
        End If
    End Sub

    Private Sub DeleteBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DeleteBarButtonItem.ItemClick
        Try
            Dim Delete = XtraMessageBox.Show("Are You Want To Delete This Record ?" + vbNewLine + "If You Delete This Record then Related Items Record also Delete..", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
            If Delete = 1 Then
                For Each row In DS.Tables("VendorPODetail").Select("PurchaseID =" & BS.Current!PurchaseID)
                    BSDetails.RemoveCurrent()
                    DADetails.Update(DS.Tables("VendorPODetail"))
                Next
                BS.RemoveCurrent()
                DA.Update(DS.Tables("VendorPO"))
                AddNew()
            End If
        Catch ex As Exception
            BS.CancelEdit()
            DS.RejectChanges()
            XtraMessageBox.Show("Operation Failed :", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveNewSimpleButton.Click
        If Validation() Then
            If BSDetails.Count = 0 Then
                XtraMessageBox.Show("Please Enter Purchase Order Items", "Info")
            Else
                BS.Current!ModifiedBy = CurrentUserName
                BS.EndEdit()
                DA.Update(DS.Tables("VendorPO"))
                If Status = 0 Then
                    Dim NewCMD As New OleDbCommand("SELECT MAX(PurchaseID) FROM VendorPO", ConStr)
                    CnnOpen() : Dim CurrentID = Val(NewCMD.ExecuteScalar & "") : CnnClose()
                    BSDetails.MoveFirst()
                    For item As Integer = 0 To BSDetails.Count - 1
                        Dim InsertCMD As New OleDbCommand("Insert Into VendorPODetail (PurchaseID,ProductName,Description,HSNACS,UOM,Qty,Rate,Amount,Discount,DiscountVal,TaxableValue,CGSTRate,CGSTAmt,SGSTRate,SGSTAmt,IGSTRate,IGSTAmt,TotalAmount) Values (@PurchaseID,@ProductName,@Description,@HSNACS,@UOM,@Qty,@Rate,@Amount,@Discount,@DiscountVal,@TaxableValue,@CGSTRate,@CGSTAmt,@SGSTRate,@SGSTAmt,@IGSTRate,@IGSTAmt,@TotalAmount)", ConStr)
                        InsertCMD.Parameters.AddWithValue("@PurchaseID", CurrentID)
                        InsertCMD.Parameters.AddWithValue("@ProductName", BSDetails.Current!ProductName)
                        InsertCMD.Parameters.AddWithValue("@Description", BSDetails.Current!Description)
                        InsertCMD.Parameters.AddWithValue("@HSNACS", BSDetails.Current!HSNACS)
                        InsertCMD.Parameters.AddWithValue("@UOM", BSDetails.Current!UOM)
                        InsertCMD.Parameters.AddWithValue("@Qty", BSDetails.Current!Qty)
                        InsertCMD.Parameters.AddWithValue("@Rate", BSDetails.Current!Rate)
                        InsertCMD.Parameters.AddWithValue("@Amount", BSDetails.Current!Amount)
                        InsertCMD.Parameters.AddWithValue("@Discount", BSDetails.Current!Discount)
                        InsertCMD.Parameters.AddWithValue("@DiscountVal", BSDetails.Current!DiscountVal)
                        InsertCMD.Parameters.AddWithValue("@TaxableValue", BSDetails.Current!TaxableValue)
                        InsertCMD.Parameters.AddWithValue("@CGSTRate", BSDetails.Current!CGSTRate)
                        InsertCMD.Parameters.AddWithValue("@CGSTAmt", BSDetails.Current!CGSTAmt)
                        InsertCMD.Parameters.AddWithValue("@SGSTRate", BSDetails.Current!SGSTRate)
                        InsertCMD.Parameters.AddWithValue("@SGSTAmt", BSDetails.Current!SGSTAmt)
                        InsertCMD.Parameters.AddWithValue("@IGSTRate", BSDetails.Current!IGSTRate)
                        InsertCMD.Parameters.AddWithValue("@IGSTAmt", BSDetails.Current!IGSTAmt)
                        InsertCMD.Parameters.AddWithValue("@TotalAmount", BSDetails.Current!TotalAmount)

                        CnnOpen() : InsertCMD.ExecuteNonQuery() : CnnClose()
                        BSDetails.MoveNext()
                    Next

                    Dim myString As String = poCount
                    Dim X As Integer = CInt(myString) + 1
                    Dim reCount As String = (Convert.ToInt32(X)).ToString.PadLeft(len, "0"c)

                    Dim CmpCmd As New OleDbCommand("Update FinancialSettings Set VendorPOCount=@VendorPOCount Where Company ='" + PubCompanyName + "' And FinancialYear = '" + GetCurrentYear() + "'", ConStr)
                    CmpCmd.Parameters.AddWithValue("@VendorPOCount", reCount)
                    CnnOpen()
                    CmpCmd.ExecuteNonQuery()
                    CnnClose()
                    AddPayment()
                ElseIf Status = 1 Then
                    BSDetails.EndEdit()
                    DADetails.Update(DS.Tables("VendorPODetail"))

                End If
                AddNew()
            End If
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If Validation() Then
            If BSDetails.Count = 0 Then
                XtraMessageBox.Show("Please Enter Purchase Order Items", "Info")
            Else
                BS.Current!ModifiedBy = CurrentUserName
                BS.EndEdit()
                DA.Update(DS.Tables("VendorPO"))

                If Status = 0 Then
                    Dim NewCMD As New OleDbCommand("SELECT MAX(PurchaseID) FROM VendorPO", ConStr)
                    CnnOpen() : Dim CurrentID = Val(NewCMD.ExecuteScalar & "") : CnnClose()
                    CurrentInvoiceNo = CurrentID
                    BSDetails.MoveFirst()
                    For item As Integer = 0 To BSDetails.Count - 1
                        Dim InsertCMD As New OleDbCommand("Insert Into VendorPODetail (PurchaseID,ProductName,Description,HSNACS,UOM,Qty,Rate,Amount,Discount,DiscountVal,TaxableValue,CGSTRate,CGSTAmt,SGSTRate,SGSTAmt,IGSTRate,IGSTAmt,TotalAmount) Values (@PurchaseID,@ProductName,@Description,@HSNACS,@UOM,@Qty,@Rate,@Amount,@Discount,@DiscountVal,@TaxableValue,@CGSTRate,@CGSTAmt,@SGSTRate,@SGSTAmt,@IGSTRate,@IGSTAmt,@TotalAmount)", ConStr)
                        InsertCMD.Parameters.AddWithValue("@PurchaseID", CurrentID)
                        InsertCMD.Parameters.AddWithValue("@ProductName", BSDetails.Current!ProductName)
                        InsertCMD.Parameters.AddWithValue("@Description", BSDetails.Current!Description)
                        InsertCMD.Parameters.AddWithValue("@HSNACS", BSDetails.Current!HSNACS)
                        InsertCMD.Parameters.AddWithValue("@UOM", BSDetails.Current!UOM)
                        InsertCMD.Parameters.AddWithValue("@Qty", BSDetails.Current!Qty)
                        InsertCMD.Parameters.AddWithValue("@Rate", BSDetails.Current!Rate)
                        InsertCMD.Parameters.AddWithValue("@Amount", BSDetails.Current!Amount)
                        InsertCMD.Parameters.AddWithValue("@Discount", BSDetails.Current!Discount)
                        InsertCMD.Parameters.AddWithValue("@DiscountVal", BSDetails.Current!DiscountVal)
                        InsertCMD.Parameters.AddWithValue("@TaxableValue", BSDetails.Current!TaxableValue)
                        InsertCMD.Parameters.AddWithValue("@CGSTRate", BSDetails.Current!CGSTRate)
                        InsertCMD.Parameters.AddWithValue("@CGSTAmt", BSDetails.Current!CGSTAmt)
                        InsertCMD.Parameters.AddWithValue("@SGSTRate", BSDetails.Current!SGSTRate)
                        InsertCMD.Parameters.AddWithValue("@SGSTAmt", BSDetails.Current!SGSTAmt)
                        InsertCMD.Parameters.AddWithValue("@IGSTRate", BSDetails.Current!IGSTRate)
                        InsertCMD.Parameters.AddWithValue("@IGSTAmt", BSDetails.Current!IGSTAmt)
                        InsertCMD.Parameters.AddWithValue("@TotalAmount", BSDetails.Current!TotalAmount)

                        CnnOpen() : InsertCMD.ExecuteNonQuery() : CnnClose()

                        BSDetails.MoveNext()
                    Next

                    Dim myString As String = poCount
                    Dim X As Integer = CInt(myString) + 1
                    Dim reCount As String = (Convert.ToInt32(X)).ToString.PadLeft(len, "0"c)
                    Dim CmpCmd As New OleDbCommand("Update FinancialSettings Set VendorPOCount=@VendorPOCount Where Company ='" + PubCompanyName + "' And FinancialYear = '" + GetCurrentYear() + "'", ConStr)
                    CmpCmd.Parameters.AddWithValue("@VendorPOCount", reCount)
                    CnnOpen()
                    CmpCmd.ExecuteNonQuery()
                    CnnClose()
                    AddPayment()
                ElseIf Status = 1 Then
                    BSDetails.EndEdit()
                    DADetails.Update(DS.Tables("VendorPODetail"))
                End If
                XtraMessageBox.Show("Purchase Order Saved Successfully...", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
                AddNew()
                SetCurrentData()
            End If
        End If
    End Sub
    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub
    Private Sub AddPayment()
        Try
            Dim RemainAmt As Decimal
            RemainAmt = Decimal.Parse(TotalAmtAfterTaxTextEdit.Text) - Decimal.Parse(AdvancePaymentTextEdit.Text)

            Dim NewCMD As New OleDbCommand("SELECT MAX(PurchaseID) FROM VendorPO", ConStr)
            CnnOpen() : Dim CurrentID = Val(NewCMD.ExecuteScalar & "") : CnnClose()
            CurrentInvoiceNo = CurrentID
            Dim InsertCMD As New OleDbCommand("Insert Into VendorPayment (PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount,POAmt) Values (@PurchaseID,@JobNo,@PONo,@CheckNo,@ChkDate,@PaidAmount,@TDS,@RemainAmount,@POAmt)", ConStr)
            InsertCMD.Parameters.AddWithValue("@PurchaseID", CurrentID)
            'InsertCMD.Parameters.AddWithValue("@JobNo", BS.Current!JobNo)
            InsertCMD.Parameters.AddWithValue("@JobNo", JobPrefixTextEdit.Text + JobNoTextEdit.Text)
            InsertCMD.Parameters.AddWithValue("@PONo", BS.Current!OrderNo)
            'InsertCMD.Parameters.AddWithValue("@PONo", OrderNoTextEdit.Text)
            InsertCMD.Parameters.AddWithValue("@CheckNo", "Random")
            InsertCMD.Parameters.AddWithValue("@ChkDate", Date.Today)
            InsertCMD.Parameters.AddWithValue("@PaidAmount", BS.Current!AdvancePayment)
            'InsertCMD.Parameters.AddWithValue("@PaidAmount", AdvancePaymentTextEdit.Text)
            InsertCMD.Parameters.AddWithValue("@TDS", 0)
            InsertCMD.Parameters.AddWithValue("@RemainAmount", RemainAmt)
            InsertCMD.Parameters.AddWithValue("@POAmt", BS.Current!GrandTotalAmount)

            CnnOpen() : InsertCMD.ExecuteNonQuery() : CnnClose()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InvoiceDetailGridControl_Leave(sender As Object, e As EventArgs) Handles InvoiceDetailGridControl.Leave
        BS.Current!TotalAmtBeforeTax = InvoiceDetailGridView.Columns("TaxableValue").SummaryItem.SummaryValue

        'BS.Current!CGST = InvoiceDetailGridView.Columns("CGSTAmt").SummaryItem.SummaryValue
        'BS.Current!SGST = InvoiceDetailGridView.Columns("SGSTAmt").SummaryItem.SummaryValue
        'BS.Current!IGST = InvoiceDetailGridView.Columns("IGSTAmt").SummaryItem.SummaryValue

        'BS.Current!TotalGSTTax = BS.Current!CGST + BS.Current!SGST + BS.Current!IGST
        'BS.Current!TotalAmtAfterTax = InvoiceDetailGridView.Columns("TotalAmount").SummaryItem.SummaryValue
        BS.EndEdit()
        Calc()
    End Sub

    Private Sub InvoiceDetailGridView_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles InvoiceDetailGridView.CellValueChanged
        Try
            If e.Column.FieldName = "Rate" Or e.Column.FieldName = "Qty" Or e.Column.FieldName = "Discount" Or e.Column.FieldName = "DiscountVal" Or e.Column.FieldName = "CGSTRate" Or e.Column.FieldName = "SGSTRate" Or e.Column.FieldName = "IGSTRate" Then
                Dim qty As Double = IIf(BSDetails.Current!Qty Is DBNull.Value, 0, BSDetails.Current!Qty)
                BSDetails.Current!Qty = Math.Round(qty, 2)

                BSDetails.Current!Amount = IIf(BSDetails.Current!Rate Is DBNull.Value, 0, BSDetails.Current!Rate) * IIf(BSDetails.Current!Qty Is DBNull.Value, 0, BSDetails.Current!Qty)

                If e.Column.FieldName = "Rate" Or e.Column.FieldName = "Qty" Or e.Column.FieldName = "Discount" Or e.Column.FieldName = "CGSTRate" Or e.Column.FieldName = "SGSTRate" Or e.Column.FieldName = "IGSTRate" Then
                    Dim Amt As Double = IIf(BSDetails.Current!Amount Is DBNull.Value, 0, BSDetails.Current!Amount) * IIf(BSDetails.Current!Discount Is DBNull.Value, 0, BSDetails.Current!Discount) / 100
                    BSDetails.Current!TaxableValue = Math.Round(IIf(BSDetails.Current!Amount Is DBNull.Value, 0, BSDetails.Current!Amount) - Amt)
                    BSDetails.Current!DiscountVal = Math.Round(Amt, 2)

                End If

                If e.Column.FieldName = "DiscountVal" Then
                    Dim Dis As Double = IIf(BSDetails.Current!DiscountVal Is DBNull.Value, 0, BSDetails.Current!DiscountVal) * 100 / IIf(BSDetails.Current!Amount Is DBNull.Value, 0, BSDetails.Current!Amount)
                    BSDetails.Current!TaxableValue = IIf(BSDetails.Current!Amount Is DBNull.Value, 0, BSDetails.Current!Amount) - IIf(BSDetails.Current!DiscountVal Is DBNull.Value, 0, BSDetails.Current!DiscountVal)
                    BSDetails.Current!Discount = Math.Round(Dis, 2)
                End If

                BSDetails.Current!CGSTAmt = IIf(BSDetails.Current!TaxableValue Is DBNull.Value, 0, BSDetails.Current!TaxableValue) * IIf(BSDetails.Current!CGSTRate Is DBNull.Value, 0, BSDetails.Current!CGSTRate) / 100
                BSDetails.Current!SGSTAmt = IIf(BSDetails.Current!TaxableValue Is DBNull.Value, 0, BSDetails.Current!TaxableValue) * IIf(BSDetails.Current!SGSTRate Is DBNull.Value, 0, BSDetails.Current!SGSTRate) / 100
                BSDetails.Current!IGSTAmt = IIf(BSDetails.Current!TaxableValue Is DBNull.Value, 0, BSDetails.Current!TaxableValue) * IIf(BSDetails.Current!IGSTRate Is DBNull.Value, 0, BSDetails.Current!IGSTRate) / 100

                BSDetails.Current!TotalAmount = IIf(BSDetails.Current!TaxableValue Is DBNull.Value, 0, BSDetails.Current!TaxableValue) + IIf(BSDetails.Current!CGSTAmt Is DBNull.Value, 0, BSDetails.Current!CGSTAmt) + IIf(BSDetails.Current!SGSTAmt Is DBNull.Value, 0, BSDetails.Current!SGSTAmt) + IIf(BSDetails.Current!IGSTAmt Is DBNull.Value, 0, BSDetails.Current!IGSTAmt)
                BSDetails.EndEdit()
            End If

            If e.Column.FieldName = "ProductName" Then
                Dim Pname As String
                Pname = DirectCast(BSDetails.Current, DataRowView).Item("ProductName").ToString

                Dim sda As New OleDbDataAdapter("Select UOM, HSNNoOrSACNo, Price, ProductName From Product Where ProductName='" + Pname + "'", ConStr)
                Dim dt As New DataTable()
                sda.Fill(dt)
                If dt.Rows.Count > 0 Then
                    BSDetails.Current!UOM = dt.Rows(0).Item("UOM").ToString
                    BSDetails.Current!HSNACS = dt.Rows(0).Item("HSNNoOrSACNo").ToString
                    BSDetails.Current!Rate = dt.Rows(0).Item("Price").ToString
                    BSDetails.Current!ProductName = dt.Rows(0).Item("ProductName").ToString
                End If
                BSDetails.EndEdit()
            End If
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub InvoiceDetailGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles InvoiceDetailGridView.KeyDown
        If e.KeyCode = Keys.Delete Then
            BSDetails.RemoveCurrent()
            BSDetails.EndEdit()
        End If
    End Sub

    Private Sub InvoiceDetailGridView_LostFocus(sender As Object, e As EventArgs) Handles InvoiceDetailGridView.LostFocus
        Try
            'BS.Current!GrandTotalAmount = Math.Round((IIf(BS.Current!TotalAmtAfterTax Is DBNull.Value, 0, BS.Current!TotalAmtAfterTax))) ' + IIf(BS.Current!IntAmt Is DBNull.Value, 0, BS.Current!IntAmt)))
            BS.Current!GrandTotalAmount = Math.Round((IIf(BS.Current!TotalAmt Is DBNull.Value, 0, BS.Current!TotalAmt))) ' + IIf(BS.Current!IntAmt Is DBNull.Value, 0, BS.Current!IntAmt)))

            BS.EndEdit()
            Calc()
            Dim bz As New CultureInfo("hi-In")
            Dim GTotal As Double = BS.Current!GrandTotalAmount
            NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)

        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Dim ciUSA As CultureInfo = New CultureInfo("hi-IN")

    Private Sub InvoiceDetailGridView_CustomColumnDisplayText(ByVal sender As Object, ByVal e As CustomColumnDisplayTextEventArgs) Handles InvoiceDetailGridView.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "Rate" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim Rate As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", Rate)
            End If
            If e.Column.FieldName = "Amount" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim Amt As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", Amt)
            End If
            If e.Column.FieldName = "TaxableValue" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim TV As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", TV)
            End If
            If e.Column.FieldName = "CGSTAmt" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim CGST As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", CGST)
            End If
            If e.Column.FieldName = "SGSTAmt" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim SGST As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", SGST)
            End If
            If e.Column.FieldName = "IGSTAmt" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim IGST As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", IGST)
            End If
            If e.Column.FieldName = "TotalAmount" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
                Dim TAmt As Decimal = Convert.ToDecimal(e.Value)
                e.DisplayText = String.Format(ciUSA, "{0:c}", TAmt)
            End If
        Catch
        End Try
    End Sub
    Private Sub PCGSTRateTextEdit_LostFocus(sender As Object, e As EventArgs) Handles PCGSTRateTextEdit.LostFocus, PSGSTRateTextEdit.LostFocus, PIGSTRateTextEdit.LostFocus, PackingTextEdit.LostFocus, TransTextEdit.LostFocus
        BS.EndEdit()
        Calc()
    End Sub
    Private Sub Calc()
        Try
            BS.Current!TotalAmtBeforeTax = Math.Round(IIf(InvoiceDetailGridView.Columns("TaxableValue").SummaryItem.SummaryValue Is DBNull.Value, 0, InvoiceDetailGridView.Columns("TaxableValue").SummaryItem.SummaryValue) + IIf(BS.Current!PackingCharge Is DBNull.Value, 0, BS.Current!PackingCharge) + IIf(BS.Current!TransCharge Is DBNull.Value, 0, BS.Current!TransCharge), 2)

            'BS.Current!PCGSTAmt = IIf(BS.Current!PackingCharge Is DBNull.Value, 0, BS.Current!PackingCharge) * IIf(BS.Current!PCGSTRate Is DBNull.Value, 0, BS.Current!PCGSTRate) / 100
            'BS.Current!PSGSTAmt = IIf(BS.Current!PackingCharge Is DBNull.Value, 0, BS.Current!PackingCharge) * IIf(BS.Current!PSGSTRate Is DBNull.Value, 0, BS.Current!PSGSTRate) / 100
            'BS.Current!PIGSTAmt = IIf(BS.Current!PackingCharge Is DBNull.Value, 0, BS.Current!PackingCharge) * IIf(BS.Current!PIGSTRate Is DBNull.Value, 0, BS.Current!PIGSTRate) / 100

            BS.Current!PCGSTAmt = IIf(BS.Current!TotalAmtBeforeTax Is DBNull.Value, 0, BS.Current!TotalAmtBeforeTax) * IIf(BS.Current!PCGSTRate Is DBNull.Value, 0, BS.Current!PCGSTRate) / 100
            BS.Current!PSGSTAmt = IIf(BS.Current!TotalAmtBeforeTax Is DBNull.Value, 0, BS.Current!TotalAmtBeforeTax) * IIf(BS.Current!PSGSTRate Is DBNull.Value, 0, BS.Current!PSGSTRate) / 100
            BS.Current!PIGSTAmt = IIf(BS.Current!TotalAmtBeforeTax Is DBNull.Value, 0, BS.Current!TotalAmtBeforeTax) * IIf(BS.Current!PIGSTRate Is DBNull.Value, 0, BS.Current!PIGSTRate) / 100

            'BS.Current!CGST = Math.Round(InvoiceDetailGridView.Columns("CGSTAmt").SummaryItem.SummaryValue + BS.Current!PCGSTAmt, 2)
            'BS.Current!SGST = Math.Round(InvoiceDetailGridView.Columns("SGSTAmt").SummaryItem.SummaryValue + BS.Current!PSGSTAmt, 2)
            'BS.Current!IGST = Math.Round(InvoiceDetailGridView.Columns("IGSTAmt").SummaryItem.SummaryValue + BS.Current!PIGSTAmt, 2)

            'BS.Current!TotalGSTTax = Math.Round(BS.Current!CGST + BS.Current!SGST + BS.Current!IGST, 2)
            BS.Current!TotalGSTTax = Math.Round(BS.Current!PCGSTAmt + BS.Current!PSGSTAmt + BS.Current!PIGSTAmt, 2)
            BS.Current!TotalAmtAfterTax = Math.Round(BS.Current!TotalAmtBeforeTax + BS.Current!TotalGSTTax, 2)

            If Not TCSRateTextEdit.Text = 0 Then
                BS.Current!TCSAmt = Math.Round(IIf(BS.Current!TotalAmtAfterTax Is DBNull.Value, 0, BS.Current!TotalAmtAfterTax) * IIf(BS.Current!TCSRate Is DBNull.Value, 0, BS.Current!TCSRate) / 100, 2)

                BS.Current!TotalAmt = BS.Current!TotalAmtAfterTax + BS.Current!TCSAmt
                BS.Current!GrandTotalAmount = Math.Round(BS.Current!TotalAmt)

                BS.Current!RoundOff = BS.Current!GrandTotalAmount - BS.Current!TotalAmt
            Else
                BS.Current!TCSAmt = Math.Round(IIf(BS.Current!TotalAmtAfterTax Is DBNull.Value, 0, BS.Current!TotalAmtAfterTax) * IIf(BS.Current!TCSRate Is DBNull.Value, 0, BS.Current!TCSRate) / 100, 2)

                BS.Current!TotalAmt = BS.Current!TotalAmtAfterTax + BS.Current!TCSAmt
                BS.Current!GrandTotalAmount = Math.Round(BS.Current!TotalAmt)

                BS.Current!RoundOff = BS.Current!GrandTotalAmount - BS.Current!TotalAmt
                'BS.Current!GrandTotalAmount = Math.Round(BS.Current!TotalAmtAfterTax)

                'BS.Current!RoundOff = BS.Current!GrandTotalAmount - BS.Current!TotalAmtAfterTax
            End If


            Dim bz As New CultureInfo("hi-IN")
            Dim GTotal As Double = BS.Current!GrandTotalAmount
            Dim TotalTax As Double = BS.Current!TotalGSTTax
            NetAmtLabelControl.Text = "Grand Amt: " + GTotal.ToString("c", bz)

            BS.Current!TotalInWords = AmtInWord(Convert.ToDecimal(GTotal))
            BS.Current!TaxInWords = AmtInWord(Convert.ToDecimal(TotalTax))

            BS.EndEdit()
        Catch

        End Try

    End Sub
    Private Sub PrintBarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PrintOriginalPrintBarButtonItem.ItemClick
        Dim DACompany As New OleDbDataAdapter
        Dim DTCompany As New DataTable
        DACompany.SelectCommand = New OleDbCommand("Select CompanyID,Name,CINNo,Address,Phone1,Phone2,Phone3,Phone4,State,StateCode,EmailID1,EmailID2,Website,GSTIN,PANNo,SupplyFrom From Company Where Name='" + PubCompanyName + "'", ConStr)
        DACompany.Fill(DTCompany)
        Dim InvoiceNo As String = BS.Current!PurchaseID
        If PubIGST = False Then
            If PubDis = False Then
                'PO Local
                Dim Rpt As New XR_VendorPONewWithoutDis
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel46.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel45.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            Else
                Dim Rpt As New XR_VendorPONew
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel45.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            End If
        Else
            If PubDis = False Then
                'PO Interstate
                Dim Rpt As New XR_VendorPONewIGSTWithoutDis
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            Else
                Dim Rpt As New XR_VendorPONewIGST
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            End If

        End If
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Dim DACompany As New OleDbDataAdapter
        Dim DTCompany As New DataTable
        DACompany.SelectCommand = New OleDbCommand("Select CompanyID,Name,Address,Phone1,Phone2,Phone3,Phone4,State,StateCode,EmailID1,EmailID2,Website,GSTIN,PANNo,SupplyFrom From Company Where Name='" + PubCompanyName + "'", ConStr)
        DACompany.Fill(DTCompany)
        Dim InvoiceNo As String = BS.Current!PurchaseID
        If PubIGST = False Then
            If PubDis = False Then
                'PO Local
                Dim Rpt As New XR_VendorPONewWithoutDis
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrPictureBox2.Visible = False
                Rpt.XrLabel90.Visible = False
                Rpt.XrLabel37.Visible = False
                Rpt.XrLabel45.Visible = False
                Rpt.XrLabel89.Visible = False
                Rpt.XrLabel42.Visible = False
                'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel46.Text = DTCompany.Rows(0).Item("Name").ToString
                'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            Else
                Dim Rpt As New XR_VendorPONew
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrPictureBox2.Visible = False
                Rpt.XrLabel90.Visible = False
                Rpt.XrLabel37.Visible = False
                Rpt.XrLabel45.Visible = False
                Rpt.XrLabel89.Visible = False
                Rpt.XrLabel42.Visible = False
                'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            End If
        Else
            If PubDis = False Then
                'PO Interstate
                Dim Rpt As New XR_VendorPONewIGSTWithoutDis
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrPictureBox2.Visible = False
                'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel90.Visible = False
                Rpt.XrLabel125.Visible = False
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel89.Visible = False
                Rpt.XrLabel42.Visible = False
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            Else
                Dim Rpt As New XR_VendorPONewIGST
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                Rpt.XrPictureBox2.Visible = False
                'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel90.Visible = False
                Rpt.XrLabel125.Visible = False
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel89.Visible = False
                Rpt.XrLabel42.Visible = False
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            End If
        End If
    End Sub
    Private Sub PrintDynamicBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PrintDynamicBarButtonItem.ItemClick
        Dim DACompany As New OleDbDataAdapter
        Dim DTCompany As New DataTable
        DACompany.SelectCommand = New OleDbCommand("Select CompanyID,Name,CINNo,Address,Phone1,Phone2,Phone3,Phone4,State,StateCode,EmailID1,EmailID2,Website,GSTIN,PANNo,SupplyFrom From Company Where Name='" + PubCompanyName + "'", ConStr)
        DACompany.Fill(DTCompany)
        Dim InvoiceNo As String = BS.Current!PurchaseID
        If PubIGST = False Then
            If PubDis = False Then
                'PO Local
                Dim Rpt As New XR_VPODynamicWithoutDis
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                'Rpt.XrPictureBox2.Visible = False
                'Rpt.XrLabel90.Visible = False
                'Rpt.XrLabel125.Visible = False
                'Rpt.XrLabel89.Visible = False
                'Rpt.XrLabel4.Visible = False
                'Rpt.XrLabel6.Visible = False
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel1.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            Else
                'PO Local
                Dim Rpt As New XR_VPODynamic
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                'Rpt.XrPictureBox2.Visible = False
                'Rpt.XrLabel90.Visible = False
                'Rpt.XrLabel125.Visible = False
                'Rpt.XrLabel89.Visible = False
                'Rpt.XrLabel4.Visible = False
                'Rpt.XrLabel6.Visible = False
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel2.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            End If
        Else
            If PubDis = False Then
                'PO Interstate
                Dim Rpt As New XR_VPOIGSTDynamicWithoutDis
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                'Rpt.XrPictureBox2.Visible = False
                'Rpt.XrLabel90.Visible = False
                'Rpt.XrLabel125.Visible = False
                'Rpt.XrLabel89.Visible = False
                'Rpt.XrLabel4.Visible = False
                'Rpt.XrLabel6.Visible = False
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel18.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            Else
                'PO Interstate
                Dim Rpt As New XR_VPOIGSTDynamic
                Rpt.Invoice.Value = InvoiceNo
                Rpt.Invoice.Visible = False
                Rpt.FillDataSource()
                'Company Info----
                'Rpt.XrPictureBox2.Visible = False
                'Rpt.XrLabel90.Visible = False
                'Rpt.XrLabel125.Visible = False
                'Rpt.XrLabel89.Visible = False
                'Rpt.XrLabel4.Visible = False
                'Rpt.XrLabel6.Visible = False
                Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                Rpt.XrLabel57.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                Rpt.XrLabel23.Text = DTCompany.Rows(0).Item("Name").ToString
                Rpt.XrLabel22.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                'Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                'Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor PO -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()
            End If
        End If
    End Sub
    Private Sub AddNewProduct_Click(sender As Object, e As EventArgs) Handles AddNewProductSimpleButton.Click
        Frm_AddProduct.StartPosition = FormStartPosition.CenterParent
        Frm_AddProduct.ShowDialog()
        If Frm_AddProduct.DialogResult = DialogResult.OK Then
            GridComboBoxFunc("Select DISTINCT ProductName FROM Product", "Product", InvoiceDetailGridControl, InvoiceDetailGridView, "ProductName", "Product")
            Frm_AddProduct.Close()
        Else
            Frm_AddProduct.Close()
        End If
    End Sub
    Private Sub InvoiceDetailGridView_InitNewRow(sender As Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles InvoiceDetailGridView.InitNewRow
        BSDetails.EndEdit()
        SendKeys.Send("{ENTER}")
    End Sub

    Private Sub ReceiverNameComboBoxEdit_KeyPress(sender As Object, e As KeyPressEventArgs)
        InitLookup()
    End Sub

    Private Sub DateDateEdit_Leave(sender As Object, e As EventArgs) Handles DateDateEdit.Leave
        Dim DAFinance As New OleDbDataAdapter("Select StartDate,EndDate From Company Where name ='" + PubCompanyName + "'", ConStr)
        Dim DTFinance As New DataTable
        DAFinance.Fill(DTFinance)
        Dim iDate As String = DateDateEdit.Text
        Dim oDate As DateTime = Convert.ToDateTime(iDate)

        Dim StartDate As String = DTFinance.Rows(0).Item("StartDate").ToString()
        Dim SubStart As DateTime = Convert.ToDateTime(StartDate.Substring(0, 9))
        Dim EndDate As String = DTFinance.Rows(0).Item("EndDate").ToString()
        Dim SubEnd As DateTime = Convert.ToDateTime(EndDate.Substring(0, 9))

        If iDate >= SubStart And iDate <= SubEnd Then
        Else
            XtraMessageBox.Show("Date is out of Range! Please Enter a Valid Date or check your Financial Period.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'MsgBox("Not between")
            DateDateEdit.Focus()
        End If
    End Sub

    Private Sub OrderNoTextEdit_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OrderNoTextEdit.Validating
        If Not OrderNoTextEdit.EditValue Is DBNull.Value Then
            Try
                Dim CMD As New OleDbCommand("SELECT Count(PurchaseID) FROM VendorPO WHERE OrderNo=@OrderNo AND PurchaseID<>@PurchaseID", ConStr)
                CMD.Parameters.AddWithValue("@OrderNo", OrderNoTextEdit.EditValue)
                CMD.Parameters.AddWithValue("@PurchaseID", IIf(BS.Current!PurchaseID Is DBNull.Value, -1, BS.Current!PurchaseID))
                CnnOpen() : Dim Verify As Integer = Val(CMD.ExecuteScalar & "") : CnnClose()
                If Verify <> 0 Then
                    XtraMessageBox.Show("Value Exist! Enter Unique Value.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    OrderNoTextEdit.Focus()
                    e.Cancel = True
                End If
            Catch ex As Exception
                'ErtOccur(ex)
            End Try
        End If
    End Sub

    Private Sub ConsignorNameComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ConsignorNameComboBoxEdit.SelectedIndexChanged
        Dim sda As New OleDbDataAdapter("Select Address,State,StateCode,GSTIN,PANNo From Party Where PartyName='" + ConsignorNameComboBoxEdit.Text + "'", ConStr)
        Dim dt As New DataTable()
        sda.Fill(dt)
        If (dt.Rows.Count > 0) Then
            ConsignorAddressTextEdit.Text = dt.Rows(0).Item("Address").ToString
            ConsignorStateTextEdit.Text = dt.Rows(0).Item("State").ToString
            ConsignorStateCodeTextEdit.Text = dt.Rows(0).Item("StateCode").ToString
            ConsignorGSTINTextEdit.Text = dt.Rows(0).Item("GSTIN").ToString
            ConsignorPANNoTextEdit.Text = dt.Rows(0).Item("PANNo").ToString
        End If
    End Sub

    Private Sub Remarks1TextEdit_Leave(sender As Object, e As EventArgs) Handles Remarks1TextEdit.Leave
        If Remarks1TextEdit.EditValue IsNot DBNull.Value Then
            If Remarks1TextEdit.Text.Length > 1501 Then
                XtraMessageBox.Show("You can enter upto 1500 characters.", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Remarks1TextEdit.Focus()
            End If
        End If
    End Sub

    Private Sub Remarks2TextEdit_Leave(sender As Object, e As EventArgs) Handles Remarks2TextEdit.Leave
        If Remarks2TextEdit.EditValue IsNot DBNull.Value Then
            If Remarks2TextEdit.Text.Length > 1501 Then
                XtraMessageBox.Show("You can enter upto 1500 characters.", "Info Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Remarks2TextEdit.Focus()
            End If
        End If
    End Sub

    Private Sub ConsignorPANNoTextEdit_Leave(sender As Object, e As EventArgs) Handles ConsignorPANNoTextEdit.Leave
        If ConsignorPANNoTextEdit.EditValue IsNot DBNull.Value Then
            If ConsignorPANNoTextEdit.Text.Length <> 10 Then
                XtraMessageBox.Show("Please Enter Valid 10 Character PAN No", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ConsignorPANNoTextEdit.Focus()
            End If
        End If
    End Sub

    Private Sub AddNewVendorSimpleButton_Click(sender As Object, e As EventArgs) Handles AddNewVendorSimpleButton.Click
        Frm_AddParty.StartPosition = FormStartPosition.CenterParent
        Frm_AddParty.ShowDialog()
        If Frm_AddParty.DialogResult = DialogResult.OK Then
            InitLookup()
            Frm_AddParty.Close()
        Else
            Frm_AddParty.Close()
        End If
    End Sub

    Private Sub JobNoTextEdit_Leave(sender As Object, e As EventArgs) Handles JobNoTextEdit.Leave

    End Sub

    Private Sub TCSRateTextEdit_Leave(sender As Object, e As EventArgs) Handles TCSRateTextEdit.Leave
        BS.EndEdit()
        Calc()
    End Sub

    'Private Sub JobNoTextEdit_EditValueChanged(sender As Object, e As EventArgs) Handles JobNoTextEdit.EditValueChanged

    '    Dim CmpCmd As New OleDbCommand("Update VendorPayment Set JobNo=@JobNo Where PurchaseID=PurchaseID", ConStr)
    '    CmpCmd.Parameters.AddWithValue("@JobNo", BS.Current!JobNoPrefix + JobNoTextEdit.Text)
    '    CnnOpen()
    '    CmpCmd.ExecuteNonQuery()
    '    CnnClose()
    'End Sub
End Class