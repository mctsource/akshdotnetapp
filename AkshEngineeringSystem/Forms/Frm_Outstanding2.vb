﻿Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports System.Globalization
Imports DevExpress.XtraPrinting
'Imports ELCF
Public Class Frm_Outstanding2
    Dim DS As New DataSet
    Dim BS As New BindingSource
    Dim BSDetails As New BindingSource
    Dim DSC As New DataSet
    Dim BSC As New BindingSource
    Dim BSDetailsC As New BindingSource
    Dim DSW As New DataSet
    Dim BSW As New BindingSource
    Dim BSDetailsW As New BindingSource
    Private Sub Frm_Outstanding2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.PanelCtrlMain.Select()
    End Sub
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'InitLookup()

        'Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)

        'If VendorPOValidation() Then
        DS = New DataSet
        BS = New BindingSource
        BSDetails = New BindingSource

        DSC = New DataSet
        BSC = New BindingSource
        BSDetailsC = New BindingSource

        DSW = New DataSet
        BSW = New BindingSource
        BSDetailsW = New BindingSource

        Dim VendorDA As New OleDbDataAdapter
        'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND POStatus ='" + "PO is open" + "'", ConStr)
        'VendorDA.SelectCommand = New OleDbCommand("Select VendorPO.PurchaseID,VendorPO.JobNoPrefix,VendorPO.JobNo,VendorPO.OrderNo,VendorPO.OrderDate,VendorPO.ConsignorName,VendorPO.TotalAmtBeforeTax,VendorPO.AdvancePayment,VendorPO.GrandTotalAmount,VendorPO.POStatus,VendorPO.POType,VendorPayment.RemainAmount From VendorPO Inner Join VendorPayment On VendorPO.PurchaseID = VendorPayment.PurchaseID where Company='" + PubCompanyName + "' AND VendorPO.POStatus ='" + "PO is open" + "' ORDER BY VendorPO.JobNo", ConStr)
        'VendorDA.SelectCommand = New OleDbCommand("SELECT  VendorPO.PurchaseID,VendorPO.JobNoPrefix,VendorPO.JobNo,VendorPO.OrderNo,VendorPO.OrderDate,VendorPO.ConsignorName,VendorPO.TotalAmtBeforeTax,VendorPO.AdvancePayment,VendorPO.GrandTotalAmount,VendorPO.POStatus,VendorPO.BillStatus,VendorPO.POType,VendorPayment.RemainAmount FROM VendorPO Inner Join VendorPayment On VendorPO.PurchaseID = VendorPayment.PurchaseID WHERE Company='" + PubCompanyName + "' AND VendorPO.POStatus = '" + "PO is open" + "' OR VendorPO.BillStatus ='" + "Bill Cycle is open" + "' AND VendorPayment.PaymentID IN (SELECT max(PaymentID) FROM VendorPayment GROUP BY PurchaseID)", ConStr)
        VendorDA.SelectCommand = New OleDbCommand("SELECT  VendorPO.PurchaseID,VendorPO.JobNoPrefix,VendorPO.JobNo,VendorPO.OrderNo,VendorPO.OrderDate,VendorPO.ConsignorName,VendorPO.TotalAmtBeforeTax,VendorPO.AdvancePayment,VendorPO.GrandTotalAmount,VendorPO.POStatus,VendorPO.BillStatus,VendorPO.POType,VendorPayment.TotalPaidAmt,VendorPayment.RemainAmount FROM VendorPO Inner Join VendorPayment On VendorPO.PurchaseID = VendorPayment.PurchaseID WHERE (VendorPO.POStatus = '" + "PO is open" + "' OR VendorPO.BillStatus ='" + "Bill Cycle is open" + "') AND Company='" + PubCompanyName + "' AND VendorPayment.PaymentID IN (SELECT max(PaymentID) FROM VendorPayment GROUP BY PurchaseID)", ConStr)
        VendorDA.Fill(DS, "VendorPO")

        Dim ClientDA As New OleDbDataAdapter
        'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND POStatus ='" + "PO is open" + "'", ConStr)
        'ClientDA.SelectCommand = New OleDbCommand("Select ClientPO.SalesID,ClientPO.JobNoPrefix,ClientPO.JobNo,ClientPO.OrderNo,ClientPO.OrderDate,ClientPO.ReceiverName,ClientPO.TotalAmtBeforeTax,ClientPO.AdvancePayment,ClientPO.GrandTotalAmount,ClientPO.POStatus,ClientPO.POType,ClientPayment.RemainAmount From ClientPO Inner Join ClientPayment On ClientPO.SalesID = ClientPayment.SalesID where Company='" + PubCompanyName + "' AND ClientPO.POStatus ='" + "PO is open" + "' ORDER BY ClientPO.JobNo", ConStr)
        'ClientDA.SelectCommand = New OleDbCommand("SELECT  ClientPO.SalesID,ClientPO.JobNoPrefix,ClientPO.JobNo,ClientPO.OrderNo,ClientPO.OrderDate,ClientPO.ReceiverName,ClientPO.TotalAmtBeforeTax,ClientPO.AdvancePayment,ClientPO.GrandTotalAmount,ClientPO.POStatus,ClientPO.BillStatus,ClientPO.POType,ClientPayment.RemainAmount FROM ClientPO Inner Join ClientPayment On ClientPO.SalesID = ClientPayment.SalesID WHERE Company='" + PubCompanyName + "' AND ClientPO.POStatus ='" + "PO is open" + "' OR ClientPO.BillStatus ='" + "Bill Cycle is open" + "' AND ClientPayment.PaymentID IN (SELECT max(PaymentID) FROM ClientPayment GROUP BY SalesID)", ConStr)
        ClientDA.SelectCommand = New OleDbCommand("SELECT  ClientPO.SalesID,ClientPO.JobNoPrefix,ClientPO.JobNo,ClientPO.OrderNo,ClientPO.OrderDate,ClientPO.ReceiverName,ClientPO.TotalAmtBeforeTax,ClientPO.AdvancePayment,ClientPO.GrandTotalAmount,ClientPO.POStatus,ClientPO.BillStatus,ClientPO.POType,ClientPO.CurrencyType,ClientPayment.TotalPaidAmt,ClientPayment.RemainAmount FROM ClientPO Inner Join ClientPayment On ClientPO.SalesID = ClientPayment.SalesID WHERE (ClientPO.POStatus ='" + "PO is open" + "' OR ClientPO.BillStatus ='" + "Bill Cycle is open" + "') AND Company='" + PubCompanyName + "' AND ClientPayment.PaymentID IN (SELECT max(PaymentID) FROM ClientPayment GROUP BY SalesID)", ConStr)
        ClientDA.Fill(DSC, "ClientPO")

        Dim WorkOrderDA As New OleDbDataAdapter
        'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND POStatus ='" + "PO is open" + "'", ConStr)
        'ClientDA.SelectCommand = New OleDbCommand("Select ClientPO.SalesID,ClientPO.JobNoPrefix,ClientPO.JobNo,ClientPO.OrderNo,ClientPO.OrderDate,ClientPO.ReceiverName,ClientPO.TotalAmtBeforeTax,ClientPO.AdvancePayment,ClientPO.GrandTotalAmount,ClientPO.POStatus,ClientPO.POType,ClientPayment.RemainAmount From ClientPO Inner Join ClientPayment On ClientPO.SalesID = ClientPayment.SalesID where Company='" + PubCompanyName + "' AND ClientPO.POStatus ='" + "PO is open" + "' ORDER BY ClientPO.JobNo", ConStr)
        'WorkOrderDA.SelectCommand = New OleDbCommand("SELECT WorkOrder.WorkOrderID, WorkOrder.JobNoPrefix, WorkOrder.JobNo, WorkOrder.WorkOrderNo, WorkOrder.WorkOrderDate, WorkOrder.ReceiverName, WorkOrder.TotalAmtBeforeTax, WorkOrder.AdvancePayment, WorkOrder.GrandTotalAmount, WorkOrder.WOStatus, WorkOrder.BillStatus,WorkOrder.InvoiceType, WorkOrderPayment.RemainAmount From WorkOrder INNER Join WorkOrderPayment On WorkOrder.WorkOrderID = WorkOrderPayment.WorkOrderID WHERE Company='" + PubCompanyName + "' AND WorkOrder.WOStatus ='" + "WO is open" + "' OR WorkOrder.BillStatus ='" + "Bill Cycle is open" + "' AND WorkOrderPayment.PaymentID IN (SELECT max(PaymentID) FROM WorkOrderPayment GROUP BY WorkOrderID)", ConStr)
        WorkOrderDA.SelectCommand = New OleDbCommand("SELECT WorkOrder.WorkOrderID, WorkOrder.JobNoPrefix, WorkOrder.JobNo, WorkOrder.WorkOrderNo, WorkOrder.WorkOrderDate, WorkOrder.ReceiverName, WorkOrder.TotalAmtBeforeTax, WorkOrder.AdvancePayment, WorkOrder.GrandTotalAmount, WorkOrder.WOStatus, WorkOrder.BillStatus,WorkOrder.InvoiceType,WorkOrderPayment.TotalPaidAmt,WorkOrderPayment.RemainAmount From WorkOrder INNER Join WorkOrderPayment On WorkOrder.WorkOrderID = WorkOrderPayment.WorkOrderID WHERE (WorkOrder.WOStatus ='" + "WO is open" + "' OR WorkOrder.BillStatus ='" + "Bill Cycle is open" + "') AND Company='" + PubCompanyName + "' AND WorkOrderPayment.PaymentID IN (SELECT max(PaymentID) FROM WorkOrderPayment GROUP BY WorkOrderID)", ConStr)
        WorkOrderDA.Fill(DSW, "WorkOrder")

        'Dim InvoiceDA As New OleDbDataAdapter
        'Dim InvoiceDT As New DataTable
        'InvoiceDA.SelectCommand = New OleDbCommand("Select BillID,BillNo,JobNoPrefix,JobNo,PONo,GrandTotalAmount From BillNew where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        'InvoiceDA.Fill(InvoiceDT)
        'BillGridControl.DataSource = InvoiceDT


        Dim PaymentDA As New OleDbDataAdapter
        PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From VendorPayment", ConStr)
        PaymentDA.Fill(DS, "VendorPayment")

        Dim PaymentDAC As New OleDbDataAdapter
        PaymentDAC.SelectCommand = New OleDbCommand("Select PaymentID,SalesID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From ClientPayment", ConStr)
        PaymentDAC.Fill(DSC, "ClientPayment")

        Dim PaymentDAW As New OleDbDataAdapter
        PaymentDAW.SelectCommand = New OleDbCommand("Select PaymentID,WorkOrderID,JobNo,WONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From WorkOrderPayment", ConStr)
        PaymentDAW.Fill(DSW, "WorkOrderPayment")

        SetRelation1()
        SetRelation2()
        SetRelation3()

        OrderGridControl.DataSource = BS
        BillGridControl.DataSource = BSC
        WorkOrderGridControl.DataSource = BSW

        setGrid()
        'GridLookUpFunc("Select PartyID,PartyName FROM Party", "Party", "PartyID", "PartyName", OrderGridControl, OrderGridView, "PartyID", "Party Name")
        'End If
    End Sub

    'Sub InitLookup()
    '    SetLookUp("SELECT PartyID, PartyName FROM Party Where Company='" + PubCompanyName + "'", "Party", "PartyID", "PartyName", PartyIDLookUpEdit, "Vendor Name")
    'End Sub

    'Function VendorPOValidation() As Boolean
    '    If PartyIDLookUpEdit.Edit9Value Is DBNull.Value Then
    '        PartyIDLookUpEdit.Focus()
    '        Return False
    '    Else
    '        Return True
    '    End If
    'End Function

    Private Sub VendorPOButton_Click(sender As Object, e As EventArgs)
        'If VendorPOValidation() Then
        '    DS = New DataSet
        '    BS = New BindingSource
        '    BSDetails = New BindingSource

        '    DSC = New DataSet
        '    BSC = New BindingSource
        '    BSDetailsC = New BindingSource

        '    Dim VendorDA As New OleDbDataAdapter
        '    'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        '    'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND POStatus ='" + "PO is open" + "'", ConStr)
        '    VendorDA.SelectCommand = New OleDbCommand("Select VendorPO.PurchaseID,VendorPO.JobNoPrefix,VendorPO.JobNo,VendorPO.OrderNo,VendorPO.OrderDate,VendorPO.ConsignorName,VendorPO.TotalAmtBeforeTax,VendorPO.AdvancePayment,VendorPO.GrandTotalAmount,VendorPO.POStatus,VendorPO.POType,VendorPayment.RemainAmount From VendorPO Inner Join VendorPayment On VendorPO.PurchaseID = VendorPayment.PurchaseID where Company='" + PubCompanyName + "' AND VendorPO.POStatus ='" + "PO is open" + "' ORDER BY VendorPO.JobNo", ConStr)
        '    VendorDA.Fill(DS, "VendorPO")

        '    Dim ClientDA As New OleDbDataAdapter
        '    'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        '    'VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus From VendorPO where Company='" + PubCompanyName + "' AND POStatus ='" + "PO is open" + "'", ConStr)
        '    ClientDA.SelectCommand = New OleDbCommand("Select ClientPO.SalesID,ClientPO.JobNoPrefix,ClientPO.JobNo,ClientPO.OrderNo,ClientPO.OrderDate,ClientPO.ReceiverName,ClientPO.TotalAmtBeforeTax,ClientPO.AdvancePayment,ClientPO.GrandTotalAmount,ClientPO.POStatus,ClientPO.POType,ClientPayment.RemainAmount From ClientPO Inner Join ClientPayment On ClientPO.SalesID = ClientPayment.SalesID where Company='" + PubCompanyName + "' AND ClientPO.POStatus ='" + "PO is open" + "' ORDER BY ClientPO.JobNo", ConStr)
        '    ClientDA.Fill(DSC, "ClientPO")

        '    'Dim InvoiceDA As New OleDbDataAdapter
        '    'Dim InvoiceDT As New DataTable
        '    'InvoiceDA.SelectCommand = New OleDbCommand("Select BillID,BillNo,JobNoPrefix,JobNo,PONo,GrandTotalAmount From BillNew where Company='" + PubCompanyName + "' AND ConsignorName='" + PartyIDLookUpEdit.Text + "'", ConStr)
        '    'InvoiceDA.Fill(InvoiceDT)
        '    'BillGridControl.DataSource = InvoiceDT


        '    Dim PaymentDA As New OleDbDataAdapter
        '    PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From VendorPayment", ConStr)
        '    PaymentDA.Fill(DS, "VendorPayment")

        '    Dim PaymentDAC As New OleDbDataAdapter
        '    PaymentDAC.SelectCommand = New OleDbCommand("Select PaymentID,SalesID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDS,RemainAmount From ClientPayment", ConStr)
        '    PaymentDAC.Fill(DSC, "ClientPayment")

        '    SetRelation1()
        '    SetRelation2()

        '    OrderGridControl.DataSource = BS
        '    BillGridControl.DataSource = BSC

        '    setGrid()

        '    'GridLookUpFunc("Select PartyID,PartyName FROM Party", "Party", "PartyID", "PartyName", OrderGridControl, OrderGridView, "PartyID", "Party Name")
        'End If
    End Sub

    Sub SetRelation1()
        DS.Relations.Add(New DataRelation("Payments", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPayment").Columns("PurchaseID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DS.Tables("VendorPO").Columns("PurchaseID"), DS.Tables("VendorPayment").Columns("PurchaseID"))
        Try
            DS.Tables("VendorPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        BS.DataSource = DS
        BS.DataMember = "VendorPO"

        BSDetails.DataSource = BS
        BSDetails.DataMember = "Payments"
    End Sub
    Sub SetRelation2()
        DSC.Relations.Add(New DataRelation("Payments", DSC.Tables("ClientPO").Columns("SalesID"), DSC.Tables("ClientPayment").Columns("SalesID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DSC.Tables("ClientPO").Columns("SalesID"), DSC.Tables("ClientPayment").Columns("SalesID"))
        Try
            DSC.Tables("ClientPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        BSC.DataSource = DSC
        BSC.DataMember = "ClientPO"

        BSDetailsC.DataSource = BSC
        BSDetailsC.DataMember = "Payments"
    End Sub
    Sub SetRelation3()
        DSW.Relations.Add(New DataRelation("Payments", DSW.Tables("WorkOrder").Columns("WorkOrderID"), DSW.Tables("WorkOrderPayment").Columns("WorkOrderID"), False))
        Dim FK_Payment As New Global.System.Data.ForeignKeyConstraint("FK_Order", DSW.Tables("WorkOrder").Columns("WorkOrderID"), DSW.Tables("WorkOrderPayment").Columns("WorkOrderID"))
        Try
            DSW.Tables("WorkOrderPayment").Constraints.Add(FK_Payment)
        Catch
        End Try

        With FK_Payment
            .AcceptRejectRule = AcceptRejectRule.None
            .DeleteRule = Rule.Cascade
            .UpdateRule = Rule.Cascade
        End With

        BSW.DataSource = DSW
        BSW.DataMember = "WorkOrder"

        BSDetailsW.DataSource = BSW
        BSDetailsW.DataMember = "Payments"
    End Sub

    Sub setGrid()
        'With BillGridView
        '    .Columns("BillID").Visible = False
        '    .Columns("JobNoPrefix").Visible = False
        '    .Columns("BillNo").OptionsColumn.AllowFocus = False
        '    .Columns("BillNo").OptionsColumn.ReadOnly = True
        '    .Columns("JobNo").OptionsColumn.AllowFocus = False
        '    .Columns("JobNo").OptionsColumn.ReadOnly = True
        '    .Columns("PONo").OptionsColumn.AllowFocus = False
        '    .Columns("PONo").OptionsColumn.ReadOnly = True
        '    .Columns("GrandTotalAmount").Caption = "Bill Amount"
        '    .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
        '    .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True

        '    .OptionsView.ShowFooter = True
        '    .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'End With
        With BillGridView
            .Columns("SalesID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("OrderNo").Caption = "PO No."
            .Columns("OrderNo").OptionsColumn.AllowFocus = False
            .Columns("OrderNo").OptionsColumn.ReadOnly = True
            .Columns("OrderDate").OptionsColumn.AllowFocus = False
            .Columns("OrderDate").OptionsColumn.ReadOnly = True
            .Columns("ReceiverName").OptionsColumn.AllowFocus = False
            .Columns("ReceiverName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Basic Amt"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").Caption = "Advance"
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "PO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("POStatus").OptionsColumn.AllowFocus = False
            .Columns("POStatus").OptionsColumn.ReadOnly = True
            .Columns("BillStatus").OptionsColumn.AllowFocus = False
            .Columns("BillStatus").OptionsColumn.ReadOnly = True
            .Columns("POType").OptionsColumn.AllowFocus = False
            .Columns("POType").OptionsColumn.ReadOnly = True
            .Columns("CurrencyType").Visible = False


            .OptionsView.ShowFooter = True
            '.Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("RemainAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        'BillGridView.BestFitColumns()

        With OrderGridView
            .Columns("PurchaseID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("OrderNo").Caption = "PO No."
            .Columns("OrderNo").OptionsColumn.AllowFocus = False
            .Columns("OrderNo").OptionsColumn.ReadOnly = True
            .Columns("OrderDate").OptionsColumn.AllowFocus = False
            .Columns("OrderDate").OptionsColumn.ReadOnly = True
            .Columns("ConsignorName").OptionsColumn.AllowFocus = False
            .Columns("ConsignorName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Basic Amt"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").Caption = "Advance"
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "PO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("POStatus").OptionsColumn.AllowFocus = False
            .Columns("POStatus").OptionsColumn.ReadOnly = True
            .Columns("BillStatus").OptionsColumn.AllowFocus = False
            .Columns("BillStatus").OptionsColumn.ReadOnly = True
            .Columns("POType").OptionsColumn.AllowFocus = False
            .Columns("POType").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            '.Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("RemainAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        'OrderGridView.BestFitColumns()
        With WorkOrderGridView
            .Columns("WorkOrderID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("WorkOrderNo").Caption = "WO No."
            .Columns("WorkOrderNo").OptionsColumn.AllowFocus = False
            .Columns("WorkOrderNo").OptionsColumn.ReadOnly = True
            .Columns("WorkOrderDate").OptionsColumn.AllowFocus = False
            .Columns("WorkOrderDate").OptionsColumn.ReadOnly = True
            .Columns("ReceiverName").OptionsColumn.AllowFocus = False
            .Columns("ReceiverName").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Basic Amt"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("AdvancePayment").Caption = "Advance"
            .Columns("AdvancePayment").OptionsColumn.AllowFocus = False
            .Columns("AdvancePayment").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "WO Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .Columns("WOStatus").OptionsColumn.AllowFocus = False
            .Columns("WOStatus").OptionsColumn.ReadOnly = True
            .Columns("BillStatus").OptionsColumn.AllowFocus = False
            .Columns("BillStatus").OptionsColumn.ReadOnly = True
            .Columns("InvoiceType").OptionsColumn.AllowFocus = False
            .Columns("InvoiceType").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            '.Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("RemainAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        'BillGridView.BestFitColumns()
    End Sub
    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
        Frm_Main.WindowState = FormWindowState.Maximized
        Frm_Main.Show()
    End Sub

    Private Sub OrderGridView_MasterRowExpanded(sender As Object, e As DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs) Handles OrderGridView.MasterRowExpanded
        Dim gridViewTests As GridView = CType(sender, GridView)
        Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
        gridViewDefects.BeginUpdate()

        With gridViewDefects
            .Columns("PaymentID").Visible = False
            .Columns("PurchaseID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("TDS").OptionsColumn.AllowFocus = False
            .Columns("TDS").OptionsColumn.ReadOnly = True
            .Columns("CheckNo").Caption = "Cheque No."
            .Columns("CheckNo").OptionsColumn.AllowFocus = False
            .Columns("CheckNo").OptionsColumn.ReadOnly = True
            .Columns("ChkDate").OptionsColumn.AllowFocus = False
            .Columns("ChkDate").OptionsColumn.ReadOnly = True
            .Columns("PaidAmount").OptionsColumn.AllowFocus = False
            .Columns("PaidAmount").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            '.Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        'gridViewDefects.BestFitColumns()
        gridViewDefects.EndUpdate()
    End Sub
    Private Sub BillGridView_MasterRowExpanded(sender As Object, e As CustomMasterRowEventArgs) Handles BillGridView.MasterRowExpanded
        Dim gridViewTests As GridView = CType(sender, GridView)
        Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
        gridViewDefects.BeginUpdate()

        With gridViewDefects
            .Columns("PaymentID").Visible = False
            .Columns("SalesID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("TDS").OptionsColumn.AllowFocus = False
            .Columns("TDS").OptionsColumn.ReadOnly = True
            .Columns("CheckNo").Caption = "Cheque No."
            .Columns("CheckNo").OptionsColumn.AllowFocus = False
            .Columns("CheckNo").OptionsColumn.ReadOnly = True
            .Columns("ChkDate").OptionsColumn.AllowFocus = False
            .Columns("ChkDate").OptionsColumn.ReadOnly = True
            .Columns("PaidAmount").OptionsColumn.AllowFocus = False
            .Columns("PaidAmount").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            '.Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        'gridViewDefects.BestFitColumns()
        gridViewDefects.EndUpdate()
    End Sub
    Private Sub WorkOrderGridView_MasterRowExpanded(sender As Object, e As CustomMasterRowEventArgs) Handles WorkOrderGridView.MasterRowExpanded
        Dim gridViewTests As GridView = CType(sender, GridView)
        Dim gridViewDefects As GridView = CType(gridViewTests.GetDetailView(e.RowHandle, e.RelationIndex), GridView)
        gridViewDefects.BeginUpdate()

        With gridViewDefects
            .Columns("PaymentID").Visible = False
            .Columns("WorkOrderID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("WONo").OptionsColumn.AllowFocus = False
            .Columns("WONo").OptionsColumn.ReadOnly = True
            .Columns("TDS").OptionsColumn.AllowFocus = False
            .Columns("TDS").OptionsColumn.ReadOnly = True
            .Columns("CheckNo").Caption = "Cheque No."
            .Columns("CheckNo").OptionsColumn.AllowFocus = False
            .Columns("CheckNo").OptionsColumn.ReadOnly = True
            .Columns("ChkDate").OptionsColumn.AllowFocus = False
            .Columns("ChkDate").OptionsColumn.ReadOnly = True
            .Columns("PaidAmount").OptionsColumn.AllowFocus = False
            .Columns("PaidAmount").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            '.Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        'gridViewDefects.BestFitColumns()
        gridViewDefects.EndUpdate()
    End Sub
    Private Sub PanelCtrlMain_MouseWheel(sender As Object, e As MouseEventArgs) Handles PanelCtrlMain.MouseWheel
        Dim myView As Point = Me.PanelCtrlMain.AutoScrollPosition
        myView.X = (myView.X + 50)
        myView.Y = (myView.Y + 50)
        Me.PanelCtrlMain.AutoScrollPosition = myView
    End Sub

    Private Sub ExportVOSSimpleButton_Click(sender As Object, e As EventArgs) Handles ExportVOSSimpleButton.Click
        'OrderGridControl.ExportToXlsx("Vendor_Outstanding.xlsx")
        'Process.Start("Vendor_Outstanding.xlsx")
        Dim value As String
        Dim filename As String
        filename = "Vendor_Outstanding_" + DateTime.Today.ToShortDateString + ".xlsx"
        value = "E:\Aksh Software\AkshEngineeringSystem\AkshEngineeringSystem\" + filename
        OrderGridControl.ExportToXlsx(value)

        'OrderGridControl.ExportToPdf(value)
        Process.Start(value)
    End Sub

    Private Sub ExportCOSSimpleButton_Click(sender As Object, e As EventArgs) Handles ExportCOSSimpleButton.Click
        'BillGridControl.ExportToXlsx("Client_Outstanding.xlsx")
        'Process.Start("Client_Outstanding.xlsx")
        Dim value As String
        Dim filename As String
        filename = "Client_Outstanding_" + DateTime.Today.ToShortDateString + ".xlsx"
        value = "E:\Aksh Software\AkshEngineeringSystem\AkshEngineeringSystem\" + filename
        BillGridControl.ExportToXlsx(value)
        BillGridView.OptionsBehavior.ReadOnly = True
        'BillGridControl.ExportToPdf(value)
        Process.Start(value)
    End Sub

    Private Sub ExportWOSSimpleButton_Click(sender As Object, e As EventArgs) Handles ExportWOSSimpleButton.Click
        'WorkOrderGridControl.ExportToXlsx("WorkOrder_Outstanding.xlsx")
        'Process.Start("WorkOrder_Outstanding.xlsx")
        Dim value As String
        Dim filename As String
        filename = "WorkOrder_Outstanding_" + DateTime.Today.ToShortDateString + ".xlsx"
        value = "E:\Aksh Software\AkshEngineeringSystem\AkshEngineeringSystem\" + filename
        WorkOrderGridControl.ExportToXlsx(value)
        'WorkOrderGridControl.ExportToPdf(value)
        Process.Start(value)
    End Sub
    '
    Dim ciUSA As CultureInfo = New CultureInfo("en-US")
    Dim ciEUR As CultureInfo = New CultureInfo("it-IT", False)
    Dim ciPOUND As CultureInfo = New CultureInfo("en-GB")
    Dim ciINR As CultureInfo = New CultureInfo("hi-IN")

    Private Sub BillGridView_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles BillGridView.CustomColumnDisplayText
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If e.Column.FieldName = "RemainAmount" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            Dim currencyType As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType").ToString
            If Not e.Value Is DBNull.Value Then
                Dim Amt As Decimal = Convert.ToDecimal(e.Value)

                'Dim cSymbol As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex - 1, "CurrencyType").ToString
                Select Case currencyType
                    Case "en-US"
                        e.DisplayText = String.Format(ciUSA, "{0:c}", Amt)
                        Exit Select
                    Case "it-IT"
                        e.DisplayText = String.Format(ciEUR, "{0:c}", Amt)
                        Exit Select
                    Case "en-GB"
                        e.DisplayText = String.Format(ciPOUND, "{0:c}", Amt)
                        Exit Select
                    Case "hi-IN"
                        e.DisplayText = String.Format(ciINR, "{0:c}", Amt)
                        Exit Select
                End Select
            End If
        End If
    End Sub

    Private Sub OrderGridView_CustomColumnDisplayText(sender As Object, e As CustomColumnDisplayTextEventArgs) Handles OrderGridView.CustomColumnDisplayText
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If e.Column.FieldName = "RemainAmount" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
            Dim Amt As Decimal = Convert.ToDecimal(e.Value)
            e.DisplayText = String.Format(ciINR, "{0:c}", Amt)
        End If
    End Sub

    Private Sub WorkOrderGridView_CustomColumnDisplayText(sender As Object, e As CustomColumnDisplayTextEventArgs) Handles WorkOrderGridView.CustomColumnDisplayText
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If e.Column.FieldName = "RemainAmount" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
            Dim Amt As Decimal = Convert.ToDecimal(e.Value)
            e.DisplayText = String.Format(ciINR, "{0:c}", Amt)
        End If
    End Sub
End Class