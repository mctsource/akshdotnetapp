﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_VendorPO

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_VendorPO))
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.TotalAmtTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalTCSTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.TotalTCSLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.TCSRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AddTCSLabel = New DevExpress.XtraEditors.LabelControl()
        Me.InvoiceDetailGridControl = New DevExpress.XtraGrid.GridControl()
        Me.InvoiceDetailGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintOriginalPrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintDynamicBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintDuplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintTriplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.TransTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.Remarks1TextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.Remarks2TextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.PaymentTermsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AdvancePaymentTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.OrderNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.JobPrefixTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.JobNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TransporterTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.PlaceOfOrderTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.DestinationTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.ModeOfDeliveryTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.QuotationRefTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.DateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.AddNewVendorSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ConsignorPANNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsignorStateCodeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsignorNameComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ConsignorStateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsignorGSTINTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsignorAddressTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.TaxInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ModifiedByTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.CreatedByTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.CreatedByLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.ModifiedByLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PIGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.PSGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.PCGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.PIGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.PSGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.PCGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.PackingTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.AddNewProductSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.TotalAmtAfterTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.GSTReverseChargeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalGSTTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.NetAmtLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.TotalAmtBeforeTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.CancelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.SaveNewSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.TotalAmtTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalTCSTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCSRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDetailGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDetailGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Remarks1TextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Remarks2TextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PaymentTermsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvancePaymentTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransporterTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PlaceOfOrderTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DestinationTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModeOfDeliveryTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QuotationRefTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsignorPANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsignorStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsignorNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsignorStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsignorGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsignorAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TaxInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModifiedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CreatedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PackingTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtAfterTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GSTReverseChargeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalGSTTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtBeforeTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.SimpleButton1)
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Controls.Add(Me.CancelSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.SaveNewSimpleButton)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(12, 37)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(1358, 588)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'SimpleButton1
        '
        Me.SimpleButton1.ImageOptions.Image = CType(resources.GetObject("SimpleButton1.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(140, 550)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 30)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "&Save"
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.TotalAmtTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl11)
        Me.PanelCtrl.Controls.Add(Me.TotalTCSTextEdit)
        Me.PanelCtrl.Controls.Add(Me.TotalTCSLabelControl)
        Me.PanelCtrl.Controls.Add(Me.TCSRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.AddTCSLabel)
        Me.PanelCtrl.Controls.Add(Me.InvoiceDetailGridControl)
        Me.PanelCtrl.Controls.Add(Me.TransTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl10)
        Me.PanelCtrl.Controls.Add(Me.LabelControl36)
        Me.PanelCtrl.Controls.Add(Me.Remarks1TextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl6)
        Me.PanelCtrl.Controls.Add(Me.LabelControl26)
        Me.PanelCtrl.Controls.Add(Me.Remarks2TextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl27)
        Me.PanelCtrl.Controls.Add(Me.PaymentTermsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.AdvancePaymentTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl25)
        Me.PanelCtrl.Controls.Add(Me.LabelControl20)
        Me.PanelCtrl.Controls.Add(Me.OrderNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.JobPrefixTextEdit)
        Me.PanelCtrl.Controls.Add(Me.JobNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Controls.Add(Me.TransporterTextEdit)
        Me.PanelCtrl.Controls.Add(Me.PlaceOfOrderTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Controls.Add(Me.LabelControl9)
        Me.PanelCtrl.Controls.Add(Me.DestinationTextEdit)
        Me.PanelCtrl.Controls.Add(Me.ModeOfDeliveryTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl3)
        Me.PanelCtrl.Controls.Add(Me.LabelControl5)
        Me.PanelCtrl.Controls.Add(Me.QuotationRefTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl16)
        Me.PanelCtrl.Controls.Add(Me.DateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl17)
        Me.PanelCtrl.Controls.Add(Me.AddNewVendorSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ConsignorPANNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl48)
        Me.PanelCtrl.Controls.Add(Me.LabelControl38)
        Me.PanelCtrl.Controls.Add(Me.ConsignorStateCodeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl19)
        Me.PanelCtrl.Controls.Add(Me.ConsignorNameComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.ConsignorStateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl24)
        Me.PanelCtrl.Controls.Add(Me.ConsignorGSTINTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl23)
        Me.PanelCtrl.Controls.Add(Me.LabelControl22)
        Me.PanelCtrl.Controls.Add(Me.LabelControl21)
        Me.PanelCtrl.Controls.Add(Me.LabelControl15)
        Me.PanelCtrl.Controls.Add(Me.ConsignorAddressTextEdit)
        Me.PanelCtrl.Controls.Add(Me.TaxInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.ModifiedByTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CreatedByTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CreatedByLabelControl)
        Me.PanelCtrl.Controls.Add(Me.ModifiedByLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PIGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl47)
        Me.PanelCtrl.Controls.Add(Me.PSGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl46)
        Me.PanelCtrl.Controls.Add(Me.PCGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl45)
        Me.PanelCtrl.Controls.Add(Me.PIGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl44)
        Me.PanelCtrl.Controls.Add(Me.PSGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl43)
        Me.PanelCtrl.Controls.Add(Me.PCGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl42)
        Me.PanelCtrl.Controls.Add(Me.PackingTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl41)
        Me.PanelCtrl.Controls.Add(Me.AddNewProductSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtAfterTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl18)
        Me.PanelCtrl.Controls.Add(Me.GSTReverseChargeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl14)
        Me.PanelCtrl.Controls.Add(Me.TotalGSTTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl13)
        Me.PanelCtrl.Controls.Add(Me.TotalInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl4)
        Me.PanelCtrl.Controls.Add(Me.NetAmtLabelControl)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtBeforeTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl7)
        Me.PanelCtrl.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(1348, 539)
        Me.PanelCtrl.TabIndex = 0
        '
        'TotalAmtTextEdit
        '
        Me.TotalAmtTextEdit.Location = New System.Drawing.Point(810, 431)
        Me.TotalAmtTextEdit.Name = "TotalAmtTextEdit"
        Me.TotalAmtTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtTextEdit.Properties.ReadOnly = True
        Me.TotalAmtTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalAmtTextEdit.TabIndex = 248
        Me.TotalAmtTextEdit.TabStop = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(648, 434)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl11.TabIndex = 249
        Me.LabelControl11.Text = "Total Amount"
        '
        'TotalTCSTextEdit
        '
        Me.TotalTCSTextEdit.Location = New System.Drawing.Point(810, 407)
        Me.TotalTCSTextEdit.Name = "TotalTCSTextEdit"
        Me.TotalTCSTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalTCSTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalTCSTextEdit.Properties.ReadOnly = True
        Me.TotalTCSTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalTCSTextEdit.TabIndex = 246
        Me.TotalTCSTextEdit.TabStop = False
        '
        'TotalTCSLabelControl
        '
        Me.TotalTCSLabelControl.Location = New System.Drawing.Point(648, 410)
        Me.TotalTCSLabelControl.Name = "TotalTCSLabelControl"
        Me.TotalTCSLabelControl.Size = New System.Drawing.Size(46, 13)
        Me.TotalTCSLabelControl.TabIndex = 247
        Me.TotalTCSLabelControl.Text = "Total TCS"
        '
        'TCSRateTextEdit
        '
        Me.TCSRateTextEdit.Location = New System.Drawing.Point(598, 407)
        Me.TCSRateTextEdit.Name = "TCSRateTextEdit"
        Me.TCSRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.TCSRateTextEdit.TabIndex = 244
        '
        'AddTCSLabel
        '
        Me.AddTCSLabel.Location = New System.Drawing.Point(535, 410)
        Me.AddTCSLabel.Name = "AddTCSLabel"
        Me.AddTCSLabel.Size = New System.Drawing.Size(55, 13)
        Me.AddTCSLabel.TabIndex = 245
        Me.AddTCSLabel.Text = "Add TCS %"
        '
        'InvoiceDetailGridControl
        '
        Me.InvoiceDetailGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.InvoiceDetailGridControl.Location = New System.Drawing.Point(7, 142)
        Me.InvoiceDetailGridControl.MainView = Me.InvoiceDetailGridView
        Me.InvoiceDetailGridControl.MenuManager = Me.BarManager1
        Me.InvoiceDetailGridControl.Name = "InvoiceDetailGridControl"
        Me.InvoiceDetailGridControl.Size = New System.Drawing.Size(1334, 182)
        Me.InvoiceDetailGridControl.TabIndex = 13
        Me.InvoiceDetailGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.InvoiceDetailGridView})
        '
        'InvoiceDetailGridView
        '
        Me.InvoiceDetailGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.InvoiceDetailGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.InvoiceDetailGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.InvoiceDetailGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.InvoiceDetailGridView.GridControl = Me.InvoiceDetailGridControl
        Me.InvoiceDetailGridView.Name = "InvoiceDetailGridView"
        Me.InvoiceDetailGridView.OptionsCustomization.AllowColumnMoving = False
        Me.InvoiceDetailGridView.OptionsCustomization.AllowFilter = False
        Me.InvoiceDetailGridView.OptionsCustomization.AllowSort = False
        Me.InvoiceDetailGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.InvoiceDetailGridView.OptionsView.ColumnAutoWidth = False
        Me.InvoiceDetailGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.InvoiceDetailGridView.OptionsView.EnableAppearanceOddRow = True
        Me.InvoiceDetailGridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.InvoiceDetailGridView.OptionsView.RowAutoHeight = True
        Me.InvoiceDetailGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.InvoiceDetailGridView.OptionsView.ShowGroupPanel = False
        Me.InvoiceDetailGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.InvoiceDetailGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem, Me.PrintOriginalPrintBarButtonItem, Me.PrintDuplicateBarButtonItem, Me.PrintTriplicateBarButtonItem, Me.BarButtonItem1, Me.PrintDynamicBarButtonItem})
        Me.BarManager1.MaxItemId = 9
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintOriginalPrintBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintDynamicBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.Caption = "&New"
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.ImageOptions.Image = CType(resources.GetObject("NewBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.NewBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("NewBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.Caption = "&Open"
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.ImageOptions.Image = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.OpenBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.ImageOptions.Image = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.DeleteBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'PrintOriginalPrintBarButtonItem
        '
        Me.PrintOriginalPrintBarButtonItem.Caption = "Print"
        Me.PrintOriginalPrintBarButtonItem.Id = 4
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintOriginalPrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintOriginalPrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintOriginalPrintBarButtonItem.Name = "PrintOriginalPrintBarButtonItem"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Print On LH"
        Me.BarButtonItem1.Id = 7
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'PrintDynamicBarButtonItem
        '
        Me.PrintDynamicBarButtonItem.Caption = "Print on Multiple Page "
        Me.PrintDynamicBarButtonItem.Id = 8
        Me.PrintDynamicBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintDynamicBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintDynamicBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintDynamicBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintDynamicBarButtonItem.Name = "PrintDynamicBarButtonItem"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1376, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 644)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1376, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 613)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1376, 31)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 613)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.Caption = "Print Bill"
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'PrintDuplicateBarButtonItem
        '
        Me.PrintDuplicateBarButtonItem.Caption = "Copy Print"
        Me.PrintDuplicateBarButtonItem.Id = 5
        Me.PrintDuplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintDuplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintDuplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintDuplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintDuplicateBarButtonItem.Name = "PrintDuplicateBarButtonItem"
        '
        'PrintTriplicateBarButtonItem
        '
        Me.PrintTriplicateBarButtonItem.Caption = "Print Triplicate"
        Me.PrintTriplicateBarButtonItem.Id = 6
        Me.PrintTriplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintTriplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintTriplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintTriplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintTriplicateBarButtonItem.Name = "PrintTriplicateBarButtonItem"
        '
        'TransTextEdit
        '
        Me.TransTextEdit.Location = New System.Drawing.Point(133, 358)
        Me.TransTextEdit.Name = "TransTextEdit"
        Me.TransTextEdit.Size = New System.Drawing.Size(185, 20)
        Me.TransTextEdit.TabIndex = 15
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(5, 361)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl10.TabIndex = 210
        Me.LabelControl10.Text = "Transportation Charges"
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl36.Appearance.Options.UseForeColor = True
        Me.LabelControl36.Location = New System.Drawing.Point(40, 9)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl36.TabIndex = 208
        Me.LabelControl36.Text = "*"
        '
        'Remarks1TextEdit
        '
        Me.Remarks1TextEdit.Location = New System.Drawing.Point(98, 455)
        Me.Remarks1TextEdit.Name = "Remarks1TextEdit"
        Me.Remarks1TextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Remarks1TextEdit.Size = New System.Drawing.Size(590, 20)
        Me.Remarks1TextEdit.TabIndex = 21
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(6, 458)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl6.TabIndex = 202
        Me.LabelControl6.Text = "Remarks 1"
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(692, 458)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl26.TabIndex = 206
        Me.LabelControl26.Text = "Remarks 2"
        '
        'Remarks2TextEdit
        '
        Me.Remarks2TextEdit.Location = New System.Drawing.Point(748, 455)
        Me.Remarks2TextEdit.Name = "Remarks2TextEdit"
        Me.Remarks2TextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Remarks2TextEdit.Size = New System.Drawing.Size(590, 20)
        Me.Remarks2TextEdit.TabIndex = 22
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(6, 484)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl27.TabIndex = 207
        Me.LabelControl27.Text = "Payment Terms"
        '
        'PaymentTermsTextEdit
        '
        Me.PaymentTermsTextEdit.Location = New System.Drawing.Point(98, 481)
        Me.PaymentTermsTextEdit.Name = "PaymentTermsTextEdit"
        Me.PaymentTermsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PaymentTermsTextEdit.Size = New System.Drawing.Size(1241, 20)
        Me.PaymentTermsTextEdit.TabIndex = 23
        '
        'AdvancePaymentTextEdit
        '
        Me.AdvancePaymentTextEdit.Location = New System.Drawing.Point(433, 383)
        Me.AdvancePaymentTextEdit.Name = "AdvancePaymentTextEdit"
        Me.AdvancePaymentTextEdit.Size = New System.Drawing.Size(206, 20)
        Me.AdvancePaymentTextEdit.TabIndex = 19
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(325, 386)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl25.TabIndex = 201
        Me.LabelControl25.Text = "Advance Payment"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 20.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(1172, 43)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(144, 33)
        Me.LabelControl20.TabIndex = 199
        Me.LabelControl20.Text = "Vendor PO"
        '
        'OrderNoTextEdit
        '
        Me.OrderNoTextEdit.Location = New System.Drawing.Point(263, 5)
        Me.OrderNoTextEdit.Name = "OrderNoTextEdit"
        Me.OrderNoTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.OrderNoTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.OrderNoTextEdit.Properties.ReadOnly = True
        Me.OrderNoTextEdit.Size = New System.Drawing.Size(164, 20)
        Me.OrderNoTextEdit.TabIndex = 198
        Me.OrderNoTextEdit.TabStop = False
        '
        'JobPrefixTextEdit
        '
        Me.JobPrefixTextEdit.EditValue = ""
        Me.JobPrefixTextEdit.Location = New System.Drawing.Point(97, 5)
        Me.JobPrefixTextEdit.Name = "JobPrefixTextEdit"
        Me.JobPrefixTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.JobPrefixTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.JobPrefixTextEdit.Properties.ReadOnly = True
        Me.JobPrefixTextEdit.Size = New System.Drawing.Size(17, 20)
        Me.JobPrefixTextEdit.TabIndex = 197
        Me.JobPrefixTextEdit.TabStop = False
        '
        'JobNoTextEdit
        '
        Me.JobNoTextEdit.Location = New System.Drawing.Point(120, 5)
        Me.JobNoTextEdit.Name = "JobNoTextEdit"
        Me.JobNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.JobNoTextEdit.Size = New System.Drawing.Size(101, 20)
        Me.JobNoTextEdit.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(5, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 196
        Me.LabelControl1.Text = "Job No"
        '
        'TransporterTextEdit
        '
        Me.TransporterTextEdit.Location = New System.Drawing.Point(97, 85)
        Me.TransporterTextEdit.Name = "TransporterTextEdit"
        Me.TransporterTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TransporterTextEdit.Size = New System.Drawing.Size(448, 20)
        Me.TransporterTextEdit.TabIndex = 6
        '
        'PlaceOfOrderTextEdit
        '
        Me.PlaceOfOrderTextEdit.Location = New System.Drawing.Point(368, 33)
        Me.PlaceOfOrderTextEdit.Name = "PlaceOfOrderTextEdit"
        Me.PlaceOfOrderTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.PlaceOfOrderTextEdit.Size = New System.Drawing.Size(177, 20)
        Me.PlaceOfOrderTextEdit.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 88)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl2.TabIndex = 195
        Me.LabelControl2.Text = "Transporter"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(280, 36)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl9.TabIndex = 192
        Me.LabelControl9.Text = "Place Of Delivery"
        '
        'DestinationTextEdit
        '
        Me.DestinationTextEdit.Location = New System.Drawing.Point(368, 59)
        Me.DestinationTextEdit.Name = "DestinationTextEdit"
        Me.DestinationTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DestinationTextEdit.Size = New System.Drawing.Size(177, 20)
        Me.DestinationTextEdit.TabIndex = 5
        '
        'ModeOfDeliveryTextEdit
        '
        Me.ModeOfDeliveryTextEdit.Location = New System.Drawing.Point(97, 33)
        Me.ModeOfDeliveryTextEdit.Name = "ModeOfDeliveryTextEdit"
        Me.ModeOfDeliveryTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ModeOfDeliveryTextEdit.Size = New System.Drawing.Size(177, 20)
        Me.ModeOfDeliveryTextEdit.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(280, 62)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl3.TabIndex = 194
        Me.LabelControl3.Text = "Destination"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(5, 36)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl5.TabIndex = 191
        Me.LabelControl5.Text = "Mode Of Delivery"
        '
        'QuotationRefTextEdit
        '
        Me.QuotationRefTextEdit.Location = New System.Drawing.Point(97, 59)
        Me.QuotationRefTextEdit.Name = "QuotationRefTextEdit"
        Me.QuotationRefTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.QuotationRefTextEdit.Size = New System.Drawing.Size(177, 20)
        Me.QuotationRefTextEdit.TabIndex = 4
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(5, 62)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl16.TabIndex = 193
        Me.LabelControl16.Text = "Quotation Ref"
        '
        'DateDateEdit
        '
        Me.DateDateEdit.EditValue = Nothing
        Me.DateDateEdit.Location = New System.Drawing.Point(462, 5)
        Me.DateDateEdit.MenuManager = Me.BarManager1
        Me.DateDateEdit.Name = "DateDateEdit"
        Me.DateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.DateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.DateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.DateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.DateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.DateDateEdit.Size = New System.Drawing.Size(83, 20)
        Me.DateDateEdit.TabIndex = 1
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(227, 8)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl17.TabIndex = 187
        Me.LabelControl17.Text = "PO No"
        '
        'AddNewVendorSimpleButton
        '
        Me.AddNewVendorSimpleButton.Appearance.Options.UseTextOptions = True
        Me.AddNewVendorSimpleButton.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.AddNewVendorSimpleButton.Location = New System.Drawing.Point(1001, 32)
        Me.AddNewVendorSimpleButton.Name = "AddNewVendorSimpleButton"
        Me.AddNewVendorSimpleButton.Size = New System.Drawing.Size(81, 22)
        Me.AddNewVendorSimpleButton.TabIndex = 168
        Me.AddNewVendorSimpleButton.TabStop = False
        Me.AddNewVendorSimpleButton.Text = "Add Vendor"
        '
        'ConsignorPANNoTextEdit
        '
        Me.ConsignorPANNoTextEdit.Location = New System.Drawing.Point(1036, 87)
        Me.ConsignorPANNoTextEdit.Name = "ConsignorPANNoTextEdit"
        Me.ConsignorPANNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsignorPANNoTextEdit.Size = New System.Drawing.Size(124, 20)
        Me.ConsignorPANNoTextEdit.TabIndex = 12
        '
        'LabelControl48
        '
        Me.LabelControl48.Location = New System.Drawing.Point(994, 89)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl48.TabIndex = 182
        Me.LabelControl48.Text = "PAN No"
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl38.Appearance.Options.UseForeColor = True
        Me.LabelControl38.Location = New System.Drawing.Point(600, 37)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl38.TabIndex = 181
        Me.LabelControl38.Text = "*"
        '
        'ConsignorStateCodeTextEdit
        '
        Me.ConsignorStateCodeTextEdit.Location = New System.Drawing.Point(777, 87)
        Me.ConsignorStateCodeTextEdit.Name = "ConsignorStateCodeTextEdit"
        Me.ConsignorStateCodeTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsignorStateCodeTextEdit.Size = New System.Drawing.Size(33, 20)
        Me.ConsignorStateCodeTextEdit.TabIndex = 10
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(717, 89)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl19.TabIndex = 180
        Me.LabelControl19.Text = "State Code"
        '
        'ConsignorNameComboBoxEdit
        '
        Me.ConsignorNameComboBoxEdit.Location = New System.Drawing.Point(612, 33)
        Me.ConsignorNameComboBoxEdit.MenuManager = Me.BarManager1
        Me.ConsignorNameComboBoxEdit.Name = "ConsignorNameComboBoxEdit"
        Me.ConsignorNameComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ConsignorNameComboBoxEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsignorNameComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ConsignorNameComboBoxEdit.Size = New System.Drawing.Size(376, 20)
        Me.ConsignorNameComboBoxEdit.TabIndex = 7
        '
        'ConsignorStateTextEdit
        '
        Me.ConsignorStateTextEdit.Location = New System.Drawing.Point(612, 87)
        Me.ConsignorStateTextEdit.Name = "ConsignorStateTextEdit"
        Me.ConsignorStateTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsignorStateTextEdit.Size = New System.Drawing.Size(99, 20)
        Me.ConsignorStateTextEdit.TabIndex = 9
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(567, 89)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl24.TabIndex = 179
        Me.LabelControl24.Text = "State"
        '
        'ConsignorGSTINTextEdit
        '
        Me.ConsignorGSTINTextEdit.Location = New System.Drawing.Point(852, 87)
        Me.ConsignorGSTINTextEdit.Name = "ConsignorGSTINTextEdit"
        Me.ConsignorGSTINTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsignorGSTINTextEdit.Size = New System.Drawing.Size(136, 20)
        Me.ConsignorGSTINTextEdit.TabIndex = 11
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(816, 89)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl23.TabIndex = 10
        Me.LabelControl23.Text = "GSTIN"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(566, 63)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl22.TabIndex = 177
        Me.LabelControl22.Text = "Address"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(567, 3)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(159, 19)
        Me.LabelControl21.TabIndex = 176
        Me.LabelControl21.Text = "Consignor Details : "
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(567, 35)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl15.TabIndex = 175
        Me.LabelControl15.Text = "Name"
        '
        'ConsignorAddressTextEdit
        '
        Me.ConsignorAddressTextEdit.Location = New System.Drawing.Point(612, 60)
        Me.ConsignorAddressTextEdit.Name = "ConsignorAddressTextEdit"
        Me.ConsignorAddressTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsignorAddressTextEdit.Size = New System.Drawing.Size(376, 20)
        Me.ConsignorAddressTextEdit.TabIndex = 8
        '
        'TaxInWordsTextEdit
        '
        Me.TaxInWordsTextEdit.Location = New System.Drawing.Point(803, 509)
        Me.TaxInWordsTextEdit.Name = "TaxInWordsTextEdit"
        Me.TaxInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TaxInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TaxInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TaxInWordsTextEdit.Properties.ReadOnly = True
        Me.TaxInWordsTextEdit.Size = New System.Drawing.Size(536, 20)
        Me.TaxInWordsTextEdit.TabIndex = 135
        Me.TaxInWordsTextEdit.TabStop = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(732, 512)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl8.TabIndex = 136
        Me.LabelControl8.Text = "Tax In Words"
        '
        'ModifiedByTextEdit
        '
        Me.ModifiedByTextEdit.Location = New System.Drawing.Point(1233, 378)
        Me.ModifiedByTextEdit.Name = "ModifiedByTextEdit"
        Me.ModifiedByTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ModifiedByTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.ModifiedByTextEdit.Properties.ReadOnly = True
        Me.ModifiedByTextEdit.Size = New System.Drawing.Size(106, 20)
        Me.ModifiedByTextEdit.TabIndex = 165
        Me.ModifiedByTextEdit.TabStop = False
        '
        'CreatedByTextEdit
        '
        Me.CreatedByTextEdit.Location = New System.Drawing.Point(1233, 352)
        Me.CreatedByTextEdit.Name = "CreatedByTextEdit"
        Me.CreatedByTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreatedByTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.CreatedByTextEdit.Properties.ReadOnly = True
        Me.CreatedByTextEdit.Size = New System.Drawing.Size(106, 20)
        Me.CreatedByTextEdit.TabIndex = 164
        Me.CreatedByTextEdit.TabStop = False
        '
        'CreatedByLabelControl
        '
        Me.CreatedByLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CreatedByLabelControl.Appearance.Options.UseFont = True
        Me.CreatedByLabelControl.Location = New System.Drawing.Point(1153, 355)
        Me.CreatedByLabelControl.Name = "CreatedByLabelControl"
        Me.CreatedByLabelControl.Size = New System.Drawing.Size(71, 13)
        Me.CreatedByLabelControl.TabIndex = 163
        Me.CreatedByLabelControl.Text = "Created By : "
        '
        'ModifiedByLabelControl
        '
        Me.ModifiedByLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ModifiedByLabelControl.Appearance.Options.UseFont = True
        Me.ModifiedByLabelControl.Location = New System.Drawing.Point(1153, 381)
        Me.ModifiedByLabelControl.Name = "ModifiedByLabelControl"
        Me.ModifiedByLabelControl.Size = New System.Drawing.Size(74, 13)
        Me.ModifiedByLabelControl.TabIndex = 162
        Me.ModifiedByLabelControl.Text = "Modified By : "
        '
        'PIGSTAmt
        '
        Me.PIGSTAmt.Location = New System.Drawing.Point(598, 358)
        Me.PIGSTAmt.Name = "PIGSTAmt"
        Me.PIGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PIGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PIGSTAmt.Properties.ReadOnly = True
        Me.PIGSTAmt.Size = New System.Drawing.Size(41, 20)
        Me.PIGSTAmt.TabIndex = 23
        Me.PIGSTAmt.TabStop = False
        '
        'LabelControl47
        '
        Me.LabelControl47.Location = New System.Drawing.Point(545, 361)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl47.TabIndex = 158
        Me.LabelControl47.Text = "IGST Amt"
        '
        'PSGSTAmt
        '
        Me.PSGSTAmt.Location = New System.Drawing.Point(492, 358)
        Me.PSGSTAmt.Name = "PSGSTAmt"
        Me.PSGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PSGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PSGSTAmt.Properties.ReadOnly = True
        Me.PSGSTAmt.Size = New System.Drawing.Size(41, 20)
        Me.PSGSTAmt.TabIndex = 155
        Me.PSGSTAmt.TabStop = False
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(433, 361)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl46.TabIndex = 24
        Me.LabelControl46.Text = "SGST Amt"
        '
        'PCGSTAmt
        '
        Me.PCGSTAmt.Location = New System.Drawing.Point(386, 358)
        Me.PCGSTAmt.Name = "PCGSTAmt"
        Me.PCGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PCGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PCGSTAmt.Properties.ReadOnly = True
        Me.PCGSTAmt.Size = New System.Drawing.Size(41, 20)
        Me.PCGSTAmt.TabIndex = 16
        Me.PCGSTAmt.TabStop = False
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(326, 361)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl45.TabIndex = 154
        Me.LabelControl45.Text = "CGST Amt"
        '
        'PIGSTRateTextEdit
        '
        Me.PIGSTRateTextEdit.Location = New System.Drawing.Point(598, 333)
        Me.PIGSTRateTextEdit.Name = "PIGSTRateTextEdit"
        Me.PIGSTRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.PIGSTRateTextEdit.TabIndex = 18
        '
        'LabelControl44
        '
        Me.LabelControl44.Location = New System.Drawing.Point(545, 336)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl44.TabIndex = 15
        Me.LabelControl44.Text = "IGST %"
        '
        'PSGSTRateTextEdit
        '
        Me.PSGSTRateTextEdit.Location = New System.Drawing.Point(492, 333)
        Me.PSGSTRateTextEdit.Name = "PSGSTRateTextEdit"
        Me.PSGSTRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.PSGSTRateTextEdit.TabIndex = 17
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(433, 336)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl43.TabIndex = 14
        Me.LabelControl43.Text = "SGST %"
        '
        'PCGSTRateTextEdit
        '
        Me.PCGSTRateTextEdit.Location = New System.Drawing.Point(386, 332)
        Me.PCGSTRateTextEdit.Name = "PCGSTRateTextEdit"
        Me.PCGSTRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.PCGSTRateTextEdit.TabIndex = 16
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(326, 336)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl42.TabIndex = 147
        Me.LabelControl42.Text = "CGST %"
        '
        'PackingTextEdit
        '
        Me.PackingTextEdit.Location = New System.Drawing.Point(133, 333)
        Me.PackingTextEdit.Name = "PackingTextEdit"
        Me.PackingTextEdit.Size = New System.Drawing.Size(186, 20)
        Me.PackingTextEdit.TabIndex = 14
        '
        'LabelControl41
        '
        Me.LabelControl41.Location = New System.Drawing.Point(6, 336)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl41.TabIndex = 145
        Me.LabelControl41.Text = "Packing Charges"
        '
        'AddNewProductSimpleButton
        '
        Me.AddNewProductSimpleButton.Location = New System.Drawing.Point(5, 113)
        Me.AddNewProductSimpleButton.Name = "AddNewProductSimpleButton"
        Me.AddNewProductSimpleButton.Size = New System.Drawing.Size(168, 23)
        Me.AddNewProductSimpleButton.TabIndex = 24
        Me.AddNewProductSimpleButton.TabStop = False
        Me.AddNewProductSimpleButton.Text = "Add New Product"
        '
        'TotalAmtAfterTaxTextEdit
        '
        Me.TotalAmtAfterTaxTextEdit.Location = New System.Drawing.Point(810, 358)
        Me.TotalAmtAfterTaxTextEdit.Name = "TotalAmtAfterTaxTextEdit"
        Me.TotalAmtAfterTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtAfterTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtAfterTaxTextEdit.Properties.ReadOnly = True
        Me.TotalAmtAfterTaxTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalAmtAfterTaxTextEdit.TabIndex = 29
        Me.TotalAmtAfterTaxTextEdit.TabStop = False
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(648, 361)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl18.TabIndex = 115
        Me.LabelControl18.Text = "Total Amount After Tax"
        '
        'GSTReverseChargeTextEdit
        '
        Me.GSTReverseChargeTextEdit.Location = New System.Drawing.Point(810, 383)
        Me.GSTReverseChargeTextEdit.Name = "GSTReverseChargeTextEdit"
        Me.GSTReverseChargeTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.GSTReverseChargeTextEdit.TabIndex = 20
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(648, 386)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(156, 13)
        Me.LabelControl14.TabIndex = 114
        Me.LabelControl14.Text = "GST Payable on Reverse Charge"
        '
        'TotalGSTTaxTextEdit
        '
        Me.TotalGSTTaxTextEdit.Location = New System.Drawing.Point(810, 332)
        Me.TotalGSTTaxTextEdit.Name = "TotalGSTTaxTextEdit"
        Me.TotalGSTTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalGSTTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalGSTTaxTextEdit.Properties.ReadOnly = True
        Me.TotalGSTTaxTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalGSTTaxTextEdit.TabIndex = 28
        Me.TotalGSTTaxTextEdit.TabStop = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(648, 335)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl13.TabIndex = 111
        Me.LabelControl13.Text = "Total Amount:GST"
        '
        'TotalInWordsTextEdit
        '
        Me.TotalInWordsTextEdit.Location = New System.Drawing.Point(97, 509)
        Me.TotalInWordsTextEdit.Name = "TotalInWordsTextEdit"
        Me.TotalInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TotalInWordsTextEdit.Properties.ReadOnly = True
        Me.TotalInWordsTextEdit.Size = New System.Drawing.Size(593, 20)
        Me.TotalInWordsTextEdit.TabIndex = 32
        Me.TotalInWordsTextEdit.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(6, 512)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl4.TabIndex = 51
        Me.LabelControl4.Text = "Rupees In Words"
        '
        'NetAmtLabelControl
        '
        Me.NetAmtLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.NetAmtLabelControl.Appearance.Options.UseFont = True
        Me.NetAmtLabelControl.Location = New System.Drawing.Point(958, 338)
        Me.NetAmtLabelControl.Name = "NetAmtLabelControl"
        Me.NetAmtLabelControl.Size = New System.Drawing.Size(66, 14)
        Me.NetAmtLabelControl.TabIndex = 11
        Me.NetAmtLabelControl.Text = "Grand Amt"
        '
        'TotalAmtBeforeTaxTextEdit
        '
        Me.TotalAmtBeforeTaxTextEdit.Location = New System.Drawing.Point(133, 384)
        Me.TotalAmtBeforeTaxTextEdit.Name = "TotalAmtBeforeTaxTextEdit"
        Me.TotalAmtBeforeTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtBeforeTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtBeforeTaxTextEdit.Properties.ReadOnly = True
        Me.TotalAmtBeforeTaxTextEdit.Size = New System.Drawing.Size(186, 20)
        Me.TotalAmtBeforeTaxTextEdit.TabIndex = 19
        Me.TotalAmtBeforeTaxTextEdit.TabStop = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(5, 386)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Total Amount Before Tax"
        '
        'CancelSimpleButton
        '
        Me.CancelSimpleButton.ImageOptions.Image = CType(resources.GetObject("CancelSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.CancelSimpleButton.Location = New System.Drawing.Point(320, 550)
        Me.CancelSimpleButton.Name = "CancelSimpleButton"
        Me.CancelSimpleButton.Size = New System.Drawing.Size(75, 30)
        Me.CancelSimpleButton.TabIndex = 3
        Me.CancelSimpleButton.Text = "&Cancel"
        '
        'SaveNewSimpleButton
        '
        Me.SaveNewSimpleButton.ImageOptions.Image = CType(resources.GetObject("SaveNewSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.SaveNewSimpleButton.Location = New System.Drawing.Point(221, 550)
        Me.SaveNewSimpleButton.Name = "SaveNewSimpleButton"
        Me.SaveNewSimpleButton.Size = New System.Drawing.Size(93, 30)
        Me.SaveNewSimpleButton.TabIndex = 2
        Me.SaveNewSimpleButton.Text = "&Save 'n' New"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(5, 34)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Size = New System.Drawing.Size(1371, 610)
        Me.SplitContainer1.SplitterDistance = 457
        Me.SplitContainer1.TabIndex = 5
        '
        'Frm_VendorPO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1370, 647)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_VendorPO"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "VendorPO"
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.TotalAmtTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalTCSTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCSRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDetailGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDetailGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Remarks1TextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Remarks2TextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PaymentTermsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvancePaymentTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransporterTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PlaceOfOrderTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DestinationTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModeOfDeliveryTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QuotationRefTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsignorPANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsignorStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsignorNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsignorStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsignorGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsignorAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TaxInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModifiedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CreatedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PackingTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtAfterTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GSTReverseChargeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalGSTTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtBeforeTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents CancelSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents SaveNewSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents NetAmtLabelControl As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents TotalAmtBeforeTaxTextEdit As DevExpress.XtraEditors.TextEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents PrintOriginalPrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TotalInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents TotalGSTTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalAmtAfterTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PrintDuplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintTriplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents AddNewProductSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TaxInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PackingTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PCGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PIGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PSGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PCGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PIGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PSGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ModifiedByLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CreatedByLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ModifiedByTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CreatedByTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GSTReverseChargeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddNewVendorSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ConsignorPANNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsignorStateCodeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsignorNameComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ConsignorStateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsignorGSTINTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsignorAddressTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents OrderNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JobPrefixTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JobNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransporterTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PlaceOfOrderTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DestinationTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ModeOfDeliveryTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents QuotationRefTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AdvancePaymentTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Remarks1TextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Remarks2TextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PaymentTermsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintDynamicBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents InvoiceDetailGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents InvoiceDetailGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TotalTCSTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TotalTCSLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TCSRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AddTCSLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalAmtTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SplitContainer1 As SplitContainer
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
End Class
