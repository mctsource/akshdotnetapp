﻿Imports DevExpress.XtraEditors
Imports System.Data.OleDb
'Imports ELCF
Public Class Frm_SearchPO


    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM VendorPO Where Company='" + PubCompanyName + "'", "VenderPO", VendorPOJobNoComboBoxEdit)
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM ClientPO Where Company='" + PubCompanyName + "'", "ClientPO", ClientPOJobNoComboBoxEdit)
    End Sub

    Function ClientPOValidation() As Boolean
        If ClientPOLookUpEdit.EditValue Is DBNull.Value Then
            ClientPOLookUpEdit.Focus()
            Return False
        ElseIf ClientPOJobNoComboBoxEdit.Text = "" Then
            ClientPOJobNoComboBoxEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Function VendorPOValidation() As Boolean
        If VendorPOLookUpEdit.EditValue Is DBNull.Value Then
            VendorPOLookUpEdit.Focus()
            Return False
        ElseIf VendorPOJobNoComboBoxEdit.Text = "" Then
            VendorPOJobNoComboBoxEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub ClientPOButton_Click(sender As Object, e As EventArgs) Handles ClientPOButton.Click
        If ClientPOValidation() Then

            LabelPanel.Controls.Clear()

            Dim ClientDA As New OleDbDataAdapter
            Dim ClientDT As New DataTable
            ClientDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,PartyID,TotalAmount,AdvancePayment,GrandTotalAmount From ClientPO where Company='" + PubCompanyName + "' AND OrderNo='" + ClientPOLookUpEdit.Text + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            ClientDA.Fill(ClientDT)

            If ClientDT.Rows.Count > 0 Then
                JobNoLabelControl.Text = ClientDT.Rows(0).Item("JobNoPrefix").ToString + ClientDT.Rows(0).Item("JobNo").ToString
                PONoLabelControl.Text = ClientDT.Rows(0).Item("OrderNo").ToString
                OrderDateLabelControl.Text = ClientDT.Rows(0).Item("OrderDate").ToString.Remove(10, 9)
                POAmountLabelControl.Text = ClientDT.Rows(0).Item("GrandTotalAmount").ToString
                AdvancePaaymentLabelControl.Text = ClientDT.Rows(0).Item("AdvancePayment").ToString

                Dim PartyDA As New OleDbDataAdapter
                Dim PartyDT As New DataTable
                PartyDA.SelectCommand = New OleDbCommand("Select PartyName From Party where Company='" + PubCompanyName + "' AND PartyID=" + ClientDT.Rows(0).Item("PartyID").ToString + "", ConStr)
                PartyDA.Fill(PartyDT)

                If PartyDT.Rows.Count > 0 Then
                    PartyNameLabelControl.Text = PartyDT.Rows(0).Item("PartyName").ToString
                End If
            End If

            Dim InvoiceDA As New OleDbDataAdapter
            Dim InvoiceDT As New DataTable
            InvoiceDA.SelectCommand = New OleDbCommand("Select InvoiceID,FullInvoiceNo,JobNoPrefix,JobNo,OfferNo,GrandTotalAmount From Invoice where Company='" + PubCompanyName + "' AND OfferNo='" + ClientPOLookUpEdit.Text + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            InvoiceDA.Fill(InvoiceDT)

            NoOfInvoicesLabelControl.Text = InvoiceDT.Rows.Count.ToString

            Dim TAmount As Double = 0
            Dim DataCount As Integer = 100
            Dim Location As Integer = -15

            If InvoiceDT.Rows.Count > 0 Then
                For Each DTRows As DataRow In InvoiceDT.Rows
                    Location = Location + 28

                    DataCount = DataCount + 1
                    Dim label1 As New LabelControl
                    label1.Name = "label" + DataCount.ToString
                    label1.Text = DTRows.Item("FullInvoiceNo").ToString
                    label1.AutoSize = True
                    label1.Location = New Point(57, Location)
                    label1.Font = New Font(label1.Font, FontStyle.Bold)
                    label1.Tag = 1
                    LabelPanel.Controls.Add(label1)

                    DataCount = DataCount + 1
                    Dim label2 As New LabelControl
                    label2.Name = "label" + DataCount.ToString
                    label2.Text = Math.Round(Convert.ToDouble(DTRows.Item("GrandTotalAmount"))).ToString
                    label2.AutoSize = True
                    label2.Location = New Point(261, Location)
                    label2.Font = New Font(label2.Font, FontStyle.Bold)
                    label2.Tag = 2
                    LabelPanel.Controls.Add(label2)

                    TAmount = Math.Round(TAmount + Convert.ToDouble(DTRows.Item("GrandTotalAmount").ToString))
                Next
            End If

            TotalAmountLabelControl.Text = TAmount.ToString

            Dim calAmount As Double = Math.Round((Convert.ToDouble(ClientDT.Rows(0).Item("GrandTotalAmount").ToString) - Convert.ToDouble(ClientDT.Rows(0).Item("AdvancePayment").ToString)) - Convert.ToDouble(TAmount.ToString))

            If calAmount = 0 Then
                StatusLabelControl.Text = "PO is Closed"
            Else
                StatusLabelControl.Text = "Remaining Amount " + calAmount.ToString
            End If
        End If
    End Sub

    Private Sub VendorPOButton_Click(sender As Object, e As EventArgs) Handles VendorPOButton.Click
        If VendorPOValidation() Then

            LabelPanel.Controls.Clear()

            Dim VendorDA As New OleDbDataAdapter
            Dim VendorDT As New DataTable
            VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,PartyID,TotalAmount,AdvancePayment,GrandTotalAmount From VendorPO where Company='" + PubCompanyName + "' AND OrderNo='" + VendorPOLookUpEdit.Text + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            VendorDA.Fill(VendorDT)

            If VendorDT.Rows.Count > 0 Then
                JobNoLabelControl.Text = VendorDT.Rows(0).Item("JobNoPrefix").ToString + VendorDT.Rows(0).Item("JobNo").ToString
                PONoLabelControl.Text = VendorDT.Rows(0).Item("OrderNo").ToString
                OrderDateLabelControl.Text = VendorDT.Rows(0).Item("OrderDate").ToString.Remove(10, 9)
                POAmountLabelControl.Text = VendorDT.Rows(0).Item("GrandTotalAmount").ToString
                AdvancePaaymentLabelControl.Text = VendorDT.Rows(0).Item("AdvancePayment").ToString

                Dim PartyDA As New OleDbDataAdapter
                Dim PartyDT As New DataTable
                PartyDA.SelectCommand = New OleDbCommand("Select PartyName From Party where Company='" + PubCompanyName + "' AND PartyID=" + VendorDT.Rows(0).Item("PartyID").ToString + "", ConStr)
                PartyDA.Fill(PartyDT)

                If PartyDT.Rows.Count > 0 Then
                    PartyNameLabelControl.Text = PartyDT.Rows(0).Item("PartyName").ToString
                End If
            End If

            Dim BillDA As New OleDbDataAdapter
            Dim BillDT As New DataTable
            BillDA.SelectCommand = New OleDbCommand("Select BillID,BillNo,JobNo,PONo,GrandTotalAmount From Bill where Company='" + PubCompanyName + "' AND PONo='" + VendorPOLookUpEdit.Text + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            BillDA.Fill(BillDT)

            NoOfInvoicesLabelControl.Text = BillDT.Rows.Count.ToString

            Dim TAmount As Double = 0
            Dim DataCount As Integer = 100
            Dim Location As Integer = -15

            If BillDT.Rows.Count > 0 Then
                For Each DTRows As DataRow In BillDT.Rows
                    Location = Location + 28

                    DataCount = DataCount + 1
                    Dim label1 As New LabelControl
                    label1.Name = "label" + DataCount.ToString
                    label1.Text = DTRows.Item("BillNo").ToString
                    label1.AutoSize = True
                    label1.Location = New Point(57, Location)
                    label1.Font = New Font(label1.Font, FontStyle.Bold)
                    label1.Tag = 1
                    LabelPanel.Controls.Add(label1)

                    DataCount = DataCount + 1
                    Dim label2 As New LabelControl
                    label2.Name = "label" + DataCount.ToString
                    label2.Text = Math.Round(Convert.ToDouble(DTRows.Item("GrandTotalAmount"))).ToString
                    label2.AutoSize = True
                    label2.Location = New Point(261, Location)
                    label2.Font = New Font(label2.Font, FontStyle.Bold)
                    label2.Tag = 2
                    LabelPanel.Controls.Add(label2)

                    TAmount = Math.Round(TAmount + Convert.ToDouble(DTRows.Item("GrandTotalAmount").ToString))
                Next
            End If

            TotalAmountLabelControl.Text = TAmount.ToString

            Dim calAmount As Double = Math.Round((Convert.ToDouble(VendorDT.Rows(0).Item("GrandTotalAmount").ToString) - Convert.ToDouble(VendorDT.Rows(0).Item("AdvancePayment").ToString)) - Convert.ToDouble(TAmount.ToString))

            If calAmount = 0 Then
                StatusLabelControl.Text = "PO is Closed"
            Else
                StatusLabelControl.Text = "Remaining Amount " + calAmount.ToString
            End If
        End If
    End Sub

    Private Sub VendorPOJobNoComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles VendorPOJobNoComboBoxEdit.SelectedIndexChanged
        If VendorPOJobNoComboBoxEdit.Text <> "" Then
            SetLookUp("SELECT PurchaseID, OrderNo FROM VendorPO Where Company='" + PubCompanyName + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", "VendorPO", "PurchaseID", "OrderNo", VendorPOLookUpEdit, "PO No")
        End If
    End Sub

    Private Sub ClientPOJobNoComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ClientPOJobNoComboBoxEdit.SelectedIndexChanged
        If ClientPOJobNoComboBoxEdit.Text <> "" Then
            SetLookUp("SELECT PurchaseID, OrderNo FROM ClientPO Where Company='" + PubCompanyName + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", "ClientPO", "PurchaseID", "OrderNo", ClientPOLookUpEdit, "PO No")
        End If
    End Sub
End Class