﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_Main

    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Main))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.PartyBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ClientPOBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.InvoiceBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.BillBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ReportSummaryBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ExitBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.Format1BarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.Format2BarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.InvoiceSummaryBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.CompanyBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.VendorPOBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.SearchPOBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.UserBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.VendorPaymentBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.EditVendorPaymentBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ClientPaymentBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.VendorSearchBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ClientSearchBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.CompanyProfileBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ProductBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.BankDetailsBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ProformaInvoiceBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ProInvoiceBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.VendorOutstandingBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.VendorOSBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.LetterHead = New DevExpress.XtraBars.BarButtonItem()
        Me.Format2 = New DevExpress.XtraBars.BarButtonItem()
        Me.RawMaterialBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.BackupBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.WorkOrderBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.WOBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.WOPaymentBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.VendorReportBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.WOBillBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ClientReportBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.WorkOrderReportBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage3 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.VendorPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ClientRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.WorkRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.MastersPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ChangeRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Format = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ExitRibbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.PartyBarButtonItem, Me.ClientPOBarButtonItem, Me.InvoiceBarButtonItem, Me.BillBarButtonItem, Me.ReportSummaryBarButtonItem, Me.ExitBarButtonItem, Me.Format1BarButtonItem, Me.Format2BarButtonItem, Me.InvoiceSummaryBarButtonItem, Me.CompanyBarButtonItem, Me.VendorPOBarButtonItem, Me.SearchPOBarButtonItem, Me.UserBarButtonItem, Me.VendorPaymentBarButtonItem, Me.EditVendorPaymentBarButtonItem, Me.ClientPaymentBarButtonItem, Me.VendorSearchBarButtonItem, Me.ClientSearchBarButtonItem, Me.CompanyProfileBarButtonItem, Me.ProductBarButtonItem, Me.BankDetailsBarButtonItem, Me.ProformaInvoiceBarButtonItem, Me.ProInvoiceBarButtonItem, Me.VendorOutstandingBarButtonItem, Me.VendorOSBarButtonItem, Me.LetterHead, Me.Format2, Me.RawMaterialBarButtonItem, Me.BackupBarButtonItem, Me.WorkOrderBarButtonItem, Me.WOBarButtonItem, Me.WOPaymentBarButtonItem, Me.VendorReportBarButtonItem, Me.WOBillBarButtonItem, Me.ClientReportBarButtonItem, Me.WorkOrderReportBarButtonItem})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 45
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage3})
        Me.RibbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice
        Me.RibbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.[False]
        Me.RibbonControl.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.ShowOnMultiplePages
        Me.RibbonControl.Size = New System.Drawing.Size(1002, 130)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'PartyBarButtonItem
        '
        Me.PartyBarButtonItem.Caption = "Party"
        Me.PartyBarButtonItem.Id = 3
        Me.PartyBarButtonItem.ImageOptions.Image = Global.AkshEngineeringSystem.My.Resources.Resources.assignto_16x16
        Me.PartyBarButtonItem.ImageOptions.LargeImage = Global.AkshEngineeringSystem.My.Resources.Resources.assignto_32x321
        Me.PartyBarButtonItem.Name = "PartyBarButtonItem"
        '
        'ClientPOBarButtonItem
        '
        Me.ClientPOBarButtonItem.Caption = "Client PO"
        Me.ClientPOBarButtonItem.Id = 4
        Me.ClientPOBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ClientPOBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ClientPOBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ClientPOBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ClientPOBarButtonItem.Name = "ClientPOBarButtonItem"
        '
        'InvoiceBarButtonItem
        '
        Me.InvoiceBarButtonItem.Caption = "Sales Invoice"
        Me.InvoiceBarButtonItem.Id = 5
        Me.InvoiceBarButtonItem.ImageOptions.Image = CType(resources.GetObject("InvoiceBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.InvoiceBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("InvoiceBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.InvoiceBarButtonItem.Name = "InvoiceBarButtonItem"
        '
        'BillBarButtonItem
        '
        Me.BillBarButtonItem.Caption = "Purchase Invoice"
        Me.BillBarButtonItem.Id = 9
        Me.BillBarButtonItem.ImageOptions.Image = CType(resources.GetObject("BillBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.BillBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("BillBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BillBarButtonItem.Name = "BillBarButtonItem"
        '
        'ReportSummaryBarButtonItem
        '
        Me.ReportSummaryBarButtonItem.Caption = "Bill Summary"
        Me.ReportSummaryBarButtonItem.Id = 10
        Me.ReportSummaryBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ReportSummaryBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ReportSummaryBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ReportSummaryBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ReportSummaryBarButtonItem.Name = "ReportSummaryBarButtonItem"
        '
        'ExitBarButtonItem
        '
        Me.ExitBarButtonItem.Caption = "Exit"
        Me.ExitBarButtonItem.Id = 11
        Me.ExitBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ExitBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ExitBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ExitBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ExitBarButtonItem.Name = "ExitBarButtonItem"
        '
        'Format1BarButtonItem
        '
        Me.Format1BarButtonItem.Caption = "Format 1"
        Me.Format1BarButtonItem.Id = 12
        Me.Format1BarButtonItem.ImageOptions.Image = CType(resources.GetObject("Format1BarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.Format1BarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("Format1BarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.Format1BarButtonItem.Name = "Format1BarButtonItem"
        '
        'Format2BarButtonItem
        '
        Me.Format2BarButtonItem.Caption = "Format 2"
        Me.Format2BarButtonItem.Id = 13
        Me.Format2BarButtonItem.ImageOptions.Image = CType(resources.GetObject("Format2BarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.Format2BarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("Format2BarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.Format2BarButtonItem.Name = "Format2BarButtonItem"
        '
        'InvoiceSummaryBarButtonItem
        '
        Me.InvoiceSummaryBarButtonItem.Caption = "Invoice Summary"
        Me.InvoiceSummaryBarButtonItem.Id = 14
        Me.InvoiceSummaryBarButtonItem.ImageOptions.Image = CType(resources.GetObject("InvoiceSummaryBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.InvoiceSummaryBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("InvoiceSummaryBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.InvoiceSummaryBarButtonItem.Name = "InvoiceSummaryBarButtonItem"
        '
        'CompanyBarButtonItem
        '
        Me.CompanyBarButtonItem.Caption = "Change Company"
        Me.CompanyBarButtonItem.Id = 15
        Me.CompanyBarButtonItem.ImageOptions.Image = CType(resources.GetObject("CompanyBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.CompanyBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("CompanyBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.CompanyBarButtonItem.Name = "CompanyBarButtonItem"
        '
        'VendorPOBarButtonItem
        '
        Me.VendorPOBarButtonItem.Caption = "Vendor PO"
        Me.VendorPOBarButtonItem.Id = 17
        Me.VendorPOBarButtonItem.ImageOptions.Image = CType(resources.GetObject("VendorPOBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.VendorPOBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("VendorPOBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.VendorPOBarButtonItem.Name = "VendorPOBarButtonItem"
        '
        'SearchPOBarButtonItem
        '
        Me.SearchPOBarButtonItem.Caption = "Search PO"
        Me.SearchPOBarButtonItem.Id = 19
        Me.SearchPOBarButtonItem.ImageOptions.Image = CType(resources.GetObject("SearchPOBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.SearchPOBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("SearchPOBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.SearchPOBarButtonItem.Name = "SearchPOBarButtonItem"
        '
        'UserBarButtonItem
        '
        Me.UserBarButtonItem.Caption = "User"
        Me.UserBarButtonItem.Id = 20
        Me.UserBarButtonItem.ImageOptions.LargeImage = Global.AkshEngineeringSystem.My.Resources.Resources.users_icon1
        Me.UserBarButtonItem.Name = "UserBarButtonItem"
        '
        'VendorPaymentBarButtonItem
        '
        Me.VendorPaymentBarButtonItem.Caption = "Vendor Payment"
        Me.VendorPaymentBarButtonItem.Id = 21
        Me.VendorPaymentBarButtonItem.ImageOptions.Image = CType(resources.GetObject("VendorPaymentBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.VendorPaymentBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("VendorPaymentBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.VendorPaymentBarButtonItem.Name = "VendorPaymentBarButtonItem"
        '
        'EditVendorPaymentBarButtonItem
        '
        Me.EditVendorPaymentBarButtonItem.Caption = "Edit Vendor Payment"
        Me.EditVendorPaymentBarButtonItem.Id = 22
        Me.EditVendorPaymentBarButtonItem.ImageOptions.Image = CType(resources.GetObject("EditVendorPaymentBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.EditVendorPaymentBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("EditVendorPaymentBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.EditVendorPaymentBarButtonItem.Name = "EditVendorPaymentBarButtonItem"
        '
        'ClientPaymentBarButtonItem
        '
        Me.ClientPaymentBarButtonItem.Caption = "Client Payment"
        Me.ClientPaymentBarButtonItem.Id = 23
        Me.ClientPaymentBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ClientPaymentBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ClientPaymentBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ClientPaymentBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ClientPaymentBarButtonItem.Name = "ClientPaymentBarButtonItem"
        '
        'VendorSearchBarButtonItem
        '
        Me.VendorSearchBarButtonItem.Caption = "Vendor Search"
        Me.VendorSearchBarButtonItem.Id = 24
        Me.VendorSearchBarButtonItem.ImageOptions.Image = CType(resources.GetObject("VendorSearchBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.VendorSearchBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("VendorSearchBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.VendorSearchBarButtonItem.Name = "VendorSearchBarButtonItem"
        '
        'ClientSearchBarButtonItem
        '
        Me.ClientSearchBarButtonItem.Caption = "Client Search"
        Me.ClientSearchBarButtonItem.Id = 25
        Me.ClientSearchBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ClientSearchBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ClientSearchBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ClientSearchBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ClientSearchBarButtonItem.Name = "ClientSearchBarButtonItem"
        '
        'CompanyProfileBarButtonItem
        '
        Me.CompanyProfileBarButtonItem.Caption = "Company Profile"
        Me.CompanyProfileBarButtonItem.Id = 27
        Me.CompanyProfileBarButtonItem.ImageOptions.Image = CType(resources.GetObject("CompanyProfileBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.CompanyProfileBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("CompanyProfileBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.CompanyProfileBarButtonItem.Name = "CompanyProfileBarButtonItem"
        '
        'ProductBarButtonItem
        '
        Me.ProductBarButtonItem.Caption = "Product"
        Me.ProductBarButtonItem.Id = 28
        Me.ProductBarButtonItem.ImageOptions.Image = Global.AkshEngineeringSystem.My.Resources.Resources.cube_16x161
        Me.ProductBarButtonItem.ImageOptions.LargeImage = Global.AkshEngineeringSystem.My.Resources.Resources.cube_32x323
        Me.ProductBarButtonItem.Name = "ProductBarButtonItem"
        '
        'BankDetailsBarButtonItem
        '
        Me.BankDetailsBarButtonItem.Caption = "Bank Details"
        Me.BankDetailsBarButtonItem.Id = 29
        Me.BankDetailsBarButtonItem.ImageOptions.Image = CType(resources.GetObject("BankDetailsBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.BankDetailsBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("BankDetailsBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BankDetailsBarButtonItem.Name = "BankDetailsBarButtonItem"
        '
        'ProformaInvoiceBarButtonItem
        '
        Me.ProformaInvoiceBarButtonItem.Caption = "Proforma Invoice"
        Me.ProformaInvoiceBarButtonItem.Id = 30
        Me.ProformaInvoiceBarButtonItem.Name = "ProformaInvoiceBarButtonItem"
        '
        'ProInvoiceBarButtonItem
        '
        Me.ProInvoiceBarButtonItem.Caption = "Proforma Invoice"
        Me.ProInvoiceBarButtonItem.Id = 31
        Me.ProInvoiceBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ProInvoiceBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ProInvoiceBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ProInvoiceBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ProInvoiceBarButtonItem.Name = "ProInvoiceBarButtonItem"
        '
        'VendorOutstandingBarButtonItem
        '
        Me.VendorOutstandingBarButtonItem.Caption = "Vendor Outstanding"
        Me.VendorOutstandingBarButtonItem.Id = 32
        Me.VendorOutstandingBarButtonItem.ImageOptions.Image = CType(resources.GetObject("VendorOutstandingBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.VendorOutstandingBarButtonItem.Name = "VendorOutstandingBarButtonItem"
        '
        'VendorOSBarButtonItem
        '
        Me.VendorOSBarButtonItem.Caption = "Outstanding"
        Me.VendorOSBarButtonItem.Id = 33
        Me.VendorOSBarButtonItem.ImageOptions.Image = CType(resources.GetObject("VendorOSBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.VendorOSBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("VendorOSBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.VendorOSBarButtonItem.Name = "VendorOSBarButtonItem"
        '
        'LetterHead
        '
        Me.LetterHead.Caption = "LetterHead"
        Me.LetterHead.Id = 34
        Me.LetterHead.ImageOptions.LargeImage = CType(resources.GetObject("LetterHead.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.LetterHead.Name = "LetterHead"
        '
        'Format2
        '
        Me.Format2.Caption = "Format2"
        Me.Format2.Id = 35
        Me.Format2.ImageOptions.LargeImage = CType(resources.GetObject("Format2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.Format2.Name = "Format2"
        '
        'RawMaterialBarButtonItem
        '
        Me.RawMaterialBarButtonItem.Caption = "Raw Material"
        Me.RawMaterialBarButtonItem.Id = 36
        Me.RawMaterialBarButtonItem.ImageOptions.Image = CType(resources.GetObject("RawMaterialBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.RawMaterialBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("RawMaterialBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.RawMaterialBarButtonItem.Name = "RawMaterialBarButtonItem"
        '
        'BackupBarButtonItem
        '
        Me.BackupBarButtonItem.Caption = "Backup"
        Me.BackupBarButtonItem.Id = 37
        Me.BackupBarButtonItem.ImageOptions.Image = CType(resources.GetObject("BackupBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.BackupBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("BackupBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BackupBarButtonItem.Name = "BackupBarButtonItem"
        '
        'WorkOrderBarButtonItem
        '
        Me.WorkOrderBarButtonItem.Caption = "Work Order"
        Me.WorkOrderBarButtonItem.Id = 38
        Me.WorkOrderBarButtonItem.ImageOptions.Image = CType(resources.GetObject("WorkOrderBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.WorkOrderBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("WorkOrderBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.WorkOrderBarButtonItem.Name = "WorkOrderBarButtonItem"
        '
        'WOBarButtonItem
        '
        Me.WOBarButtonItem.Caption = "Work Order"
        Me.WOBarButtonItem.Id = 39
        Me.WOBarButtonItem.ImageOptions.Image = CType(resources.GetObject("WOBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.WOBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("WOBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.WOBarButtonItem.Name = "WOBarButtonItem"
        '
        'WOPaymentBarButtonItem
        '
        Me.WOPaymentBarButtonItem.Caption = "Work Order Payment"
        Me.WOPaymentBarButtonItem.Id = 40
        Me.WOPaymentBarButtonItem.ImageOptions.Image = CType(resources.GetObject("WOPaymentBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.WOPaymentBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("WOPaymentBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.WOPaymentBarButtonItem.Name = "WOPaymentBarButtonItem"
        '
        'VendorReportBarButtonItem
        '
        Me.VendorReportBarButtonItem.Caption = "Vendor Report"
        Me.VendorReportBarButtonItem.Id = 41
        Me.VendorReportBarButtonItem.ImageOptions.Image = CType(resources.GetObject("VendorReportBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.VendorReportBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("VendorReportBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.VendorReportBarButtonItem.Name = "VendorReportBarButtonItem"
        '
        'WOBillBarButtonItem
        '
        Me.WOBillBarButtonItem.Caption = "WO Invoice"
        Me.WOBillBarButtonItem.Id = 42
        Me.WOBillBarButtonItem.ImageOptions.Image = CType(resources.GetObject("WOBillBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.WOBillBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("WOBillBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.WOBillBarButtonItem.Name = "WOBillBarButtonItem"
        '
        'ClientReportBarButtonItem
        '
        Me.ClientReportBarButtonItem.Caption = "Client Report"
        Me.ClientReportBarButtonItem.Id = 43
        Me.ClientReportBarButtonItem.ImageOptions.Image = CType(resources.GetObject("ClientReportBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.ClientReportBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("ClientReportBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.ClientReportBarButtonItem.Name = "ClientReportBarButtonItem"
        '
        'WorkOrderReportBarButtonItem
        '
        Me.WorkOrderReportBarButtonItem.Caption = "WorkOrder Report"
        Me.WorkOrderReportBarButtonItem.Id = 44
        Me.WorkOrderReportBarButtonItem.ImageOptions.Image = CType(resources.GetObject("WorkOrderReportBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.WorkOrderReportBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("WorkOrderReportBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.WorkOrderReportBarButtonItem.Name = "WorkOrderReportBarButtonItem"
        '
        'RibbonPage3
        '
        Me.RibbonPage3.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.VendorPageGroup, Me.ClientRibbonPageGroup, Me.WorkRibbonPageGroup, Me.MastersPageGroup, Me.ChangeRibbonPageGroup, Me.Format, Me.ExitRibbonPageGroup})
        Me.RibbonPage3.Name = "RibbonPage3"
        Me.RibbonPage3.Text = "FormPage"
        '
        'VendorPageGroup
        '
        Me.VendorPageGroup.ItemLinks.Add(Me.VendorPOBarButtonItem)
        Me.VendorPageGroup.ItemLinks.Add(Me.BillBarButtonItem)
        Me.VendorPageGroup.ItemLinks.Add(Me.VendorPaymentBarButtonItem)
        Me.VendorPageGroup.ItemLinks.Add(Me.VendorSearchBarButtonItem)
        Me.VendorPageGroup.ItemLinks.Add(Me.VendorReportBarButtonItem)
        Me.VendorPageGroup.Name = "VendorPageGroup"
        Me.VendorPageGroup.Text = "Vendor"
        '
        'ClientRibbonPageGroup
        '
        Me.ClientRibbonPageGroup.ItemLinks.Add(Me.ClientPOBarButtonItem)
        Me.ClientRibbonPageGroup.ItemLinks.Add(Me.ProInvoiceBarButtonItem)
        Me.ClientRibbonPageGroup.ItemLinks.Add(Me.InvoiceBarButtonItem)
        Me.ClientRibbonPageGroup.ItemLinks.Add(Me.ClientPaymentBarButtonItem)
        Me.ClientRibbonPageGroup.ItemLinks.Add(Me.ClientSearchBarButtonItem)
        Me.ClientRibbonPageGroup.ItemLinks.Add(Me.ClientReportBarButtonItem)
        Me.ClientRibbonPageGroup.Name = "ClientRibbonPageGroup"
        Me.ClientRibbonPageGroup.Text = "Client"
        '
        'WorkRibbonPageGroup
        '
        Me.WorkRibbonPageGroup.ItemLinks.Add(Me.WOBarButtonItem)
        Me.WorkRibbonPageGroup.ItemLinks.Add(Me.WOPaymentBarButtonItem)
        Me.WorkRibbonPageGroup.ItemLinks.Add(Me.WOBillBarButtonItem)
        Me.WorkRibbonPageGroup.ItemLinks.Add(Me.WorkOrderReportBarButtonItem)
        Me.WorkRibbonPageGroup.Name = "WorkRibbonPageGroup"
        Me.WorkRibbonPageGroup.Text = "Work Order"
        '
        'MastersPageGroup
        '
        Me.MastersPageGroup.ItemLinks.Add(Me.PartyBarButtonItem)
        Me.MastersPageGroup.ItemLinks.Add(Me.CompanyProfileBarButtonItem)
        Me.MastersPageGroup.ItemLinks.Add(Me.ProductBarButtonItem)
        Me.MastersPageGroup.ItemLinks.Add(Me.BankDetailsBarButtonItem)
        Me.MastersPageGroup.ItemLinks.Add(Me.RawMaterialBarButtonItem)
        Me.MastersPageGroup.Name = "MastersPageGroup"
        Me.MastersPageGroup.Text = "Masters"
        '
        'ChangeRibbonPageGroup
        '
        Me.ChangeRibbonPageGroup.ItemLinks.Add(Me.UserBarButtonItem)
        Me.ChangeRibbonPageGroup.ItemLinks.Add(Me.CompanyBarButtonItem)
        Me.ChangeRibbonPageGroup.Name = "ChangeRibbonPageGroup"
        Me.ChangeRibbonPageGroup.Text = "Change"
        '
        'Format
        '
        Me.Format.ItemLinks.Add(Me.LetterHead)
        Me.Format.Name = "Format"
        Me.Format.Text = "Format"
        '
        'ExitRibbonPageGroup
        '
        Me.ExitRibbonPageGroup.ItemLinks.Add(Me.VendorOSBarButtonItem)
        Me.ExitRibbonPageGroup.ItemLinks.Add(Me.BackupBarButtonItem)
        Me.ExitRibbonPageGroup.ItemLinks.Add(Me.ExitBarButtonItem)
        Me.ExitRibbonPageGroup.Name = "ExitRibbonPageGroup"
        Me.ExitRibbonPageGroup.Text = "Close"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 432)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(1002, 31)
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "RibbonPageGroup1"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "RibbonPage1"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "RibbonPage2"
        '
        'Frm_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1002, 463)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.IsMdiContainer = True
        Me.Name = "Frm_Main"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Aksh Engineering Systems"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents PartyBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ClientPOBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage3 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents VendorPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents MastersPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents InvoiceBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BillBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ReportSummaryBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ExitBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ExitRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents Format1BarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Format2BarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents InvoiceSummaryBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CompanyBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ChangeRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents VendorPOBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SearchPOBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ClientRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents UserBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents VendorPaymentBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents EditVendorPaymentBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ClientPaymentBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents VendorSearchBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ClientSearchBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CompanyProfileBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProductBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BankDetailsBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProformaInvoiceBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProInvoiceBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents VendorOutstandingBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents VendorOSBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Protected WithEvents Format As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents LetterHead As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Format2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RawMaterialBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BackupBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents WorkOrderBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents WorkRibbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents WOBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents WOPaymentBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents VendorReportBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents WOBillBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ClientReportBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents WorkOrderReportBarButtonItem As DevExpress.XtraBars.BarButtonItem
End Class
