﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI
Imports System.Data.OleDb
'Imports ELCF
Public Class Frm_WOPaymentNew
    Dim DS As New DataSet

    Dim PaymentDA As New OleDbDataAdapter
    Dim PaymentBS As New BindingSource

    Dim WorkOrderID As Integer = 0
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM WorkOrder Where Company='" + PubCompanyName + "'", "WorkOrder", WOJobNoComboBoxEdit)
    End Sub

    Function VendorPOValidation() As Boolean
        If WOLookUpEdit.EditValue Is DBNull.Value Then
            WOLookUpEdit.Focus()
            Return False
        ElseIf WOJobNoComboBoxEdit.Text = "" Then
            WOJobNoComboBoxEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub VendorPOButton_Click(sender As Object, e As EventArgs) Handles WOButton.Click
        If VendorPOValidation() Then
            Dim VendorDA As New OleDbDataAdapter
            Dim VendorDT As New DataTable
            VendorDA.SelectCommand = New OleDbCommand("Select WorkOrderID,JobNoPrefix,JobNo,WorkOrderNo,WorkOrderDate,ReceiverName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,WoStatus,BillStatus From WorkOrder where Company='" + PubCompanyName + "' AND WorkOrderNo='" + WOLookUpEdit.Text + "' AND JobNo='" + WOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            VendorDA.Fill(VendorDT)

            If VendorDT.Rows.Count > 0 Then
                JobNoLabelControl.Text = VendorDT.Rows(0).Item("JobNoPrefix").ToString + VendorDT.Rows(0).Item("JobNo").ToString
                WONoLabelControl.Text = VendorDT.Rows(0).Item("WorkOrderNo").ToString
                OrderDateLabelControl.Text = VendorDT.Rows(0).Item("WorkOrderDate").ToString.Remove(10, 8)
                WOBasicAmtLabelControl.Text = VendorDT.Rows(0).Item("TotalAmtBeforeTax").ToString
                POAmountLabelControl.Text = VendorDT.Rows(0).Item("GrandTotalAmount").ToString
                AdvancePaaymentLabelControl.Text = VendorDT.Rows(0).Item("AdvancePayment").ToString
                POStatus.Text = VendorDT.Rows(0).Item("WoStatus").ToString
                BillStatus.Text = VendorDT.Rows(0).Item("BillStatus").ToString
                WorkOrderID = Convert.ToInt32(VendorDT.Rows(0).Item("WorkOrderID").ToString)

                Dim PartyDA As New OleDbDataAdapter
                Dim PartyDT As New DataTable
                PartyDA.SelectCommand = New OleDbCommand("Select PartyName From Party where Company='" + PubCompanyName + "' AND PartyName= '" + VendorDT.Rows(0).Item("ReceiverName").ToString + "'", ConStr)
                PartyDA.Fill(PartyDT)

                If PartyDT.Rows.Count > 0 Then
                    PartyNameLabelControl.Text = PartyDT.Rows(0).Item("PartyName").ToString
                End If
            End If

            Dim InvoiceDA As New OleDbDataAdapter
            Dim InvoiceDT As New DataTable
            InvoiceDA.SelectCommand = New OleDbCommand("Select WOBillID,InvoiceNo,JobNoPrefix,JobNo,WONo,TotalAmtBeforeTax,GrandTotalAmount From WOBill where Company='" + PubCompanyName + "' AND WONo='" + WOLookUpEdit.Text + "' AND JobNo='" + WOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            InvoiceDA.Fill(InvoiceDT)
            BillGridControl.DataSource = InvoiceDT

            NoOfInvoicesLabelControl.Text = InvoiceDT.Rows.Count.ToString

            DS = New DataSet
            PaymentBS = New BindingSource

            PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,WorkOrderID,JobNo,WONo,CheckNo,ChkDate,PaidAmount,TDSPer,TDS,Remarks,RemainAmount,IsPrinted,WOAmt,TotalPaidAmt From WorkOrderPayment Where WONo='" + WOLookUpEdit.Text + "' AND JobNo='" + WOJobNoComboBoxEdit.Text + "' Order By PaymentID", ConStr)
            PaymentDA.Fill(DS, "WorkOrderPayment")
            PaymentBS.DataSource = DS
            PaymentBS.DataMember = "WorkOrderPayment"
            PaymentGridControl.DataSource = PaymentBS

            Dim PaymentPOAmt As Double
            Dim diff As Decimal
            PaymentPOAmt = PaymentBS.Current!WOAmt
            Dim POAmt = Convert.ToDecimal(POAmountLabelControl.Text)
            If PaymentPOAmt < POAmt Then
                diff = POAmt - PaymentPOAmt
                PaymentBS.Current!RemainAmount = PaymentBS.Current!RemainAmount + diff
                Dim CmpCmd As New OleDbCommand("Update WorkOrderPayment Set RemainAmount=@RemainAmount,WOAmt=@WOAmt Where WorkOrderID=@WorkOrderID", ConStr)
                CmpCmd.Parameters.AddWithValue("@RemainAmount", PaymentBS.Current!RemainAmount)
                CmpCmd.Parameters.AddWithValue("@WOAmt", POAmountLabelControl.Text)
                CmpCmd.Parameters.AddWithValue("@WorkOrderID", PaymentBS.Current!WorkOrderID)
                CnnOpen()
                CmpCmd.ExecuteNonQuery()
                CnnClose()
            ElseIf PaymentPOAmt > POAmt Then
                diff = PaymentPOAmt - POAmt
                PaymentBS.Current!RemainAmount = PaymentBS.Current!RemainAmount - diff
                Dim CmpCmd As New OleDbCommand("Update WorkOrderPayment Set RemainAmount=@RemainAmount,WOAmt=@WOAmt Where WorkOrderID=@WorkOrderID", ConStr)
                CmpCmd.Parameters.AddWithValue("@RemainAmount", PaymentBS.Current!RemainAmount)
                CmpCmd.Parameters.AddWithValue("@WOAmt", POAmountLabelControl.Text)
                CmpCmd.Parameters.AddWithValue("@WorkOrderID", PaymentBS.Current!WorkOrderID)
                CnnOpen()
                CmpCmd.ExecuteNonQuery()
                CnnClose()
            Else
            End If
            SetQuery()
            setGrid()
            WOStatusCheck()
        End If
    End Sub

    'Sub WOStatusCheck()
    '    Dim PayAmount As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue) + Convert.ToDouble(PaymentGridView.Columns("TDS").SummaryItem.SummaryValue)
    '    'Dim GTAmount As Double = Convert.ToDouble(BillGridView.Columns("GrandTotalAmount").SummaryItem.SummaryValue)
    '    Dim POAmt As Double = Decimal.Parse(POAmountLabelControl.Text)
    '    If POAmt = PayAmount And POAmt <> 0 Then
    '        POStatus.Text = "WO is closed"
    '    Else
    '        POStatus.Text = "WO is open"
    '    End If
    'End Sub
    Sub WOStatusCheck()
        Dim BillBasicAmt As Double = Convert.ToDouble(BillGridView.Columns("TotalAmtBeforeTax").SummaryItem.SummaryValue)
        Dim POBasicAmt As Double = Convert.ToDouble(WOBasicAmtLabelControl.Text)

        Dim PayAmount As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue) + Convert.ToDouble(PaymentGridView.Columns("TDS").SummaryItem.SummaryValue)
        Dim POTotalAmt As Double = Convert.ToDouble(POAmountLabelControl.Text)

        'If BillBasicAmt = PayAmount And BillBasicAmt <> 0 Then
        '    POStatus.Text = "PO is closed"
        'Else
        '    POStatus.Text = "PO is open"
        'End If

        If BillBasicAmt >= POBasicAmt Then
            BillStatus.Text = "Bill Cycle is closed"
        Else
            BillStatus.Text = "Bill Cycle is open"
        End If

        If PayAmount = POTotalAmt And PayAmount <> 0 Then
            POStatus.Text = "WO is closed"
        Else
            POStatus.Text = "WO is open"
        End If
    End Sub

    Sub SetQuery()
        PaymentDA.InsertCommand = New OleDbCommand("Insert Into WorkOrderPayment (WorkOrderID,JobNo,WONo,CheckNo,ChkDate,PaidAmount,TDSPer,TDS,Remarks,RemainAmount,WOAmt,TotalPaidAmt) Values (@WorkOrderID,@JobNo,@WONo,@CheckNo,@ChkDate,@PaidAmount,@TDSPer,@TDS,@Remarks,@RemainAmount,@WOAmt,@TotalPaidAmt)", ConStr)
        PaymentDA.InsertCommand.Parameters.Add("@WorkOrderID", OleDbType.Integer, 4, "WorkOrderID")
        PaymentDA.InsertCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        PaymentDA.InsertCommand.Parameters.Add("@WONo", OleDbType.VarChar, 50, "WONo")
        PaymentDA.InsertCommand.Parameters.Add("@CheckNo", OleDbType.VarChar, 50, "CheckNo")
        PaymentDA.InsertCommand.Parameters.Add("@ChkDate", OleDbType.Date, 3, "ChkDate")
        PaymentDA.InsertCommand.Parameters.Add("@PaidAmount", OleDbType.Double, 8, "PaidAmount")
        PaymentDA.InsertCommand.Parameters.Add("@TDSPer", OleDbType.Double, 8, "TDSPer")
        PaymentDA.InsertCommand.Parameters.Add("@TDS", OleDbType.Double, 8, "TDS")
        PaymentDA.InsertCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 250, "Remarks")
        PaymentDA.InsertCommand.Parameters.Add("@RemainAmount", OleDbType.Double, 8, "RemainAmount")
        PaymentDA.InsertCommand.Parameters.Add("@WOAmt", OleDbType.Double, 8, "WOAmt")
        PaymentDA.InsertCommand.Parameters.Add("@TotalPaidAmt", OleDbType.Double, 8, "TotalPaidAmt")

        PaymentDA.UpdateCommand = New OleDbCommand("Update WorkOrderPayment Set WorkOrderID=@WorkOrderID,JobNo=@JobNo,WONo=@WONo,CheckNo=@CheckNo,ChkDate=@ChkDate,PaidAmount=@PaidAmount,TDSPer=@TDSPer,TDS=@TDS,Remarks=@Remarks,RemainAmount=@RemainAmount,WOAmt=@WOAmt,TotalPaidAmt=@TotalPaidAmt Where PaymentID=@PaymentID", ConStr)
        PaymentDA.UpdateCommand.Parameters.Add("@WorkOrderID", OleDbType.Integer, 4, "WorkOrderID")
        PaymentDA.UpdateCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        PaymentDA.UpdateCommand.Parameters.Add("@WONo", OleDbType.VarChar, 50, "WONo")
        PaymentDA.UpdateCommand.Parameters.Add("@CheckNo", OleDbType.VarChar, 50, "CheckNo")
        PaymentDA.UpdateCommand.Parameters.Add("@ChkDate", OleDbType.Date, 3, "ChkDate")
        PaymentDA.UpdateCommand.Parameters.Add("@PaidAmount", OleDbType.Double, 8, "PaidAmount")
        PaymentDA.UpdateCommand.Parameters.Add("@TDSPer", OleDbType.Double, 8, "TDSPer")
        PaymentDA.UpdateCommand.Parameters.Add("@TDS", OleDbType.Double, 8, "TDS")
        PaymentDA.UpdateCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 250, "Remarks")
        PaymentDA.UpdateCommand.Parameters.Add("@RemainAmount", OleDbType.Double, 8, "RemainAmount")
        PaymentDA.UpdateCommand.Parameters.Add("@WOAmt", OleDbType.Double, 8, "WOAmt")
        PaymentDA.UpdateCommand.Parameters.Add("@TotalPaidAmt", OleDbType.Double, 8, "TotalPaidAmt")
        PaymentDA.UpdateCommand.Parameters.Add("@PaymentID", OleDbType.Integer, 4, "PaymentID")

        PaymentDA.DeleteCommand = New OleDbCommand("Delete From WorkOrderPayment Where PaymentID=@PaymentID", ConStr)
        PaymentDA.DeleteCommand.Parameters.Add("@PaymentID", OleDbType.Integer, 4, "PaymentID")
    End Sub

    Sub setGrid()
        With BillGridView
            .Columns("WOBillID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("InvoiceNo").OptionsColumn.AllowFocus = False
            .Columns("InvoiceNo").OptionsColumn.ReadOnly = True
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("WONo").OptionsColumn.AllowFocus = False
            .Columns("WONo").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Bill Basic Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "Bill Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        BillGridView.BestFitColumns()
        BillGridView.Columns("GrandTotalAmount").Width = 150

        With PaymentGridView
            .Columns("PaymentID").Visible = False
            .Columns("WorkOrderID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("WONo").OptionsColumn.AllowFocus = False
            .Columns("WONo").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True
            .Columns("IsPrinted").OptionsColumn.AllowFocus = False
            .Columns("IsPrinted").OptionsColumn.ReadOnly = True
            .Columns("TDSPer").Caption = "TDS %"
            .Columns("TDS").Caption = "TDS Val"
            .Columns("WOAmt").Visible = False
            .Columns("TotalPaidAmt").Visible = False

            .OptionsView.ShowFooter = True
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        PaymentGridView.BestFitColumns()
        PaymentGridView.Columns("JobNo").Width = 50
        PaymentGridView.Columns("WONo").Width = 90
        PaymentGridView.Columns("PaidAmount").Width = 80
        PaymentGridView.Columns("PaidAmount").Caption = "Paid Amt"
        PaymentGridView.Columns("TDSPer").Width = 50
        PaymentGridView.Columns("TDS").Width = 50
        PaymentGridView.Columns("Remarks").Width = 200
        PaymentGridView.Columns("CheckNo").Width = 60
        PaymentGridView.Columns("CheckNo").Caption = "Chk No"
        PaymentGridView.Columns("ChkDate").Width = 60
        PaymentGridView.Columns("RemainAmount").Caption = "Remain Amt"
        PaymentGridView.Columns("RemainAmount").Width = 80
    End Sub

    Private Sub VendorPOJobNoComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles WOJobNoComboBoxEdit.SelectedIndexChanged
        If WOJobNoComboBoxEdit.Text <> "" Then
            SetLookUp("Select WorkOrderID, WorkOrderNo FROM WorkOrder Where Company='" + PubCompanyName + "' AND JobNo='" + WOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", "WorkOrder", "WorkOrderID", "WorkOrderNo", WOLookUpEdit, "")
        End If
    End Sub

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveSimpleButton.Click
        If PaymentBS.Count > 0 Then
            Dim totalPaid As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue)
            PaymentBS.Current!TotalPaidAmt = totalPaid
            PaymentBS.Current!WOAmt = Convert.ToDouble(POAmountLabelControl.Text)
            PaymentBS.Current!IsPrinted = False
            PaymentBS.EndEdit()
            PaymentDA.Update(DS.Tables("WorkOrderPayment"))

            WOStatusCheck()
            Dim POCmd As New OleDbCommand("Update WorkOrder set WoStatus=@WoStatus,BillStatus=@BillStatus where WorkOrderID=@WorkOrderID", ConStr)
            POCmd.Parameters.AddWithValue("@WoStatus", POStatus.Text)
            POCmd.Parameters.AddWithValue("@BillStatus", BillStatus.Text)
            POCmd.Parameters.AddWithValue("@WorkOrderID", WorkOrderID)
            CnnOpen()
            POCmd.ExecuteNonQuery()
            CnnClose()

            Dim save = XtraMessageBox.Show("Payment saved successfully", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            DS.Clear()
            PaymentDA.Fill(DS, "WorkOrderPayment")
            PaymentBS.DataSource = DS
            PaymentBS.DataMember = "WorkOrderPayment"
            PaymentGridControl.DataSource = PaymentBS
            WOStatusCheck()
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub PaymentGridView_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles PaymentGridView.CellValueChanged
        'Try
        '    If e.Column.FieldName = "PaidAmount" Or e.Column.FieldName = "TDS" Then
        '        If PaymentBS.Count > 1 And POAmountLabelControl.Text <> 0 Then
        '            PaymentBS.MovePrevious()
        '            Dim PreValue = PaymentBS.Current!RemainAmount
        '            PaymentBS.MoveNext()
        '            PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
        '            PaymentBS.EndEdit()
        '        ElseIf POAmountLabelControl.Text <> 0 Then
        '            PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
        '            PaymentBS.EndEdit()
        '        End If
        '        WOStatusCheck()
        '    End If
        'Catch ex As Exception
        '    'ErtOccur(ex)
        'End Try
        Try
            If e.Column.FieldName = "PaidAmount" Or e.Column.FieldName = "TDSPer" Or e.Column.FieldName = "TDS" Then
                If PaymentBS.Count > 1 And POAmountLabelControl.Text <> 0 Then
                    PaymentBS.MovePrevious()
                    Dim PreValue = PaymentBS.Current!RemainAmount
                    PaymentBS.MoveNext()
                    If e.Column.FieldName = "TDSPer" Then
                        Dim TDSAmt As Double = WOBasicAmtLabelControl.Text * IIf(PaymentBS.Current!TDSPer Is DBNull.Value, 0, PaymentBS.Current!TDSPer) / 100
                        'PaymentBS.Current!RemainAmount = Math.Round(IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + TDSAmt))
                        PaymentBS.Current!TDS = Math.Round(TDSAmt, 2)
                    End If
                    If e.Column.FieldName = "TDS" Then
                        Dim TdsPer As Double = IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS) * 100 / WOBasicAmtLabelControl.Text
                        'PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                        PaymentBS.Current!TDSPer = Math.Round(TdsPer, 2)
                    End If
                    PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                    PaymentBS.EndEdit()

                ElseIf POAmountLabelControl.Text <> 0 Then
                    If e.Column.FieldName = "TDSPer" Then
                        Dim TDSAmt As Double = WOBasicAmtLabelControl.Text * IIf(PaymentBS.Current!TDSPer Is DBNull.Value, 0, PaymentBS.Current!TDSPer) / 100
                        'PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + TDSAmt)                    
                        PaymentBS.Current!TDS = Math.Round(TDSAmt, 2)
                    End If
                    If e.Column.FieldName = "TDS" Then
                        Dim TdsPer As Double = IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS) * 100 / WOBasicAmtLabelControl.Text
                        'PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                        PaymentBS.Current!TDSPer = Math.Round(TdsPer, 2)
                    End If
                    PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                    PaymentBS.EndEdit()
                End If
                WOStatusCheck()
            End If
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub AddSimpleButton_Click(sender As Object, e As EventArgs) Handles AddSimpleButton.Click
        PaymentBS.AddNew()
        PaymentBS.Current!JobNo = WOJobNoComboBoxEdit.Text
        PaymentBS.Current!WONo = WOLookUpEdit.Text
        PaymentBS.Current!WorkOrderID = WorkOrderID
        PaymentBS.EndEdit()
    End Sub

    Private Sub PaymentGridView_ValidateRow(sender As Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles PaymentGridView.ValidateRow
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim Paid As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("PaidAmount")
        Dim Remain As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("RemainAmount")

        Dim PaidAmt As Double = If(View.GetRowCellValue(e.RowHandle, Paid) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Paid), Double))
        Dim RemainAmt As Double = If(View.GetRowCellValue(e.RowHandle, Remain) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Remain), Double))

        If PaidAmt = 0 Then
            e.Valid = False
            View.SetColumnError(Paid, "Enter Amount")
        End If
        If RemainAmt < 0 Then
            e.Valid = False
            View.SetColumnError(Remain, "Negative Amount")
        End If
    End Sub

    Private Sub PaymentGridView_InvalidRowException(sender As Object, e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles PaymentGridView.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub PaymentGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles PaymentGridView.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to delete the Payment?" & vbCrLf & vbCrLf & "Click Yes to delete and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                PaymentBS.RemoveCurrent()
                PaymentBS.EndEdit()
            End If
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If PaymentBS.Count = 0 Then
            XtraMessageBox.Show("Please select the Payment first.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim AmtWord As String
            AmtWord = AmtInWord(PaymentBS.Current!PaidAmount)
            AmtWord = AmtWord.Replace("Rupees", "")
            Dim Amtdate As Date
            Amtdate = PaymentBS.Current!ChkDate
            Dim Dt As String
            Dt = Amtdate.ToString("dd/MM/yyyy")
            Dt = Dt.Replace("-", " ")
            Dim Paid As String
            Paid = CType((PaymentBS.Current!PaidAmount), String)
            Dim Rmrk As String
            If Not PaymentBS.Current!Remarks Is DBNull.Value Then
                Rmrk = PaymentBS.Current!Remarks
            End If
            'Rmrk = PaymentBS.Current!Remarks
            Dim tds As String
            If Not PaymentBS.Current!TDS Is DBNull.Value Then
                tds = CType((PaymentBS.Current!TDS), String)
            End If
            'Dim Rpt As New XR_ChequePrintNew

            'Rpt.FillDataSource()
            'Rpt.ToXrLabel.Text = PartyNameLabelControl.Text
            'Rpt.AmtXrLabel.Text = Paid + "/-"
            'For Each c As Char In Dt
            '    Rpt.DateXrLabel.Text = Rpt.DateXrLabel.Text + c + " "
            'Next

            'Rpt.AmtWordXrLabel.Text = AmtWord
            'Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
            'ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Cheque -")
            'End_Waiting()
            'Rpt.ShowRibbonPreviewDialog()
            If PaymentBS.Current!IsPrinted Then
                Dim Frm As New Frm_ChequeAuth()
                Frm.ShowDialog()
                If Frm.DialogResult = DialogResult.OK Then
                    Dim Rpt As New XR_ChequePrintNew
                    Rpt.FillDataSource()
                    Rpt.ToXrLabel.Text = PartyNameLabelControl.Text
                    Rpt.AmtXrLabel.Text = Paid + "/-"
                    For Each c As Char In Dt
                        Rpt.DateXrLabel.Text = Rpt.DateXrLabel.Text + c + " "
                    Next

                    Rpt.AmtWordXrLabel.Text = AmtWord
                    Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Cheque -")
                    End_Waiting()
                    Rpt.ShowRibbonPreviewDialog()
                    Dim Rpt1 As New XR_VoucherPrintWO
                    Rpt1.FillDataSource()
                    Rpt1.CmpXrLabel.Text = PubCompanyName

                    Dim sda As New OleDbDataAdapter("Select BankName FROM Bank Where Company = '" + PubCompanyName + "'", ConStr)
                    Dim dt1 As New DataTable()
                    sda.Fill(dt1)
                    If (dt1.Rows.Count > 0) Then
                        Rpt1.BankNameXrLabel.Text = dt1.Rows(0).Item("BankName").ToString
                    End If
                    Rpt1.ToXrLabel.Text = PartyNameLabelControl.Text
                    Rpt1.JobNoXrLabel.Text = JobNoLabelControl.Text
                    Rpt1.PONoXrLabel.Text = WONoLabelControl.Text
                    Rpt1.ChqNoXrLabel.Text = PaymentBS.Current!CheckNo
                    Rpt1.DateXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")
                    Rpt1.RemarksXrLabel.Text = Rmrk
                    Rpt1.DrAmtXrLabel.Text = Paid
                    Rpt1.TDSAmtXrLabel.Text = tds
                    Rpt1.SumDrXrLabel.Text = Paid
                    Rpt1.SumTDSXrLabel.Text = tds
                    Rpt1.UserXrLabel.Text = CurrentUserName.ToString()
                    Rpt1.DtXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")

                    Dim ReportPrintTool1 = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool1.PreviewRibbonForm.Text = String.Format("- Voucher -")
                    End_Waiting()
                    Rpt1.ShowRibbonPreviewDialog()
                End If
            Else
                Dim newCmd As New OleDbCommand("Update WorkOrderPayment set IsPrinted=@IsPrinted where PaymentID=@PaymentID", ConStr)
                newCmd.Parameters.AddWithValue("@IsPrinted", True)
                newCmd.Parameters.AddWithValue("@PaymentID", PaymentBS.Current!PaymentID)
                CnnOpen()
                newCmd.ExecuteNonQuery()
                CnnClose()

                DS.Clear()
                PaymentDA.Fill(DS, "WorkOrderPayment")
                PaymentBS.DataSource = DS
                PaymentBS.DataMember = "WorkOrderPayment"
                PaymentGridControl.DataSource = PaymentBS


                Dim Rpt As New XR_ChequePrintNew
                Rpt.FillDataSource()
                Rpt.ToXrLabel.Text = PartyNameLabelControl.Text
                Rpt.AmtXrLabel.Text = Paid + "/-"
                For Each c As Char In Dt
                    Rpt.DateXrLabel.Text = Rpt.DateXrLabel.Text + c + " "
                Next

                Rpt.AmtWordXrLabel.Text = AmtWord
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Cheque -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()

                Dim Rpt1 As New XR_VoucherPrintWO
                Rpt1.FillDataSource()
                Rpt1.CmpXrLabel.Text = PubCompanyName

                Dim sda As New OleDbDataAdapter("Select BankName FROM Bank Where Company = '" + PubCompanyName + "'", ConStr)
                Dim dt1 As New DataTable()
                sda.Fill(dt1)
                If (dt1.Rows.Count > 0) Then
                    Rpt1.BankNameXrLabel.Text = dt1.Rows(0).Item("BankName").ToString
                End If
                Rpt1.ToXrLabel.Text = PartyNameLabelControl.Text
                Rpt1.JobNoXrLabel.Text = JobNoLabelControl.Text
                Rpt1.PONoXrLabel.Text = WONoLabelControl.Text
                Rpt1.ChqNoXrLabel.Text = PaymentBS.Current!CheckNo
                Rpt1.DateXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")
                Rpt1.RemarksXrLabel.Text = Rmrk
                Rpt1.DrAmtXrLabel.Text = Paid
                Rpt1.TDSAmtXrLabel.Text = tds
                Rpt1.SumDrXrLabel.Text = Paid
                Rpt1.SumTDSXrLabel.Text = tds
                Rpt1.UserXrLabel.Text = CurrentUserName.ToString()
                Rpt1.DtXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")


                Dim ReportPrintTool1 = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool1.PreviewRibbonForm.Text = String.Format("- Voucher -")
                End_Waiting()
                Rpt1.ShowRibbonPreviewDialog()
            End If
        End If
    End Sub

    Private Sub PrintSimpleButton_Click(sender As Object, e As EventArgs) Handles PrintSimpleButton.Click
        Dim Rpt As New XR_WorkOrderPayment
        Rpt.Invoice.Value = WorkOrderID
        Rpt.Invoice.Visible = False
        Rpt.FillDataSource()

        Rpt.JobNoXrLabel.Text = JobNoLabelControl.Text
        Rpt.PONoXrLabel.Text = WONoLabelControl.Text
        Rpt.DateXrLabel.Text = OrderDateLabelControl.Text
        Rpt.POBAmtXrLabel.Text = WOBasicAmtLabelControl.Text
        Rpt.POTAmtXrLabel.Text = POAmountLabelControl.Text
        Rpt.AdvanceXrLabel.Text = AdvancePaaymentLabelControl.Text
        Rpt.POStXrLabel.Text = POStatus.Text
        Rpt.BillStXrLabel.Text = BillStatus.Text
        Rpt.PartyNameXrLabel.Text = PartyNameLabelControl.Text

        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- WorkOrder Payment -")
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub
End Class