﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_SearchPO
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_SearchPO))
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.VendorPOJobNoComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.StatusLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelPanel = New DevExpress.XtraEditors.PanelControl()
        Me.TotalAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.NoOfInvoicesLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.AdvancePaaymentLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.POAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PartyNameLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.OrderDateLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PONoLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.JobNoLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.VendorPOButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ClientPOButton = New DevExpress.XtraEditors.SimpleButton()
        Me.VendorPOLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ClientPOLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.ClientPOJobNoComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.VendorPOJobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LabelPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VendorPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        CType(Me.ClientPOJobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.ClientPOJobNoComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.VendorPOJobNoComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.StatusLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl14)
        Me.PanelCtrl.Controls.Add(Me.LabelControl10)
        Me.PanelCtrl.Controls.Add(Me.LabelControl11)
        Me.PanelCtrl.Controls.Add(Me.LabelPanel)
        Me.PanelCtrl.Controls.Add(Me.TotalAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl12)
        Me.PanelCtrl.Controls.Add(Me.LabelControl9)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.NoOfInvoicesLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl15)
        Me.PanelCtrl.Controls.Add(Me.AdvancePaaymentLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl13)
        Me.PanelCtrl.Controls.Add(Me.POAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PartyNameLabelControl)
        Me.PanelCtrl.Controls.Add(Me.OrderDateLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PONoLabelControl)
        Me.PanelCtrl.Controls.Add(Me.JobNoLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl7)
        Me.PanelCtrl.Controls.Add(Me.LabelControl6)
        Me.PanelCtrl.Controls.Add(Me.LabelControl5)
        Me.PanelCtrl.Controls.Add(Me.LabelControl4)
        Me.PanelCtrl.Controls.Add(Me.LabelControl3)
        Me.PanelCtrl.Controls.Add(Me.VendorPOButton)
        Me.PanelCtrl.Controls.Add(Me.ClientPOButton)
        Me.PanelCtrl.Controls.Add(Me.VendorPOLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Controls.Add(Me.ClientPOLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(796, 541)
        Me.PanelCtrl.TabIndex = 0
        '
        'VendorPOJobNoComboBoxEdit
        '
        Me.VendorPOJobNoComboBoxEdit.Location = New System.Drawing.Point(50, 31)
        Me.VendorPOJobNoComboBoxEdit.MenuManager = Me.BarManager1
        Me.VendorPOJobNoComboBoxEdit.Name = "VendorPOJobNoComboBoxEdit"
        Me.VendorPOJobNoComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.VendorPOJobNoComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.VendorPOJobNoComboBoxEdit.Size = New System.Drawing.Size(119, 20)
        Me.VendorPOJobNoComboBoxEdit.TabIndex = 33
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem})
        Me.BarManager1.MaxItemId = 4
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.Caption = "New"
        Me.NewBarButtonItem.Glyph = CType(resources.GetObject("NewBarButtonItem.Glyph"), System.Drawing.Image)
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.LargeGlyph = CType(resources.GetObject("NewBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.Caption = "Open"
        Me.OpenBarButtonItem.Glyph = CType(resources.GetObject("OpenBarButtonItem.Glyph"), System.Drawing.Image)
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.LargeGlyph = CType(resources.GetObject("OpenBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Glyph = CType(resources.GetObject("DeleteBarButtonItem.Glyph"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.LargeGlyph = CType(resources.GetObject("DeleteBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(885, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 600)
        Me.barDockControlBottom.Size = New System.Drawing.Size(885, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 569)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(885, 31)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 569)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.Caption = "Print"
        Me.PrintBarButtonItem.Glyph = CType(resources.GetObject("PrintBarButtonItem.Glyph"), System.Drawing.Image)
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.LargeGlyph = CType(resources.GetObject("PrintBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'StatusLabelControl
        '
        Me.StatusLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.StatusLabelControl.Location = New System.Drawing.Point(119, 297)
        Me.StatusLabelControl.Name = "StatusLabelControl"
        Me.StatusLabelControl.Size = New System.Drawing.Size(37, 13)
        Me.StatusLabelControl.TabIndex = 32
        Me.StatusLabelControl.Text = "Status"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(7, 297)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl14.TabIndex = 31
        Me.LabelControl14.Text = "Status                        :"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(443, 68)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl10.TabIndex = 26
        Me.LabelControl10.Text = "Bill/Invoice No."
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(647, 68)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl11.TabIndex = 27
        Me.LabelControl11.Text = "Amount"
        '
        'LabelPanel
        '
        Me.LabelPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelPanel.Location = New System.Drawing.Point(386, 87)
        Me.LabelPanel.Name = "LabelPanel"
        Me.LabelPanel.Size = New System.Drawing.Size(349, 449)
        Me.LabelPanel.TabIndex = 30
        '
        'TotalAmountLabelControl
        '
        Me.TotalAmountLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TotalAmountLabelControl.Location = New System.Drawing.Point(119, 269)
        Me.TotalAmountLabelControl.Name = "TotalAmountLabelControl"
        Me.TotalAmountLabelControl.Size = New System.Drawing.Size(77, 13)
        Me.TotalAmountLabelControl.TabIndex = 29
        Me.TotalAmountLabelControl.Text = "Total Amount"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(7, 269)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl12.TabIndex = 28
        Me.LabelControl12.Text = "Total Amount             :"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(7, 8)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl9.TabIndex = 24
        Me.LabelControl9.Text = "Job No."
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(7, 34)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl8.TabIndex = 22
        Me.LabelControl8.Text = "Job No."
        '
        'NoOfInvoicesLabelControl
        '
        Me.NoOfInvoicesLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NoOfInvoicesLabelControl.Location = New System.Drawing.Point(707, 21)
        Me.NoOfInvoicesLabelControl.Name = "NoOfInvoicesLabelControl"
        Me.NoOfInvoicesLabelControl.Size = New System.Drawing.Size(14, 13)
        Me.NoOfInvoicesLabelControl.TabIndex = 21
        Me.NoOfInvoicesLabelControl.Text = "No"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(621, 21)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl15.TabIndex = 20
        Me.LabelControl15.Text = "No. of Invoices :"
        '
        'AdvancePaaymentLabelControl
        '
        Me.AdvancePaaymentLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.AdvancePaaymentLabelControl.Location = New System.Drawing.Point(119, 241)
        Me.AdvancePaaymentLabelControl.Name = "AdvancePaaymentLabelControl"
        Me.AdvancePaaymentLabelControl.Size = New System.Drawing.Size(103, 13)
        Me.AdvancePaaymentLabelControl.TabIndex = 19
        Me.AdvancePaaymentLabelControl.Text = "Advance Payment"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(7, 241)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl13.TabIndex = 18
        Me.LabelControl13.Text = "Advance Payment     :"
        '
        'POAmountLabelControl
        '
        Me.POAmountLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.POAmountLabelControl.Location = New System.Drawing.Point(119, 213)
        Me.POAmountLabelControl.Name = "POAmountLabelControl"
        Me.POAmountLabelControl.Size = New System.Drawing.Size(63, 13)
        Me.POAmountLabelControl.TabIndex = 17
        Me.POAmountLabelControl.Text = "PO Amount"
        '
        'PartyNameLabelControl
        '
        Me.PartyNameLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.PartyNameLabelControl.Location = New System.Drawing.Point(119, 185)
        Me.PartyNameLabelControl.Name = "PartyNameLabelControl"
        Me.PartyNameLabelControl.Size = New System.Drawing.Size(66, 13)
        Me.PartyNameLabelControl.TabIndex = 16
        Me.PartyNameLabelControl.Text = "Party Name"
        '
        'OrderDateLabelControl
        '
        Me.OrderDateLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.OrderDateLabelControl.Location = New System.Drawing.Point(119, 157)
        Me.OrderDateLabelControl.Name = "OrderDateLabelControl"
        Me.OrderDateLabelControl.Size = New System.Drawing.Size(62, 13)
        Me.OrderDateLabelControl.TabIndex = 15
        Me.OrderDateLabelControl.Text = "Order Date"
        '
        'PONoLabelControl
        '
        Me.PONoLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.PONoLabelControl.Location = New System.Drawing.Point(119, 128)
        Me.PONoLabelControl.Name = "PONoLabelControl"
        Me.PONoLabelControl.Size = New System.Drawing.Size(52, 13)
        Me.PONoLabelControl.TabIndex = 14
        Me.PONoLabelControl.Text = "Order No."
        '
        'JobNoLabelControl
        '
        Me.JobNoLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.JobNoLabelControl.Location = New System.Drawing.Point(119, 100)
        Me.JobNoLabelControl.Name = "JobNoLabelControl"
        Me.JobNoLabelControl.Size = New System.Drawing.Size(40, 13)
        Me.JobNoLabelControl.TabIndex = 13
        Me.JobNoLabelControl.Text = "Job No."
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 213)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "PO Amount                :"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(7, 185)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl6.TabIndex = 11
        Me.LabelControl6.Text = "Party Name               :"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(7, 157)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Order Date                :"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(7, 128)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl4.TabIndex = 9
        Me.LabelControl4.Text = "PO No.                       :"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 100)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl3.TabIndex = 8
        Me.LabelControl3.Text = "Job No.                      :"
        '
        'VendorPOButton
        '
        Me.VendorPOButton.Image = CType(resources.GetObject("VendorPOButton.Image"), System.Drawing.Image)
        Me.VendorPOButton.Location = New System.Drawing.Point(468, 31)
        Me.VendorPOButton.Name = "VendorPOButton"
        Me.VendorPOButton.Size = New System.Drawing.Size(75, 20)
        Me.VendorPOButton.TabIndex = 7
        Me.VendorPOButton.Text = "Search"
        '
        'ClientPOButton
        '
        Me.ClientPOButton.Image = CType(resources.GetObject("ClientPOButton.Image"), System.Drawing.Image)
        Me.ClientPOButton.Location = New System.Drawing.Point(468, 5)
        Me.ClientPOButton.Name = "ClientPOButton"
        Me.ClientPOButton.Size = New System.Drawing.Size(75, 20)
        Me.ClientPOButton.TabIndex = 3
        Me.ClientPOButton.Text = "Search"
        '
        'VendorPOLookUpEdit
        '
        Me.VendorPOLookUpEdit.Location = New System.Drawing.Point(252, 31)
        Me.VendorPOLookUpEdit.Name = "VendorPOLookUpEdit"
        Me.VendorPOLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.VendorPOLookUpEdit.Properties.NullText = ""
        Me.VendorPOLookUpEdit.Size = New System.Drawing.Size(210, 20)
        Me.VendorPOLookUpEdit.TabIndex = 6
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(175, 34)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Vendor PO No."
        '
        'ClientPOLookUpEdit
        '
        Me.ClientPOLookUpEdit.Location = New System.Drawing.Point(252, 5)
        Me.ClientPOLookUpEdit.Name = "ClientPOLookUpEdit"
        Me.ClientPOLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ClientPOLookUpEdit.Properties.NullText = ""
        Me.ClientPOLookUpEdit.Size = New System.Drawing.Size(210, 20)
        Me.ClientPOLookUpEdit.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(175, 8)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Client PO No."
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(12, 37)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(806, 551)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'ClientPOJobNoComboBoxEdit
        '
        Me.ClientPOJobNoComboBoxEdit.Location = New System.Drawing.Point(50, 5)
        Me.ClientPOJobNoComboBoxEdit.MenuManager = Me.BarManager1
        Me.ClientPOJobNoComboBoxEdit.Name = "ClientPOJobNoComboBoxEdit"
        Me.ClientPOJobNoComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ClientPOJobNoComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ClientPOJobNoComboBoxEdit.Size = New System.Drawing.Size(119, 20)
        Me.ClientPOJobNoComboBoxEdit.TabIndex = 34
        '
        'Frm_SearchPO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(885, 600)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_SearchPO"
        Me.Text = "Party"
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.VendorPOJobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LabelPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VendorPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        CType(Me.ClientPOJobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents VendorPOLookUpEdit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ClientPOLookUpEdit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents VendorPOButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ClientPOButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents POAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PartyNameLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OrderDateLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PONoLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JobNoLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AdvancePaaymentLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NoOfInvoicesLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents StatusLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents VendorPOJobNoComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ClientPOJobNoComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
End Class
