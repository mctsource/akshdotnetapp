﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI
Imports System.Data.OleDb
'Imports ELCF
Public Class Frm_VenderPaymentNew
    Dim DS As New DataSet

    Dim PaymentDA As New OleDbDataAdapter
    Dim PaymentBS As New BindingSource

    Dim PurchaseID As Integer = 0
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM VendorPO Where Company='" + PubCompanyName + "'", "VenderPO", VendorPOJobNoComboBoxEdit)
    End Sub

    Function VendorPOValidation() As Boolean
        If VendorPOLookUpEdit.EditValue Is DBNull.Value Then
            VendorPOLookUpEdit.Focus()
            Return False
        ElseIf VendorPOJobNoComboBoxEdit.Text = "" Then
            VendorPOJobNoComboBoxEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub VendorPOButton_Click(sender As Object, e As EventArgs) Handles VendorPOButton.Click
        If VendorPOValidation() Then
            Dim VendorDA As New OleDbDataAdapter
            Dim VendorDT As New DataTable
            VendorDA.SelectCommand = New OleDbCommand("Select PurchaseID,JobNoPrefix,JobNo,OrderNo,OrderDate,ConsignorName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus,BillStatus From VendorPO where Company='" + PubCompanyName + "' AND OrderNo='" + VendorPOLookUpEdit.Text + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            VendorDA.Fill(VendorDT)

            If VendorDT.Rows.Count > 0 Then
                JobNoLabelControl.Text = VendorDT.Rows(0).Item("JobNoPrefix").ToString + VendorDT.Rows(0).Item("JobNo").ToString
                PONoLabelControl.Text = VendorDT.Rows(0).Item("OrderNo").ToString
                OrderDateLabelControl.Text = VendorDT.Rows(0).Item("OrderDate").ToString.Remove(10, 8)
                POAmountLabelControl.Text = VendorDT.Rows(0).Item("GrandTotalAmount").ToString
                POBasicAmtLabelControl.Text = VendorDT.Rows(0).Item("TotalAmtBeforeTax").ToString
                AdvancePaaymentLabelControl.Text = VendorDT.Rows(0).Item("AdvancePayment").ToString
                POStatus.Text = VendorDT.Rows(0).Item("POStatus").ToString
                BillStatus.Text = VendorDT.Rows(0).Item("BillStatus").ToString
                PurchaseID = Convert.ToInt32(VendorDT.Rows(0).Item("PurchaseID").ToString)

                Dim PartyDA As New OleDbDataAdapter
                Dim PartyDT As New DataTable
                PartyDA.SelectCommand = New OleDbCommand("Select PartyName From Party where Company='" + PubCompanyName + "' AND PartyName= '" + VendorDT.Rows(0).Item("ConsignorName").ToString + "'", ConStr)
                PartyDA.Fill(PartyDT)

                If PartyDT.Rows.Count > 0 Then
                    PartyNameLabelControl.Text = PartyDT.Rows(0).Item("PartyName").ToString
                End If
            End If

            Dim InvoiceDA As New OleDbDataAdapter
            Dim InvoiceDT As New DataTable
            InvoiceDA.SelectCommand = New OleDbCommand("Select BillID,BillNo,JobNoPrefix,JobNo,PONo,TotalAmtBeforeTax,GrandTotalAmount From BillNew where Company='" + PubCompanyName + "' AND PONo='" + VendorPOLookUpEdit.Text + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            InvoiceDA.Fill(InvoiceDT)
            BillGridControl.DataSource = InvoiceDT

            NoOfInvoicesLabelControl.Text = InvoiceDT.Rows.Count.ToString

            DS = New DataSet
            PaymentBS = New BindingSource
            PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDSPer,TDS,Remarks,RemainAmount,IsPrinted,POAmt,TotalPaidAmt From VendorPayment Where PONo='" + VendorPOLookUpEdit.Text + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text + "' Order By PaymentID", ConStr)
            PaymentDA.Fill(DS, "VendorPayment")
            PaymentBS.DataSource = DS
            PaymentBS.DataMember = "VendorPayment"
            PaymentGridControl.DataSource = PaymentBS

            Dim POAmtDA As New OleDbDataAdapter
            Dim POAmtDT As New DataTable
            POAmtDA.SelectCommand = New OleDbCommand("Select PaymentID,RemainAmount,POAmt From VendorPayment Where PaymentID IN(Select Max(PaymentID) From VendorPayment WHERE PurchaseID = " + PurchaseID.ToString + ")", ConStr)
            POAmtDA.Fill(POAmtDT)
            Dim PaymentPOAmt As Double
            Dim RemainAmt As Double
            Dim PayID As Integer
            If POAmtDT.Rows.Count > 0 Then
                PayID = POAmtDT.Rows(0).Item("PaymentID").ToString
                RemainAmt = POAmtDT.Rows(0).Item("RemainAmount").ToString
                PaymentPOAmt = POAmtDT.Rows(0).Item("POAmt").ToString
            End If

            Dim diff As Decimal
            'PaymentPOAmt = PaymentBS.Current!POAmt
            Dim POAmt = Convert.ToDecimal(POAmountLabelControl.Text)
            If PaymentPOAmt < POAmt Then
                diff = POAmt - PaymentPOAmt
                RemainAmt = RemainAmt + diff
                'PaymentBS.Current!RemainAmount = PaymentBS.Current!RemainAmount + diff
                Dim CmpCmd As New OleDbCommand("Update VendorPayment Set RemainAmount=@RemainAmount,POAmt=@POAmt Where PaymentID=@PaymentID", ConStr)
                CmpCmd.Parameters.AddWithValue("@RemainAmount", RemainAmt.ToString)
                CmpCmd.Parameters.AddWithValue("@POAmt", POAmountLabelControl.Text)
                CmpCmd.Parameters.AddWithValue("@PaymentID", PayID.ToString)
                CnnOpen()
                CmpCmd.ExecuteNonQuery()
                CnnClose()
            ElseIf PaymentPOAmt > POAmt Then
                diff = PaymentPOAmt - POAmt
                RemainAmt = RemainAmt - diff
                'PaymentBS.Current!RemainAmount = PaymentBS.Current!RemainAmount - diff
                Dim CmpCmd As New OleDbCommand("Update VendorPayment Set RemainAmount=@RemainAmount,POAmt=@POAmt Where PaymentID=@PaymentID", ConStr)
                CmpCmd.Parameters.AddWithValue("@RemainAmount", RemainAmt.ToString)
                CmpCmd.Parameters.AddWithValue("@POAmt", POAmountLabelControl.Text)
                CmpCmd.Parameters.AddWithValue("@PaymentID", PayID.ToString)
                CnnOpen()
                CmpCmd.ExecuteNonQuery()
                CnnClose()
            Else
            End If
            SetQuery()
            setGrid()
            POStatusCheck()

            PaymentGridControl.DataSource = PaymentBS
        End If
    End Sub

    Sub POStatusCheck()
        Dim BillBasicAmt As Double = Convert.ToDouble(BillGridView.Columns("TotalAmtBeforeTax").SummaryItem.SummaryValue)
        Dim POBasicAmt As Double = Convert.ToDouble(POBasicAmtLabelControl.Text)

        Dim PayAmount As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue) + Convert.ToDouble(PaymentGridView.Columns("TDS").SummaryItem.SummaryValue)
        Dim POTotalAmt As Double = Convert.ToDouble(POAmountLabelControl.Text)

        'If BillBasicAmt = PayAmount And BillBasicAmt <> 0 Then
        '    POStatus.Text = "PO Is closed"
        'Else
        '    POStatus.Text = "PO Is open"
        'End If

        If BillBasicAmt >= POBasicAmt Then
            BillStatus.Text = "Bill Cycle Is closed"
        Else
            BillStatus.Text = "Bill Cycle Is open"
        End If

        If PayAmount = POTotalAmt And PayAmount <> 0 Then
            POStatus.Text = "PO Is closed"
        Else
            POStatus.Text = "PO Is open"
        End If
    End Sub

    Sub SetQuery()
        PaymentDA.InsertCommand = New OleDbCommand("Insert Into VendorPayment (PurchaseID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDSPer,TDS,Remarks,RemainAmount,POAmt,TotalPaidAmt) Values (@PurchaseID,@JobNo,@PONo,@CheckNo,@ChkDate,@PaidAmount,@TDSPer,@TDS,@Remarks,@RemainAmount,@POAmt,@TotalPaidAmt)", ConStr)
        PaymentDA.InsertCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")
        PaymentDA.InsertCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        PaymentDA.InsertCommand.Parameters.Add("@PONo", OleDbType.VarChar, 50, "PONo")
        PaymentDA.InsertCommand.Parameters.Add("@CheckNo", OleDbType.VarChar, 50, "CheckNo")
        PaymentDA.InsertCommand.Parameters.Add("@ChkDate", OleDbType.Date, 3, "ChkDate")
        PaymentDA.InsertCommand.Parameters.Add("@PaidAmount", OleDbType.Double, 8, "PaidAmount")
        PaymentDA.InsertCommand.Parameters.Add("@TDSPer", OleDbType.Double, 8, "TDSPer")
        PaymentDA.InsertCommand.Parameters.Add("@TDS", OleDbType.Double, 8, "TDS")
        PaymentDA.InsertCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 250, "Remarks")
        PaymentDA.InsertCommand.Parameters.Add("@RemainAmount", OleDbType.Double, 8, "RemainAmount")
        PaymentDA.InsertCommand.Parameters.Add("@POAmt", OleDbType.Double, 8, "POAmt")
        PaymentDA.InsertCommand.Parameters.Add("@TotalPaidAmt", OleDbType.Double, 8, "TotalPaidAmt")


        PaymentDA.UpdateCommand = New OleDbCommand("Update VendorPayment Set PurchaseID=@PurchaseID,JobNo=@JobNo,PONo=@PONo,CheckNo=@CheckNo,ChkDate=@ChkDate,PaidAmount=@PaidAmount,TDSPer=@TDSPer,TDS=@TDS,Remarks=@Remarks,RemainAmount=@RemainAmount,POAmt=@POAmt,TotalPaidAmt=@TotalPaidAmt Where PaymentID=@PaymentID", ConStr)
        PaymentDA.UpdateCommand.Parameters.Add("@PurchaseID", OleDbType.Integer, 4, "PurchaseID")
        PaymentDA.UpdateCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        PaymentDA.UpdateCommand.Parameters.Add("@PONo", OleDbType.VarChar, 50, "PONo")
        PaymentDA.UpdateCommand.Parameters.Add("@CheckNo", OleDbType.VarChar, 50, "CheckNo")
        PaymentDA.UpdateCommand.Parameters.Add("@ChkDate", OleDbType.Date, 3, "ChkDate")
        PaymentDA.UpdateCommand.Parameters.Add("@PaidAmount", OleDbType.Double, 8, "PaidAmount")
        PaymentDA.UpdateCommand.Parameters.Add("@TDSPer", OleDbType.Double, 8, "TDSPer")
        PaymentDA.UpdateCommand.Parameters.Add("@TDS", OleDbType.Double, 8, "TDS")
        PaymentDA.UpdateCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 250, "Remarks")
        PaymentDA.UpdateCommand.Parameters.Add("@RemainAmount", OleDbType.Double, 8, "RemainAmount")
        PaymentDA.UpdateCommand.Parameters.Add("@POAmt", OleDbType.Double, 8, "POAmt")
        PaymentDA.UpdateCommand.Parameters.Add("@TotalPaidAmt", OleDbType.Double, 8, "TotalPaidAmt")
        PaymentDA.UpdateCommand.Parameters.Add("@PaymentID", OleDbType.Integer, 4, "PaymentID")

        PaymentDA.DeleteCommand = New OleDbCommand("Delete From VendorPayment Where PaymentID=@PaymentID", ConStr)
        PaymentDA.DeleteCommand.Parameters.Add("@PaymentID", OleDbType.Integer, 4, "PaymentID")
    End Sub

    Sub setGrid()
        With BillGridView
            .Columns("BillID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("BillNo").OptionsColumn.AllowFocus = False
            .Columns("BillNo").OptionsColumn.ReadOnly = True
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Bill Basic Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "Bill Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True
            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        BillGridView.BestFitColumns()
        BillGridView.Columns("GrandTotalAmount").Width = 150

        With PaymentGridView
            .Columns("PaymentID").Visible = False
            .Columns("PurchaseID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True
            .Columns("IsPrinted").OptionsColumn.AllowFocus = False
            .Columns("IsPrinted").OptionsColumn.ReadOnly = True
            .Columns("TDSPer").Caption = "TDS %"
            .Columns("TDS").Caption = "TDS Val"
            .Columns("POAmt").Visible = False
            .Columns("TotalPaidAmt").Visible = False

            .OptionsView.ShowFooter = True
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        PaymentGridView.BestFitColumns()
        PaymentGridView.Columns("JobNo").Width = 50
        PaymentGridView.Columns("PONo").Width = 90
        PaymentGridView.Columns("PaidAmount").Width = 80
        PaymentGridView.Columns("PaidAmount").Caption = "Paid Amt"
        PaymentGridView.Columns("TDSPer").Width = 50
        PaymentGridView.Columns("TDS").Width = 50
        PaymentGridView.Columns("Remarks").Width = 200
        PaymentGridView.Columns("CheckNo").Width = 60
        PaymentGridView.Columns("CheckNo").Caption = "Chk No"
        PaymentGridView.Columns("ChkDate").Width = 60
        PaymentGridView.Columns("RemainAmount").Caption = "Remain Amt"
        PaymentGridView.Columns("RemainAmount").Width = 80
    End Sub

    Private Sub VendorPOJobNoComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles VendorPOJobNoComboBoxEdit.SelectedIndexChanged
        If VendorPOJobNoComboBoxEdit.Text <> "" Then
            SetLookUp("Select PurchaseID, OrderNo FROM VendorPO Where Company='" + PubCompanyName + "' AND JobNo='" + VendorPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", "VendorPO", "PurchaseID", "OrderNo", VendorPOLookUpEdit, "PO No")
        End If
    End Sub

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveSimpleButton.Click
        If PaymentBS.Count > 0 Then
            Dim totalPaid As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue)
            PaymentBS.Current!TotalPaidAmt = totalPaid
            PaymentBS.Current!POAmt = Convert.ToDouble(POAmountLabelControl.Text)
            PaymentBS.Current!IsPrinted = False
            PaymentBS.EndEdit()
            PaymentDA.Update(DS.Tables("VendorPayment"))
            POStatusCheck()

            'Remove comment on 19Aprr21
            Dim POCmd As New OleDbCommand("Update VendorPO set POStatus=@POStatus,BillStatus=@BillStatus where PurchaseID=@PurchaseID", ConStr)
            'Dim POCmd As New OleDbCommand("Update VendorPO set POStatus=@POStatus where PurchaseID=@PurchaseID", ConStr)
            POCmd.Parameters.AddWithValue("@POStatus", POStatus.Text)
            POCmd.Parameters.AddWithValue("@BillStatus", BillStatus.Text)
            POCmd.Parameters.AddWithValue("@PurchaseID", PurchaseID)
            CnnOpen()
            POCmd.ExecuteNonQuery()
            CnnClose()
            Dim save = XtraMessageBox.Show("Payment saved successfully", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'PaymentGridControl.DataSource = DS.Tables(0)            
            DS.Clear()
            PaymentDA.Fill(DS, "VendorPayment")
            PaymentBS.DataSource = DS
            PaymentBS.DataMember = "VendorPayment"
            PaymentGridControl.DataSource = PaymentBS
            POStatusCheck()
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub PaymentGridView_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles PaymentGridView.CellValueChanged
        Try
            If e.Column.FieldName = "PaidAmount" Or e.Column.FieldName = "TDSPer" Or e.Column.FieldName = "TDS" Then
                If PaymentBS.Count > 1 And POAmountLabelControl.Text <> 0 Then
                    PaymentBS.MovePrevious()
                    Dim PreValue = PaymentBS.Current!RemainAmount
                    PaymentBS.MoveNext()
                    If e.Column.FieldName = "TDSPer" Then
                        Dim TDSAmt As Double = POBasicAmtLabelControl.Text * IIf(PaymentBS.Current!TDSPer Is DBNull.Value, 0, PaymentBS.Current!TDSPer) / 100
                        'PaymentBS.Current!RemainAmount = Math.Round(IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + TDSAmt))
                        PaymentBS.Current!TDS = Math.Round(TDSAmt, 2)
                    End If
                    If e.Column.FieldName = "TDS" Then
                        Dim TdsPer As Double = IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS) * 100 / POBasicAmtLabelControl.Text
                        'PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                        PaymentBS.Current!TDSPer = Math.Round(TdsPer, 2)
                    End If
                    PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                    PaymentBS.EndEdit()

                ElseIf POAmountLabelControl.Text <> 0 Then
                    If e.Column.FieldName = "TDSPer" Then
                        Dim TDSAmt As Double = POBasicAmtLabelControl.Text * IIf(PaymentBS.Current!TDSPer Is DBNull.Value, 0, PaymentBS.Current!TDSPer) / 100
                        'PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, 1]\.Current!PaidAmount) + TDSAmt)                    
                        PaymentBS.Current!TDS = Math.Round(TDSAmt, 2)
                    End If
                    If e.Column.FieldName = "TDS" Then
                        Dim TdsPer As Double = IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS) * 100 / POBasicAmtLabelControl.Text
                        'PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                        PaymentBS.Current!TDSPer = Math.Round(TdsPer, 2)
                    End If
                    PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                    PaymentBS.EndEdit()
                End If
                POStatusCheck()
            End If
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub AddSimpleButton_Click(sender As Object, e As EventArgs) Handles AddSimpleButton.Click
        PaymentBS.AddNew()
        PaymentBS.Current!JobNo = VendorPOJobNoComboBoxEdit.Text
        PaymentBS.Current!PONo = VendorPOLookUpEdit.Text
        PaymentBS.Current!PurchaseID = PurchaseID
        PaymentBS.EndEdit()
    End Sub

    Private Sub PaymentGridView_ValidateRow(sender As Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles PaymentGridView.ValidateRow
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim Paid As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("PaidAmount")
        Dim Remain As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("RemainAmount")
        'Dim TDS As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("TDS")

        Dim PaidAmt As Double = If(View.GetRowCellValue(e.RowHandle, Paid) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Paid), Double))
        Dim RemainAmt As Double = If(View.GetRowCellValue(e.RowHandle, Remain) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Remain), Double))
        'Dim TDS As Double = If(View.GetRowCellValue(e.RowHandle, Remain) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Remain), Double))

        If PaidAmt = 0 Then
            e.Valid = False
            View.SetColumnError(Paid, "Enter Amount")
        End If
        If RemainAmt < 0 Then
            e.Valid = False
            View.SetColumnError(Remain, "Negative Amount")
        End If
    End Sub

    Private Sub PaymentGridView_InvalidRowException(sender As Object, e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles PaymentGridView.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub PaymentGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles PaymentGridView.KeyDown
        If e.KeyCode = Keys.Delete Then
            'Dim result As DialogResult = XtraMessageBox.Show("Do you want to delete the Payment?" & vbCrLf & vbCrLf & "Click Yes to delete and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            'If result = DialogResult.Yes Then
            '    PaymentBS.RemoveCurrent()
            '    PaymentBS.EndEdit()
            'End If
            Dim Frm As New Frm_ChequeAuth()
            Frm.ShowDialog()
            If Frm.DialogResult = DialogResult.OK Then
                PaymentBS.RemoveCurrent()
                PaymentBS.EndEdit()
            End If
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If PaymentBS.Count = 0 Then
            XtraMessageBox.Show("Please select the Payment first.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim AmtWord As String
            AmtWord = AmtInWord(PaymentBS.Current!PaidAmount)
            AmtWord = AmtWord.Replace("Rupees", "")
            Dim Amtdate As Date
            Amtdate = PaymentBS.Current!ChkDate
            Dim Dt As String
            Dt = Amtdate.ToString("dd/MM/yyyy")
            Dt = Dt.Replace("-", " ")
            Dim Paid As String
            Paid = CType((PaymentBS.Current!PaidAmount), String)
            Dim Rmrk As String
            If Not PaymentBS.Current!Remarks Is DBNull.Value Then
                Rmrk = PaymentBS.Current!Remarks
            End If
            'Rmrk = PaymentBS.Current!Remarks
            Dim tds As String
            If Not PaymentBS.Current!TDS Is DBNull.Value Then
                tds = CType((PaymentBS.Current!TDS), String)
            End If

            If PaymentBS.Current!IsPrinted Is Nothing Or PaymentBS.Current!IsPrinted Is DBNull.Value Then
                Dim newCmd As New OleDbCommand("Update VendorPayment set IsPrinted=@IsPrinted where PaymentID=@PaymentID", ConStr)
                newCmd.Parameters.AddWithValue("@IsPrinted", True)
                newCmd.Parameters.AddWithValue("@PaymentID", PaymentBS.Current!PaymentID)
                CnnOpen()
                newCmd.ExecuteNonQuery()
                CnnClose()

                DS.Clear()
                PaymentDA.Fill(DS, "VendorPayment")
                PaymentBS.DataSource = DS
                PaymentBS.DataMember = "VendorPayment"
                PaymentGridControl.DataSource = PaymentBS


                Dim Rpt As New XR_ChequePrintNew
                Rpt.FillDataSource()
                Rpt.ToXrLabel.Text = PartyNameLabelControl.Text
                Rpt.AmtXrLabel.Text = Paid + "/-"
                For Each c As Char In Dt
                    Rpt.DateXrLabel.Text = Rpt.DateXrLabel.Text + c + " "
                Next
                Rpt.AmtWordXrLabel.Text = AmtWord
                Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Cheque -")
                End_Waiting()
                Rpt.ShowRibbonPreviewDialog()

                Dim Rpt1 As New XR_VoucherPrint
                Rpt1.FillDataSource()
                Rpt1.CmpXrLabel.Text = PubCompanyName

                Dim sda As New OleDbDataAdapter("Select BankName FROM Bank Where Company = '" + PubCompanyName + "'", ConStr)
                Dim dt1 As New DataTable()
                sda.Fill(dt1)
                If (dt1.Rows.Count > 0) Then
                    Rpt1.BankNameXrLabel.Text = dt1.Rows(0).Item("BankName").ToString
                End If
                Rpt1.ToXrLabel.Text = PartyNameLabelControl.Text
                Rpt1.JobNoXrLabel.Text = JobNoLabelControl.Text
                Rpt1.PONoXrLabel.Text = PONoLabelControl.Text
                Rpt1.ChqNoXrLabel.Text = PaymentBS.Current!CheckNo
                Rpt1.DateXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")
                Rpt1.RemarksXrLabel.Text = Rmrk
                Rpt1.DrAmtXrLabel.Text = Paid
                Rpt1.TDSAmtXrLabel.Text = tds
                Rpt1.SumDrXrLabel.Text = Paid
                Rpt1.SumTDSXrLabel.Text = tds
                Rpt1.UserXrLabel.Text = CurrentUserName.ToString()
                Rpt1.DtXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")


                Dim ReportPrintTool1 = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                ReportPrintTool1.PreviewRibbonForm.Text = String.Format("- Voucher -")
                End_Waiting()
                Rpt1.ShowRibbonPreviewDialog()
            Else
                If PaymentBS.Current!IsPrinted Then
                    Dim Frm As New Frm_ChequeAuth()
                    Frm.ShowDialog()
                    If Frm.DialogResult = DialogResult.OK Then
                        Dim Rpt As New XR_ChequePrintNew
                        Rpt.FillDataSource()
                        Rpt.ToXrLabel.Text = PartyNameLabelControl.Text
                        Rpt.AmtXrLabel.Text = Paid + "/-"
                        For Each c As Char In Dt
                            Rpt.DateXrLabel.Text = Rpt.DateXrLabel.Text + c + " "
                        Next

                        Rpt.AmtWordXrLabel.Text = AmtWord
                        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Cheque -")
                        End_Waiting()
                        Rpt.ShowRibbonPreviewDialog()

                        Dim Rpt1 As New XR_VoucherPrint
                        Rpt1.FillDataSource()
                        Rpt1.CmpXrLabel.Text = PubCompanyName

                        Dim sda As New OleDbDataAdapter("Select BankName FROM Bank Where Company = '" + PubCompanyName + "'", ConStr)
                        Dim dt1 As New DataTable()
                        sda.Fill(dt1)
                        If (dt1.Rows.Count > 0) Then
                            Rpt1.BankNameXrLabel.Text = dt1.Rows(0).Item("BankName").ToString
                        End If
                        Rpt1.ToXrLabel.Text = PartyNameLabelControl.Text
                        Rpt1.JobNoXrLabel.Text = JobNoLabelControl.Text
                        Rpt1.PONoXrLabel.Text = PONoLabelControl.Text
                        Rpt1.ChqNoXrLabel.Text = PaymentBS.Current!CheckNo
                        Rpt1.DateXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")
                        Rpt1.RemarksXrLabel.Text = Rmrk
                        Rpt1.DrAmtXrLabel.Text = Paid
                        Rpt1.TDSAmtXrLabel.Text = tds
                        Rpt1.SumDrXrLabel.Text = Paid
                        Rpt1.SumTDSXrLabel.Text = tds
                        Rpt1.UserXrLabel.Text = CurrentUserName.ToString()
                        Rpt1.DtXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")

                        Dim ReportPrintTool1 = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                        ReportPrintTool1.PreviewRibbonForm.Text = String.Format("- Voucher -")
                        End_Waiting()
                        Rpt1.ShowRibbonPreviewDialog()
                    End If
                Else
                    Dim newCmd As New OleDbCommand("Update VendorPayment set IsPrinted=@IsPrinted where PaymentID=@PaymentID", ConStr)
                    newCmd.Parameters.AddWithValue("@IsPrinted", True)
                    newCmd.Parameters.AddWithValue("@PaymentID", PaymentBS.Current!PaymentID)
                    CnnOpen()
                    newCmd.ExecuteNonQuery()
                    CnnClose()

                    DS.Clear()
                    PaymentDA.Fill(DS, "VendorPayment")
                    PaymentBS.DataSource = DS
                    PaymentBS.DataMember = "VendorPayment"
                    PaymentGridControl.DataSource = PaymentBS


                    Dim Rpt As New XR_ChequePrintNew
                    Rpt.FillDataSource()
                    Rpt.ToXrLabel.Text = PartyNameLabelControl.Text
                    Rpt.AmtXrLabel.Text = Paid + "/-"
                    For Each c As Char In Dt
                        Rpt.DateXrLabel.Text = Rpt.DateXrLabel.Text + c + " "
                    Next
                    Rpt.AmtWordXrLabel.Text = AmtWord
                    Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Cheque -")
                    End_Waiting()
                    Rpt.ShowRibbonPreviewDialog()

                    Dim Rpt1 As New XR_VoucherPrint
                    Rpt1.FillDataSource()
                    Rpt1.CmpXrLabel.Text = PubCompanyName

                    Dim sda As New OleDbDataAdapter("Select BankName FROM Bank Where Company = '" + PubCompanyName + "'", ConStr)
                    Dim dt1 As New DataTable()
                    sda.Fill(dt1)
                    If (dt1.Rows.Count > 0) Then
                        Rpt1.BankNameXrLabel.Text = dt1.Rows(0).Item("BankName").ToString
                    End If
                    Rpt1.ToXrLabel.Text = PartyNameLabelControl.Text
                    Rpt1.JobNoXrLabel.Text = JobNoLabelControl.Text
                    Rpt1.PONoXrLabel.Text = PONoLabelControl.Text
                    Rpt1.ChqNoXrLabel.Text = PaymentBS.Current!CheckNo
                    Rpt1.DateXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")
                    Rpt1.RemarksXrLabel.Text = Rmrk
                    Rpt1.DrAmtXrLabel.Text = Paid
                    Rpt1.TDSAmtXrLabel.Text = tds
                    Rpt1.SumDrXrLabel.Text = Paid
                    Rpt1.SumTDSXrLabel.Text = tds
                    Rpt1.UserXrLabel.Text = CurrentUserName.ToString()
                    Rpt1.DtXrLabel.Text = Amtdate.ToString("dd/MM/yyyy")


                    Dim ReportPrintTool1 = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                    ReportPrintTool1.PreviewRibbonForm.Text = String.Format("- Voucher -")
                    End_Waiting()
                    Rpt1.ShowRibbonPreviewDialog()
                End If

            End If
        End If
    End Sub

    Private Sub PrintSimpleButton_Click(sender As Object, e As EventArgs) Handles PrintSimpleButton.Click
        Dim Rpt As New XR_VendorPayment
        Rpt.Invoice.Value = PurchaseID
        Rpt.Invoice.Visible = False
        Rpt.FillDataSource()

        Rpt.JobNoXrLabel.Text = JobNoLabelControl.Text
        Rpt.PONoXrLabel.Text = PONoLabelControl.Text
        Rpt.DateXrLabel.Text = OrderDateLabelControl.Text
        Rpt.POBAmtXrLabel.Text = POBasicAmtLabelControl.Text
        Rpt.POTAmtXrLabel.Text = POAmountLabelControl.Text
        Rpt.AdvanceXrLabel.Text = AdvancePaaymentLabelControl.Text
        Rpt.POStXrLabel.Text = POStatus.Text
        Rpt.BillStXrLabel.Text = BillStatus.Text
        Rpt.PartyNameXrLabel.Text = PartyNameLabelControl.Text

        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Vendor Payment -")
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub
End Class