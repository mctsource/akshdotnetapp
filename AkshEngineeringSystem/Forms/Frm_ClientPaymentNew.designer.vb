﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_ClientPaymentNew
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_ClientPaymentNew))
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.BillStatus = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.POBasicAmtLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.POStatus = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.JobNoLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PONoLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.OrderDateLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PartyNameLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.AdvancePaaymentLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.POAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PaymentGridControl = New DevExpress.XtraGrid.GridControl()
        Me.PaymentGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.BTALabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.RemainAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PaidAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.BillIDLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.BillGridControl = New DevExpress.XtraGrid.GridControl()
        Me.BillGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ClientPOJobNoComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.NoOfInvoicesLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.ClientPOButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ClientPOLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.AddSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.PrintSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.CancelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.SaveSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PaymentGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PaymentGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientPOJobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.PanelControl1)
        Me.PanelCtrl.Controls.Add(Me.PaymentGridControl)
        Me.PanelCtrl.Controls.Add(Me.BTALabelControl)
        Me.PanelCtrl.Controls.Add(Me.RemainAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PaidAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.BillIDLabelControl)
        Me.PanelCtrl.Controls.Add(Me.BillGridControl)
        Me.PanelCtrl.Controls.Add(Me.ClientPOJobNoComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.NoOfInvoicesLabelControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl15)
        Me.PanelCtrl.Controls.Add(Me.ClientPOButton)
        Me.PanelCtrl.Controls.Add(Me.ClientPOLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(868, 509)
        Me.PanelCtrl.TabIndex = 0
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.BillStatus)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.POBasicAmtLabelControl)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.POStatus)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.JobNoLabelControl)
        Me.PanelControl1.Controls.Add(Me.PONoLabelControl)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.OrderDateLabelControl)
        Me.PanelControl1.Controls.Add(Me.PartyNameLabelControl)
        Me.PanelControl1.Controls.Add(Me.AdvancePaaymentLabelControl)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.POAmountLabelControl)
        Me.PanelControl1.Location = New System.Drawing.Point(5, 33)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(854, 97)
        Me.PanelControl1.TabIndex = 104
        '
        'BillStatus
        '
        Me.BillStatus.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BillStatus.Appearance.Options.UseFont = True
        Me.BillStatus.Location = New System.Drawing.Point(356, 74)
        Me.BillStatus.Name = "BillStatus"
        Me.BillStatus.Size = New System.Drawing.Size(0, 13)
        Me.BillStatus.TabIndex = 110
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(246, 74)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl5.TabIndex = 109
        Me.LabelControl5.Text = "Bill Status                  :"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(15, 74)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl2.TabIndex = 107
        Me.LabelControl2.Text = "PO Basic Amount       :"
        '
        'POBasicAmtLabelControl
        '
        Me.POBasicAmtLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.POBasicAmtLabelControl.Appearance.Options.UseFont = True
        Me.POBasicAmtLabelControl.Location = New System.Drawing.Point(127, 74)
        Me.POBasicAmtLabelControl.Name = "POBasicAmtLabelControl"
        Me.POBasicAmtLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.POBasicAmtLabelControl.TabIndex = 108
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(15, 18)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl11.TabIndex = 77
        Me.LabelControl11.Text = "Job No.                      :"
        '
        'POStatus
        '
        Me.POStatus.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.POStatus.Appearance.Options.UseFont = True
        Me.POStatus.Location = New System.Drawing.Point(667, 74)
        Me.POStatus.Name = "POStatus"
        Me.POStatus.Size = New System.Drawing.Size(0, 13)
        Me.POStatus.TabIndex = 103
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(15, 46)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl10.TabIndex = 78
        Me.LabelControl10.Text = "PO No.                       :"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(555, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl3.TabIndex = 102
        Me.LabelControl3.Text = "Status                        :"
        '
        'JobNoLabelControl
        '
        Me.JobNoLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.JobNoLabelControl.Appearance.Options.UseFont = True
        Me.JobNoLabelControl.Location = New System.Drawing.Point(127, 18)
        Me.JobNoLabelControl.Name = "JobNoLabelControl"
        Me.JobNoLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.JobNoLabelControl.TabIndex = 82
        '
        'PONoLabelControl
        '
        Me.PONoLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.PONoLabelControl.Appearance.Options.UseFont = True
        Me.PONoLabelControl.Location = New System.Drawing.Point(127, 46)
        Me.PONoLabelControl.Name = "PONoLabelControl"
        Me.PONoLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.PONoLabelControl.TabIndex = 83
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(246, 18)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl9.TabIndex = 79
        Me.LabelControl9.Text = "Order Date                :"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(246, 46)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl6.TabIndex = 80
        Me.LabelControl6.Text = "Party Name               :"
        '
        'OrderDateLabelControl
        '
        Me.OrderDateLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.OrderDateLabelControl.Appearance.Options.UseFont = True
        Me.OrderDateLabelControl.Location = New System.Drawing.Point(358, 18)
        Me.OrderDateLabelControl.Name = "OrderDateLabelControl"
        Me.OrderDateLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.OrderDateLabelControl.TabIndex = 84
        '
        'PartyNameLabelControl
        '
        Me.PartyNameLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.PartyNameLabelControl.Appearance.Options.UseFont = True
        Me.PartyNameLabelControl.Location = New System.Drawing.Point(358, 46)
        Me.PartyNameLabelControl.Name = "PartyNameLabelControl"
        Me.PartyNameLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.PartyNameLabelControl.TabIndex = 85
        '
        'AdvancePaaymentLabelControl
        '
        Me.AdvancePaaymentLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.AdvancePaaymentLabelControl.Appearance.Options.UseFont = True
        Me.AdvancePaaymentLabelControl.Location = New System.Drawing.Point(667, 46)
        Me.AdvancePaaymentLabelControl.Name = "AdvancePaaymentLabelControl"
        Me.AdvancePaaymentLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.AdvancePaaymentLabelControl.TabIndex = 88
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(555, 18)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl7.TabIndex = 81
        Me.LabelControl7.Text = "PO Amount                :"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(555, 46)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl13.TabIndex = 87
        Me.LabelControl13.Text = "Advance Payment     :"
        '
        'POAmountLabelControl
        '
        Me.POAmountLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.POAmountLabelControl.Appearance.Options.UseFont = True
        Me.POAmountLabelControl.Location = New System.Drawing.Point(667, 18)
        Me.POAmountLabelControl.Name = "POAmountLabelControl"
        Me.POAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.POAmountLabelControl.TabIndex = 86
        '
        'PaymentGridControl
        '
        Me.PaymentGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.PaymentGridControl.Location = New System.Drawing.Point(5, 312)
        Me.PaymentGridControl.MainView = Me.PaymentGridView
        Me.PaymentGridControl.MenuManager = Me.BarManager1
        Me.PaymentGridControl.Name = "PaymentGridControl"
        Me.PaymentGridControl.Size = New System.Drawing.Size(854, 191)
        Me.PaymentGridControl.TabIndex = 99
        Me.PaymentGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.PaymentGridView})
        '
        'PaymentGridView
        '
        Me.PaymentGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PaymentGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.PaymentGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.PaymentGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.PaymentGridView.GridControl = Me.PaymentGridControl
        Me.PaymentGridView.Name = "PaymentGridView"
        Me.PaymentGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.PaymentGridView.OptionsCustomization.AllowSort = False
        Me.PaymentGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.PaymentGridView.OptionsNavigation.UseTabKey = False
        Me.PaymentGridView.OptionsView.ColumnAutoWidth = False
        Me.PaymentGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.PaymentGridView.OptionsView.EnableAppearanceOddRow = True
        Me.PaymentGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.PaymentGridView.OptionsView.ShowGroupPanel = False
        Me.PaymentGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.PaymentGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem})
        Me.BarManager1.MaxItemId = 4
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.Caption = "New"
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.ImageOptions.Image = CType(resources.GetObject("NewBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.NewBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("NewBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.Caption = "Open"
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.ImageOptions.Image = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.OpenBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.ImageOptions.Image = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.DeleteBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(902, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 595)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(902, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 564)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(902, 31)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 564)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.Caption = "Print"
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'BTALabelControl
        '
        Me.BTALabelControl.Location = New System.Drawing.Point(-487, -206)
        Me.BTALabelControl.Name = "BTALabelControl"
        Me.BTALabelControl.Size = New System.Drawing.Size(0, 13)
        Me.BTALabelControl.TabIndex = 98
        '
        'RemainAmountLabelControl
        '
        Me.RemainAmountLabelControl.Location = New System.Drawing.Point(-499, 237)
        Me.RemainAmountLabelControl.Name = "RemainAmountLabelControl"
        Me.RemainAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.RemainAmountLabelControl.TabIndex = 97
        '
        'PaidAmountLabelControl
        '
        Me.PaidAmountLabelControl.Location = New System.Drawing.Point(-499, -221)
        Me.PaidAmountLabelControl.Name = "PaidAmountLabelControl"
        Me.PaidAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.PaidAmountLabelControl.TabIndex = 96
        '
        'BillIDLabelControl
        '
        Me.BillIDLabelControl.Location = New System.Drawing.Point(-499, -202)
        Me.BillIDLabelControl.Name = "BillIDLabelControl"
        Me.BillIDLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.BillIDLabelControl.TabIndex = 95
        '
        'BillGridControl
        '
        Me.BillGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BillGridControl.Location = New System.Drawing.Point(5, 136)
        Me.BillGridControl.MainView = Me.BillGridView
        Me.BillGridControl.MenuManager = Me.BarManager1
        Me.BillGridControl.Name = "BillGridControl"
        Me.BillGridControl.Size = New System.Drawing.Size(854, 170)
        Me.BillGridControl.TabIndex = 3
        Me.BillGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.BillGridView})
        '
        'BillGridView
        '
        Me.BillGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BillGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.BillGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.BillGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.BillGridView.GridControl = Me.BillGridControl
        Me.BillGridView.Name = "BillGridView"
        Me.BillGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.BillGridView.OptionsDetail.EnableMasterViewMode = False
        Me.BillGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.BillGridView.OptionsNavigation.UseTabKey = False
        Me.BillGridView.OptionsView.ColumnAutoWidth = False
        Me.BillGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.BillGridView.OptionsView.EnableAppearanceOddRow = True
        Me.BillGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.BillGridView.OptionsView.ShowGroupPanel = False
        Me.BillGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.BillGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'ClientPOJobNoComboBoxEdit
        '
        Me.ClientPOJobNoComboBoxEdit.Location = New System.Drawing.Point(49, 5)
        Me.ClientPOJobNoComboBoxEdit.MenuManager = Me.BarManager1
        Me.ClientPOJobNoComboBoxEdit.Name = "ClientPOJobNoComboBoxEdit"
        Me.ClientPOJobNoComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ClientPOJobNoComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ClientPOJobNoComboBoxEdit.Size = New System.Drawing.Size(119, 20)
        Me.ClientPOJobNoComboBoxEdit.TabIndex = 0
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(6, 8)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl8.TabIndex = 22
        Me.LabelControl8.Text = "Job No."
        '
        'NoOfInvoicesLabelControl
        '
        Me.NoOfInvoicesLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NoOfInvoicesLabelControl.Appearance.Options.UseFont = True
        Me.NoOfInvoicesLabelControl.Location = New System.Drawing.Point(839, 8)
        Me.NoOfInvoicesLabelControl.Name = "NoOfInvoicesLabelControl"
        Me.NoOfInvoicesLabelControl.Size = New System.Drawing.Size(14, 13)
        Me.NoOfInvoicesLabelControl.TabIndex = 21
        Me.NoOfInvoicesLabelControl.Text = "No"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(753, 8)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl15.TabIndex = 20
        Me.LabelControl15.Text = "No. of Invoices :"
        '
        'ClientPOButton
        '
        Me.ClientPOButton.ImageOptions.Image = CType(resources.GetObject("ClientPOButton.ImageOptions.Image"), System.Drawing.Image)
        Me.ClientPOButton.Location = New System.Drawing.Point(471, 5)
        Me.ClientPOButton.Name = "ClientPOButton"
        Me.ClientPOButton.Size = New System.Drawing.Size(75, 20)
        Me.ClientPOButton.TabIndex = 2
        Me.ClientPOButton.Text = "Search"
        '
        'ClientPOLookUpEdit
        '
        Me.ClientPOLookUpEdit.Location = New System.Drawing.Point(251, 5)
        Me.ClientPOLookUpEdit.Name = "ClientPOLookUpEdit"
        Me.ClientPOLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ClientPOLookUpEdit.Properties.NullText = ""
        Me.ClientPOLookUpEdit.Size = New System.Drawing.Size(214, 20)
        Me.ClientPOLookUpEdit.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(174, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Client PO No."
        '
        'AddSimpleButton
        '
        Me.AddSimpleButton.ImageOptions.Image = CType(resources.GetObject("AddSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.AddSimpleButton.Location = New System.Drawing.Point(77, 520)
        Me.AddSimpleButton.Name = "AddSimpleButton"
        Me.AddSimpleButton.Size = New System.Drawing.Size(96, 31)
        Me.AddSimpleButton.TabIndex = 1
        Me.AddSimpleButton.Text = "Add Payment"
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.PrintSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.CancelSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.SaveSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.AddSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(12, 37)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(884, 554)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'PrintSimpleButton
        '
        Me.PrintSimpleButton.ImageOptions.Image = CType(resources.GetObject("PrintSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintSimpleButton.Location = New System.Drawing.Point(775, 520)
        Me.PrintSimpleButton.Name = "PrintSimpleButton"
        Me.PrintSimpleButton.Size = New System.Drawing.Size(98, 30)
        Me.PrintSimpleButton.TabIndex = 4
        Me.PrintSimpleButton.Text = "Print"
        '
        'CancelSimpleButton
        '
        Me.CancelSimpleButton.ImageOptions.Image = CType(resources.GetObject("CancelSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.CancelSimpleButton.Location = New System.Drawing.Point(260, 520)
        Me.CancelSimpleButton.Name = "CancelSimpleButton"
        Me.CancelSimpleButton.Size = New System.Drawing.Size(75, 30)
        Me.CancelSimpleButton.TabIndex = 3
        Me.CancelSimpleButton.Text = "Cancel"
        '
        'SaveSimpleButton
        '
        Me.SaveSimpleButton.ImageOptions.Image = CType(resources.GetObject("SaveSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.SaveSimpleButton.Location = New System.Drawing.Point(179, 520)
        Me.SaveSimpleButton.Name = "SaveSimpleButton"
        Me.SaveSimpleButton.Size = New System.Drawing.Size(75, 30)
        Me.SaveSimpleButton.TabIndex = 2
        Me.SaveSimpleButton.Text = "Save"
        '
        'Frm_ClientPaymentNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 595)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_ClientPaymentNew"
        Me.Text = "ClientPayment"
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PaymentGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PaymentGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientPOJobNoComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents ClientPOLookUpEdit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ClientPOButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NoOfInvoicesLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ClientPOJobNoComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents BillGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents BillGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CancelSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SaveSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RemainAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PaidAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BillIDLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BTALabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PaymentGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents PaymentGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents AddSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents POStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AdvancePaaymentLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents POAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PartyNameLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OrderDateLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PONoLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JobNoLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BillStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents POBasicAmtLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PrintSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
End Class
