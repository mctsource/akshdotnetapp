﻿Imports System.ComponentModel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI

Public Class Frm_Main
    Public Sub OpenForm(Form As XtraForm, Optional Index As Boolean = False, Optional ByRef Index1 As Boolean = False)
        If Index1 = False Then Start_Waiting()
        Form.MdiParent = Me
        If Index = False Then Form.WindowState = FormWindowState.Maximized Else Form.WindowState = FormWindowState.Normal
        Form.Show()
        Form.BringToFront()
    End Sub

    Private Sub PartyBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PartyBarButtonItem.ItemClick
        OpenForm(Frm_Party)
        End_Waiting()
    End Sub

    Private Sub ClientPOBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ClientPOBarButtonItem.ItemClick
        If Frm_ClientPO.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_ClientPO.Close()
                Frm_SelectCOType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectCOType.ControlBox = True
                Frm_SelectCOType.ShowDialog()
            Else
                Frm_ClientPO.Show()
            End If
        Else
            Frm_ClientPO.Close()
            Frm_SelectCOType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectCOType.ControlBox = True
            Frm_SelectCOType.ShowDialog()
        End If
    End Sub

    Private Sub VendorPOBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles VendorPOBarButtonItem.ItemClick
        If Frm_VendorPO.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_VendorPO.Close()
                Frm_SelectPOType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectPOType.ControlBox = True
                Frm_SelectPOType.ShowDialog()
            Else
                Frm_VendorPO.Show()
            End If
        Else
            Frm_SelectPOType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectPOType.ControlBox = True
            Frm_SelectPOType.ShowDialog()
        End If
    End Sub

    Private Sub InvoiceBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles InvoiceBarButtonItem.ItemClick
        If Frm_InvoicePL.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_InvoicePL.Close()
                Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInvoiceType.ControlBox = True
                Frm_SelectInvoiceType.ShowDialog()
            Else
                Frm_InvoicePL.Show()
            End If
        ElseIf Frm_Invoice_Export.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_Invoice_Export.Close()
                Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectInvoiceType.ControlBox = True
                Frm_SelectInvoiceType.ShowDialog()
            Else
                Frm_Invoice_Export.Show()
            End If
        Else
            Frm_InvoicePL.Close()
            Frm_Invoice_Export.Close()
            Frm_SelectInvoiceType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectInvoiceType.ControlBox = True
            Frm_SelectInvoiceType.ShowDialog()
        End If
    End Sub

    Private Sub InvoiceReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim Rpt As New XR_InvoiceOld
        Rpt.FillDataSource()
        Dim ReportPrintTool = New ReportPrintTool(Rpt)
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub PurchaseOrderReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim Rpt As New XR_ClientPO
        Rpt.FillDataSource()
        Dim ReportPrintTool = New ReportPrintTool(Rpt)
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub Frm_Main_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Application.Exit()
    End Sub

    Private Sub BillBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BillBarButtonItem.ItemClick
        'OpenForm(Frm_Bill)
        'With Frm_Bill
        '    .InitLookup()
        'End With
        'End_Waiting()
        If Frm_BillNew.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_BillNew.Close()
                Frm_SelectBillType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectBillType.ControlBox = True
                Frm_SelectBillType.ShowDialog()
            Else
                Frm_BillNew.Show()
            End If
        Else
            Frm_SelectBillType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectBillType.ControlBox = True
            Frm_SelectBillType.ShowDialog()
        End If
    End Sub

    Private Sub ReportSummaryBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ReportSummaryBarButtonItem.ItemClick
        Dim Rpt As New XR_ReportSummary
        Rpt.FillDataSource()
        Dim ReportPrintTool = New ReportPrintTool(Rpt)
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub ExitBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ExitBarButtonItem.ItemClick
        Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the Program?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            'If Not PubCurrency Is Nothing Then
            '    PubCurrency.ClearCachedData()
            'End If
            Application.Exit()
        Else
        End If
    End Sub
    Private Sub InvoiceSummaryBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles InvoiceSummaryBarButtonItem.ItemClick
        Dim Rpt As New XR_ReportSummary2
        Rpt.FillDataSource()
        Dim ReportPrintTool = New ReportPrintTool(Rpt)
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub CompanyBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CompanyBarButtonItem.ItemClick
        If Me.MdiChildren.Length <= 0 Then
            Frm_SelectCompany.StartPosition = FormStartPosition.CenterParent
            Frm_SelectCompany.ControlBox = True
            Frm_SelectCompany.ShowDialog()
        Else
            XtraMessageBox.Show("Please Close All Opened Form(s)", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub SearchPOBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles SearchPOBarButtonItem.ItemClick
        OpenForm(Frm_SearchPO)
        With Frm_SearchPO
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub UserBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles UserBarButtonItem.ItemClick
        OpenForm(Frm_User)
        With Frm_User
            '.InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub VendorPaymentBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles VendorPaymentBarButtonItem.ItemClick
        OpenForm(Frm_VenderPaymentNew)
        With Frm_VenderPaymentNew
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub Frm_Main_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not PubIsAdmin Then
            InvoiceBarButtonItem.Enabled = False
            'VendorPaymentBarButtonItem.Enabled = False
            'WOPaymentBarButtonItem.Enabled = False
            UserBarButtonItem.Enabled = False
        End If
        If Not CurrentUserName = "AUTHORITY" Then
            UserBarButtonItem.Enabled = False
        End If
    End Sub

    Private Sub ClientPaymentBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ClientPaymentBarButtonItem.ItemClick
        OpenForm(Frm_ClientPaymentNew)
        With Frm_ClientPaymentNew
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub VendorSearchBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles VendorSearchBarButtonItem.ItemClick
        OpenForm(Frm_VenderSearchNew)
        With Frm_VenderSearchNew
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub ClientSearchBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ClientSearchBarButtonItem.ItemClick
        OpenForm(Frm_ClientSearchNew)
        With Frm_ClientSearchNew
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub CompanyProfileBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CompanyProfileBarButtonItem.ItemClick
        OpenForm(Frm_Company)
        End_Waiting()
    End Sub

    Private Sub ProductBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProductBarButtonItem.ItemClick
        OpenForm(Frm_Product)
        End_Waiting()
    End Sub

    Private Sub BankDetailsBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BankDetailsBarButtonItem.ItemClick
        OpenForm(Frm_Bank)
        End_Waiting()
    End Sub

    Private Sub ProInvoiceBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProInvoiceBarButtonItem.ItemClick
        If Frm_ProInvoiceNew.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_ProInvoiceNew.Close()
                Frm_SelectProformaInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectProformaInvoiceType.ControlBox = True
                Frm_SelectProformaInvoiceType.ShowDialog()
            Else
                Frm_ProInvoiceNew.Show()
            End If
        Else
            Frm_SelectProformaInvoiceType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectProformaInvoiceType.ControlBox = True
            Frm_SelectProformaInvoiceType.ShowDialog()
        End If
    End Sub
    Private Sub VendorOSBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles VendorOSBarButtonItem.ItemClick
        OpenForm(Frm_Outstanding2)
        End_Waiting()
    End Sub

    Private Sub Format1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles LetterHead.ItemClick
        OpenForm(Frm_Paragraph)
        End_Waiting()
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles RawMaterialBarButtonItem.ItemClick
        OpenForm(Frm_RawMaterial)
        End_Waiting()
    End Sub

    Private Sub BackupBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BackupBarButtonItem.ItemClick
        Dim portfolioPath As String = My.Settings.dbAkshSystemsConnectionString1
        Dim length As Integer = Len(portfolioPath)
        Dim subPath As String = portfolioPath.Substring(47, length - 48)
        Try
            'FileCopy(subPath, "C:\Users\bhavsar\Google Drive\Back up of Preevol DB\dbPreevolTechnics.accdb")
            'FileCopy(subPath, "C:\Users\mct\GOOGLE DRIVE\Backup of AkshDB\dbAkshSystems.accdb")
            'FileCopy(subPath, "C:\Users\Account\Google Drive\Backup of AkshDB\dbAkshSystemsGSTReady.accdb")
            'FileCopy(subPath, "C:\Users\mct\GOOGLE DRIVE\Backup of AkshDB\dbAkshSystemsGSTReady.accdb")
            FileCopy(subPath, "C:\Users\System_2\GOOGLE DRIVE\Backup of AkshDB\dbAkshSystemsGSTReady.accdb")
            XtraMessageBox.Show("Database Backup Successfully Done...", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            'MsgBox(ex.Message)
            XtraMessageBox.Show("Please select the correct path.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'FileCopy(subPath, "\\BHAVSAR-PC\Users\bhavsar\Google Drive\Back up of Preevol DB\dbPreevolTechnics.accdb")
            'XtraMessageBox.Show("Database Backup Successfully Done...", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub


    Private Sub WOBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles WOBarButtonItem.ItemClick
        If Frm_WorkOrder.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_WorkOrder.Close()
                Frm_SelectWOType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectWOType.ControlBox = True
                Frm_SelectWOType.ShowDialog()
            Else
                Frm_WorkOrder.Show()
            End If
        Else
            Frm_SelectWOType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectWOType.ControlBox = True
            Frm_SelectWOType.ShowDialog()
        End If
    End Sub

    Private Sub WOPaymentBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles WOPaymentBarButtonItem.ItemClick
        OpenForm(Frm_WOPaymentNew)
        With Frm_WOPaymentNew
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub VendorReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles VendorReportBarButtonItem.ItemClick
        OpenForm(Frm_VendorReport)
        With Frm_VendorReport
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub WOBillBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles WOBillBarButtonItem.ItemClick
        If Frm_WorkOrderBill.Visible Then
            Dim result As DialogResult = XtraMessageBox.Show("Do you want to close the form?" & vbCrLf & vbCrLf & "Click Yes to close and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.Yes Then
                Frm_WorkOrderBill.Close()
                Frm_SelectWOInvoiceType.StartPosition = FormStartPosition.CenterParent
                Frm_SelectWOInvoiceType.ControlBox = True
                Frm_SelectWOInvoiceType.ShowDialog()
            Else
                Frm_WorkOrderBill.Show()
            End If
        Else
            Frm_SelectWOInvoiceType.StartPosition = FormStartPosition.CenterParent
            Frm_SelectWOInvoiceType.ControlBox = True
            Frm_SelectWOInvoiceType.ShowDialog()
        End If
    End Sub

    Private Sub ClientReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ClientReportBarButtonItem.ItemClick
        OpenForm(Frm_ClientReport)
        With Frm_ClientReport
            .InitLookup()
        End With
        End_Waiting()
    End Sub

    Private Sub WorkOrderReportBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles WorkOrderReportBarButtonItem.ItemClick
        OpenForm(Frm_WorkOrderReport)
        With Frm_WorkOrderReport
            .InitLookup()
        End With
        End_Waiting()
    End Sub
End Class