﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_Outstanding2
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Outstanding2))
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.ExportWOSSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ExportCOSSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ExportVOSSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.WorkOrderGridControl = New DevExpress.XtraGrid.GridControl()
        Me.WorkOrderGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.CancelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.OrderGridControl = New DevExpress.XtraGrid.GridControl()
        Me.OrderGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.BTALabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.RemainAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PaidAmountLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.BillIDLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.BillGridControl = New DevExpress.XtraGrid.GridControl()
        Me.BillGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.WorkOrderGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WorkOrderGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelCtrl.Controls.Add(Me.ExportWOSSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ExportCOSSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ExportVOSSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Controls.Add(Me.WorkOrderGridControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Controls.Add(Me.LabelControl21)
        Me.PanelCtrl.Controls.Add(Me.CancelSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.OrderGridControl)
        Me.PanelCtrl.Controls.Add(Me.BTALabelControl)
        Me.PanelCtrl.Controls.Add(Me.RemainAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PaidAmountLabelControl)
        Me.PanelCtrl.Controls.Add(Me.BillIDLabelControl)
        Me.PanelCtrl.Controls.Add(Me.BillGridControl)
        Me.PanelCtrl.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(1335, 662)
        Me.PanelCtrl.TabIndex = 0
        '
        'ExportWOSSimpleButton
        '
        Me.ExportWOSSimpleButton.Location = New System.Drawing.Point(1163, 623)
        Me.ExportWOSSimpleButton.Name = "ExportWOSSimpleButton"
        Me.ExportWOSSimpleButton.Size = New System.Drawing.Size(163, 27)
        Me.ExportWOSSimpleButton.TabIndex = 183
        Me.ExportWOSSimpleButton.Text = "&Export WorkOrder Outstanding"
        '
        'ExportCOSSimpleButton
        '
        Me.ExportCOSSimpleButton.Location = New System.Drawing.Point(1163, 428)
        Me.ExportCOSSimpleButton.Name = "ExportCOSSimpleButton"
        Me.ExportCOSSimpleButton.Size = New System.Drawing.Size(163, 27)
        Me.ExportCOSSimpleButton.TabIndex = 182
        Me.ExportCOSSimpleButton.Text = "&Export Client Outstanding"
        '
        'ExportVOSSimpleButton
        '
        Me.ExportVOSSimpleButton.Location = New System.Drawing.Point(1163, 214)
        Me.ExportVOSSimpleButton.Name = "ExportVOSSimpleButton"
        Me.ExportVOSSimpleButton.Size = New System.Drawing.Size(163, 27)
        Me.ExportVOSSimpleButton.TabIndex = 181
        Me.ExportVOSSimpleButton.Text = "&Export Vendor Outstanding"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(9, 436)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(203, 19)
        Me.LabelControl2.TabIndex = 180
        Me.LabelControl2.Text = "Work Order Outstanding "
        '
        'WorkOrderGridControl
        '
        Me.WorkOrderGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.WorkOrderGridControl.Location = New System.Drawing.Point(6, 461)
        Me.WorkOrderGridControl.MainView = Me.WorkOrderGridView
        Me.WorkOrderGridControl.MenuManager = Me.BarManager1
        Me.WorkOrderGridControl.Name = "WorkOrderGridControl"
        Me.WorkOrderGridControl.Size = New System.Drawing.Size(1320, 160)
        Me.WorkOrderGridControl.TabIndex = 179
        Me.WorkOrderGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.WorkOrderGridView})
        '
        'WorkOrderGridView
        '
        Me.WorkOrderGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.WorkOrderGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.WorkOrderGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.WorkOrderGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.WorkOrderGridView.GridControl = Me.WorkOrderGridControl
        Me.WorkOrderGridView.Name = "WorkOrderGridView"
        Me.WorkOrderGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.WorkOrderGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.WorkOrderGridView.OptionsNavigation.UseTabKey = False
        Me.WorkOrderGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.WorkOrderGridView.OptionsView.EnableAppearanceOddRow = True
        Me.WorkOrderGridView.OptionsView.RowAutoHeight = True
        Me.WorkOrderGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.WorkOrderGridView.OptionsView.ShowGroupPanel = False
        Me.WorkOrderGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.WorkOrderGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem})
        Me.BarManager1.MaxItemId = 4
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1355, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 682)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1355, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 682)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1355, 0)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 682)
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.Caption = "New"
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.ImageOptions.Image = CType(resources.GetObject("NewBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.NewBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("NewBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.Caption = "Open"
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.ImageOptions.Image = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.OpenBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.Caption = "Print"
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.ImageOptions.Image = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.DeleteBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(6, 219)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(157, 19)
        Me.LabelControl1.TabIndex = 178
        Me.LabelControl1.Text = "Client Outstanding "
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(6, 6)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(167, 19)
        Me.LabelControl21.TabIndex = 177
        Me.LabelControl21.Text = "Vendor Outstanding "
        '
        'CancelSimpleButton
        '
        Me.CancelSimpleButton.ImageOptions.Image = CType(resources.GetObject("CancelSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.CancelSimpleButton.Location = New System.Drawing.Point(1306, 5)
        Me.CancelSimpleButton.Name = "CancelSimpleButton"
        Me.CancelSimpleButton.Size = New System.Drawing.Size(23, 20)
        Me.CancelSimpleButton.TabIndex = 2
        '
        'OrderGridControl
        '
        Me.OrderGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.OrderGridControl.Location = New System.Drawing.Point(6, 32)
        Me.OrderGridControl.MainView = Me.OrderGridView
        Me.OrderGridControl.MenuManager = Me.BarManager1
        Me.OrderGridControl.Name = "OrderGridControl"
        Me.OrderGridControl.Size = New System.Drawing.Size(1320, 180)
        Me.OrderGridControl.TabIndex = 100
        Me.OrderGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.OrderGridView})
        '
        'OrderGridView
        '
        Me.OrderGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.OrderGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.OrderGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.OrderGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.OrderGridView.GridControl = Me.OrderGridControl
        Me.OrderGridView.Name = "OrderGridView"
        Me.OrderGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.OrderGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.OrderGridView.OptionsNavigation.UseTabKey = False
        Me.OrderGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.OrderGridView.OptionsView.EnableAppearanceOddRow = True
        Me.OrderGridView.OptionsView.RowAutoHeight = True
        Me.OrderGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.OrderGridView.OptionsView.ShowGroupPanel = False
        Me.OrderGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.OrderGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'BTALabelControl
        '
        Me.BTALabelControl.Location = New System.Drawing.Point(-487, -206)
        Me.BTALabelControl.Name = "BTALabelControl"
        Me.BTALabelControl.Size = New System.Drawing.Size(0, 13)
        Me.BTALabelControl.TabIndex = 98
        '
        'RemainAmountLabelControl
        '
        Me.RemainAmountLabelControl.Location = New System.Drawing.Point(-499, 237)
        Me.RemainAmountLabelControl.Name = "RemainAmountLabelControl"
        Me.RemainAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.RemainAmountLabelControl.TabIndex = 97
        '
        'PaidAmountLabelControl
        '
        Me.PaidAmountLabelControl.Location = New System.Drawing.Point(-499, -221)
        Me.PaidAmountLabelControl.Name = "PaidAmountLabelControl"
        Me.PaidAmountLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.PaidAmountLabelControl.TabIndex = 96
        '
        'BillIDLabelControl
        '
        Me.BillIDLabelControl.Location = New System.Drawing.Point(-499, -202)
        Me.BillIDLabelControl.Name = "BillIDLabelControl"
        Me.BillIDLabelControl.Size = New System.Drawing.Size(0, 13)
        Me.BillIDLabelControl.TabIndex = 95
        '
        'BillGridControl
        '
        Me.BillGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BillGridControl.Location = New System.Drawing.Point(6, 247)
        Me.BillGridControl.MainView = Me.BillGridView
        Me.BillGridControl.MenuManager = Me.BarManager1
        Me.BillGridControl.Name = "BillGridControl"
        Me.BillGridControl.Size = New System.Drawing.Size(1320, 180)
        Me.BillGridControl.TabIndex = 3
        Me.BillGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.BillGridView})
        '
        'BillGridView
        '
        Me.BillGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BillGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.BillGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.BillGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.BillGridView.GridControl = Me.BillGridControl
        Me.BillGridView.Name = "BillGridView"
        Me.BillGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.BillGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.BillGridView.OptionsNavigation.UseTabKey = False
        Me.BillGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.BillGridView.OptionsView.EnableAppearanceOddRow = True
        Me.BillGridView.OptionsView.RowAutoHeight = True
        Me.BillGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.BillGridView.OptionsView.ShowGroupPanel = False
        Me.BillGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.BillGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(1345, 672)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(5, 5)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Size = New System.Drawing.Size(1345, 640)
        Me.SplitContainer1.SplitterDistance = 448
        Me.SplitContainer1.TabIndex = 5
        '
        'Frm_Outstanding2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 682)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_Outstanding2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Outstanding"
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.WorkOrderGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WorkOrderGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents BillGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents BillGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CancelSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RemainAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PaidAmountLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BillIDLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BTALabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OrderGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents OrderGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents WorkOrderGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents WorkOrderGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ExportVOSSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ExportWOSSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ExportCOSSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
End Class
