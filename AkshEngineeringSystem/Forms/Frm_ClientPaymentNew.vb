﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI
Imports System.Data.OleDb
'Imports ELCF
Public Class Frm_ClientPaymentNew
    Dim DS As New DataSet

    Dim PaymentDA As New OleDbDataAdapter
    Dim PaymentBS As New BindingSource

    Dim SalesID As Integer = 0
    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()

        Bar1.Visible = False
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub

    Sub InitLookup()
        SetGridCommboBox("SELECT DISTINCT JobNoPrefix + JobNo AS JobNo FROM ClientPO Where Company='" + PubCompanyName + "'", "ClientPO", ClientPOJobNoComboBoxEdit)
    End Sub

    Function ClientPOValidation() As Boolean
        If ClientPOLookUpEdit.EditValue Is DBNull.Value Then
            ClientPOLookUpEdit.Focus()
            Return False
        ElseIf ClientPOJobNoComboBoxEdit.Text = "" Then
            ClientPOJobNoComboBoxEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub ClientPOButton_Click(sender As Object, e As EventArgs) Handles ClientPOButton.Click
        If ClientPOValidation() Then
            Dim ClientDA As New OleDbDataAdapter
            Dim ClientDT As New DataTable
            ClientDA.SelectCommand = New OleDbCommand("Select SalesID,JobNoPrefix,JobNo,OrderNo,OrderDate,ReceiverName,TotalAmtBeforeTax,AdvancePayment,GrandTotalAmount,POStatus,BillStatus From ClientPO where Company='" + PubCompanyName + "' AND OrderNo='" + ClientPOLookUpEdit.Text + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            ClientDA.Fill(ClientDT)

            If ClientDT.Rows.Count > 0 Then
                JobNoLabelControl.Text = ClientDT.Rows(0).Item("JobNoPrefix").ToString + ClientDT.Rows(0).Item("JobNo").ToString
                PONoLabelControl.Text = ClientDT.Rows(0).Item("OrderNo").ToString
                OrderDateLabelControl.Text = ClientDT.Rows(0).Item("OrderDate").ToString.Remove(10, 8)
                POAmountLabelControl.Text = ClientDT.Rows(0).Item("GrandTotalAmount").ToString
                POBasicAmtLabelControl.Text = ClientDT.Rows(0).Item("TotalAmtBeforeTax").ToString
                AdvancePaaymentLabelControl.Text = ClientDT.Rows(0).Item("AdvancePayment").ToString
                POStatus.Text = ClientDT.Rows(0).Item("POStatus").ToString
                BillStatus.Text = ClientDT.Rows(0).Item("BillStatus").ToString
                SalesID = Convert.ToInt32(ClientDT.Rows(0).Item("SalesID").ToString)

                Dim PartyDA As New OleDbDataAdapter
                Dim PartyDT As New DataTable
                'PartyDA.SelectCommand = New OleDbCommand("Select PartyName From Party where Company='" + PubCompanyName + "' AND PartyID=" + ClientDT.Rows(0).Item("PartyID").ToString + "", ConStr)
                PartyDA.SelectCommand = New OleDbCommand("Select PartyName From Party where Company='" + PubCompanyName + "' AND PartyName= '" + ClientDT.Rows(0).Item("ReceiverName").ToString + "'", ConStr)
                PartyDA.Fill(PartyDT)

                If PartyDT.Rows.Count > 0 Then
                    PartyNameLabelControl.Text = PartyDT.Rows(0).Item("PartyName").ToString
                End If
            End If

            Dim InvoiceDA As New OleDbDataAdapter
            Dim InvoiceDT As New DataTable
            InvoiceDA.SelectCommand = New OleDbCommand("Select InvoiceID,InvoiceNo,JobNoPrefix,JobNo,PO,TotalAmtBeforeTax,GrandTotalAmount From Invoice where Company='" + PubCompanyName + "' AND PO='" + ClientPOLookUpEdit.Text + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", ConStr)
            InvoiceDA.Fill(InvoiceDT)
            BillGridControl.DataSource = InvoiceDT

            NoOfInvoicesLabelControl.Text = InvoiceDT.Rows.Count.ToString

            DS = New DataSet
            PaymentBS = New BindingSource

            PaymentDA.SelectCommand = New OleDbCommand("Select PaymentID,SalesID,JobNo,PONo,CheckNo,ChkDate,PaidAmount,TDSPer,TDS,Remarks,RemainAmount,POAmt,TotalPaidAmt From ClientPayment Where PONo='" + ClientPOLookUpEdit.Text + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text + "' Order By PaymentID", ConStr)
            PaymentDA.Fill(DS, "ClientPayment")
            PaymentBS.DataSource = DS
            PaymentBS.DataMember = "ClientPayment"
            PaymentGridControl.DataSource = PaymentBS

            Dim POAmtDA As New OleDbDataAdapter
            Dim POAmtDT As New DataTable
            POAmtDA.SelectCommand = New OleDbCommand("Select PaymentID,RemainAmount,POAmt From ClientPayment Where SalesID IN(Select Max(SalesID) From ClientPayment WHERE SalesID = " + SalesID.ToString + ")", ConStr)
            POAmtDA.Fill(POAmtDT)
            Dim PaymentPOAmt As Double
            Dim RemainAmt As Double
            Dim PayID As Integer
            If POAmtDT.Rows.Count > 0 Then
                PayID = POAmtDT.Rows(0).Item("PaymentID").ToString
                RemainAmt = POAmtDT.Rows(0).Item("RemainAmount").ToString
                PaymentPOAmt = POAmtDT.Rows(0).Item("POAmt").ToString
            End If
            Dim diff As Decimal
            'PaymentPOAmt = PaymentBS.Current!POAmt
            Dim POAmt = Convert.ToDecimal(POAmountLabelControl.Text)
            If PaymentPOAmt < POAmt Then
                diff = POAmt - PaymentPOAmt
                RemainAmt = RemainAmt + diff
                'PaymentBS.Current!RemainAmount = PaymentBS.Current!RemainAmount + diff
                Dim CmpCmd As New OleDbCommand("Update ClientPayment Set RemainAmount=@RemainAmount,POAmt=@POAmt Where SalesID=@SalesID", ConStr)
                CmpCmd.Parameters.AddWithValue("@RemainAmount", RemainAmt.ToString)
                CmpCmd.Parameters.AddWithValue("@POAmt", POAmountLabelControl.Text)
                CmpCmd.Parameters.AddWithValue("@PaymentID", PayID.ToString)
                CnnOpen()
                CmpCmd.ExecuteNonQuery()
                CnnClose()
            ElseIf PaymentPOAmt > POAmt Then
                diff = PaymentPOAmt - POAmt
                RemainAmt = RemainAmt - diff
                'PaymentBS.Current!RemainAmount = PaymentBS.Current!RemainAmount - diff
                Dim CmpCmd As New OleDbCommand("Update ClientPayment Set RemainAmount=@RemainAmount,POAmt=@POAmt Where SalesID=@SalesID", ConStr)
                CmpCmd.Parameters.AddWithValue("@RemainAmount", RemainAmt.ToString)
                CmpCmd.Parameters.AddWithValue("@POAmt", POAmountLabelControl.Text)
                CmpCmd.Parameters.AddWithValue("@PaymentID", PayID.ToString)
                CnnOpen()
                CmpCmd.ExecuteNonQuery()
                CnnClose()
            Else
            End If

            SetQuery()
            setGrid()
            POStatusCheck()
        End If
    End Sub

    Sub POStatusCheck()
        'Dim PayAmount As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue) + Convert.ToDouble(PaymentGridView.Columns("TDS").SummaryItem.SummaryValue)
        'Dim BillBasicAmt  As Double = Convert.ToDouble(BillGridView.Columns("GrandTotalAmount").SummaryItem.SummaryValue)

        'If BillBasicAmt  = PayAmount And BillBasicAmt  <> 0 Then
        '    POStatus.Text = "PO Is closed"
        'Else
        '    POStatus.Text = "PO Is open"
        'End If
        Dim BillBasicAmt As Double = Convert.ToDouble(BillGridView.Columns("TotalAmtBeforeTax").SummaryItem.SummaryValue)
        Dim POBasicAmt As Double = Convert.ToDouble(POBasicAmtLabelControl.Text)

        Dim PayAmount As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue) + Convert.ToDouble(PaymentGridView.Columns("TDS").SummaryItem.SummaryValue)
        Dim POTotalAmt As Double = Convert.ToDouble(POAmountLabelControl.Text)

        If BillBasicAmt >= POBasicAmt Then
            BillStatus.Text = "Bill Cycle Is closed"
        Else
            BillStatus.Text = "Bill Cycle Is open"
        End If

        If PayAmount = POTotalAmt And PayAmount <> 0 Then
            POStatus.Text = "PO Is closed"
        Else
            POStatus.Text = "PO Is open"
        End If
    End Sub


    Sub SetQuery()
        PaymentDA.InsertCommand = New OleDbCommand("Insert Into ClientPayment (SalesID, JobNo, PONo, CheckNo, ChkDate, PaidAmount, TDSPer, TDS, Remarks, RemainAmount,POAmt,TotalPaidAmt) Values (@SalesID,@JobNo,@PONo,@CheckNo,@ChkDate,@PaidAmount,@TDSPer,@TDS,@Remarks,@RemainAmount,@POAmt,@TotalPaidAmt)", ConStr)
        PaymentDA.InsertCommand.Parameters.Add("@SalesID", OleDbType.Integer, 4, "SalesID")
        PaymentDA.InsertCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        PaymentDA.InsertCommand.Parameters.Add("@PONo", OleDbType.VarChar, 50, "PONo")
        PaymentDA.InsertCommand.Parameters.Add("@CheckNo", OleDbType.VarChar, 50, "CheckNo")
        PaymentDA.InsertCommand.Parameters.Add("@ChkDate", OleDbType.Date, 3, "ChkDate")
        PaymentDA.InsertCommand.Parameters.Add("@PaidAmount", OleDbType.Double, 8, "PaidAmount")
        PaymentDA.InsertCommand.Parameters.Add("@TDSPer", OleDbType.Double, 8, "TDSPer")
        PaymentDA.InsertCommand.Parameters.Add("@TDS", OleDbType.Double, 8, "TDS")
        PaymentDA.InsertCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 250, "Remarks")
        PaymentDA.InsertCommand.Parameters.Add("@RemainAmount", OleDbType.Double, 8, "RemainAmount")
        PaymentDA.InsertCommand.Parameters.Add("@POAmt", OleDbType.Double, 8, "POAmt")
        PaymentDA.InsertCommand.Parameters.Add("@TotalPaidAmt", OleDbType.Double, 8, "TotalPaidAmt")

        PaymentDA.UpdateCommand = New OleDbCommand("Update ClientPayment Set SalesID=@SalesID,JobNo=@JobNo,PONo=@PONo,CheckNo=@CheckNo,ChkDate=@ChkDate,PaidAmount=@PaidAmount,TDSPer=@TDSPer,TDS=@TDS,Remarks=@Remarks,RemainAmount=@RemainAmount,POAmt=@POAmt,TotalPaidAmt=@TotalPaidAmt Where PaymentID=@PaymentID", ConStr)
        PaymentDA.UpdateCommand.Parameters.Add("@SalesID", OleDbType.Integer, 4, "SalesID")
        PaymentDA.UpdateCommand.Parameters.Add("@JobNo", OleDbType.VarChar, 50, "JobNo")
        PaymentDA.UpdateCommand.Parameters.Add("@PONo", OleDbType.VarChar, 50, "PONo")
        PaymentDA.UpdateCommand.Parameters.Add("@CheckNo", OleDbType.VarChar, 50, "CheckNo")
        PaymentDA.UpdateCommand.Parameters.Add("@ChkDate", OleDbType.Date, 3, "ChkDate")
        PaymentDA.UpdateCommand.Parameters.Add("@PaidAmount", OleDbType.Double, 8, "PaidAmount")
        PaymentDA.UpdateCommand.Parameters.Add("@TDSPer", OleDbType.Double, 8, "TDSPer")
        PaymentDA.UpdateCommand.Parameters.Add("@TDS", OleDbType.Double, 8, "TDS")
        PaymentDA.UpdateCommand.Parameters.Add("@Remarks", OleDbType.VarChar, 250, "Remarks")
        PaymentDA.UpdateCommand.Parameters.Add("@RemainAmount", OleDbType.Double, 8, "RemainAmount")
        PaymentDA.UpdateCommand.Parameters.Add("@POAmt", OleDbType.Double, 8, "POAmt")
        PaymentDA.UpdateCommand.Parameters.Add("@TotalPaidAmt", OleDbType.Double, 8, "TotalPaidAmt")
        PaymentDA.UpdateCommand.Parameters.Add("@PaymentID", OleDbType.Integer, 4, "PaymentID")

        PaymentDA.DeleteCommand = New OleDbCommand("Delete From ClientPayment Where PaymentID=@PaymentID", ConStr)
        PaymentDA.DeleteCommand.Parameters.Add("@PaymentID", OleDbType.Integer, 4, "PaymentID")
    End Sub

    Sub setGrid()
        With BillGridView
            .Columns("InvoiceID").Visible = False
            .Columns("JobNoPrefix").Visible = False
            .Columns("InvoiceNo").Caption = "Invoice No"
            .Columns("InvoiceNo").OptionsColumn.AllowFocus = False
            .Columns("InvoiceNo").OptionsColumn.ReadOnly = True
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PO").Caption = "PO No"
            .Columns("PO").OptionsColumn.AllowFocus = False
            .Columns("PO").OptionsColumn.ReadOnly = True
            .Columns("TotalAmtBeforeTax").Caption = "Bill Basic Amount"
            .Columns("TotalAmtBeforeTax").OptionsColumn.AllowFocus = False
            .Columns("TotalAmtBeforeTax").OptionsColumn.ReadOnly = True
            .Columns("GrandTotalAmount").Caption = "Bill Amount"
            .Columns("GrandTotalAmount").OptionsColumn.AllowFocus = False
            .Columns("GrandTotalAmount").OptionsColumn.ReadOnly = True

            .OptionsView.ShowFooter = True
            .Columns("TotalAmtBeforeTax").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("GrandTotalAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With

        BillGridView.BestFitColumns()
        BillGridView.Columns("GrandTotalAmount").Width = 150

        With PaymentGridView
            .Columns("PaymentID").Visible = False
            .Columns("SalesID").Visible = False
            .Columns("JobNo").OptionsColumn.AllowFocus = False
            .Columns("JobNo").OptionsColumn.ReadOnly = True
            .Columns("PONo").OptionsColumn.AllowFocus = False
            .Columns("PONo").OptionsColumn.ReadOnly = True
            .Columns("RemainAmount").OptionsColumn.AllowFocus = False
            .Columns("RemainAmount").OptionsColumn.ReadOnly = True
            .Columns("TDSPer").Caption = "TDS %"
            .Columns("TDS").Caption = "TDS Val"
            .Columns("POAmt").Visible = False
            .Columns("TotalPaidAmt").Visible = False

            .OptionsView.ShowFooter = True
            .Columns("PaidAmount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("TDS").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        End With
        PaymentGridView.BestFitColumns()
        PaymentGridView.Columns("JobNo").Width = 50
        PaymentGridView.Columns("PONo").Width = 90
        PaymentGridView.Columns("PaidAmount").Width = 80
        PaymentGridView.Columns("PaidAmount").Caption = "Paid Amt"
        PaymentGridView.Columns("TDSPer").Width = 50
        PaymentGridView.Columns("TDS").Width = 50
        PaymentGridView.Columns("Remarks").Width = 200
        PaymentGridView.Columns("CheckNo").Width = 60
        PaymentGridView.Columns("CheckNo").Caption = "Chk No"
        PaymentGridView.Columns("ChkDate").Width = 60
        PaymentGridView.Columns("RemainAmount").Caption = "Remain Amt"
        PaymentGridView.Columns("RemainAmount").Width = 80
    End Sub

    Private Sub ClientPOJobNoComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ClientPOJobNoComboBoxEdit.SelectedIndexChanged
        If ClientPOJobNoComboBoxEdit.Text <> "" Then
            SetLookUp("Select SalesID, OrderNo FROM ClientPO Where Company='" + PubCompanyName + "' AND JobNo='" + ClientPOJobNoComboBoxEdit.Text.Remove(0, 2) + "'", "ClientPO", "SalesID", "OrderNo", ClientPOLookUpEdit, "PO No")
        End If
    End Sub

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveSimpleButton.Click
        If PaymentBS.Count > 0 Then
            Dim totalPaid As Double = Convert.ToDouble(PaymentGridView.Columns("PaidAmount").SummaryItem.SummaryValue)
            PaymentBS.Current!TotalPaidAmt = totalPaid
            PaymentBS.Current!POAmt = Convert.ToDouble(POAmountLabelControl.Text)
            'PaymentBS.Current!IsPrinted = False
            PaymentBS.EndEdit()
            PaymentDA.Update(DS.Tables("ClientPayment"))

            POStatusCheck()

            Dim POCmd As New OleDbCommand("Update ClientPO set POStatus=@POStatus,BillStatus=@BillStatus where SalesID=@SalesID", ConStr)
            POCmd.Parameters.AddWithValue("@POStatus", POStatus.Text)
            POCmd.Parameters.AddWithValue("@BillStatus", BillStatus.Text)
            POCmd.Parameters.AddWithValue("@SalesID", SalesID)
            CnnOpen()
            POCmd.ExecuteNonQuery()
            CnnClose()

            Dim save = XtraMessageBox.Show("Payment saved successfully", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            DS.Clear()
            PaymentDA.Fill(DS, "ClientPayment")
            PaymentBS.DataSource = DS
            PaymentBS.DataMember = "ClientPayment"
            PaymentGridControl.DataSource = PaymentBS
            POStatusCheck()
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub PaymentGridView_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles PaymentGridView.CellValueChanged
        'Try
        '    If e.Column.FieldName = "PaidAmount" Or e.Column.FieldName = "TDS" Then
        '        If PaymentBS.Count > 1 And POAmountLabelControl.Text <> 0 Then
        '            PaymentBS.MovePrevious()
        '            Dim PreValue = PaymentBS.Current!RemainAmount
        '            PaymentBS.MoveNext()
        '            PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
        '            PaymentBS.EndEdit()
        '        ElseIf POAmountLabelControl.Text <> 0 Then
        '            PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
        '            PaymentBS.EndEdit()
        '        End If
        '        POStatusCheck()
        '    End If
        'Catch ex As Exception
        '    'ErtOccur(ex)
        'End Try
        Try
            If e.Column.FieldName = "PaidAmount" Or e.Column.FieldName = "TDSPer" Or e.Column.FieldName = "TDS" Then
                If PaymentBS.Count > 1 And POAmountLabelControl.Text <> 0 Then
                    PaymentBS.MovePrevious()
                    Dim PreValue = PaymentBS.Current!RemainAmount
                    PaymentBS.MoveNext()
                    If e.Column.FieldName = "TDSPer" Then
                        Dim TDSAmt As Double = POBasicAmtLabelControl.Text * IIf(PaymentBS.Current!TDSPer Is DBNull.Value, 0, PaymentBS.Current!TDSPer) / 100
                        'PaymentBS.Current!RemainAmount = Math.Round(IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + TDSAmt))
                        PaymentBS.Current!TDS = Math.Round(TDSAmt, 2)
                    End If
                    If e.Column.FieldName = "TDS" Then
                        Dim TdsPer As Double = IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS) * 100 / POBasicAmtLabelControl.Text
                        'PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                        PaymentBS.Current!TDSPer = Math.Round(TdsPer, 2)
                    End If
                    PaymentBS.Current!RemainAmount = IIf(PreValue Is DBNull.Value, 0, PreValue) - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                    PaymentBS.EndEdit()

                ElseIf POAmountLabelControl.Text <> 0 Then
                    If e.Column.FieldName = "TDSPer" Then
                        Dim TDSAmt As Double = POBasicAmtLabelControl.Text * IIf(PaymentBS.Current!TDSPer Is DBNull.Value, 0, PaymentBS.Current!TDSPer) / 100
                        'PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + TDSAmt)                    
                        PaymentBS.Current!TDS = Math.Round(TDSAmt, 2)
                    End If
                    If e.Column.FieldName = "TDS" Then
                        Dim TdsPer As Double = IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS) * 100 / POBasicAmtLabelControl.Text
                        'PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                        PaymentBS.Current!TDSPer = Math.Round(TdsPer, 2)
                    End If
                    PaymentBS.Current!RemainAmount = POAmountLabelControl.Text - (IIf(PaymentBS.Current!PaidAmount Is DBNull.Value, 0, PaymentBS.Current!PaidAmount) + IIf(PaymentBS.Current!TDS Is DBNull.Value, 0, PaymentBS.Current!TDS))
                    PaymentBS.EndEdit()
                End If
                POStatusCheck()
            End If
        Catch ex As Exception
            'ErtOccur(ex)
        End Try
    End Sub

    Private Sub AddSimpleButton_Click(sender As Object, e As EventArgs) Handles AddSimpleButton.Click
        PaymentBS.AddNew()
        PaymentBS.Current!JobNo = ClientPOJobNoComboBoxEdit.Text
        PaymentBS.Current!PONo = ClientPOLookUpEdit.Text
        PaymentBS.Current!SalesID = SalesID
        PaymentBS.EndEdit()
    End Sub

    Private Sub PaymentGridView_ValidateRow(sender As Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles PaymentGridView.ValidateRow
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim Paid As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("PaidAmount")
        Dim Remain As DevExpress.XtraGrid.Columns.GridColumn = View.Columns("RemainAmount")

        Dim PaidAmt As Double = If(View.GetRowCellValue(e.RowHandle, Paid) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Paid), Double))
        Dim RemainAmt As Double = If(View.GetRowCellValue(e.RowHandle, Remain) Is DBNull.Value, 0, CType(View.GetRowCellValue(e.RowHandle, Remain), Double))

        If PaidAmt = 0 Then
            e.Valid = False
            View.SetColumnError(Paid, "Enter Amount")
        End If
        If RemainAmt < 0 Then
            e.Valid = False
            View.SetColumnError(Remain, "Negative Amount")
        End If
    End Sub

    Private Sub PaymentGridView_InvalidRowException(sender As Object, e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles PaymentGridView.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

    Private Sub PaymentGridView_KeyDown(sender As Object, e As KeyEventArgs) Handles PaymentGridView.KeyDown
        If e.KeyCode = Keys.Delete Then
            'Dim result As DialogResult = XtraMessageBox.Show("Do you want to delete the Payment?" & vbCrLf & vbCrLf & "Click Yes to delete and No to go back." & vbCrLf & vbCrLf, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            'If result = DialogResult.Yes Then
            '    PaymentBS.RemoveCurrent()
            '    PaymentBS.EndEdit()
            'End If
            Dim Frm As New Frm_ChequeAuth()
            Frm.ShowDialog()
            If Frm.DialogResult = DialogResult.OK Then
                PaymentBS.RemoveCurrent()
                PaymentBS.EndEdit()
            End If
        End If
    End Sub

    Private Sub PrintSimpleButton_Click(sender As Object, e As EventArgs) Handles PrintSimpleButton.Click
        Dim Rpt As New XR_ClientPayment
        Rpt.Invoice.Value = SalesID
        Rpt.Invoice.Visible = False
        Rpt.FillDataSource()

        Rpt.JobNoXrLabel.Text = JobNoLabelControl.Text
        Rpt.PONoXrLabel.Text = PONoLabelControl.Text
        Rpt.DateXrLabel.Text = OrderDateLabelControl.Text
        Rpt.POBAmtXrLabel.Text = POBasicAmtLabelControl.Text
        Rpt.POTAmtXrLabel.Text = POAmountLabelControl.Text
        Rpt.AdvanceXrLabel.Text = AdvancePaaymentLabelControl.Text
        Rpt.POStXrLabel.Text = POStatus.Text
        Rpt.BillStXrLabel.Text = BillStatus.Text
        Rpt.PartyNameXrLabel.Text = PartyNameLabelControl.Text

        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Client Payment -")
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub
End Class