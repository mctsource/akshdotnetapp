﻿Imports System.Globalization
Public Class Frm_SelectCOType
    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Public Sub OpenForm(Form As DevExpress.XtraEditors.XtraForm, Optional Index As Boolean = False, Optional ByRef Index1 As Boolean = False)
        If Index1 = False Then Start_Waiting()
        Form.MdiParent = Frm_Main
        If Index = False Then Form.WindowState = FormWindowState.Maximized Else Form.WindowState = FormWindowState.Normal
        Form.Show()
        Form.BringToFront()
    End Sub

    Private Sub OkSimpleButton_Click(sender As Object, e As EventArgs) Handles OkSimpleButton.Click
        If InvoiceTypeComboBoxEdit.Text = "Sales Order" Then
            PubIGST = InterStateRadioButton.Checked
            If PubIGST Then
                PubPOType = "Sales Order IGST"
            Else
                PubPOType = "Sales Order"
            End If
            PubCurrency = New CultureInfo("hi-IN")
        ElseIf InvoiceTypeComboBoxEdit.Text = "SEZ Sales Order" Then
            PubIGST = InterStateRadioButton.Checked
            If PubIGST Then
                PubPOType = "SEZ Sales Order IGST"
            Else
                PubPOType = "SEZ Sales Order"
            End If
            PubCurrency = New CultureInfo("hi-IN")
        ElseIf InvoiceTypeComboBoxEdit.Text = "Export Sales Order" Then
            PubIGST = InterStateRadioButton.Checked
            If PubIGST Then
                PubPOType = "Export Sales Order IGST"
            Else
                PubPOType = "Export Sales Order"
            End If
        End If
        OpenForm(Frm_ClientPO)
        With Frm_ClientPO
            .InitLookup()
        End With
        End_Waiting()
        Me.Close()
    End Sub
    Private Sub Frm_SelectInvoiceType_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InvoiceTypeComboBoxEdit.Text = "Sales Order"
    End Sub

    Private Sub InvoiceTypeComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles InvoiceTypeComboBoxEdit.SelectedIndexChanged
        If InvoiceTypeComboBoxEdit.Text = "SEZ Sales Order" Then
            LocalStateRadioButton.Enabled = True
            LocalStateRadioButton.Checked = True
            InterStateRadioButton.Checked = False
            PriceTypeComboBoxEdit.Enabled = False
            PriceTypeComboBoxEdit.Enabled = False
            PriceTypeComboBoxEdit.SelectedIndex = -1
        ElseIf InvoiceTypeComboBoxEdit.Text = "Export Sales Order" Then
            LocalStateRadioButton.Enabled = False
            InterStateRadioButton.Enabled = True
            InterStateRadioButton.Checked = True
            PriceTypeComboBoxEdit.Enabled = True
            PriceTypeComboBoxEdit.SelectedIndex = 0
        Else
            LocalStateRadioButton.Enabled = True
            LocalStateRadioButton.Checked = True
            InterStateRadioButton.Checked = False
            PriceTypeComboBoxEdit.Enabled = False
            PriceTypeComboBoxEdit.Enabled = False
            PriceTypeComboBoxEdit.SelectedIndex = -1
        End If
    End Sub

    Private Sub PriceTypeComboBoxEdit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles PriceTypeComboBoxEdit.SelectedIndexChanged
        If PriceTypeComboBoxEdit.Text = "Dollar ($)" Then
            PubCurrency = New CultureInfo("en-US")
        ElseIf PriceTypeComboBoxEdit.Text = "Euro (€)" Then
            PubCurrency = New CultureInfo("it_IT")
        ElseIf PriceTypeComboBoxEdit.Text = "Pound (£)" Then
            PubCurrency = New CultureInfo("en-GB")
        Else
            PubCurrency = New CultureInfo("hi-IN")
        End If
    End Sub
End Class