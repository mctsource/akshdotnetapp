﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_InvoiceOld

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_InvoiceOld))
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.DiscountAmountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.DiscountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.SubTotalTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.FreightAmountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.InvoiceYearTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.InvoicePrefixTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.JobNoPrefixTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.SwachataAbhiyanTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.KrushiKalyanTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.CSTAmountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.CSTTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.PODateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintBarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.IsVATORCSTComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SerTaxAmountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.SerTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.RemarksTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.JobNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.AddAmountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.VATAmountTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AddTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.VATTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.OfferNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.DCNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PartyIDLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.DateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.NetAmtLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.BillDetailGridControl = New DevExpress.XtraGrid.GridControl()
        Me.BillDetailGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.TotalAmtTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.InvoiceNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.CancelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.SaveSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.TransportationTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.DiscountAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DiscountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubTotalTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FreightAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceYearTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoicePrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobNoPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SwachataAbhiyanTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KrushiKalyanTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CSTAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CSTTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PODateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PODateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IsVATORCSTComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SerTaxAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SerTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemarksTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VATAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VATTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OfferNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DCNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PartyIDLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillDetailGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillDetailGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransportationTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Controls.Add(Me.CancelSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.SaveSimpleButton)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(12, 37)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(956, 520)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.TransportationTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl20)
        Me.PanelCtrl.Controls.Add(Me.DiscountAmountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.DiscountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl19)
        Me.PanelCtrl.Controls.Add(Me.SubTotalTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl17)
        Me.PanelCtrl.Controls.Add(Me.FreightAmountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl16)
        Me.PanelCtrl.Controls.Add(Me.InvoiceYearTextEdit)
        Me.PanelCtrl.Controls.Add(Me.InvoicePrefixTextEdit)
        Me.PanelCtrl.Controls.Add(Me.JobNoPrefixTextEdit)
        Me.PanelCtrl.Controls.Add(Me.SwachataAbhiyanTextEdit)
        Me.PanelCtrl.Controls.Add(Me.KrushiKalyanTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl12)
        Me.PanelCtrl.Controls.Add(Me.LabelControl11)
        Me.PanelCtrl.Controls.Add(Me.CSTAmountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CSTTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl10)
        Me.PanelCtrl.Controls.Add(Me.PODateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.TotalInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl4)
        Me.PanelCtrl.Controls.Add(Me.IsVATORCSTComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.SerTaxAmountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.SerTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl18)
        Me.PanelCtrl.Controls.Add(Me.RemarksTextEdit)
        Me.PanelCtrl.Controls.Add(Me.JobNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl15)
        Me.PanelCtrl.Controls.Add(Me.AddAmountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.VATAmountTextEdit)
        Me.PanelCtrl.Controls.Add(Me.AddTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl14)
        Me.PanelCtrl.Controls.Add(Me.VATTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl13)
        Me.PanelCtrl.Controls.Add(Me.OfferNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl9)
        Me.PanelCtrl.Controls.Add(Me.DCNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl5)
        Me.PanelCtrl.Controls.Add(Me.PartyIDLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.DateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl3)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Controls.Add(Me.LabelControl6)
        Me.PanelCtrl.Controls.Add(Me.NetAmtLabelControl)
        Me.PanelCtrl.Controls.Add(Me.BillDetailGridControl)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtTextEdit)
        Me.PanelCtrl.Controls.Add(Me.InvoiceNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Controls.Add(Me.LabelControl7)
        Me.PanelCtrl.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(942, 470)
        Me.PanelCtrl.TabIndex = 0
        '
        'DiscountAmountTextEdit
        '
        Me.DiscountAmountTextEdit.Location = New System.Drawing.Point(129, 312)
        Me.DiscountAmountTextEdit.Name = "DiscountAmountTextEdit"
        Me.DiscountAmountTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DiscountAmountTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.DiscountAmountTextEdit.Properties.ReadOnly = True
        Me.DiscountAmountTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.DiscountAmountTextEdit.TabIndex = 77
        Me.DiscountAmountTextEdit.TabStop = False
        '
        'DiscountTextEdit
        '
        Me.DiscountTextEdit.Location = New System.Drawing.Point(94, 312)
        Me.DiscountTextEdit.Name = "DiscountTextEdit"
        Me.DiscountTextEdit.Size = New System.Drawing.Size(29, 20)
        Me.DiscountTextEdit.TabIndex = 9
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(5, 315)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl19.TabIndex = 76
        Me.LabelControl19.Text = "Discount(%)"
        '
        'SubTotalTextEdit
        '
        Me.SubTotalTextEdit.Location = New System.Drawing.Point(94, 393)
        Me.SubTotalTextEdit.Name = "SubTotalTextEdit"
        Me.SubTotalTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SubTotalTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.SubTotalTextEdit.Properties.ReadOnly = True
        Me.SubTotalTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.SubTotalTextEdit.TabIndex = 67
        Me.SubTotalTextEdit.TabStop = False
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(5, 393)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl17.TabIndex = 66
        Me.LabelControl17.Text = "Sub Total"
        '
        'FreightAmountTextEdit
        '
        Me.FreightAmountTextEdit.Location = New System.Drawing.Point(94, 367)
        Me.FreightAmountTextEdit.Name = "FreightAmountTextEdit"
        Me.FreightAmountTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.FreightAmountTextEdit.TabIndex = 11
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(5, 367)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl16.TabIndex = 64
        Me.LabelControl16.Text = "Freight/Packing"
        '
        'InvoiceYearTextEdit
        '
        Me.InvoiceYearTextEdit.Location = New System.Drawing.Point(401, 5)
        Me.InvoiceYearTextEdit.Name = "InvoiceYearTextEdit"
        Me.InvoiceYearTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.InvoiceYearTextEdit.Size = New System.Drawing.Size(44, 20)
        Me.InvoiceYearTextEdit.TabIndex = 2
        '
        'InvoicePrefixTextEdit
        '
        Me.InvoicePrefixTextEdit.Location = New System.Drawing.Point(254, 5)
        Me.InvoicePrefixTextEdit.Name = "InvoicePrefixTextEdit"
        Me.InvoicePrefixTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.InvoicePrefixTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.InvoicePrefixTextEdit.Properties.ReadOnly = True
        Me.InvoicePrefixTextEdit.Size = New System.Drawing.Size(80, 20)
        Me.InvoicePrefixTextEdit.TabIndex = 62
        Me.InvoicePrefixTextEdit.TabStop = False
        '
        'JobNoPrefixTextEdit
        '
        Me.JobNoPrefixTextEdit.Location = New System.Drawing.Point(67, 5)
        Me.JobNoPrefixTextEdit.Name = "JobNoPrefixTextEdit"
        Me.JobNoPrefixTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.JobNoPrefixTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.JobNoPrefixTextEdit.Properties.ReadOnly = True
        Me.JobNoPrefixTextEdit.Size = New System.Drawing.Size(17, 20)
        Me.JobNoPrefixTextEdit.TabIndex = 61
        Me.JobNoPrefixTextEdit.TabStop = False
        '
        'SwachataAbhiyanTextEdit
        '
        Me.SwachataAbhiyanTextEdit.Location = New System.Drawing.Point(547, 312)
        Me.SwachataAbhiyanTextEdit.Name = "SwachataAbhiyanTextEdit"
        Me.SwachataAbhiyanTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SwachataAbhiyanTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.SwachataAbhiyanTextEdit.Properties.ReadOnly = True
        Me.SwachataAbhiyanTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.SwachataAbhiyanTextEdit.TabIndex = 60
        Me.SwachataAbhiyanTextEdit.TabStop = False
        '
        'KrushiKalyanTextEdit
        '
        Me.KrushiKalyanTextEdit.Location = New System.Drawing.Point(547, 286)
        Me.KrushiKalyanTextEdit.Name = "KrushiKalyanTextEdit"
        Me.KrushiKalyanTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.KrushiKalyanTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.KrushiKalyanTextEdit.Properties.ReadOnly = True
        Me.KrushiKalyanTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.KrushiKalyanTextEdit.TabIndex = 59
        Me.KrushiKalyanTextEdit.TabStop = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(414, 315)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(127, 13)
        Me.LabelControl12.TabIndex = 58
        Me.LabelControl12.Text = "Swachata Abhiyan (0.5%)"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(414, 289)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl11.TabIndex = 57
        Me.LabelControl11.Text = "Krushi Kalyan (0.5%)"
        '
        'CSTAmountTextEdit
        '
        Me.CSTAmountTextEdit.Location = New System.Drawing.Point(324, 393)
        Me.CSTAmountTextEdit.Name = "CSTAmountTextEdit"
        Me.CSTAmountTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CSTAmountTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.CSTAmountTextEdit.Properties.ReadOnly = True
        Me.CSTAmountTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.CSTAmountTextEdit.TabIndex = 56
        Me.CSTAmountTextEdit.TabStop = False
        '
        'CSTTextEdit
        '
        Me.CSTTextEdit.Location = New System.Drawing.Point(289, 393)
        Me.CSTTextEdit.Name = "CSTTextEdit"
        Me.CSTTextEdit.Size = New System.Drawing.Size(29, 20)
        Me.CSTTextEdit.TabIndex = 16
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(216, 396)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl10.TabIndex = 55
        Me.LabelControl10.Text = "CST(%)"
        '
        'PODateDateEdit
        '
        Me.PODateDateEdit.EditValue = Nothing
        Me.PODateDateEdit.Location = New System.Drawing.Point(480, 57)
        Me.PODateDateEdit.MenuManager = Me.BarManager1
        Me.PODateDateEdit.Name = "PODateDateEdit"
        Me.PODateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PODateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PODateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.PODateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.PODateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.PODateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.PODateDateEdit.Size = New System.Drawing.Size(83, 20)
        Me.PODateDateEdit.TabIndex = 7
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem, Me.PrintBarButtonItem1})
        Me.BarManager1.MaxItemId = 5
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintBarButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.Caption = "&New"
        Me.NewBarButtonItem.Glyph = CType(resources.GetObject("NewBarButtonItem.Glyph"), System.Drawing.Image)
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.LargeGlyph = CType(resources.GetObject("NewBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.Caption = "&Open"
        Me.OpenBarButtonItem.Glyph = CType(resources.GetObject("OpenBarButtonItem.Glyph"), System.Drawing.Image)
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.LargeGlyph = CType(resources.GetObject("OpenBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Glyph = CType(resources.GetObject("DeleteBarButtonItem.Glyph"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.LargeGlyph = CType(resources.GetObject("DeleteBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'PrintBarButtonItem1
        '
        Me.PrintBarButtonItem1.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem1.Caption = "Print"
        Me.PrintBarButtonItem1.Glyph = CType(resources.GetObject("PrintBarButtonItem1.Glyph"), System.Drawing.Image)
        Me.PrintBarButtonItem1.Id = 4
        Me.PrintBarButtonItem1.LargeGlyph = CType(resources.GetObject("PrintBarButtonItem1.LargeGlyph"), System.Drawing.Image)
        Me.PrintBarButtonItem1.Name = "PrintBarButtonItem1"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(1081, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 617)
        Me.barDockControlBottom.Size = New System.Drawing.Size(1081, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 586)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1081, 31)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 586)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.Caption = "Print Bill"
        Me.PrintBarButtonItem.Glyph = CType(resources.GetObject("PrintBarButtonItem.Glyph"), System.Drawing.Image)
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.LargeGlyph = CType(resources.GetObject("PrintBarButtonItem.LargeGlyph"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(434, 60)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl8.TabIndex = 53
        Me.LabelControl8.Text = "PO Date"
        '
        'TotalInWordsTextEdit
        '
        Me.TotalInWordsTextEdit.Location = New System.Drawing.Point(94, 445)
        Me.TotalInWordsTextEdit.Name = "TotalInWordsTextEdit"
        Me.TotalInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TotalInWordsTextEdit.Properties.ReadOnly = True
        Me.TotalInWordsTextEdit.Size = New System.Drawing.Size(843, 20)
        Me.TotalInWordsTextEdit.TabIndex = 12
        Me.TotalInWordsTextEdit.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(5, 448)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl4.TabIndex = 51
        Me.LabelControl4.Text = "Rupees In Words"
        '
        'IsVATORCSTComboBoxEdit
        '
        Me.IsVATORCSTComboBoxEdit.Location = New System.Drawing.Point(216, 286)
        Me.IsVATORCSTComboBoxEdit.MenuManager = Me.BarManager1
        Me.IsVATORCSTComboBoxEdit.Name = "IsVATORCSTComboBoxEdit"
        Me.IsVATORCSTComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.IsVATORCSTComboBoxEdit.Properties.Items.AddRange(New Object() {"VAT", "Service Tax", "CST"})
        Me.IsVATORCSTComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.IsVATORCSTComboBoxEdit.Size = New System.Drawing.Size(189, 20)
        Me.IsVATORCSTComboBoxEdit.TabIndex = 12
        '
        'SerTaxAmountTextEdit
        '
        Me.SerTaxAmountTextEdit.Location = New System.Drawing.Point(324, 312)
        Me.SerTaxAmountTextEdit.Name = "SerTaxAmountTextEdit"
        Me.SerTaxAmountTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SerTaxAmountTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.SerTaxAmountTextEdit.Properties.ReadOnly = True
        Me.SerTaxAmountTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.SerTaxAmountTextEdit.TabIndex = 50
        Me.SerTaxAmountTextEdit.TabStop = False
        '
        'SerTaxTextEdit
        '
        Me.SerTaxTextEdit.Location = New System.Drawing.Point(289, 312)
        Me.SerTaxTextEdit.Name = "SerTaxTextEdit"
        Me.SerTaxTextEdit.Size = New System.Drawing.Size(29, 20)
        Me.SerTaxTextEdit.TabIndex = 13
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(216, 315)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl18.TabIndex = 49
        Me.LabelControl18.Text = "SerTax(%)"
        '
        'RemarksTextEdit
        '
        Me.RemarksTextEdit.Location = New System.Drawing.Point(94, 419)
        Me.RemarksTextEdit.Name = "RemarksTextEdit"
        Me.RemarksTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RemarksTextEdit.Size = New System.Drawing.Size(843, 20)
        Me.RemarksTextEdit.TabIndex = 17
        '
        'JobNoTextEdit
        '
        Me.JobNoTextEdit.Location = New System.Drawing.Point(90, 5)
        Me.JobNoTextEdit.Name = "JobNoTextEdit"
        Me.JobNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.JobNoTextEdit.Size = New System.Drawing.Size(101, 20)
        Me.JobNoTextEdit.TabIndex = 0
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(5, 8)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl15.TabIndex = 43
        Me.LabelControl15.Text = "Job No"
        '
        'AddAmountTextEdit
        '
        Me.AddAmountTextEdit.Location = New System.Drawing.Point(324, 367)
        Me.AddAmountTextEdit.Name = "AddAmountTextEdit"
        Me.AddAmountTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.AddAmountTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.AddAmountTextEdit.Properties.ReadOnly = True
        Me.AddAmountTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.AddAmountTextEdit.TabIndex = 41
        Me.AddAmountTextEdit.TabStop = False
        '
        'VATAmountTextEdit
        '
        Me.VATAmountTextEdit.Location = New System.Drawing.Point(324, 341)
        Me.VATAmountTextEdit.Name = "VATAmountTextEdit"
        Me.VATAmountTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VATAmountTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.VATAmountTextEdit.Properties.ReadOnly = True
        Me.VATAmountTextEdit.Size = New System.Drawing.Size(81, 20)
        Me.VATAmountTextEdit.TabIndex = 40
        Me.VATAmountTextEdit.TabStop = False
        '
        'AddTextEdit
        '
        Me.AddTextEdit.Location = New System.Drawing.Point(289, 367)
        Me.AddTextEdit.Name = "AddTextEdit"
        Me.AddTextEdit.Size = New System.Drawing.Size(29, 20)
        Me.AddTextEdit.TabIndex = 15
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(216, 370)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl14.TabIndex = 38
        Me.LabelControl14.Text = "Add Tax(%)"
        '
        'VATTextEdit
        '
        Me.VATTextEdit.Location = New System.Drawing.Point(289, 341)
        Me.VATTextEdit.Name = "VATTextEdit"
        Me.VATTextEdit.Size = New System.Drawing.Size(29, 20)
        Me.VATTextEdit.TabIndex = 14
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(216, 344)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl13.TabIndex = 35
        Me.LabelControl13.Text = "VAT(%)"
        '
        'OfferNoTextEdit
        '
        Me.OfferNoTextEdit.Location = New System.Drawing.Point(285, 57)
        Me.OfferNoTextEdit.Name = "OfferNoTextEdit"
        Me.OfferNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OfferNoTextEdit.Size = New System.Drawing.Size(143, 20)
        Me.OfferNoTextEdit.TabIndex = 6
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(197, 60)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl9.TabIndex = 22
        Me.LabelControl9.Text = "Offer No / PO No"
        '
        'DCNoTextEdit
        '
        Me.DCNoTextEdit.Location = New System.Drawing.Point(67, 57)
        Me.DCNoTextEdit.Name = "DCNoTextEdit"
        Me.DCNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DCNoTextEdit.Size = New System.Drawing.Size(124, 20)
        Me.DCNoTextEdit.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(5, 60)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "D.C. No"
        '
        'PartyIDLookUpEdit
        '
        Me.PartyIDLookUpEdit.Location = New System.Drawing.Point(67, 31)
        Me.PartyIDLookUpEdit.Name = "PartyIDLookUpEdit"
        Me.PartyIDLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PartyIDLookUpEdit.Properties.NullText = ""
        Me.PartyIDLookUpEdit.Size = New System.Drawing.Size(496, 20)
        Me.PartyIDLookUpEdit.TabIndex = 4
        '
        'DateDateEdit
        '
        Me.DateDateEdit.EditValue = Nothing
        Me.DateDateEdit.Location = New System.Drawing.Point(480, 5)
        Me.DateDateEdit.MenuManager = Me.BarManager1
        Me.DateDateEdit.Name = "DateDateEdit"
        Me.DateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.DateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.DateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.DateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.DateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.DateDateEdit.Size = New System.Drawing.Size(83, 20)
        Me.DateDateEdit.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(451, 8)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Date"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 34)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl2.TabIndex = 18
        Me.LabelControl2.Text = "Party Name"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(5, 422)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl6.TabIndex = 7
        Me.LabelControl6.Text = "Remarks"
        '
        'NetAmtLabelControl
        '
        Me.NetAmtLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.NetAmtLabelControl.Location = New System.Drawing.Point(414, 369)
        Me.NetAmtLabelControl.Name = "NetAmtLabelControl"
        Me.NetAmtLabelControl.Size = New System.Drawing.Size(66, 14)
        Me.NetAmtLabelControl.TabIndex = 11
        Me.NetAmtLabelControl.Text = "Grand Amt"
        '
        'BillDetailGridControl
        '
        Me.BillDetailGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BillDetailGridControl.Location = New System.Drawing.Point(5, 83)
        Me.BillDetailGridControl.MainView = Me.BillDetailGridView
        Me.BillDetailGridControl.MenuManager = Me.BarManager1
        Me.BillDetailGridControl.Name = "BillDetailGridControl"
        Me.BillDetailGridControl.Size = New System.Drawing.Size(932, 197)
        Me.BillDetailGridControl.TabIndex = 8
        Me.BillDetailGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.BillDetailGridView})
        '
        'BillDetailGridView
        '
        Me.BillDetailGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BillDetailGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.BillDetailGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.BillDetailGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.BillDetailGridView.GridControl = Me.BillDetailGridControl
        Me.BillDetailGridView.Name = "BillDetailGridView"
        Me.BillDetailGridView.OptionsBehavior.FocusLeaveOnTab = True
        Me.BillDetailGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.BillDetailGridView.OptionsNavigation.UseTabKey = False
        Me.BillDetailGridView.OptionsView.ColumnAutoWidth = False
        Me.BillDetailGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.BillDetailGridView.OptionsView.EnableAppearanceOddRow = True
        Me.BillDetailGridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.BillDetailGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.BillDetailGridView.OptionsView.ShowGroupPanel = False
        Me.BillDetailGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.BillDetailGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'TotalAmtTextEdit
        '
        Me.TotalAmtTextEdit.Location = New System.Drawing.Point(94, 286)
        Me.TotalAmtTextEdit.Name = "TotalAmtTextEdit"
        Me.TotalAmtTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtTextEdit.Properties.ReadOnly = True
        Me.TotalAmtTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalAmtTextEdit.TabIndex = 7
        Me.TotalAmtTextEdit.TabStop = False
        '
        'InvoiceNoTextEdit
        '
        Me.InvoiceNoTextEdit.Location = New System.Drawing.Point(340, 5)
        Me.InvoiceNoTextEdit.Name = "InvoiceNoTextEdit"
        Me.InvoiceNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.InvoiceNoTextEdit.Size = New System.Drawing.Size(55, 20)
        Me.InvoiceNoTextEdit.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(197, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Invoice No"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(5, 289)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Total Amt"
        '
        'CancelSimpleButton
        '
        Me.CancelSimpleButton.Image = CType(resources.GetObject("CancelSimpleButton.Image"), System.Drawing.Image)
        Me.CancelSimpleButton.Location = New System.Drawing.Point(180, 481)
        Me.CancelSimpleButton.Name = "CancelSimpleButton"
        Me.CancelSimpleButton.Size = New System.Drawing.Size(75, 30)
        Me.CancelSimpleButton.TabIndex = 2
        Me.CancelSimpleButton.Text = "&Cancel"
        '
        'SaveSimpleButton
        '
        Me.SaveSimpleButton.Image = CType(resources.GetObject("SaveSimpleButton.Image"), System.Drawing.Image)
        Me.SaveSimpleButton.Location = New System.Drawing.Point(99, 481)
        Me.SaveSimpleButton.Name = "SaveSimpleButton"
        Me.SaveSimpleButton.Size = New System.Drawing.Size(75, 30)
        Me.SaveSimpleButton.TabIndex = 1
        Me.SaveSimpleButton.Text = "&Save"
        '
        'TransportationTextEdit
        '
        Me.TransportationTextEdit.Location = New System.Drawing.Point(94, 341)
        Me.TransportationTextEdit.Name = "TransportationTextEdit"
        Me.TransportationTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TransportationTextEdit.TabIndex = 10
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(5, 344)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl20.TabIndex = 79
        Me.LabelControl20.Text = "Transportation"
        '
        'Frm_Invoice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1081, 617)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_Invoice"
        Me.Text = "Sales Invoice"
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.DiscountAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DiscountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubTotalTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FreightAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceYearTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoicePrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobNoPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SwachataAbhiyanTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KrushiKalyanTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CSTAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CSTTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PODateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PODateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IsVATORCSTComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SerTaxAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SerTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemarksTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VATAmountTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VATTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OfferNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DCNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PartyIDLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillDetailGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillDetailGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransportationTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents CancelSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents SaveSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents NetAmtLabelControl As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
    Friend WithEvents DateDateEdit As DevExpress.XtraEditors.DateEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.
    Friend WithEvents BillDetailGridControl As DevExpress.XtraGrid.GridControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
    Friend WithEvents BillDetailGridView As DevExpress.XtraGrid.Views.Grid.GridView
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents TotalAmtTextEdit As DevExpress.XtraEditors.TextEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents InvoiceNoTextEdit As DevExpress.XtraEditors.TextEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
    Friend WithEvents PartyIDLookUpEdit As DevExpress.XtraEditors.LookUpEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents OfferNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DCNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents VATTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddAmountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents VATAmountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PrintBarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents JobNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RemarksTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SerTaxAmountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SerTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents IsVATORCSTComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TotalInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PODateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CSTAmountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CSTTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SwachataAbhiyanTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KrushiKalyanTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JobNoPrefixTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents InvoicePrefixTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents InvoiceYearTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents FreightAmountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SubTotalTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DiscountAmountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DiscountTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransportationTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
End Class
