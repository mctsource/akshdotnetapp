﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_InvoicePL

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_InvoicePL))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintDuplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintTriplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintLPOriginalBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintLPDuplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintLPTriplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintOriginalPrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.SaveSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.CancelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.ReceiverAddressTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalAmtBeforeTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.InvoiceDetailGridControl = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.InvoiceDetailGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.NetAmtLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.InvoiceDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LRNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.RemarksTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TraNameTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverGSTINTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverStateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneeGSTINTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneeStateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AddressCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalGSTTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.GSTReverseChargeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalAmtAfterTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.ReceiverNameComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ConsigneeNameComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverStateCodeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneeStateCodeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.OfferDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.PODateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.VehicalNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.ConsigneeAddressTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AddNewProductSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.PackingTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.PCGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.PSGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.PIGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.PCGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.PSGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.PIGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.ModifiedByLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.CreatedByLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.CreatedByTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.ModifiedByTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TaxInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverPANNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneePANNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.InvoiceNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.OfferNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AddNewPartySimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.JobNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.JobPrefixTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.TransTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.ChallanNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.ChallanDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.ChallanDetailGridControl = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ChallanDetailGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.AddNewRawMaterialSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ClientPOLookUpEdit = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.ARNNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.AddTCSLabel = New DevExpress.XtraEditors.LabelControl()
        Me.TCSRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.TotalTCSLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.TotalTCSTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalAmtTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.ReceiverAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtBeforeTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDetailGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDetailGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LRNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemarksTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TraNameTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddressCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalGSTTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GSTReverseChargeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtAfterTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OfferDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OfferDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PODateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PODateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VehicalNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PackingTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CreatedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModifiedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TaxInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverPANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneePANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OfferNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TransTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanDetailGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanDetailGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ARNNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TCSRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalTCSTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem, Me.PrintOriginalPrintBarButtonItem, Me.PrintDuplicateBarButtonItem, Me.PrintTriplicateBarButtonItem, Me.PrintLPOriginalBarButtonItem, Me.PrintLPDuplicateBarButtonItem, Me.PrintLPTriplicateBarButtonItem, Me.BarButtonItem8})
        Me.BarManager1.MaxItemId = 11
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarButtonItem8, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintDuplicateBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintTriplicateBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintLPOriginalBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintLPDuplicateBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintLPTriplicateBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.Caption = "&New"
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.ImageOptions.Image = CType(resources.GetObject("NewBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.NewBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("NewBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.Caption = "&Open"
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.ImageOptions.Image = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.OpenBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.ImageOptions.Image = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.DeleteBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "Print Original"
        Me.BarButtonItem8.Id = 10
        Me.BarButtonItem8.ImageOptions.Image = CType(resources.GetObject("BarButtonItem8.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem8.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem8.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'PrintDuplicateBarButtonItem
        '
        Me.PrintDuplicateBarButtonItem.Caption = "Print Duplicate"
        Me.PrintDuplicateBarButtonItem.Id = 5
        Me.PrintDuplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintDuplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintDuplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintDuplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintDuplicateBarButtonItem.Name = "PrintDuplicateBarButtonItem"
        '
        'PrintTriplicateBarButtonItem
        '
        Me.PrintTriplicateBarButtonItem.Caption = "Print Triplicate"
        Me.PrintTriplicateBarButtonItem.Id = 6
        Me.PrintTriplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintTriplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintTriplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintTriplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintTriplicateBarButtonItem.Name = "PrintTriplicateBarButtonItem"
        '
        'PrintLPOriginalBarButtonItem
        '
        Me.PrintLPOriginalBarButtonItem.Caption = "Print LH Original"
        Me.PrintLPOriginalBarButtonItem.Id = 7
        Me.PrintLPOriginalBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintLPOriginalBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintLPOriginalBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintLPOriginalBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintLPOriginalBarButtonItem.Name = "PrintLPOriginalBarButtonItem"
        Me.PrintLPOriginalBarButtonItem.RememberLastCommand = True
        '
        'PrintLPDuplicateBarButtonItem
        '
        Me.PrintLPDuplicateBarButtonItem.Caption = "Print LH Duplicate"
        Me.PrintLPDuplicateBarButtonItem.Id = 8
        Me.PrintLPDuplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintLPDuplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintLPDuplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintLPDuplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintLPDuplicateBarButtonItem.Name = "PrintLPDuplicateBarButtonItem"
        '
        'PrintLPTriplicateBarButtonItem
        '
        Me.PrintLPTriplicateBarButtonItem.Caption = "Print LH Triplicate"
        Me.PrintLPTriplicateBarButtonItem.Id = 9
        Me.PrintLPTriplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintLPTriplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintLPTriplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintLPTriplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintLPTriplicateBarButtonItem.Name = "PrintLPTriplicateBarButtonItem"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Margin = New System.Windows.Forms.Padding(4)
        Me.barDockControlTop.Size = New System.Drawing.Size(2055, 46)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 1136)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Margin = New System.Windows.Forms.Padding(4)
        Me.barDockControlBottom.Size = New System.Drawing.Size(2055, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 46)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Margin = New System.Windows.Forms.Padding(4)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 1090)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(2055, 46)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Margin = New System.Windows.Forms.Padding(4)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 1090)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.Caption = "Print Bill"
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'PrintOriginalPrintBarButtonItem
        '
        Me.PrintOriginalPrintBarButtonItem.Caption = "Print Original"
        Me.PrintOriginalPrintBarButtonItem.Id = 4
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintOriginalPrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintOriginalPrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintOriginalPrintBarButtonItem.Name = "PrintOriginalPrintBarButtonItem"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Print Original"
        Me.BarButtonItem1.Id = 4
        Me.BarButtonItem1.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Print Original"
        Me.BarButtonItem2.Id = 4
        Me.BarButtonItem2.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Print Original"
        Me.BarButtonItem3.Id = 4
        Me.BarButtonItem3.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem3.ImageOptions.Image = CType(resources.GetObject("BarButtonItem3.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem3.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem3.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Print Original"
        Me.BarButtonItem4.Id = 4
        Me.BarButtonItem4.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem4.ImageOptions.Image = CType(resources.GetObject("BarButtonItem4.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem4.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem4.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Print Original"
        Me.BarButtonItem5.Id = 4
        Me.BarButtonItem5.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem5.ImageOptions.Image = CType(resources.GetObject("BarButtonItem5.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem5.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem5.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Print Original"
        Me.BarButtonItem6.Id = 4
        Me.BarButtonItem6.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "Print Original"
        Me.BarButtonItem7.Id = 4
        Me.BarButtonItem7.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.BarButtonItem7.ImageOptions.Image = CType(resources.GetObject("BarButtonItem7.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem7.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem7.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'SaveSimpleButton
        '
        Me.SaveSimpleButton.Location = New System.Drawing.Point(332, 1019)
        Me.SaveSimpleButton.Margin = New System.Windows.Forms.Padding(4)
        Me.SaveSimpleButton.Name = "SaveSimpleButton"
        Me.SaveSimpleButton.Size = New System.Drawing.Size(140, 44)
        Me.SaveSimpleButton.TabIndex = 2
        Me.SaveSimpleButton.Text = "&Save 'n' New"
        '
        'CancelSimpleButton
        '
        Me.CancelSimpleButton.Location = New System.Drawing.Point(480, 1019)
        Me.CancelSimpleButton.Margin = New System.Windows.Forms.Padding(4)
        Me.CancelSimpleButton.Name = "CancelSimpleButton"
        Me.CancelSimpleButton.Size = New System.Drawing.Size(112, 44)
        Me.CancelSimpleButton.TabIndex = 3
        Me.CancelSimpleButton.Text = "&Cancel"
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.TotalAmtTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl16)
        Me.PanelCtrl.Controls.Add(Me.TotalTCSTextEdit)
        Me.PanelCtrl.Controls.Add(Me.TotalTCSLabelControl)
        Me.PanelCtrl.Controls.Add(Me.TCSRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.AddTCSLabel)
        Me.PanelCtrl.Controls.Add(Me.ARNNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl20)
        Me.PanelCtrl.Controls.Add(Me.ClientPOLookUpEdit)
        Me.PanelCtrl.Controls.Add(Me.AddNewRawMaterialSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.LabelControl53)
        Me.PanelCtrl.Controls.Add(Me.ChallanDetailGridControl)
        Me.PanelCtrl.Controls.Add(Me.LabelControl10)
        Me.PanelCtrl.Controls.Add(Me.LabelControl11)
        Me.PanelCtrl.Controls.Add(Me.ChallanDateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl12)
        Me.PanelCtrl.Controls.Add(Me.ChallanNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl52)
        Me.PanelCtrl.Controls.Add(Me.TransTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl36)
        Me.PanelCtrl.Controls.Add(Me.LabelControl50)
        Me.PanelCtrl.Controls.Add(Me.JobPrefixTextEdit)
        Me.PanelCtrl.Controls.Add(Me.JobNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl51)
        Me.PanelCtrl.Controls.Add(Me.AddNewPartySimpleButton)
        Me.PanelCtrl.Controls.Add(Me.OfferNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.InvoiceNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.ConsigneePANNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl49)
        Me.PanelCtrl.Controls.Add(Me.ReceiverPANNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl48)
        Me.PanelCtrl.Controls.Add(Me.TaxInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.ModifiedByTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CreatedByTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CreatedByLabelControl)
        Me.PanelCtrl.Controls.Add(Me.ModifiedByLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PIGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl47)
        Me.PanelCtrl.Controls.Add(Me.PSGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl46)
        Me.PanelCtrl.Controls.Add(Me.PCGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl45)
        Me.PanelCtrl.Controls.Add(Me.PIGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl44)
        Me.PanelCtrl.Controls.Add(Me.PSGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl43)
        Me.PanelCtrl.Controls.Add(Me.PCGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl42)
        Me.PanelCtrl.Controls.Add(Me.PackingTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl41)
        Me.PanelCtrl.Controls.Add(Me.LabelControl40)
        Me.PanelCtrl.Controls.Add(Me.LabelControl39)
        Me.PanelCtrl.Controls.Add(Me.LabelControl38)
        Me.PanelCtrl.Controls.Add(Me.LabelControl37)
        Me.PanelCtrl.Controls.Add(Me.LabelControl17)
        Me.PanelCtrl.Controls.Add(Me.LabelControl9)
        Me.PanelCtrl.Controls.Add(Me.AddNewProductSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeAddressTextEdit)
        Me.PanelCtrl.Controls.Add(Me.VehicalNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl35)
        Me.PanelCtrl.Controls.Add(Me.PODateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl33)
        Me.PanelCtrl.Controls.Add(Me.LabelControl34)
        Me.PanelCtrl.Controls.Add(Me.OfferDateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl32)
        Me.PanelCtrl.Controls.Add(Me.LabelControl31)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeStateCodeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl30)
        Me.PanelCtrl.Controls.Add(Me.ReceiverStateCodeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl19)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeNameComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.ReceiverNameComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtAfterTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl18)
        Me.PanelCtrl.Controls.Add(Me.GSTReverseChargeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl14)
        Me.PanelCtrl.Controls.Add(Me.TotalGSTTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl13)
        Me.PanelCtrl.Controls.Add(Me.AddressCheckEdit)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeStateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl26)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeGSTINTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl27)
        Me.PanelCtrl.Controls.Add(Me.LabelControl28)
        Me.PanelCtrl.Controls.Add(Me.LabelControl29)
        Me.PanelCtrl.Controls.Add(Me.LabelControl25)
        Me.PanelCtrl.Controls.Add(Me.ReceiverStateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl24)
        Me.PanelCtrl.Controls.Add(Me.ReceiverGSTINTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl23)
        Me.PanelCtrl.Controls.Add(Me.LabelControl22)
        Me.PanelCtrl.Controls.Add(Me.LabelControl21)
        Me.PanelCtrl.Controls.Add(Me.LabelControl15)
        Me.PanelCtrl.Controls.Add(Me.TraNameTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Controls.Add(Me.TotalInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl4)
        Me.PanelCtrl.Controls.Add(Me.RemarksTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LRNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl5)
        Me.PanelCtrl.Controls.Add(Me.InvoiceDateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl3)
        Me.PanelCtrl.Controls.Add(Me.LabelControl6)
        Me.PanelCtrl.Controls.Add(Me.NetAmtLabelControl)
        Me.PanelCtrl.Controls.Add(Me.InvoiceDetailGridControl)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtBeforeTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Controls.Add(Me.LabelControl7)
        Me.PanelCtrl.Controls.Add(Me.ReceiverAddressTextEdit)
        Me.PanelCtrl.Location = New System.Drawing.Point(8, 7)
        Me.PanelCtrl.Margin = New System.Windows.Forms.Padding(4)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(2000, 994)
        Me.PanelCtrl.TabIndex = 0
        '
        'ReceiverAddressTextEdit
        '
        Me.ReceiverAddressTextEdit.Location = New System.Drawing.Point(735, 94)
        Me.ReceiverAddressTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ReceiverAddressTextEdit.Name = "ReceiverAddressTextEdit"
        Me.ReceiverAddressTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverAddressTextEdit.Size = New System.Drawing.Size(513, 26)
        Me.ReceiverAddressTextEdit.TabIndex = 14
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 590)
        Me.LabelControl7.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(179, 19)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Total Amount Before Tax"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 45)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(76, 19)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Invoice No"
        '
        'TotalAmtBeforeTaxTextEdit
        '
        Me.TotalAmtBeforeTaxTextEdit.Location = New System.Drawing.Point(200, 589)
        Me.TotalAmtBeforeTaxTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalAmtBeforeTaxTextEdit.Name = "TotalAmtBeforeTaxTextEdit"
        Me.TotalAmtBeforeTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtBeforeTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtBeforeTaxTextEdit.Properties.ReadOnly = True
        Me.TotalAmtBeforeTaxTextEdit.Size = New System.Drawing.Size(232, 26)
        Me.TotalAmtBeforeTaxTextEdit.TabIndex = 19
        Me.TotalAmtBeforeTaxTextEdit.TabStop = False
        '
        'InvoiceDetailGridControl
        '
        Me.InvoiceDetailGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.InvoiceDetailGridControl.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.InvoiceDetailGridControl.Location = New System.Drawing.Point(6, 286)
        Me.InvoiceDetailGridControl.MainView = Me.InvoiceDetailGridView
        Me.InvoiceDetailGridControl.Margin = New System.Windows.Forms.Padding(4)
        Me.InvoiceDetailGridControl.MenuManager = Me.BarManager1
        Me.InvoiceDetailGridControl.Name = "InvoiceDetailGridControl"
        Me.InvoiceDetailGridControl.Size = New System.Drawing.Size(1956, 222)
        Me.InvoiceDetailGridControl.TabIndex = 26
        Me.InvoiceDetailGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.InvoiceDetailGridView, Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.InvoiceDetailGridControl
        Me.GridView1.Name = "GridView1"
        '
        'InvoiceDetailGridView
        '
        Me.InvoiceDetailGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.InvoiceDetailGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.InvoiceDetailGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.InvoiceDetailGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.InvoiceDetailGridView.GridControl = Me.InvoiceDetailGridControl
        Me.InvoiceDetailGridView.Name = "InvoiceDetailGridView"
        Me.InvoiceDetailGridView.OptionsCustomization.AllowColumnMoving = False
        Me.InvoiceDetailGridView.OptionsCustomization.AllowFilter = False
        Me.InvoiceDetailGridView.OptionsCustomization.AllowSort = False
        Me.InvoiceDetailGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.InvoiceDetailGridView.OptionsView.ColumnAutoWidth = False
        Me.InvoiceDetailGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.InvoiceDetailGridView.OptionsView.EnableAppearanceOddRow = True
        Me.InvoiceDetailGridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.InvoiceDetailGridView.OptionsView.RowAutoHeight = True
        Me.InvoiceDetailGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.InvoiceDetailGridView.OptionsView.ShowGroupPanel = False
        Me.InvoiceDetailGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.InvoiceDetailGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'NetAmtLabelControl
        '
        Me.NetAmtLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.NetAmtLabelControl.Appearance.Options.UseFont = True
        Me.NetAmtLabelControl.Location = New System.Drawing.Point(1400, 528)
        Me.NetAmtLabelControl.Margin = New System.Windows.Forms.Padding(4)
        Me.NetAmtLabelControl.Name = "NetAmtLabelControl"
        Me.NetAmtLabelControl.Size = New System.Drawing.Size(96, 22)
        Me.NetAmtLabelControl.TabIndex = 11
        Me.NetAmtLabelControl.Text = "Grand Amt"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 636)
        Me.LabelControl6.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(61, 19)
        Me.LabelControl6.TabIndex = 7
        Me.LabelControl6.Text = "Remarks"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(398, 45)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(32, 19)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Date"
        '
        'InvoiceDateDateEdit
        '
        Me.InvoiceDateDateEdit.EditValue = Nothing
        Me.InvoiceDateDateEdit.Location = New System.Drawing.Point(507, 41)
        Me.InvoiceDateDateEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.InvoiceDateDateEdit.MenuManager = Me.BarManager1
        Me.InvoiceDateDateEdit.Name = "InvoiceDateDateEdit"
        Me.InvoiceDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.InvoiceDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.InvoiceDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.InvoiceDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.InvoiceDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.InvoiceDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.InvoiceDateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.InvoiceDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.InvoiceDateDateEdit.Size = New System.Drawing.Size(123, 26)
        Me.InvoiceDateDateEdit.TabIndex = 2
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(398, 209)
        Me.LabelControl5.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(43, 19)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "LR No"
        '
        'LRNoTextEdit
        '
        Me.LRNoTextEdit.Location = New System.Drawing.Point(507, 205)
        Me.LRNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.LRNoTextEdit.Name = "LRNoTextEdit"
        Me.LRNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LRNoTextEdit.Size = New System.Drawing.Size(124, 26)
        Me.LRNoTextEdit.TabIndex = 12
        '
        'RemarksTextEdit
        '
        Me.RemarksTextEdit.Location = New System.Drawing.Point(200, 631)
        Me.RemarksTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.RemarksTextEdit.Name = "RemarksTextEdit"
        Me.RemarksTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RemarksTextEdit.Size = New System.Drawing.Size(732, 26)
        Me.RemarksTextEdit.TabIndex = 33
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(9, 674)
        Me.LabelControl4.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(121, 19)
        Me.LabelControl4.TabIndex = 51
        Me.LabelControl4.Text = "Rupees In Words"
        '
        'TotalInWordsTextEdit
        '
        Me.TotalInWordsTextEdit.Location = New System.Drawing.Point(200, 669)
        Me.TotalInWordsTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalInWordsTextEdit.Name = "TotalInWordsTextEdit"
        Me.TotalInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TotalInWordsTextEdit.Properties.ReadOnly = True
        Me.TotalInWordsTextEdit.Size = New System.Drawing.Size(890, 26)
        Me.TotalInWordsTextEdit.TabIndex = 32
        Me.TotalInWordsTextEdit.TabStop = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 177)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(115, 19)
        Me.LabelControl2.TabIndex = 81
        Me.LabelControl2.Text = "Transport Name"
        '
        'TraNameTextEdit
        '
        Me.TraNameTextEdit.Location = New System.Drawing.Point(132, 172)
        Me.TraNameTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TraNameTextEdit.Name = "TraNameTextEdit"
        Me.TraNameTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TraNameTextEdit.Size = New System.Drawing.Size(246, 26)
        Me.TraNameTextEdit.TabIndex = 9
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(668, 54)
        Me.LabelControl15.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(41, 19)
        Me.LabelControl15.TabIndex = 83
        Me.LabelControl15.Text = "Name"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(668, 4)
        Me.LabelControl21.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(114, 29)
        Me.LabelControl21.TabIndex = 84
        Me.LabelControl21.Text = "Billed to :"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(666, 96)
        Me.LabelControl22.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(57, 19)
        Me.LabelControl22.TabIndex = 86
        Me.LabelControl22.Text = "Address"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(668, 143)
        Me.LabelControl23.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(47, 19)
        Me.LabelControl23.TabIndex = 89
        Me.LabelControl23.Text = "GSTIN"
        '
        'ReceiverGSTINTextEdit
        '
        Me.ReceiverGSTINTextEdit.Location = New System.Drawing.Point(735, 137)
        Me.ReceiverGSTINTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ReceiverGSTINTextEdit.Name = "ReceiverGSTINTextEdit"
        Me.ReceiverGSTINTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverGSTINTextEdit.Size = New System.Drawing.Size(177, 26)
        Me.ReceiverGSTINTextEdit.TabIndex = 15
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(921, 143)
        Me.LabelControl24.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(35, 19)
        Me.LabelControl24.TabIndex = 91
        Me.LabelControl24.Text = "State"
        '
        'ReceiverStateTextEdit
        '
        Me.ReceiverStateTextEdit.Location = New System.Drawing.Point(969, 137)
        Me.ReceiverStateTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ReceiverStateTextEdit.Name = "ReceiverStateTextEdit"
        Me.ReceiverStateTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverStateTextEdit.Size = New System.Drawing.Size(130, 26)
        Me.ReceiverStateTextEdit.TabIndex = 16
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(1362, 6)
        Me.LabelControl25.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(144, 29)
        Me.LabelControl25.TabIndex = 92
        Me.LabelControl25.Text = "Shipped to :"
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(1360, 54)
        Me.LabelControl29.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(41, 19)
        Me.LabelControl29.TabIndex = 94
        Me.LabelControl29.Text = "Name"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(1360, 96)
        Me.LabelControl28.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(57, 19)
        Me.LabelControl28.TabIndex = 95
        Me.LabelControl28.Text = "Address"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(1360, 143)
        Me.LabelControl27.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(47, 19)
        Me.LabelControl27.TabIndex = 98
        Me.LabelControl27.Text = "GSTIN"
        '
        'ConsigneeGSTINTextEdit
        '
        Me.ConsigneeGSTINTextEdit.Location = New System.Drawing.Point(1430, 137)
        Me.ConsigneeGSTINTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsigneeGSTINTextEdit.Name = "ConsigneeGSTINTextEdit"
        Me.ConsigneeGSTINTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeGSTINTextEdit.Size = New System.Drawing.Size(177, 26)
        Me.ConsigneeGSTINTextEdit.TabIndex = 22
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(1616, 143)
        Me.LabelControl26.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(35, 19)
        Me.LabelControl26.TabIndex = 100
        Me.LabelControl26.Text = "State"
        '
        'ConsigneeStateTextEdit
        '
        Me.ConsigneeStateTextEdit.Location = New System.Drawing.Point(1664, 137)
        Me.ConsigneeStateTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsigneeStateTextEdit.Name = "ConsigneeStateTextEdit"
        Me.ConsigneeStateTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeStateTextEdit.Size = New System.Drawing.Size(130, 26)
        Me.ConsigneeStateTextEdit.TabIndex = 23
        '
        'AddressCheckEdit
        '
        Me.AddressCheckEdit.Location = New System.Drawing.Point(972, 180)
        Me.AddressCheckEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.AddressCheckEdit.MenuManager = Me.BarManager1
        Me.AddressCheckEdit.Name = "AddressCheckEdit"
        Me.AddressCheckEdit.Properties.Caption = "If Billing Address is same as Shipping Address"
        Me.AddressCheckEdit.Size = New System.Drawing.Size(352, 23)
        Me.AddressCheckEdit.TabIndex = 19
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(966, 523)
        Me.LabelControl13.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(134, 19)
        Me.LabelControl13.TabIndex = 111
        Me.LabelControl13.Text = "Total Amount:GST"
        '
        'TotalGSTTaxTextEdit
        '
        Me.TotalGSTTaxTextEdit.Location = New System.Drawing.Point(1209, 519)
        Me.TotalGSTTaxTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalGSTTaxTextEdit.Name = "TotalGSTTaxTextEdit"
        Me.TotalGSTTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalGSTTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalGSTTaxTextEdit.Properties.ReadOnly = True
        Me.TotalGSTTaxTextEdit.Size = New System.Drawing.Size(174, 26)
        Me.TotalGSTTaxTextEdit.TabIndex = 28
        Me.TotalGSTTaxTextEdit.TabStop = False
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(1392, 637)
        Me.LabelControl14.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(227, 19)
        Me.LabelControl14.TabIndex = 114
        Me.LabelControl14.Text = "GST Payable on Reverse Charge"
        '
        'GSTReverseChargeTextEdit
        '
        Me.GSTReverseChargeTextEdit.Location = New System.Drawing.Point(1635, 633)
        Me.GSTReverseChargeTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.GSTReverseChargeTextEdit.Name = "GSTReverseChargeTextEdit"
        Me.GSTReverseChargeTextEdit.Size = New System.Drawing.Size(174, 26)
        Me.GSTReverseChargeTextEdit.TabIndex = 32
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(966, 561)
        Me.LabelControl18.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(169, 19)
        Me.LabelControl18.TabIndex = 115
        Me.LabelControl18.Text = "Total Amount After Tax"
        '
        'TotalAmtAfterTaxTextEdit
        '
        Me.TotalAmtAfterTaxTextEdit.Location = New System.Drawing.Point(1209, 557)
        Me.TotalAmtAfterTaxTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalAmtAfterTaxTextEdit.Name = "TotalAmtAfterTaxTextEdit"
        Me.TotalAmtAfterTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtAfterTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtAfterTaxTextEdit.Properties.ReadOnly = True
        Me.TotalAmtAfterTaxTextEdit.Size = New System.Drawing.Size(174, 26)
        Me.TotalAmtAfterTaxTextEdit.TabIndex = 29
        Me.TotalAmtAfterTaxTextEdit.TabStop = False
        '
        'ReceiverNameComboBoxEdit
        '
        Me.ReceiverNameComboBoxEdit.Location = New System.Drawing.Point(735, 50)
        Me.ReceiverNameComboBoxEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ReceiverNameComboBoxEdit.MenuManager = Me.BarManager1
        Me.ReceiverNameComboBoxEdit.Name = "ReceiverNameComboBoxEdit"
        Me.ReceiverNameComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ReceiverNameComboBoxEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverNameComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ReceiverNameComboBoxEdit.Size = New System.Drawing.Size(513, 26)
        Me.ReceiverNameComboBoxEdit.TabIndex = 13
        '
        'ConsigneeNameComboBoxEdit
        '
        Me.ConsigneeNameComboBoxEdit.Location = New System.Drawing.Point(1430, 50)
        Me.ConsigneeNameComboBoxEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsigneeNameComboBoxEdit.MenuManager = Me.BarManager1
        Me.ConsigneeNameComboBoxEdit.Name = "ConsigneeNameComboBoxEdit"
        Me.ConsigneeNameComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ConsigneeNameComboBoxEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeNameComboBoxEdit.Size = New System.Drawing.Size(513, 26)
        Me.ConsigneeNameComboBoxEdit.TabIndex = 20
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(1108, 143)
        Me.LabelControl19.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(76, 19)
        Me.LabelControl19.TabIndex = 120
        Me.LabelControl19.Text = "State Code"
        '
        'ReceiverStateCodeTextEdit
        '
        Me.ReceiverStateCodeTextEdit.Location = New System.Drawing.Point(1198, 137)
        Me.ReceiverStateCodeTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ReceiverStateCodeTextEdit.Name = "ReceiverStateCodeTextEdit"
        Me.ReceiverStateCodeTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverStateCodeTextEdit.Size = New System.Drawing.Size(50, 26)
        Me.ReceiverStateCodeTextEdit.TabIndex = 17
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(1803, 143)
        Me.LabelControl30.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(76, 19)
        Me.LabelControl30.TabIndex = 122
        Me.LabelControl30.Text = "State Code"
        '
        'ConsigneeStateCodeTextEdit
        '
        Me.ConsigneeStateCodeTextEdit.Location = New System.Drawing.Point(1893, 137)
        Me.ConsigneeStateCodeTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsigneeStateCodeTextEdit.Name = "ConsigneeStateCodeTextEdit"
        Me.ConsigneeStateCodeTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeStateCodeTextEdit.Size = New System.Drawing.Size(50, 26)
        Me.ConsigneeStateCodeTextEdit.TabIndex = 24
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(4, 79)
        Me.LabelControl31.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(61, 19)
        Me.LabelControl31.TabIndex = 124
        Me.LabelControl31.Text = "Offer No"
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(398, 79)
        Me.LabelControl32.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(32, 19)
        Me.LabelControl32.TabIndex = 126
        Me.LabelControl32.Text = "Date"
        '
        'OfferDateDateEdit
        '
        Me.OfferDateDateEdit.EditValue = Nothing
        Me.OfferDateDateEdit.Location = New System.Drawing.Point(507, 75)
        Me.OfferDateDateEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.OfferDateDateEdit.MenuManager = Me.BarManager1
        Me.OfferDateDateEdit.Name = "OfferDateDateEdit"
        Me.OfferDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.OfferDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.OfferDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.OfferDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.OfferDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.OfferDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.OfferDateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.OfferDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.OfferDateDateEdit.Size = New System.Drawing.Size(124, 26)
        Me.OfferDateDateEdit.TabIndex = 4
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(8, 107)
        Me.LabelControl34.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(36, 19)
        Me.LabelControl34.TabIndex = 128
        Me.LabelControl34.Text = "P. O."
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(398, 107)
        Me.LabelControl33.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(58, 19)
        Me.LabelControl33.TabIndex = 130
        Me.LabelControl33.Text = "PO Date"
        '
        'PODateDateEdit
        '
        Me.PODateDateEdit.EditValue = Nothing
        Me.PODateDateEdit.Location = New System.Drawing.Point(507, 107)
        Me.PODateDateEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.PODateDateEdit.MenuManager = Me.BarManager1
        Me.PODateDateEdit.Name = "PODateDateEdit"
        Me.PODateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PODateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PODateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.PODateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.PODateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.PODateDateEdit.Size = New System.Drawing.Size(124, 26)
        Me.PODateDateEdit.TabIndex = 6
        '
        'LabelControl35
        '
        Me.LabelControl35.Location = New System.Drawing.Point(398, 177)
        Me.LabelControl35.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(75, 19)
        Me.LabelControl35.TabIndex = 132
        Me.LabelControl35.Text = "Vehical No"
        '
        'VehicalNoTextEdit
        '
        Me.VehicalNoTextEdit.Location = New System.Drawing.Point(507, 172)
        Me.VehicalNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.VehicalNoTextEdit.Name = "VehicalNoTextEdit"
        Me.VehicalNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.VehicalNoTextEdit.Size = New System.Drawing.Size(124, 26)
        Me.VehicalNoTextEdit.TabIndex = 10
        '
        'ConsigneeAddressTextEdit
        '
        Me.ConsigneeAddressTextEdit.Location = New System.Drawing.Point(1430, 94)
        Me.ConsigneeAddressTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsigneeAddressTextEdit.Name = "ConsigneeAddressTextEdit"
        Me.ConsigneeAddressTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeAddressTextEdit.Size = New System.Drawing.Size(513, 26)
        Me.ConsigneeAddressTextEdit.TabIndex = 21
        '
        'AddNewProductSimpleButton
        '
        Me.AddNewProductSimpleButton.Location = New System.Drawing.Point(8, 244)
        Me.AddNewProductSimpleButton.Margin = New System.Windows.Forms.Padding(4)
        Me.AddNewProductSimpleButton.Name = "AddNewProductSimpleButton"
        Me.AddNewProductSimpleButton.Size = New System.Drawing.Size(252, 34)
        Me.AddNewProductSimpleButton.TabIndex = 24
        Me.AddNewProductSimpleButton.TabStop = False
        Me.AddNewProductSimpleButton.Text = "Add New Product"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(93, 50)
        Me.LabelControl9.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl9.TabIndex = 135
        Me.LabelControl9.Text = "*"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl17.Appearance.Options.UseForeColor = True
        Me.LabelControl17.Location = New System.Drawing.Point(441, 82)
        Me.LabelControl17.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl17.TabIndex = 137
        Me.LabelControl17.Text = "*"
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl37.Appearance.Options.UseForeColor = True
        Me.LabelControl37.Location = New System.Drawing.Point(441, 50)
        Me.LabelControl37.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl37.TabIndex = 140
        Me.LabelControl37.Text = "*"
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl38.Appearance.Options.UseForeColor = True
        Me.LabelControl38.Location = New System.Drawing.Point(717, 57)
        Me.LabelControl38.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl38.TabIndex = 141
        Me.LabelControl38.Text = "*"
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl39.Appearance.Options.UseForeColor = True
        Me.LabelControl39.Location = New System.Drawing.Point(1412, 57)
        Me.LabelControl39.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl39.TabIndex = 142
        Me.LabelControl39.Text = "*"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl40.Appearance.Options.UseForeColor = True
        Me.LabelControl40.Location = New System.Drawing.Point(54, 110)
        Me.LabelControl40.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl40.TabIndex = 143
        Me.LabelControl40.Text = "*"
        '
        'LabelControl41
        '
        Me.LabelControl41.Location = New System.Drawing.Point(9, 523)
        Me.LabelControl41.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(116, 19)
        Me.LabelControl41.TabIndex = 145
        Me.LabelControl41.Text = "Packing Charges"
        '
        'PackingTextEdit
        '
        Me.PackingTextEdit.Location = New System.Drawing.Point(200, 519)
        Me.PackingTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.PackingTextEdit.Name = "PackingTextEdit"
        Me.PackingTextEdit.Size = New System.Drawing.Size(232, 26)
        Me.PackingTextEdit.TabIndex = 27
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(462, 523)
        Me.LabelControl42.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(61, 19)
        Me.LabelControl42.TabIndex = 28
        Me.LabelControl42.Text = "CGST %"
        '
        'PCGSTRateTextEdit
        '
        Me.PCGSTRateTextEdit.Location = New System.Drawing.Point(552, 517)
        Me.PCGSTRateTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.PCGSTRateTextEdit.Name = "PCGSTRateTextEdit"
        Me.PCGSTRateTextEdit.Size = New System.Drawing.Size(62, 26)
        Me.PCGSTRateTextEdit.TabIndex = 29
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(622, 523)
        Me.LabelControl43.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(60, 19)
        Me.LabelControl43.TabIndex = 29
        Me.LabelControl43.Text = "SGST %"
        '
        'PSGSTRateTextEdit
        '
        Me.PSGSTRateTextEdit.Location = New System.Drawing.Point(711, 519)
        Me.PSGSTRateTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.PSGSTRateTextEdit.Name = "PSGSTRateTextEdit"
        Me.PSGSTRateTextEdit.Size = New System.Drawing.Size(62, 26)
        Me.PSGSTRateTextEdit.TabIndex = 30
        '
        'LabelControl44
        '
        Me.LabelControl44.Location = New System.Drawing.Point(794, 522)
        Me.LabelControl44.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(57, 19)
        Me.LabelControl44.TabIndex = 30
        Me.LabelControl44.Text = "IGST %"
        '
        'PIGSTRateTextEdit
        '
        Me.PIGSTRateTextEdit.Location = New System.Drawing.Point(870, 519)
        Me.PIGSTRateTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.PIGSTRateTextEdit.Name = "PIGSTRateTextEdit"
        Me.PIGSTRateTextEdit.Size = New System.Drawing.Size(62, 26)
        Me.PIGSTRateTextEdit.TabIndex = 31
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(462, 560)
        Me.LabelControl45.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(75, 19)
        Me.LabelControl45.TabIndex = 154
        Me.LabelControl45.Text = "CGST Amt"
        '
        'PCGSTAmt
        '
        Me.PCGSTAmt.Location = New System.Drawing.Point(552, 555)
        Me.PCGSTAmt.Margin = New System.Windows.Forms.Padding(4)
        Me.PCGSTAmt.Name = "PCGSTAmt"
        Me.PCGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PCGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PCGSTAmt.Properties.ReadOnly = True
        Me.PCGSTAmt.Size = New System.Drawing.Size(62, 26)
        Me.PCGSTAmt.TabIndex = 153
        Me.PCGSTAmt.TabStop = False
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(622, 560)
        Me.LabelControl46.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(74, 19)
        Me.LabelControl46.TabIndex = 24
        Me.LabelControl46.Text = "SGST Amt"
        '
        'PSGSTAmt
        '
        Me.PSGSTAmt.Location = New System.Drawing.Point(711, 555)
        Me.PSGSTAmt.Margin = New System.Windows.Forms.Padding(4)
        Me.PSGSTAmt.Name = "PSGSTAmt"
        Me.PSGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PSGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PSGSTAmt.Properties.ReadOnly = True
        Me.PSGSTAmt.Size = New System.Drawing.Size(62, 26)
        Me.PSGSTAmt.TabIndex = 155
        Me.PSGSTAmt.TabStop = False
        '
        'LabelControl47
        '
        Me.LabelControl47.Location = New System.Drawing.Point(794, 561)
        Me.LabelControl47.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(71, 19)
        Me.LabelControl47.TabIndex = 158
        Me.LabelControl47.Text = "IGST Amt"
        '
        'PIGSTAmt
        '
        Me.PIGSTAmt.Location = New System.Drawing.Point(870, 555)
        Me.PIGSTAmt.Margin = New System.Windows.Forms.Padding(4)
        Me.PIGSTAmt.Name = "PIGSTAmt"
        Me.PIGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PIGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PIGSTAmt.Properties.ReadOnly = True
        Me.PIGSTAmt.Size = New System.Drawing.Size(62, 26)
        Me.PIGSTAmt.TabIndex = 23
        Me.PIGSTAmt.TabStop = False
        '
        'ModifiedByLabelControl
        '
        Me.ModifiedByLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ModifiedByLabelControl.Appearance.Options.UseFont = True
        Me.ModifiedByLabelControl.Location = New System.Drawing.Point(1684, 595)
        Me.ModifiedByLabelControl.Margin = New System.Windows.Forms.Padding(4)
        Me.ModifiedByLabelControl.Name = "ModifiedByLabelControl"
        Me.ModifiedByLabelControl.Size = New System.Drawing.Size(118, 21)
        Me.ModifiedByLabelControl.TabIndex = 162
        Me.ModifiedByLabelControl.Text = "Modified By : "
        '
        'CreatedByLabelControl
        '
        Me.CreatedByLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CreatedByLabelControl.Appearance.Options.UseFont = True
        Me.CreatedByLabelControl.Location = New System.Drawing.Point(1684, 558)
        Me.CreatedByLabelControl.Margin = New System.Windows.Forms.Padding(4)
        Me.CreatedByLabelControl.Name = "CreatedByLabelControl"
        Me.CreatedByLabelControl.Size = New System.Drawing.Size(109, 21)
        Me.CreatedByLabelControl.TabIndex = 163
        Me.CreatedByLabelControl.Text = "Created By : "
        '
        'CreatedByTextEdit
        '
        Me.CreatedByTextEdit.Location = New System.Drawing.Point(1804, 554)
        Me.CreatedByTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.CreatedByTextEdit.Name = "CreatedByTextEdit"
        Me.CreatedByTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreatedByTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.CreatedByTextEdit.Properties.ReadOnly = True
        Me.CreatedByTextEdit.Size = New System.Drawing.Size(159, 26)
        Me.CreatedByTextEdit.TabIndex = 164
        Me.CreatedByTextEdit.TabStop = False
        '
        'ModifiedByTextEdit
        '
        Me.ModifiedByTextEdit.Location = New System.Drawing.Point(1804, 592)
        Me.ModifiedByTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ModifiedByTextEdit.Name = "ModifiedByTextEdit"
        Me.ModifiedByTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ModifiedByTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.ModifiedByTextEdit.Properties.ReadOnly = True
        Me.ModifiedByTextEdit.Size = New System.Drawing.Size(159, 26)
        Me.ModifiedByTextEdit.TabIndex = 165
        Me.ModifiedByTextEdit.TabStop = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(1098, 674)
        Me.LabelControl8.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(96, 19)
        Me.LabelControl8.TabIndex = 136
        Me.LabelControl8.Text = "Tax In Words"
        '
        'TaxInWordsTextEdit
        '
        Me.TaxInWordsTextEdit.Location = New System.Drawing.Point(1204, 669)
        Me.TaxInWordsTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TaxInWordsTextEdit.Name = "TaxInWordsTextEdit"
        Me.TaxInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TaxInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TaxInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TaxInWordsTextEdit.Properties.ReadOnly = True
        Me.TaxInWordsTextEdit.Size = New System.Drawing.Size(759, 26)
        Me.TaxInWordsTextEdit.TabIndex = 135
        Me.TaxInWordsTextEdit.TabStop = False
        '
        'LabelControl48
        '
        Me.LabelControl48.Location = New System.Drawing.Point(668, 183)
        Me.LabelControl48.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(56, 19)
        Me.LabelControl48.TabIndex = 167
        Me.LabelControl48.Text = "PAN No"
        '
        'ReceiverPANNoTextEdit
        '
        Me.ReceiverPANNoTextEdit.Location = New System.Drawing.Point(735, 180)
        Me.ReceiverPANNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ReceiverPANNoTextEdit.Name = "ReceiverPANNoTextEdit"
        Me.ReceiverPANNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverPANNoTextEdit.Size = New System.Drawing.Size(177, 26)
        Me.ReceiverPANNoTextEdit.TabIndex = 18
        '
        'LabelControl49
        '
        Me.LabelControl49.Location = New System.Drawing.Point(1360, 183)
        Me.LabelControl49.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(56, 19)
        Me.LabelControl49.TabIndex = 169
        Me.LabelControl49.Text = "PAN No"
        '
        'ConsigneePANNoTextEdit
        '
        Me.ConsigneePANNoTextEdit.Location = New System.Drawing.Point(1430, 180)
        Me.ConsigneePANNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ConsigneePANNoTextEdit.Name = "ConsigneePANNoTextEdit"
        Me.ConsigneePANNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneePANNoTextEdit.Size = New System.Drawing.Size(177, 26)
        Me.ConsigneePANNoTextEdit.TabIndex = 25
        '
        'InvoiceNoTextEdit
        '
        Me.InvoiceNoTextEdit.Location = New System.Drawing.Point(117, 41)
        Me.InvoiceNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.InvoiceNoTextEdit.Name = "InvoiceNoTextEdit"
        Me.InvoiceNoTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.InvoiceNoTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.InvoiceNoTextEdit.Size = New System.Drawing.Size(261, 26)
        Me.InvoiceNoTextEdit.TabIndex = 1
        '
        'OfferNoTextEdit
        '
        Me.OfferNoTextEdit.Location = New System.Drawing.Point(117, 75)
        Me.OfferNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.OfferNoTextEdit.Name = "OfferNoTextEdit"
        Me.OfferNoTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.OfferNoTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.OfferNoTextEdit.Size = New System.Drawing.Size(261, 26)
        Me.OfferNoTextEdit.TabIndex = 3
        '
        'AddNewPartySimpleButton
        '
        Me.AddNewPartySimpleButton.Appearance.Options.UseTextOptions = True
        Me.AddNewPartySimpleButton.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.AddNewPartySimpleButton.Location = New System.Drawing.Point(1257, 47)
        Me.AddNewPartySimpleButton.Margin = New System.Windows.Forms.Padding(4)
        Me.AddNewPartySimpleButton.Name = "AddNewPartySimpleButton"
        Me.AddNewPartySimpleButton.Size = New System.Drawing.Size(68, 34)
        Me.AddNewPartySimpleButton.TabIndex = 11
        Me.AddNewPartySimpleButton.TabStop = False
        Me.AddNewPartySimpleButton.Text = "Add"
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(8, 13)
        Me.LabelControl51.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(50, 19)
        Me.LabelControl51.TabIndex = 214
        Me.LabelControl51.Text = "Job No"
        '
        'JobNoTextEdit
        '
        Me.JobNoTextEdit.Location = New System.Drawing.Point(170, 9)
        Me.JobNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.JobNoTextEdit.Name = "JobNoTextEdit"
        Me.JobNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.JobNoTextEdit.Size = New System.Drawing.Size(208, 26)
        Me.JobNoTextEdit.TabIndex = 0
        '
        'JobPrefixTextEdit
        '
        Me.JobPrefixTextEdit.EditValue = ""
        Me.JobPrefixTextEdit.Location = New System.Drawing.Point(117, 9)
        Me.JobPrefixTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.JobPrefixTextEdit.Name = "JobPrefixTextEdit"
        Me.JobPrefixTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.JobPrefixTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.JobPrefixTextEdit.Properties.ReadOnly = True
        Me.JobPrefixTextEdit.Size = New System.Drawing.Size(44, 26)
        Me.JobPrefixTextEdit.TabIndex = 215
        Me.JobPrefixTextEdit.TabStop = False
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl50.Appearance.Options.UseForeColor = True
        Me.LabelControl50.Location = New System.Drawing.Point(60, 15)
        Me.LabelControl50.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl50.TabIndex = 216
        Me.LabelControl50.Text = "*"
        '
        'LabelControl36
        '
        Me.LabelControl36.Location = New System.Drawing.Point(9, 558)
        Me.LabelControl36.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(166, 19)
        Me.LabelControl36.TabIndex = 218
        Me.LabelControl36.Text = "Transportation Charges"
        '
        'TransTextEdit
        '
        Me.TransTextEdit.Location = New System.Drawing.Point(200, 554)
        Me.TransTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TransTextEdit.Name = "TransTextEdit"
        Me.TransTextEdit.Size = New System.Drawing.Size(232, 26)
        Me.TransTextEdit.TabIndex = 28
        '
        'LabelControl52
        '
        Me.LabelControl52.Location = New System.Drawing.Point(8, 139)
        Me.LabelControl52.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(77, 19)
        Me.LabelControl52.TabIndex = 221
        Me.LabelControl52.Text = "Challan No"
        '
        'ChallanNoTextEdit
        '
        Me.ChallanNoTextEdit.Location = New System.Drawing.Point(117, 139)
        Me.ChallanNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ChallanNoTextEdit.Name = "ChallanNoTextEdit"
        Me.ChallanNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ChallanNoTextEdit.Size = New System.Drawing.Size(261, 26)
        Me.ChallanNoTextEdit.TabIndex = 7
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(398, 139)
        Me.LabelControl12.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(89, 19)
        Me.LabelControl12.TabIndex = 222
        Me.LabelControl12.Text = "Challan Date"
        '
        'ChallanDateDateEdit
        '
        Me.ChallanDateDateEdit.EditValue = Nothing
        Me.ChallanDateDateEdit.Location = New System.Drawing.Point(507, 139)
        Me.ChallanDateDateEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ChallanDateDateEdit.MenuManager = Me.BarManager1
        Me.ChallanDateDateEdit.Name = "ChallanDateDateEdit"
        Me.ChallanDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ChallanDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ChallanDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.ChallanDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.ChallanDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.ChallanDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.ChallanDateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.ChallanDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.ChallanDateDateEdit.Size = New System.Drawing.Size(124, 26)
        Me.ChallanDateDateEdit.TabIndex = 8
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl11.Appearance.Options.UseForeColor = True
        Me.LabelControl11.Location = New System.Drawing.Point(492, 142)
        Me.LabelControl11.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl11.TabIndex = 223
        Me.LabelControl11.Text = "*"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(99, 142)
        Me.LabelControl10.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(9, 19)
        Me.LabelControl10.TabIndex = 224
        Me.LabelControl10.Text = "*"
        '
        'ChallanDetailGridControl
        '
        Me.ChallanDetailGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ChallanDetailGridControl.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.ChallanDetailGridControl.Location = New System.Drawing.Point(6, 759)
        Me.ChallanDetailGridControl.MainView = Me.ChallanDetailGridView
        Me.ChallanDetailGridControl.Margin = New System.Windows.Forms.Padding(4)
        Me.ChallanDetailGridControl.MenuManager = Me.BarManager1
        Me.ChallanDetailGridControl.Name = "ChallanDetailGridControl"
        Me.ChallanDetailGridControl.Size = New System.Drawing.Size(1956, 222)
        Me.ChallanDetailGridControl.TabIndex = 34
        Me.ChallanDetailGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ChallanDetailGridView, Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.ChallanDetailGridControl
        Me.GridView2.Name = "GridView2"
        '
        'ChallanDetailGridView
        '
        Me.ChallanDetailGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ChallanDetailGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.ChallanDetailGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.ChallanDetailGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.ChallanDetailGridView.GridControl = Me.ChallanDetailGridControl
        Me.ChallanDetailGridView.Name = "ChallanDetailGridView"
        Me.ChallanDetailGridView.OptionsCustomization.AllowColumnMoving = False
        Me.ChallanDetailGridView.OptionsCustomization.AllowFilter = False
        Me.ChallanDetailGridView.OptionsCustomization.AllowSort = False
        Me.ChallanDetailGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.ChallanDetailGridView.OptionsView.ColumnAutoWidth = False
        Me.ChallanDetailGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.ChallanDetailGridView.OptionsView.EnableAppearanceOddRow = True
        Me.ChallanDetailGridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.ChallanDetailGridView.OptionsView.RowAutoHeight = True
        Me.ChallanDetailGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.ChallanDetailGridView.OptionsView.ShowGroupPanel = False
        Me.ChallanDetailGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChallanDetailGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(9, 723)
        Me.LabelControl53.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(208, 24)
        Me.LabelControl53.TabIndex = 226
        Me.LabelControl53.Text = "Packing List/Challan"
        '
        'AddNewRawMaterialSimpleButton
        '
        Me.AddNewRawMaterialSimpleButton.Location = New System.Drawing.Point(226, 715)
        Me.AddNewRawMaterialSimpleButton.Margin = New System.Windows.Forms.Padding(4)
        Me.AddNewRawMaterialSimpleButton.Name = "AddNewRawMaterialSimpleButton"
        Me.AddNewRawMaterialSimpleButton.Size = New System.Drawing.Size(252, 34)
        Me.AddNewRawMaterialSimpleButton.TabIndex = 227
        Me.AddNewRawMaterialSimpleButton.TabStop = False
        Me.AddNewRawMaterialSimpleButton.Text = "Add New Raw Material"
        '
        'ClientPOLookUpEdit
        '
        Me.ClientPOLookUpEdit.Location = New System.Drawing.Point(117, 107)
        Me.ClientPOLookUpEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ClientPOLookUpEdit.Name = "ClientPOLookUpEdit"
        Me.ClientPOLookUpEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ClientPOLookUpEdit.Properties.NullText = ""
        Me.ClientPOLookUpEdit.Size = New System.Drawing.Size(261, 26)
        Me.ClientPOLookUpEdit.TabIndex = 5
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(8, 210)
        Me.LabelControl20.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(94, 19)
        Me.LabelControl20.TabIndex = 231
        Me.LabelControl20.Text = "ARN Number"
        '
        'ARNNoTextEdit
        '
        Me.ARNNoTextEdit.Location = New System.Drawing.Point(132, 206)
        Me.ARNNoTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.ARNNoTextEdit.Name = "ARNNoTextEdit"
        Me.ARNNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ARNNoTextEdit.Size = New System.Drawing.Size(246, 26)
        Me.ARNNoTextEdit.TabIndex = 11
        '
        'AddTCSLabel
        '
        Me.AddTCSLabel.Location = New System.Drawing.Point(778, 599)
        Me.AddTCSLabel.Margin = New System.Windows.Forms.Padding(4)
        Me.AddTCSLabel.Name = "AddTCSLabel"
        Me.AddTCSLabel.Size = New System.Drawing.Size(84, 19)
        Me.AddTCSLabel.TabIndex = 235
        Me.AddTCSLabel.Text = "Add TCS %"
        '
        'TCSRateTextEdit
        '
        Me.TCSRateTextEdit.Location = New System.Drawing.Point(870, 593)
        Me.TCSRateTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TCSRateTextEdit.Name = "TCSRateTextEdit"
        Me.TCSRateTextEdit.Size = New System.Drawing.Size(62, 26)
        Me.TCSRateTextEdit.TabIndex = 234
        '
        'TotalTCSLabelControl
        '
        Me.TotalTCSLabelControl.Location = New System.Drawing.Point(966, 599)
        Me.TotalTCSLabelControl.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalTCSLabelControl.Name = "TotalTCSLabelControl"
        Me.TotalTCSLabelControl.Size = New System.Drawing.Size(70, 19)
        Me.TotalTCSLabelControl.TabIndex = 237
        Me.TotalTCSLabelControl.Text = "Total TCS"
        '
        'TotalTCSTextEdit
        '
        Me.TotalTCSTextEdit.Location = New System.Drawing.Point(1209, 595)
        Me.TotalTCSTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalTCSTextEdit.Name = "TotalTCSTextEdit"
        Me.TotalTCSTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalTCSTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalTCSTextEdit.Properties.ReadOnly = True
        Me.TotalTCSTextEdit.Size = New System.Drawing.Size(174, 26)
        Me.TotalTCSTextEdit.TabIndex = 236
        Me.TotalTCSTextEdit.TabStop = False
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(966, 637)
        Me.LabelControl16.Margin = New System.Windows.Forms.Padding(4)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(98, 19)
        Me.LabelControl16.TabIndex = 239
        Me.LabelControl16.Text = "Total Amount"
        '
        'TotalAmtTextEdit
        '
        Me.TotalAmtTextEdit.Location = New System.Drawing.Point(1209, 633)
        Me.TotalAmtTextEdit.Margin = New System.Windows.Forms.Padding(4)
        Me.TotalAmtTextEdit.Name = "TotalAmtTextEdit"
        Me.TotalAmtTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtTextEdit.Properties.ReadOnly = True
        Me.TotalAmtTextEdit.Size = New System.Drawing.Size(174, 26)
        Me.TotalAmtTextEdit.TabIndex = 238
        Me.TotalAmtTextEdit.TabStop = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(210, 1019)
        Me.SimpleButton1.Margin = New System.Windows.Forms.Padding(4)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(112, 44)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "&Save"
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.SimpleButton1)
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Controls.Add(Me.CancelSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.SaveSimpleButton)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(15, 51)
        Me.PanelCtrlMain.Margin = New System.Windows.Forms.Padding(4)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(2014, 1080)
        Me.PanelCtrlMain.TabIndex = 10
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(15, 54)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Size = New System.Drawing.Size(2040, 1082)
        Me.SplitContainer1.SplitterDistance = 679
        Me.SplitContainer1.SplitterWidth = 6
        Me.SplitContainer1.TabIndex = 2
        '
        'Frm_InvoicePL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1946, 1106)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Frm_InvoicePL"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Invoice"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.ReceiverAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtBeforeTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDetailGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDetailGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LRNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemarksTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TraNameTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddressCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalGSTTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GSTReverseChargeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtAfterTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OfferDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OfferDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PODateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PODateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VehicalNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PackingTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CreatedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModifiedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TaxInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverPANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneePANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OfferNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TransTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanDetailGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanDetailGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientPOLookUpEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ARNNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TCSRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalTCSTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents PrintOriginalPrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents PrintDuplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintTriplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintLPOriginalBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintLPDuplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintLPTriplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TotalAmtTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalTCSTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TotalTCSLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TCSRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AddTCSLabel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ARNNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ClientPOLookUpEdit As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents AddNewRawMaterialSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ChallanDetailGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents ChallanDetailGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ChallanDateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ChallanNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JobPrefixTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JobNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddNewPartySimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents OfferNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents InvoiceNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ConsigneePANNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverPANNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TaxInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ModifiedByTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CreatedByTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CreatedByLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ModifiedByLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PIGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PSGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PCGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PIGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PSGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PCGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PackingTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddNewProductSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ConsigneeAddressTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents VehicalNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PODateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OfferDateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeStateCodeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverStateCodeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeNameComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ReceiverNameComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TotalAmtAfterTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GSTReverseChargeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalGSTTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddressCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ConsigneeStateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeGSTINTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverStateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverGSTINTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TraNameTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RemarksTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LRNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents InvoiceDateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NetAmtLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents InvoiceDetailGridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents InvoiceDetailGridView As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TotalAmtBeforeTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverAddressTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CancelSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SaveSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SplitContainer1 As SplitContainer
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
End Class
