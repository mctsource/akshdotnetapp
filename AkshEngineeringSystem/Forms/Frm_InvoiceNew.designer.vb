﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Frm_InvoiceNew

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.
    Inherits DevExpress.XtraEditors.XtraForm
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.XtraForm' is not defined.

    'Form overrides dispose to clean up the component list.
#Disable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
#Enable Warning BC30284 ' sub 'Dispose' cannot be declared 'Overrides' because it does not override a sub in a base class.
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_InvoiceNew))
        Me.PanelCtrlMain = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelCtrl = New DevExpress.XtraEditors.PanelControl()
        Me.TransTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.JobPrefixTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.JobNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.AddNewPartySimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ChallanNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.InvoiceNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.ConsigneePANNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverPANNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.TaxInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ModifiedByTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.CreatedByTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.CreatedByLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.ModifiedByLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.PIGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.PSGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.PCGSTAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.PIGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.PSGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.PCGSTRateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.PackingTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.AddNewProductSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ConsigneeAddressTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.VehicalNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.PODateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.NewBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.OpenBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.DeleteBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintOriginalPrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintDuplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.PrintTriplicateBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.PrintBarButtonItem = New DevExpress.XtraBars.BarButtonItem()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.POTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.ChallanDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneeStateCodeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverStateCodeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneeNameComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ReceiverNameComboBoxEdit = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TotalAmtAfterTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.GSTReverseChargeTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalGSTTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.AddressCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.ConsigneeStateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.ConsigneeGSTINTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverStateTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverGSTINTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TraNameTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TotalInWordsTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.RemarksTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LRNoTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.InvoiceDateDateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.NetAmtLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.InvoiceDetailGridControl = New DevExpress.XtraGrid.GridControl()
        Me.InvoiceDetailGridView = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.TotalAmtBeforeTaxTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ReceiverAddressTextEdit = New DevExpress.XtraEditors.TextEdit()
        Me.CancelSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.SaveNewSimpleButton = New DevExpress.XtraEditors.SimpleButton()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrlMain.SuspendLayout()
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCtrl.SuspendLayout()
        CType(Me.TransTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneePANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverPANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TaxInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModifiedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CreatedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCGSTAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PCGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PackingTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VehicalNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PODateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PODateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.POTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChallanDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtAfterTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GSTReverseChargeTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalGSTTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AddressCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsigneeGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TraNameTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RemarksTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LRNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDetailGridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceDetailGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TotalAmtBeforeTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceiverAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelCtrlMain
        '
        Me.PanelCtrlMain.Controls.Add(Me.SimpleButton1)
        Me.PanelCtrlMain.Controls.Add(Me.PanelCtrl)
        Me.PanelCtrlMain.Controls.Add(Me.CancelSimpleButton)
        Me.PanelCtrlMain.Controls.Add(Me.SaveNewSimpleButton)
        Me.PanelCtrlMain.Location = New System.Drawing.Point(12, 37)
        Me.PanelCtrlMain.Name = "PanelCtrlMain"
        Me.PanelCtrlMain.Size = New System.Drawing.Size(1358, 508)
        Me.PanelCtrlMain.TabIndex = 0
        '
        'SimpleButton1
        '
        Me.SimpleButton1.ImageOptions.Image = CType(resources.GetObject("SimpleButton1.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(140, 469)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 30)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "&Save"
        '
        'PanelCtrl
        '
        Me.PanelCtrl.Controls.Add(Me.TransTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl36)
        Me.PanelCtrl.Controls.Add(Me.LabelControl50)
        Me.PanelCtrl.Controls.Add(Me.JobPrefixTextEdit)
        Me.PanelCtrl.Controls.Add(Me.JobNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl51)
        Me.PanelCtrl.Controls.Add(Me.AddNewPartySimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ChallanNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.InvoiceNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.ConsigneePANNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl49)
        Me.PanelCtrl.Controls.Add(Me.ReceiverPANNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl48)
        Me.PanelCtrl.Controls.Add(Me.TaxInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl8)
        Me.PanelCtrl.Controls.Add(Me.ModifiedByTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CreatedByTextEdit)
        Me.PanelCtrl.Controls.Add(Me.CreatedByLabelControl)
        Me.PanelCtrl.Controls.Add(Me.ModifiedByLabelControl)
        Me.PanelCtrl.Controls.Add(Me.PIGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl47)
        Me.PanelCtrl.Controls.Add(Me.PSGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl46)
        Me.PanelCtrl.Controls.Add(Me.PCGSTAmt)
        Me.PanelCtrl.Controls.Add(Me.LabelControl45)
        Me.PanelCtrl.Controls.Add(Me.PIGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl44)
        Me.PanelCtrl.Controls.Add(Me.PSGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl43)
        Me.PanelCtrl.Controls.Add(Me.PCGSTRateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl42)
        Me.PanelCtrl.Controls.Add(Me.PackingTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl41)
        Me.PanelCtrl.Controls.Add(Me.LabelControl40)
        Me.PanelCtrl.Controls.Add(Me.LabelControl39)
        Me.PanelCtrl.Controls.Add(Me.LabelControl38)
        Me.PanelCtrl.Controls.Add(Me.LabelControl37)
        Me.PanelCtrl.Controls.Add(Me.LabelControl20)
        Me.PanelCtrl.Controls.Add(Me.LabelControl17)
        Me.PanelCtrl.Controls.Add(Me.LabelControl16)
        Me.PanelCtrl.Controls.Add(Me.LabelControl9)
        Me.PanelCtrl.Controls.Add(Me.AddNewProductSimpleButton)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeAddressTextEdit)
        Me.PanelCtrl.Controls.Add(Me.VehicalNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl35)
        Me.PanelCtrl.Controls.Add(Me.PODateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl33)
        Me.PanelCtrl.Controls.Add(Me.POTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl34)
        Me.PanelCtrl.Controls.Add(Me.ChallanDateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl32)
        Me.PanelCtrl.Controls.Add(Me.LabelControl31)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeStateCodeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl30)
        Me.PanelCtrl.Controls.Add(Me.ReceiverStateCodeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl19)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeNameComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.ReceiverNameComboBoxEdit)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtAfterTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl18)
        Me.PanelCtrl.Controls.Add(Me.GSTReverseChargeTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl14)
        Me.PanelCtrl.Controls.Add(Me.TotalGSTTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl13)
        Me.PanelCtrl.Controls.Add(Me.AddressCheckEdit)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeStateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl26)
        Me.PanelCtrl.Controls.Add(Me.ConsigneeGSTINTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl27)
        Me.PanelCtrl.Controls.Add(Me.LabelControl28)
        Me.PanelCtrl.Controls.Add(Me.LabelControl29)
        Me.PanelCtrl.Controls.Add(Me.LabelControl25)
        Me.PanelCtrl.Controls.Add(Me.ReceiverStateTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl24)
        Me.PanelCtrl.Controls.Add(Me.ReceiverGSTINTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl23)
        Me.PanelCtrl.Controls.Add(Me.LabelControl22)
        Me.PanelCtrl.Controls.Add(Me.LabelControl21)
        Me.PanelCtrl.Controls.Add(Me.LabelControl15)
        Me.PanelCtrl.Controls.Add(Me.TraNameTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl2)
        Me.PanelCtrl.Controls.Add(Me.TotalInWordsTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl4)
        Me.PanelCtrl.Controls.Add(Me.RemarksTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LRNoTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl5)
        Me.PanelCtrl.Controls.Add(Me.InvoiceDateDateEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl3)
        Me.PanelCtrl.Controls.Add(Me.LabelControl6)
        Me.PanelCtrl.Controls.Add(Me.NetAmtLabelControl)
        Me.PanelCtrl.Controls.Add(Me.InvoiceDetailGridControl)
        Me.PanelCtrl.Controls.Add(Me.TotalAmtBeforeTaxTextEdit)
        Me.PanelCtrl.Controls.Add(Me.LabelControl1)
        Me.PanelCtrl.Controls.Add(Me.LabelControl7)
        Me.PanelCtrl.Controls.Add(Me.ReceiverAddressTextEdit)
        Me.PanelCtrl.Location = New System.Drawing.Point(5, 5)
        Me.PanelCtrl.Name = "PanelCtrl"
        Me.PanelCtrl.Size = New System.Drawing.Size(1348, 458)
        Me.PanelCtrl.TabIndex = 0
        '
        'TransTextEdit
        '
        Me.TransTextEdit.Location = New System.Drawing.Point(133, 352)
        Me.TransTextEdit.Name = "TransTextEdit"
        Me.TransTextEdit.Size = New System.Drawing.Size(155, 20)
        Me.TransTextEdit.TabIndex = 25
        '
        'LabelControl36
        '
        Me.LabelControl36.Location = New System.Drawing.Point(6, 355)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl36.TabIndex = 218
        Me.LabelControl36.Text = "Transportation Charges"
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl50.Appearance.Options.UseForeColor = True
        Me.LabelControl50.Location = New System.Drawing.Point(40, 10)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl50.TabIndex = 216
        Me.LabelControl50.Text = "*"
        '
        'JobPrefixTextEdit
        '
        Me.JobPrefixTextEdit.EditValue = ""
        Me.JobPrefixTextEdit.Location = New System.Drawing.Point(78, 6)
        Me.JobPrefixTextEdit.Name = "JobPrefixTextEdit"
        Me.JobPrefixTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.JobPrefixTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.JobPrefixTextEdit.Properties.ReadOnly = True
        Me.JobPrefixTextEdit.Size = New System.Drawing.Size(29, 20)
        Me.JobPrefixTextEdit.TabIndex = 215
        Me.JobPrefixTextEdit.TabStop = False
        '
        'JobNoTextEdit
        '
        Me.JobNoTextEdit.Location = New System.Drawing.Point(113, 6)
        Me.JobNoTextEdit.Name = "JobNoTextEdit"
        Me.JobNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.JobNoTextEdit.Size = New System.Drawing.Size(139, 20)
        Me.JobNoTextEdit.TabIndex = 0
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl51.TabIndex = 214
        Me.LabelControl51.Text = "Job No"
        '
        'AddNewPartySimpleButton
        '
        Me.AddNewPartySimpleButton.Appearance.Options.UseTextOptions = True
        Me.AddNewPartySimpleButton.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.AddNewPartySimpleButton.Location = New System.Drawing.Point(838, 32)
        Me.AddNewPartySimpleButton.Name = "AddNewPartySimpleButton"
        Me.AddNewPartySimpleButton.Size = New System.Drawing.Size(45, 23)
        Me.AddNewPartySimpleButton.TabIndex = 11
        Me.AddNewPartySimpleButton.TabStop = False
        Me.AddNewPartySimpleButton.Text = "Add"
        '
        'ChallanNoTextEdit
        '
        Me.ChallanNoTextEdit.Location = New System.Drawing.Point(78, 57)
        Me.ChallanNoTextEdit.Name = "ChallanNoTextEdit"
        Me.ChallanNoTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.ChallanNoTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.ChallanNoTextEdit.Size = New System.Drawing.Size(174, 20)
        Me.ChallanNoTextEdit.TabIndex = 3
        '
        'InvoiceNoTextEdit
        '
        Me.InvoiceNoTextEdit.Location = New System.Drawing.Point(78, 31)
        Me.InvoiceNoTextEdit.Name = "InvoiceNoTextEdit"
        Me.InvoiceNoTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.InvoiceNoTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.InvoiceNoTextEdit.Size = New System.Drawing.Size(174, 20)
        Me.InvoiceNoTextEdit.TabIndex = 1
        '
        'ConsigneePANNoTextEdit
        '
        Me.ConsigneePANNoTextEdit.Location = New System.Drawing.Point(953, 123)
        Me.ConsigneePANNoTextEdit.Name = "ConsigneePANNoTextEdit"
        Me.ConsigneePANNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneePANNoTextEdit.Size = New System.Drawing.Size(118, 20)
        Me.ConsigneePANNoTextEdit.TabIndex = 22
        '
        'LabelControl49
        '
        Me.LabelControl49.Location = New System.Drawing.Point(907, 125)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl49.TabIndex = 169
        Me.LabelControl49.Text = "PAN No"
        '
        'ReceiverPANNoTextEdit
        '
        Me.ReceiverPANNoTextEdit.Location = New System.Drawing.Point(490, 123)
        Me.ReceiverPANNoTextEdit.Name = "ReceiverPANNoTextEdit"
        Me.ReceiverPANNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverPANNoTextEdit.Size = New System.Drawing.Size(118, 20)
        Me.ReceiverPANNoTextEdit.TabIndex = 15
        '
        'LabelControl48
        '
        Me.LabelControl48.Location = New System.Drawing.Point(445, 125)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl48.TabIndex = 167
        Me.LabelControl48.Text = "PAN No"
        '
        'TaxInWordsTextEdit
        '
        Me.TaxInWordsTextEdit.Location = New System.Drawing.Point(803, 431)
        Me.TaxInWordsTextEdit.Name = "TaxInWordsTextEdit"
        Me.TaxInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TaxInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TaxInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TaxInWordsTextEdit.Properties.ReadOnly = True
        Me.TaxInWordsTextEdit.Size = New System.Drawing.Size(536, 20)
        Me.TaxInWordsTextEdit.TabIndex = 135
        Me.TaxInWordsTextEdit.TabStop = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(732, 434)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl8.TabIndex = 136
        Me.LabelControl8.Text = "Tax In Words"
        '
        'ModifiedByTextEdit
        '
        Me.ModifiedByTextEdit.Location = New System.Drawing.Point(1233, 378)
        Me.ModifiedByTextEdit.Name = "ModifiedByTextEdit"
        Me.ModifiedByTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ModifiedByTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.ModifiedByTextEdit.Properties.ReadOnly = True
        Me.ModifiedByTextEdit.Size = New System.Drawing.Size(106, 20)
        Me.ModifiedByTextEdit.TabIndex = 165
        Me.ModifiedByTextEdit.TabStop = False
        '
        'CreatedByTextEdit
        '
        Me.CreatedByTextEdit.Location = New System.Drawing.Point(1233, 352)
        Me.CreatedByTextEdit.Name = "CreatedByTextEdit"
        Me.CreatedByTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CreatedByTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.CreatedByTextEdit.Properties.ReadOnly = True
        Me.CreatedByTextEdit.Size = New System.Drawing.Size(106, 20)
        Me.CreatedByTextEdit.TabIndex = 164
        Me.CreatedByTextEdit.TabStop = False
        '
        'CreatedByLabelControl
        '
        Me.CreatedByLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CreatedByLabelControl.Appearance.Options.UseFont = True
        Me.CreatedByLabelControl.Location = New System.Drawing.Point(1153, 355)
        Me.CreatedByLabelControl.Name = "CreatedByLabelControl"
        Me.CreatedByLabelControl.Size = New System.Drawing.Size(71, 13)
        Me.CreatedByLabelControl.TabIndex = 163
        Me.CreatedByLabelControl.Text = "Created By : "
        '
        'ModifiedByLabelControl
        '
        Me.ModifiedByLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ModifiedByLabelControl.Appearance.Options.UseFont = True
        Me.ModifiedByLabelControl.Location = New System.Drawing.Point(1153, 380)
        Me.ModifiedByLabelControl.Name = "ModifiedByLabelControl"
        Me.ModifiedByLabelControl.Size = New System.Drawing.Size(74, 13)
        Me.ModifiedByLabelControl.TabIndex = 162
        Me.ModifiedByLabelControl.Text = "Modified By : "
        '
        'PIGSTAmt
        '
        Me.PIGSTAmt.Location = New System.Drawing.Point(580, 353)
        Me.PIGSTAmt.Name = "PIGSTAmt"
        Me.PIGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PIGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PIGSTAmt.Properties.ReadOnly = True
        Me.PIGSTAmt.Size = New System.Drawing.Size(41, 20)
        Me.PIGSTAmt.TabIndex = 23
        Me.PIGSTAmt.TabStop = False
        '
        'LabelControl47
        '
        Me.LabelControl47.Location = New System.Drawing.Point(527, 356)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl47.TabIndex = 158
        Me.LabelControl47.Text = "IGST Amt"
        '
        'PSGSTAmt
        '
        Me.PSGSTAmt.Location = New System.Drawing.Point(474, 353)
        Me.PSGSTAmt.Name = "PSGSTAmt"
        Me.PSGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PSGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PSGSTAmt.Properties.ReadOnly = True
        Me.PSGSTAmt.Size = New System.Drawing.Size(41, 20)
        Me.PSGSTAmt.TabIndex = 155
        Me.PSGSTAmt.TabStop = False
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(415, 356)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl46.TabIndex = 24
        Me.LabelControl46.Text = "SGST Amt"
        '
        'PCGSTAmt
        '
        Me.PCGSTAmt.Location = New System.Drawing.Point(368, 353)
        Me.PCGSTAmt.Name = "PCGSTAmt"
        Me.PCGSTAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PCGSTAmt.Properties.Appearance.Options.UseBackColor = True
        Me.PCGSTAmt.Properties.ReadOnly = True
        Me.PCGSTAmt.Size = New System.Drawing.Size(41, 20)
        Me.PCGSTAmt.TabIndex = 153
        Me.PCGSTAmt.TabStop = False
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(308, 356)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl45.TabIndex = 154
        Me.LabelControl45.Text = "CGST Amt"
        '
        'PIGSTRateTextEdit
        '
        Me.PIGSTRateTextEdit.Location = New System.Drawing.Point(580, 328)
        Me.PIGSTRateTextEdit.Name = "PIGSTRateTextEdit"
        Me.PIGSTRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.PIGSTRateTextEdit.TabIndex = 28
        '
        'LabelControl44
        '
        Me.LabelControl44.Location = New System.Drawing.Point(527, 331)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl44.TabIndex = 151
        Me.LabelControl44.Text = "IGST %"
        '
        'PSGSTRateTextEdit
        '
        Me.PSGSTRateTextEdit.Location = New System.Drawing.Point(474, 328)
        Me.PSGSTRateTextEdit.Name = "PSGSTRateTextEdit"
        Me.PSGSTRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.PSGSTRateTextEdit.TabIndex = 27
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(415, 331)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl43.TabIndex = 21
        Me.LabelControl43.Text = "SGST %"
        '
        'PCGSTRateTextEdit
        '
        Me.PCGSTRateTextEdit.Location = New System.Drawing.Point(368, 327)
        Me.PCGSTRateTextEdit.Name = "PCGSTRateTextEdit"
        Me.PCGSTRateTextEdit.Size = New System.Drawing.Size(41, 20)
        Me.PCGSTRateTextEdit.TabIndex = 26
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(308, 331)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl42.TabIndex = 147
        Me.LabelControl42.Text = "CGST %"
        '
        'PackingTextEdit
        '
        Me.PackingTextEdit.Location = New System.Drawing.Point(133, 328)
        Me.PackingTextEdit.Name = "PackingTextEdit"
        Me.PackingTextEdit.Size = New System.Drawing.Size(155, 20)
        Me.PackingTextEdit.TabIndex = 24
        '
        'LabelControl41
        '
        Me.LabelControl41.Location = New System.Drawing.Point(6, 331)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl41.TabIndex = 145
        Me.LabelControl41.Text = "Packing Charges"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl40.Appearance.Options.UseForeColor = True
        Me.LabelControl40.Location = New System.Drawing.Point(36, 85)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl40.TabIndex = 143
        Me.LabelControl40.Text = "*"
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl39.Appearance.Options.UseForeColor = True
        Me.LabelControl39.Location = New System.Drawing.Point(941, 39)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl39.TabIndex = 142
        Me.LabelControl39.Text = "*"
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl38.Appearance.Options.UseForeColor = True
        Me.LabelControl38.Location = New System.Drawing.Point(478, 39)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl38.TabIndex = 141
        Me.LabelControl38.Text = "*"
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl37.Appearance.Options.UseForeColor = True
        Me.LabelControl37.Location = New System.Drawing.Point(294, 37)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl37.TabIndex = 140
        Me.LabelControl37.Text = "*"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl20.Appearance.Options.UseForeColor = True
        Me.LabelControl20.Location = New System.Drawing.Point(309, 85)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl20.TabIndex = 138
        Me.LabelControl20.Text = "*"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl17.Appearance.Options.UseForeColor = True
        Me.LabelControl17.Location = New System.Drawing.Point(294, 61)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl17.TabIndex = 137
        Me.LabelControl17.Text = "*"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl16.Appearance.Options.UseForeColor = True
        Me.LabelControl16.Location = New System.Drawing.Point(49, 60)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl16.TabIndex = 136
        Me.LabelControl16.Text = "*"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(62, 34)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(6, 13)
        Me.LabelControl9.TabIndex = 135
        Me.LabelControl9.Text = "*"
        '
        'AddNewProductSimpleButton
        '
        Me.AddNewProductSimpleButton.Location = New System.Drawing.Point(5, 135)
        Me.AddNewProductSimpleButton.Name = "AddNewProductSimpleButton"
        Me.AddNewProductSimpleButton.Size = New System.Drawing.Size(168, 23)
        Me.AddNewProductSimpleButton.TabIndex = 24
        Me.AddNewProductSimpleButton.TabStop = False
        Me.AddNewProductSimpleButton.Text = "Add New Product"
        '
        'ConsigneeAddressTextEdit
        '
        Me.ConsigneeAddressTextEdit.Location = New System.Drawing.Point(953, 64)
        Me.ConsigneeAddressTextEdit.Name = "ConsigneeAddressTextEdit"
        Me.ConsigneeAddressTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeAddressTextEdit.Size = New System.Drawing.Size(342, 20)
        Me.ConsigneeAddressTextEdit.TabIndex = 18
        '
        'VehicalNoTextEdit
        '
        Me.VehicalNoTextEdit.Location = New System.Drawing.Point(327, 108)
        Me.VehicalNoTextEdit.Name = "VehicalNoTextEdit"
        Me.VehicalNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.VehicalNoTextEdit.Size = New System.Drawing.Size(83, 20)
        Me.VehicalNoTextEdit.TabIndex = 8
        '
        'LabelControl35
        '
        Me.LabelControl35.Location = New System.Drawing.Point(265, 111)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(49, 13)
        Me.LabelControl35.TabIndex = 132
        Me.LabelControl35.Text = "Vehical No"
        '
        'PODateDateEdit
        '
        Me.PODateDateEdit.EditValue = Nothing
        Me.PODateDateEdit.Location = New System.Drawing.Point(327, 83)
        Me.PODateDateEdit.MenuManager = Me.BarManager1
        Me.PODateDateEdit.Name = "PODateDateEdit"
        Me.PODateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PODateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PODateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.PODateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.PODateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.PODateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.PODateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.PODateDateEdit.Size = New System.Drawing.Size(83, 20)
        Me.PODateDateEdit.TabIndex = 6
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.NewBarButtonItem, Me.OpenBarButtonItem, Me.PrintBarButtonItem, Me.DeleteBarButtonItem, Me.PrintOriginalPrintBarButtonItem, Me.PrintDuplicateBarButtonItem, Me.PrintTriplicateBarButtonItem})
        Me.BarManager1.MaxItemId = 7
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.NewBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.OpenBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.DeleteBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintOriginalPrintBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintDuplicateBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.PrintTriplicateBarButtonItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'NewBarButtonItem
        '
        Me.NewBarButtonItem.Caption = "&New"
        Me.NewBarButtonItem.Id = 0
        Me.NewBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.NewBarButtonItem.ImageOptions.Image = CType(resources.GetObject("NewBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.NewBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("NewBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.NewBarButtonItem.Name = "NewBarButtonItem"
        '
        'OpenBarButtonItem
        '
        Me.OpenBarButtonItem.Caption = "&Open"
        Me.OpenBarButtonItem.Id = 1
        Me.OpenBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.OpenBarButtonItem.ImageOptions.Image = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.OpenBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("OpenBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.OpenBarButtonItem.Name = "OpenBarButtonItem"
        '
        'DeleteBarButtonItem
        '
        Me.DeleteBarButtonItem.Caption = "Delete"
        Me.DeleteBarButtonItem.Id = 3
        Me.DeleteBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.DeleteBarButtonItem.ImageOptions.Image = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.DeleteBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("DeleteBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.DeleteBarButtonItem.Name = "DeleteBarButtonItem"
        '
        'PrintOriginalPrintBarButtonItem
        '
        Me.PrintOriginalPrintBarButtonItem.Caption = "Print Original"
        Me.PrintOriginalPrintBarButtonItem.Id = 4
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintOriginalPrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintOriginalPrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintOriginalPrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintOriginalPrintBarButtonItem.Name = "PrintOriginalPrintBarButtonItem"
        '
        'PrintDuplicateBarButtonItem
        '
        Me.PrintDuplicateBarButtonItem.Caption = "Print Duplicate"
        Me.PrintDuplicateBarButtonItem.Id = 5
        Me.PrintDuplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintDuplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintDuplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintDuplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintDuplicateBarButtonItem.Name = "PrintDuplicateBarButtonItem"
        '
        'PrintTriplicateBarButtonItem
        '
        Me.PrintTriplicateBarButtonItem.Caption = "Print Triplicate"
        Me.PrintTriplicateBarButtonItem.Id = 6
        Me.PrintTriplicateBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintTriplicateBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintTriplicateBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintTriplicateBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintTriplicateBarButtonItem.Name = "PrintTriplicateBarButtonItem"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1370, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 553)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1370, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 522)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1370, 31)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 522)
        '
        'PrintBarButtonItem
        '
        Me.PrintBarButtonItem.Caption = "Print Bill"
        Me.PrintBarButtonItem.Id = 2
        Me.PrintBarButtonItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.[True]
        Me.PrintBarButtonItem.ImageOptions.Image = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.Image"), System.Drawing.Image)
        Me.PrintBarButtonItem.ImageOptions.LargeImage = CType(resources.GetObject("PrintBarButtonItem.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.PrintBarButtonItem.Name = "PrintBarButtonItem"
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(265, 83)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl33.TabIndex = 130
        Me.LabelControl33.Text = "PO Date"
        '
        'POTextEdit
        '
        Me.POTextEdit.Location = New System.Drawing.Point(78, 83)
        Me.POTextEdit.Name = "POTextEdit"
        Me.POTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.POTextEdit.Size = New System.Drawing.Size(174, 20)
        Me.POTextEdit.TabIndex = 5
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(5, 83)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl34.TabIndex = 128
        Me.LabelControl34.Text = "P. O."
        '
        'ChallanDateDateEdit
        '
        Me.ChallanDateDateEdit.EditValue = Nothing
        Me.ChallanDateDateEdit.Location = New System.Drawing.Point(327, 57)
        Me.ChallanDateDateEdit.MenuManager = Me.BarManager1
        Me.ChallanDateDateEdit.Name = "ChallanDateDateEdit"
        Me.ChallanDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ChallanDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ChallanDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.ChallanDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.ChallanDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.ChallanDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.ChallanDateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.ChallanDateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.ChallanDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.ChallanDateDateEdit.Size = New System.Drawing.Size(83, 20)
        Me.ChallanDateDateEdit.TabIndex = 4
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(265, 60)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl32.TabIndex = 126
        Me.LabelControl32.Text = "Date"
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(3, 60)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl31.TabIndex = 124
        Me.LabelControl31.Text = "Offer No"
        '
        'ConsigneeStateCodeTextEdit
        '
        Me.ConsigneeStateCodeTextEdit.Location = New System.Drawing.Point(1262, 94)
        Me.ConsigneeStateCodeTextEdit.Name = "ConsigneeStateCodeTextEdit"
        Me.ConsigneeStateCodeTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeStateCodeTextEdit.Size = New System.Drawing.Size(33, 20)
        Me.ConsigneeStateCodeTextEdit.TabIndex = 21
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(1202, 98)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl30.TabIndex = 122
        Me.LabelControl30.Text = "State Code"
        '
        'ReceiverStateCodeTextEdit
        '
        Me.ReceiverStateCodeTextEdit.Location = New System.Drawing.Point(799, 94)
        Me.ReceiverStateCodeTextEdit.Name = "ReceiverStateCodeTextEdit"
        Me.ReceiverStateCodeTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverStateCodeTextEdit.Size = New System.Drawing.Size(33, 20)
        Me.ReceiverStateCodeTextEdit.TabIndex = 14
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(739, 98)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl19.TabIndex = 120
        Me.LabelControl19.Text = "State Code"
        '
        'ConsigneeNameComboBoxEdit
        '
        Me.ConsigneeNameComboBoxEdit.Location = New System.Drawing.Point(953, 34)
        Me.ConsigneeNameComboBoxEdit.MenuManager = Me.BarManager1
        Me.ConsigneeNameComboBoxEdit.Name = "ConsigneeNameComboBoxEdit"
        Me.ConsigneeNameComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ConsigneeNameComboBoxEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeNameComboBoxEdit.Size = New System.Drawing.Size(342, 20)
        Me.ConsigneeNameComboBoxEdit.TabIndex = 17
        '
        'ReceiverNameComboBoxEdit
        '
        Me.ReceiverNameComboBoxEdit.Location = New System.Drawing.Point(490, 34)
        Me.ReceiverNameComboBoxEdit.MenuManager = Me.BarManager1
        Me.ReceiverNameComboBoxEdit.Name = "ReceiverNameComboBoxEdit"
        Me.ReceiverNameComboBoxEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ReceiverNameComboBoxEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverNameComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ReceiverNameComboBoxEdit.Size = New System.Drawing.Size(342, 20)
        Me.ReceiverNameComboBoxEdit.TabIndex = 10
        '
        'TotalAmtAfterTaxTextEdit
        '
        Me.TotalAmtAfterTaxTextEdit.Location = New System.Drawing.Point(806, 354)
        Me.TotalAmtAfterTaxTextEdit.Name = "TotalAmtAfterTaxTextEdit"
        Me.TotalAmtAfterTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtAfterTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtAfterTaxTextEdit.Properties.ReadOnly = True
        Me.TotalAmtAfterTaxTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalAmtAfterTaxTextEdit.TabIndex = 29
        Me.TotalAmtAfterTaxTextEdit.TabStop = False
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(644, 357)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl18.TabIndex = 115
        Me.LabelControl18.Text = "Total Amount After Tax"
        '
        'GSTReverseChargeTextEdit
        '
        Me.GSTReverseChargeTextEdit.Location = New System.Drawing.Point(806, 379)
        Me.GSTReverseChargeTextEdit.Name = "GSTReverseChargeTextEdit"
        Me.GSTReverseChargeTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.GSTReverseChargeTextEdit.TabIndex = 29
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(644, 382)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(156, 13)
        Me.LabelControl14.TabIndex = 114
        Me.LabelControl14.Text = "GST Payable on Reverse Charge"
        '
        'TotalGSTTaxTextEdit
        '
        Me.TotalGSTTaxTextEdit.Location = New System.Drawing.Point(806, 328)
        Me.TotalGSTTaxTextEdit.Name = "TotalGSTTaxTextEdit"
        Me.TotalGSTTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalGSTTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalGSTTaxTextEdit.Properties.ReadOnly = True
        Me.TotalGSTTaxTextEdit.Size = New System.Drawing.Size(116, 20)
        Me.TotalGSTTaxTextEdit.TabIndex = 28
        Me.TotalGSTTaxTextEdit.TabStop = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(644, 331)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl13.TabIndex = 111
        Me.LabelControl13.Text = "Total Amount:GST"
        '
        'AddressCheckEdit
        '
        Me.AddressCheckEdit.Location = New System.Drawing.Point(648, 123)
        Me.AddressCheckEdit.MenuManager = Me.BarManager1
        Me.AddressCheckEdit.Name = "AddressCheckEdit"
        Me.AddressCheckEdit.Properties.Caption = "If Billing Address is same as Shipping Address"
        Me.AddressCheckEdit.Size = New System.Drawing.Size(235, 19)
        Me.AddressCheckEdit.TabIndex = 16
        '
        'ConsigneeStateTextEdit
        '
        Me.ConsigneeStateTextEdit.Location = New System.Drawing.Point(1109, 94)
        Me.ConsigneeStateTextEdit.Name = "ConsigneeStateTextEdit"
        Me.ConsigneeStateTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeStateTextEdit.Size = New System.Drawing.Size(87, 20)
        Me.ConsigneeStateTextEdit.TabIndex = 20
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(1077, 98)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl26.TabIndex = 100
        Me.LabelControl26.Text = "State"
        '
        'ConsigneeGSTINTextEdit
        '
        Me.ConsigneeGSTINTextEdit.Location = New System.Drawing.Point(953, 94)
        Me.ConsigneeGSTINTextEdit.Name = "ConsigneeGSTINTextEdit"
        Me.ConsigneeGSTINTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ConsigneeGSTINTextEdit.Size = New System.Drawing.Size(118, 20)
        Me.ConsigneeGSTINTextEdit.TabIndex = 19
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(907, 98)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl27.TabIndex = 98
        Me.LabelControl27.Text = "GSTIN"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(907, 66)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl28.TabIndex = 95
        Me.LabelControl28.Text = "Address"
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(907, 37)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl29.TabIndex = 94
        Me.LabelControl29.Text = "Name"
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(908, 4)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(98, 19)
        Me.LabelControl25.TabIndex = 92
        Me.LabelControl25.Text = "Shipped to :"
        '
        'ReceiverStateTextEdit
        '
        Me.ReceiverStateTextEdit.Location = New System.Drawing.Point(646, 94)
        Me.ReceiverStateTextEdit.Name = "ReceiverStateTextEdit"
        Me.ReceiverStateTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverStateTextEdit.Size = New System.Drawing.Size(87, 20)
        Me.ReceiverStateTextEdit.TabIndex = 13
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(614, 98)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl24.TabIndex = 91
        Me.LabelControl24.Text = "State"
        '
        'ReceiverGSTINTextEdit
        '
        Me.ReceiverGSTINTextEdit.Location = New System.Drawing.Point(490, 94)
        Me.ReceiverGSTINTextEdit.Name = "ReceiverGSTINTextEdit"
        Me.ReceiverGSTINTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverGSTINTextEdit.Size = New System.Drawing.Size(118, 20)
        Me.ReceiverGSTINTextEdit.TabIndex = 12
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(445, 98)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl23.TabIndex = 89
        Me.LabelControl23.Text = "GSTIN"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(444, 66)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl22.TabIndex = 86
        Me.LabelControl22.Text = "Address"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(445, 3)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(79, 19)
        Me.LabelControl21.TabIndex = 84
        Me.LabelControl21.Text = "Billed to :"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(445, 37)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl15.TabIndex = 83
        Me.LabelControl15.Text = "Name"
        '
        'TraNameTextEdit
        '
        Me.TraNameTextEdit.Location = New System.Drawing.Point(88, 108)
        Me.TraNameTextEdit.Name = "TraNameTextEdit"
        Me.TraNameTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TraNameTextEdit.Size = New System.Drawing.Size(164, 20)
        Me.TraNameTextEdit.TabIndex = 7
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 111)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl2.TabIndex = 81
        Me.LabelControl2.Text = "Transport Name"
        '
        'TotalInWordsTextEdit
        '
        Me.TotalInWordsTextEdit.Location = New System.Drawing.Point(133, 431)
        Me.TotalInWordsTextEdit.Name = "TotalInWordsTextEdit"
        Me.TotalInWordsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalInWordsTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalInWordsTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TotalInWordsTextEdit.Properties.ReadOnly = True
        Me.TotalInWordsTextEdit.Size = New System.Drawing.Size(593, 20)
        Me.TotalInWordsTextEdit.TabIndex = 32
        Me.TotalInWordsTextEdit.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(6, 434)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl4.TabIndex = 51
        Me.LabelControl4.Text = "Rupees In Words"
        '
        'RemarksTextEdit
        '
        Me.RemarksTextEdit.Location = New System.Drawing.Point(133, 405)
        Me.RemarksTextEdit.Name = "RemarksTextEdit"
        Me.RemarksTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RemarksTextEdit.Size = New System.Drawing.Size(1206, 20)
        Me.RemarksTextEdit.TabIndex = 30
        '
        'LRNoTextEdit
        '
        Me.LRNoTextEdit.Location = New System.Drawing.Point(327, 135)
        Me.LRNoTextEdit.Name = "LRNoTextEdit"
        Me.LRNoTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LRNoTextEdit.Size = New System.Drawing.Size(83, 20)
        Me.LRNoTextEdit.TabIndex = 9
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(265, 138)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "LR No"
        '
        'InvoiceDateDateEdit
        '
        Me.InvoiceDateDateEdit.EditValue = Nothing
        Me.InvoiceDateDateEdit.Location = New System.Drawing.Point(327, 31)
        Me.InvoiceDateDateEdit.MenuManager = Me.BarManager1
        Me.InvoiceDateDateEdit.Name = "InvoiceDateDateEdit"
        Me.InvoiceDateDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.InvoiceDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.InvoiceDateDateEdit.Properties.DisplayFormat.FormatString = "dd-MM-yyyy"
        Me.InvoiceDateDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.InvoiceDateDateEdit.Properties.EditFormat.FormatString = "dd-MM-yyyy"
        Me.InvoiceDateDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.InvoiceDateDateEdit.Properties.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.InvoiceDateDateEdit.Properties.Mask.EditMask = "dd-MM-yyyy"
        Me.InvoiceDateDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.InvoiceDateDateEdit.Size = New System.Drawing.Size(82, 20)
        Me.InvoiceDateDateEdit.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(265, 34)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Date"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(6, 408)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl6.TabIndex = 7
        Me.LabelControl6.Text = "Remarks"
        '
        'NetAmtLabelControl
        '
        Me.NetAmtLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.NetAmtLabelControl.Appearance.Options.UseFont = True
        Me.NetAmtLabelControl.Location = New System.Drawing.Point(933, 363)
        Me.NetAmtLabelControl.Name = "NetAmtLabelControl"
        Me.NetAmtLabelControl.Size = New System.Drawing.Size(66, 14)
        Me.NetAmtLabelControl.TabIndex = 11
        Me.NetAmtLabelControl.Text = "Grand Amt"
        '
        'InvoiceDetailGridControl
        '
        Me.InvoiceDetailGridControl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.InvoiceDetailGridControl.Location = New System.Drawing.Point(5, 164)
        Me.InvoiceDetailGridControl.MainView = Me.InvoiceDetailGridView
        Me.InvoiceDetailGridControl.MenuManager = Me.BarManager1
        Me.InvoiceDetailGridControl.Name = "InvoiceDetailGridControl"
        Me.InvoiceDetailGridControl.Size = New System.Drawing.Size(1334, 158)
        Me.InvoiceDetailGridControl.TabIndex = 23
        Me.InvoiceDetailGridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.InvoiceDetailGridView})
        '
        'InvoiceDetailGridView
        '
        Me.InvoiceDetailGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.InvoiceDetailGridView.Appearance.EvenRow.Options.UseBackColor = True
        Me.InvoiceDetailGridView.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.InvoiceDetailGridView.Appearance.OddRow.Options.UseBackColor = True
        Me.InvoiceDetailGridView.GridControl = Me.InvoiceDetailGridControl
        Me.InvoiceDetailGridView.Name = "InvoiceDetailGridView"
        Me.InvoiceDetailGridView.OptionsCustomization.AllowColumnMoving = False
        Me.InvoiceDetailGridView.OptionsCustomization.AllowFilter = False
        Me.InvoiceDetailGridView.OptionsCustomization.AllowSort = False
        Me.InvoiceDetailGridView.OptionsNavigation.EnterMoveNextColumn = True
        Me.InvoiceDetailGridView.OptionsView.ColumnAutoWidth = False
        Me.InvoiceDetailGridView.OptionsView.EnableAppearanceEvenRow = True
        Me.InvoiceDetailGridView.OptionsView.EnableAppearanceOddRow = True
        Me.InvoiceDetailGridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.InvoiceDetailGridView.OptionsView.RowAutoHeight = True
        Me.InvoiceDetailGridView.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.InvoiceDetailGridView.OptionsView.ShowGroupPanel = False
        Me.InvoiceDetailGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.InvoiceDetailGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'TotalAmtBeforeTaxTextEdit
        '
        Me.TotalAmtBeforeTaxTextEdit.Location = New System.Drawing.Point(133, 376)
        Me.TotalAmtBeforeTaxTextEdit.Name = "TotalAmtBeforeTaxTextEdit"
        Me.TotalAmtBeforeTaxTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalAmtBeforeTaxTextEdit.Properties.Appearance.Options.UseBackColor = True
        Me.TotalAmtBeforeTaxTextEdit.Properties.ReadOnly = True
        Me.TotalAmtBeforeTaxTextEdit.Size = New System.Drawing.Size(186, 20)
        Me.TotalAmtBeforeTaxTextEdit.TabIndex = 19
        Me.TotalAmtBeforeTaxTextEdit.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(5, 34)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Invoice No"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(6, 377)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Total Amount Before Tax"
        '
        'ReceiverAddressTextEdit
        '
        Me.ReceiverAddressTextEdit.Location = New System.Drawing.Point(490, 64)
        Me.ReceiverAddressTextEdit.Name = "ReceiverAddressTextEdit"
        Me.ReceiverAddressTextEdit.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ReceiverAddressTextEdit.Size = New System.Drawing.Size(342, 20)
        Me.ReceiverAddressTextEdit.TabIndex = 11
        '
        'CancelSimpleButton
        '
        Me.CancelSimpleButton.ImageOptions.Image = CType(resources.GetObject("CancelSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.CancelSimpleButton.Location = New System.Drawing.Point(320, 469)
        Me.CancelSimpleButton.Name = "CancelSimpleButton"
        Me.CancelSimpleButton.Size = New System.Drawing.Size(75, 30)
        Me.CancelSimpleButton.TabIndex = 3
        Me.CancelSimpleButton.Text = "&Cancel"
        '
        'SaveNewSimpleButton
        '
        Me.SaveNewSimpleButton.ImageOptions.Image = CType(resources.GetObject("SaveNewSimpleButton.ImageOptions.Image"), System.Drawing.Image)
        Me.SaveNewSimpleButton.Location = New System.Drawing.Point(221, 469)
        Me.SaveNewSimpleButton.Name = "SaveNewSimpleButton"
        Me.SaveNewSimpleButton.Size = New System.Drawing.Size(93, 30)
        Me.SaveNewSimpleButton.TabIndex = 2
        Me.SaveNewSimpleButton.Text = "&Save 'n' New"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'Frm_InvoiceNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 553)
        Me.Controls.Add(Me.PanelCtrlMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_InvoiceNew"
        Me.Text = "Invoice"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PanelCtrlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrlMain.ResumeLayout(False)
        CType(Me.PanelCtrl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCtrl.ResumeLayout(False)
        Me.PanelCtrl.PerformLayout()
        CType(Me.TransTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobPrefixTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneePANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverPANNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TaxInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModifiedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CreatedByTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCGSTAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PCGSTRateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PackingTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VehicalNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PODateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PODateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.POTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChallanDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverStateCodeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverNameComboBoxEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtAfterTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GSTReverseChargeTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalGSTTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AddressCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsigneeGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverStateTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverGSTINTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TraNameTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalInWordsTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RemarksTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LRNoTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDateDateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDateDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDetailGridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceDetailGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TotalAmtBeforeTaxTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceiverAddressTextEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrlMain As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarManager' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.Bar' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents NewBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents OpenBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents PrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.
    Friend WithEvents DeleteBarButtonItem As DevExpress.XtraBars.BarButtonItem
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarButtonItem' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraBars.BarDockControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents CancelSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.
    Friend WithEvents SaveNewSimpleButton As DevExpress.XtraEditors.SimpleButton
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.SimpleButton' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
    Friend WithEvents PanelCtrl As DevExpress.XtraEditors.PanelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.PanelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents NetAmtLabelControl As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
    Friend WithEvents InvoiceDateDateEdit As DevExpress.XtraEditors.DateEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.DateEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.
    Friend WithEvents InvoiceDetailGridControl As DevExpress.XtraGrid.GridControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.GridControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
    Friend WithEvents InvoiceDetailGridView As DevExpress.XtraGrid.Views.Grid.GridView
#Enable Warning BC30002 ' Type 'DevExpress.XtraGrid.Views.Grid.GridView' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents TotalAmtBeforeTaxTextEdit As DevExpress.XtraEditors.TextEdit
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.MemoEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.

#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LookUpEdit' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.LabelControl' is not defined.
#Disable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
    Friend WithEvents LRNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PrintOriginalPrintBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RemarksTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TotalInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TraNameTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ReceiverStateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverGSTINTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeStateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeGSTINTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AddressCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TotalGSTTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TotalAmtAfterTaxTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReceiverNameComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ConsigneeNameComboBoxEdit As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ReceiverStateCodeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeStateCodeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PODateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents POTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ChallanDateDateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents VehicalNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneeAddressTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ReceiverAddressTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PrintDuplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintTriplicateBarButtonItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents AddNewProductSimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TaxInWordsTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PackingTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PCGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PIGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PSGSTRateTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PCGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PIGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PSGSTAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ModifiedByLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CreatedByLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ModifiedByTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CreatedByTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ReceiverPANNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ConsigneePANNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents InvoiceNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ChallanNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents AddNewPartySimpleButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GSTReverseChargeTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JobPrefixTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JobNoTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TransTextEdit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
#Enable Warning BC30002 ' Type 'DevExpress.XtraEditors.TextEdit' is not defined.
End Class
