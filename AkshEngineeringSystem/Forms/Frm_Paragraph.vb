﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI
'Imports ELCF
Public Class Frm_Paragraph
    Dim DA As New OleDbDataAdapter
    Dim DS As New DataSet
    Dim BS As New BindingSource
    Dim DADetails As New OleDbDataAdapter
    Dim Status As Integer = 0

    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetDataTable()
        SetQuery()
        SetBinding()
        'InitLookup()
        'AddNew()
        ParagraphMemoEdit.Focus()
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
        'Dim ID = ShowRecord("Select * From Product Where Company ='" + PubCompanyName + "'", "ProductID")
        Dim ID = ShowRecord("Select * From Paragraph", "ParagraphID")
        If ID > 0 Then
            Try
                DS.Tables("Paragraph").Clear()
            Catch
            End Try
            DA.SelectCommand.Parameters("@ParagraphID").Value = ID
            DA.Fill(DS, "Paragraph")
        End If
    End Sub

    Function SetDataTable()
        DS.Tables.Add("Paragraph")
        With DS.Tables("Paragraph").Columns
            .Add("ParagraphID", GetType(Integer))
            .Add("ParagraphText", GetType(String))
        End With

        With DS.Tables("Paragraph").Columns("ParagraphID")
            .AutoIncrement = True
            .AutoIncrementSeed = -1
            .AutoIncrementStep = -1
            .ReadOnly = True
            .Unique = True
        End With
        Return True
    End Function

    Function SetQuery()
        DA.SelectCommand = New OleDbCommand("Select ParagraphID,ParagraphText From Paragraph Where ParagraphID=@ParagraphID", ConStr)
        DA.SelectCommand.Parameters.Add("@ParagraphID", OleDbType.Integer, 4, "ParagraphID")

        DA.UpdateCommand = New OleDbCommand("Update Paragraph Set ParagraphText=@ParagraphText Where ParagraphID=@ParagraphID", ConStr)
        DA.UpdateCommand.Parameters.Add("@ParagraphText", OleDbType.VarChar, 536870900, "ParagraphText")
        DA.UpdateCommand.Parameters.Add("@paragraphID", OleDbType.Integer, 4, "ParagraphID")

        'DA.DeleteCommand = New OleDbCommand("Delete From Product Where ProductID=@ProductID", ConStr)
        'DA.DeleteCommand.Parameters.Add("@ProductID", OleDbType.Integer, 4, "ProductID")
        Return True
    End Function

    Function SetBinding()
        BS.DataSource = DS.Tables("Paragraph")
        ParagraphMemoEdit.DataBindings.Add(New Binding("EditValue", BS, "ParagraphText"))
        Return True
    End Function
    'Sub InitLookup()
    '    SetLookUp("SELECT CategoryID, CategoryName FROM Category", "Category", "CategoryID", "CategoryName", CategoryIDLookUpEdit, "Category")
    'End Sub

    Sub AddNew()
        BS.AddNew()
        ParagraphMemoEdit.Focus()
    End Sub

    'Private Sub NewBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles NewBarButtonItem.ItemClick
    '    BS.CancelEdit()
    '    DS.RejectChanges()
    '    AddNew()
    '    ParagraphMemoEdit.Focus()
    'End Sub

    'Private Sub OpenBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles OpenBarButtonItem.ItemClick
    '    Dim ID = ShowRecord("Select * From Product Where Company ='" + PubCompanyName + "'", "ProductID")
    '    If ID > 0 Then
    '        Try
    '            DS.Tables("Product").Clear()
    '        Catch
    '        End Try
    '        DA.SelectCommand.Parameters("@ProductID").Value = ID
    '        DA.Fill(DS, "Product")
    '    End If
    'End Sub

    'Private Sub DeleteBarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DeleteBarButtonItem.ItemClick
    '    Try
    '        Dim Delete = XtraMessageBox.Show("Are You Want To Delete This Record", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
    '        If Delete = 1 Then
    '            BS.RemoveCurrent()
    '            DA.Update(DS.Tables("Product"))
    '            AddNew()
    '        End If
    '    Catch ex As Exception
    '        BS.CancelEdit()
    '        DS.RejectChanges()
    '        XtraMessageBox.Show("Operation Failed :", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Function Validation() As Boolean
        If ParagraphMemoEdit.EditValue Is DBNull.Value Or ParagraphMemoEdit.EditValue Is "" Then
            ParagraphMemoEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles SaveSimpleButton.Click
        If Validation() Then
            BS.Current!ParagraphText = ParagraphMemoEdit.Text
            BS.EndEdit()
            DA.Update(DS.Tables("Paragraph"))
            XtraMessageBox.Show("Paragraph Saved Successfully...", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'AddNew()
        Else
            XtraMessageBox.Show("Please Enter Some Text", "Info")
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub

    Private Sub PrintFormat1BarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PrintFormat1BarButtonItem.ItemClick
        Dim Rpt As New XR_LetterHeadWithParagraph
        Rpt.FillDataSource()
        Rpt.XrLabel1.Text = ParagraphMemoEdit.Text
        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Letter Head -")
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub PrintFormat2BarButtonItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PrintFormat2BarButtonItem.ItemClick
        Dim Rpt As New XR_LetterHead2WithParagraph
        Rpt.FillDataSource()
        Rpt.XrLabel1.Text = ParagraphMemoEdit.Text
        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Letter Head -")
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Dim DACompany As New OleDbDataAdapter
        Dim DTCompany As New DataTable

        DACompany.SelectCommand = New OleDbCommand("Select CompanyID,Name,CINNo,Phone4,EmailID1,EmailID2,Website From Company Where Name='" + PubCompanyName + "'", ConStr)
        DACompany.Fill(DTCompany)

        Dim Rpt As New XR_LetterHead3WithParagraph
        Rpt.FillDataSource()
        Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
        Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
        'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
        Rpt.XrLabel125.Text = " Ram Equators, Shop No. 806, 8th Floor, Sr.No. 150, 151P, 152P, 153P, Plot No.1, At.Morwadi,Pimpri,Pune,Maharashtra,India-411033"
        Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
        Rpt.XrLabel42.Text = "Phone: Mo.8411002347, " + DTCompany.Rows(0).Item("Phone4").ToString

        Rpt.XrLabel1.Text = ParagraphMemoEdit.Text
        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Letter Head -")
        End_Waiting()
        Rpt.ShowRibbonPreviewDialog()
    End Sub

    'Private Sub ProductNameTextEdit_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
    '    If Not ProductNameTextEdit.EditValue Is DBNull.Value Then
    '        Try
    '            Dim CMD As New OleDbCommand("SELECT Count(ProductID) FROM Product WHERE ProductName=@ProductName AND CategoryID=@CategoryID AND ProductID<>@ProductID", ConStr)
    '            CMD.Parameters.AddWithValue("@ProductName", ProductNameTextEdit.EditValue)
    '            CMD.Parameters.AddWithValue("@CategoryID", CategoryIDLookUpEdit.EditValue)
    '            CMD.Parameters.AddWithValue("@ProductID", IIf(BS.Current!ProductID Is DBNull.Value, -1, BS.Current!ProductID))
    '            CnnOpen() : Dim Verify As Integer = Val(CMD.ExecuteScalar & "") : CnnClose()
    '            If Verify <> 0 Then
    '                XtraMessageBox.Show("Value Exist! Enter Unique Value.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                ProductNameTextEdit.Focus()
    '                e.Cancel = True
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    End If
    'End Sub

    'Private Sub OpenStockTextEdit_Leave(sender As Object, e As EventArgs)
    '    CurrentStockTextEdit.Text = OpenStockTextEdit.Text
    'End Sub


    'Private Sub HSNNoOrSACNoTextEdit_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles HSNNoOrSACNoTextEdit.Validating
    '    Dim iNumber As Integer
    '    If Not Integer.TryParse(HSNNoOrSACNoTextEdit.Text, iNumber) Then
    '        XtraMessageBox.Show("Not a Number! Please enter only Numerical Values.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        HSNNoOrSACNoTextEdit.Focus()
    '        e.Cancel = True
    '    End If
    'End Sub

    'Private Sub CategoryIDLookUpEdit_EditValueChanged(sender As Object, e As EventArgs) Handles CategoryIDLookUpEdit.EditValueChanged
    '    Try
    '        If (CategoryIDLookUpEdit.EditValue IsNot DBNull.Value) Then
    '            SetLookUp("Select SubCategoryID,SubCategoryName From SubCategory Where CategoryID = " + CategoryIDLookUpEdit.EditValue.ToString + "", "SubCategory", "SubCategoryID", "SubCategoryName", SubCategoryIDLookUpEdit, "Sub Category")
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub
End Class