﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraReports.UI
'Imports ELCF
Public Class Frm_SelectBank

    Dim Invoice As String
    Dim Val As Integer
    Dim ChkIGST As Boolean
    Dim InvoiceType As String

    Public Sub New(ByVal InvoiceID As String, ByVal Flag As Integer, ByVal isIGST As Boolean, ByVal type As String)
        InitializeComponent()
        Invoice = InvoiceID
        Val = Flag
        ChkIGST = isIGST
        InvoiceType = type
    End Sub

    Private Sub FrmSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitLookup()
        Bank1IDLookUpEdit.Focus()
        PanelCtrlMain.Dock = DockStyle.Fill
        themes(PanelCtrl)
    End Sub
    Sub InitLookup()
        SetLookUp("SELECT BankID,BankName FROM Bank Where Company = '" + PubCompanyName + "'", "Bank", "BankID", "BankName", Bank1IDLookUpEdit, "Bank")
    End Sub
    Function Validation() As Boolean
        If Bank1IDLookUpEdit.EditValue Is Nothing Then
            Bank1IDLookUpEdit.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveSimpleButton_Click(sender As Object, e As EventArgs) Handles OkSimpleButton.Click
        If Validation() Then
            Dim DA As New OleDbDataAdapter
            Dim DT As New DataTable

            DA.SelectCommand = New OleDbCommand("Select BankID,CompanyID,BankName,BranchName,AccountNo,MICRCode,IFSCCode,SwiftCode From Bank Where BankID=" + Bank1IDLookUpEdit.EditValue.ToString, ConStr)
            DA.Fill(DT)

            Dim DACompany As New OleDbDataAdapter
            Dim DTCompany As New DataTable

            DACompany.SelectCommand = New OleDbCommand("Select CompanyID,Name,CINNo,Address,Phone1,Phone2,Phone3,Phone4,State,StateCode,EmailID1,EmailID2,Website,GSTIN,PANNo,SupplyFrom From Company Where Name='" + PubCompanyName + "'", ConStr)
            DACompany.Fill(DTCompany)

            Me.Close()

            If ChkIGST = False Then
                If InvoiceType = "Sales Invoice" Then
                    If Val = 1 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_Invoice_Copy
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----                     
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_Invoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----                     
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan Original
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel145.Text = "✔"
                        'Company Info----
                        RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()

                    ElseIf Val = 2 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Duplicate
                            Dim Rpt As New XR_Invoice_Copy
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Duplicate
                            Dim Rpt As New XR_Invoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                        'Challan Duplicate
                        Dim RptCh As New XR_Invoice_Challan

                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel144.Text = "✔"
                        'Company Info----
                        RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()

                    ElseIf Val = 3 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Triplicate
                            Dim Rpt As New XR_Invoice_Copy
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Triplicate
                            Dim Rpt As New XR_Invoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel7.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel4.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                        'Challan Triplicate
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel143.Text = "✔"
                        'Company Info----
                        RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel6.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()
                    ElseIf Val = 4 Then
                        If SingleRadioButton.Checked Then
                            'Invoice LH Original
                            Dim Rpt As New XR_Invoice_Copy
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel37.Visible = False
                            Rpt.XrLabel125.Visible = False
                            Rpt.XrLabel89.Visible = False
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice LH Original
                            Dim Rpt As New XR_Invoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel6.Visible = False
                            Rpt.XrLabel125.Visible = False
                            Rpt.XrLabel89.Visible = False
                            Rpt.XrLabel4.Visible = False
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                        'Challan LH Original                
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel145.Text = "✔"
                        'Company Info----
                        RptCh.XrPictureBox2.Visible = False
                        RptCh.XrLabel90.Visible = False
                        RptCh.XrLabel37.Visible = False
                        RptCh.XrLabel125.Visible = False
                        RptCh.XrLabel89.Visible = False
                        RptCh.XrLabel42.Visible = False
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()

                    ElseIf Val = 5 Then
                        If SingleRadioButton.Checked Then
                            'Invoice LH Duplicate
                            Dim Rpt As New XR_Invoice_Copy
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel37.Visible = False
                            Rpt.XrLabel125.Visible = False
                            Rpt.XrLabel89.Visible = False
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString

                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice LH Duplicate
                            Dim Rpt As New XR_Invoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel6.Visible = False
                            Rpt.XrLabel125.Visible = False
                            Rpt.XrLabel89.Visible = False
                            Rpt.XrLabel4.Visible = False
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString

                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan LH Duplicate
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel144.Text = "✔"
                        'Company Info----
                        RptCh.XrPictureBox2.Visible = False
                        RptCh.XrLabel90.Visible = False
                        RptCh.XrLabel37.Visible = False
                        RptCh.XrLabel125.Visible = False
                        RptCh.XrLabel89.Visible = False
                        RptCh.XrLabel42.Visible = False
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()
                    ElseIf Val = 6 Then
                        If SingleRadioButton.Checked Then
                            'Invoice LH Triplicate
                            Dim Rpt As New XR_Invoice_Copy
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"
                            'Company Info----
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel37.Visible = False
                            Rpt.XrLabel125.Visible = False
                            Rpt.XrLabel89.Visible = False
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice LH Triplicate
                            Dim Rpt As New XR_Invoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel7.Text = "✔"
                            'Company Info----
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel6.Visible = False
                            Rpt.XrLabel125.Visible = False
                            Rpt.XrLabel89.Visible = False
                            Rpt.XrLabel4.Visible = False
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan LH Duplicate
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel144.Text = "✔"
                        'Company Info----
                        RptCh.XrPictureBox2.Visible = False
                        RptCh.XrLabel90.Visible = False
                        RptCh.XrLabel37.Visible = False
                        RptCh.XrLabel125.Visible = False
                        RptCh.XrLabel89.Visible = False
                        RptCh.XrLabel42.Visible = False
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()
                    End If
                ElseIf InvoiceType = "WO Bill" Then
                    If Val = 1 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_WOBillLH
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_WOBillDynamic
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                    ElseIf Val = 2 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_WOBillLH
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_WOBillDynamic
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                    ElseIf Val = 3 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_WOBillLH
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_WOBillDynamic
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                    End If
                ElseIf InvoiceType = "Sales Proforma Invoice" Then
                    If Val = 1 Then
                        'Proforma Invoice 
                        Dim Rpt As New XR_ProInvoice_Copy
                        Rpt.Invoice.Value = Invoice
                        Rpt.Invoice.Visible = False
                        Rpt.FillDataSource()
                        'Rpt.XrLabel143.Text = "✔"
                        'Company Info----
                        Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                        Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                        'Bank Info----
                        Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                        Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                        Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                        Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                        Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Proforma Invoice -")
                        End_Waiting()
                        Rpt.ShowRibbonPreviewDialog()
                    ElseIf Val = 2 Then
                        'Proforma LH Invoice 
                        Dim Rpt As New XR_ProInvoice_Copy
                        Rpt.Invoice.Value = Invoice
                        Rpt.Invoice.Visible = False
                        Rpt.FillDataSource()
                        'Rpt.XrLabel143.Text = "✔"
                        'Company Info----
                        Rpt.XrPictureBox2.Visible = False
                        'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        Rpt.XrLabel90.Visible = False
                        Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                        'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        Rpt.XrLabel37.Visible = False
                        'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        Rpt.XrLabel125.Visible = False
                        'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        Rpt.XrLabel89.Visible = False
                        'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        Rpt.XrLabel42.Visible = False
                        Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                        'Bank Info----
                        Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                        Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                        Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                        Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                        Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Proforma Invoice -")
                        End_Waiting()
                        Rpt.ShowRibbonPreviewDialog()
                    End If
                ElseIf InvoiceType = "SEZ Sales Invoice" Then
                    If PubWithLUT Then
                        If Val = 1 Then
                            'Invoice Original
                            Dim Rpt As New XR_SEZInvoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            'Company Info----                     
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString

                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                            'Challan Original
                            Dim RptCh As New XR_Invoice_Challan
                            RptCh.Invoice.Value = Invoice
                            RptCh.Invoice.Visible = False
                            RptCh.FillDataSource()
                            RptCh.XrLabel145.Text = "✔"
                            'Company Info----
                            RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                            Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                            ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                            End_Waiting()
                            RptCh.ShowRibbonPreviewDialog()
                        End If
                    Else
                        If Val = 1 Then
                            'Invoice Original
                            Dim Rpt As New XR_SEZInvoiceWithoutLUT
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            'Company Info----                     
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString

                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                            'Challan Original
                            Dim RptCh As New XR_Invoice_Challan
                            RptCh.Invoice.Value = Invoice
                            RptCh.Invoice.Visible = False
                            RptCh.FillDataSource()
                            RptCh.XrLabel145.Text = "✔"
                            'Company Info----
                            RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                            Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                            ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                            End_Waiting()
                            RptCh.ShowRibbonPreviewDialog()
                        End If
                    End If
                End If
            ElseIf ChkIGST = True Then
                If InvoiceType = "Sales Invoice IGST" Then
                    If Val = 1 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_Invoice_CopyIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_InvoiceIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan Original
                        'Dim RptCh As New XR_Invoice_Challan
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel145.Text = "✔"
                        'Company Info----
                        'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        'RptCh.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'RptCh.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()

                    ElseIf Val = 2 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Duplicate
                            Dim Rpt As New XR_Invoice_CopyIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Duplicate
                            Dim Rpt As New XR_InvoiceIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan Duplicate
                        'Dim RptCh As New XR_Invoice_Challan
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel145.Text = "✔"
                        'Company Info----
                        'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        'RptCh.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'RptCh.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()

                    ElseIf Val = 3 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Triplicate
                            Dim Rpt As New XR_Invoice_CopyIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Triplicate
                            Dim Rpt As New XR_InvoiceIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel3.Text = "✔"
                            'Company Info----
                            Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                        'Challan Triplicate
                        'Dim RptCh As New XR_Invoice_Challan
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel145.Text = "✔"
                        'Company Info----
                        'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        'RptCh.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'RptCh.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()
                    ElseIf Val = 4 Then
                        If SingleRadioButton.Checked Then
                            'Invoice LH Original
                            Dim Rpt As New XR_Invoice_CopyIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else 'Invoice LH Original
                            Dim Rpt As New XR_InvoiceIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            'Company Info----
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan LH Original                       
                        'Dim RptCh As New XR_Invoice_Challan
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel145.Text = "✔"
                        'Company Info----
                        'RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrPictureBox2.Visible = False
                        RptCh.XrLabel90.Visible = False
                        'RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel37.Visible = False
                        'RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel125.Visible = False
                        'RptCh.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'RptCh.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        'RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel89.Visible = False
                        'RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel42.Visible = False
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()

                    ElseIf Val = 5 Then
                        If SingleRadioButton.Checked Then
                            'Invoice LH Duplicate
                            Dim Rpt As New XR_Invoice_CopyIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice LH Duplicate
                            Dim Rpt As New XR_InvoiceIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"
                            'Company Info----
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan LH Duplicate
                        'Dim RptCh As New XR_Invoice_Challan
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel144.Text = "✔"
                        'Company Info----
                        'RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrPictureBox2.Visible = False
                        RptCh.XrLabel90.Visible = False
                        'RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel37.Visible = False
                        'RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel125.Visible = False
                        'RptCh.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'RptCh.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        'RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel89.Visible = False
                        'RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel42.Visible = False
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()
                    ElseIf Val = 6 Then
                        If SingleRadioButton.Checked Then
                            'Invoice LH Triplicate
                            Dim Rpt As New XR_Invoice_CopyIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"
                            'Company Info----
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel60.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice LH Triplicate
                            Dim Rpt As New XR_InvoiceIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel3.Text = "✔"
                            'Company Info----
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.XrLabel90.Visible = False
                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                            'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                        'Challan LH Duplicate
                        'Dim RptCh As New XR_Invoice_Challan
                        Dim RptCh As New XR_Invoice_Challan
                        RptCh.Invoice.Value = Invoice
                        RptCh.Invoice.Visible = False
                        RptCh.FillDataSource()
                        RptCh.XrLabel144.Text = "✔"
                        'Company Info----
                        'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                        'RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrPictureBox2.Visible = False
                        RptCh.XrLabel90.Visible = False
                        'RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        RptCh.XrLabel37.Visible = False
                        'RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        RptCh.XrLabel125.Visible = False
                        'RptCh.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'RptCh.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        'RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        RptCh.XrLabel89.Visible = False
                        'RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        RptCh.XrLabel42.Visible = False
                        RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                        RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                        Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                        ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                        End_Waiting()
                        RptCh.ShowRibbonPreviewDialog()
                    End If
                ElseIf InvoiceType = "WO Bill IGST" Then
                    If Val = 1 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_WOBillLHIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_WOBillIGSTDynamic
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel145.Text = "✔"
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If
                    ElseIf Val = 2 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_WOBillLHIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"

                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_WOBillIGSTDynamic
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel144.Text = "✔"

                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        End If

                    ElseIf Val = 3 Then
                        If SingleRadioButton.Checked Then
                            'Invoice Original
                            Dim Rpt As New XR_WOBillLHIGST
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"

                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()
                        Else
                            'Invoice Original
                            Dim Rpt As New XR_WOBillIGSTDynamic
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrLabel143.Text = "✔"

                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabelGSTNo.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                        End If

                    End If
                ElseIf InvoiceType = "Sales Proforma Invoice IGST" Then
                    If Val = 1 Then
                        'Invoice Duplicate
                        Dim Rpt As New XR_ProInvoice_CopyIGST
                        Rpt.Invoice.Value = Invoice
                        Rpt.Invoice.Visible = False
                        Rpt.FillDataSource()
                        'Rpt.XrLabel144.Text = "✔"
                        'Company Info----
                        Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                        Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                        'Bank Info----
                        Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                        Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                        Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                        Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                        Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Proforma Invoice -")
                        End_Waiting()
                        Rpt.ShowRibbonPreviewDialog()
                    ElseIf Val = 2 Then
                        'Proforma LH Invoice 
                        Dim Rpt As New XR_ProInvoice_CopyIGST
                        Rpt.Invoice.Value = Invoice
                        Rpt.Invoice.Visible = False
                        Rpt.FillDataSource()
                        'Rpt.XrLabel143.Text = "✔"
                        'Company Info----
                        Rpt.XrPictureBox2.Visible = False
                        'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                        Rpt.XrLabel90.Visible = False
                        Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString
                        'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                        Rpt.XrLabel37.Visible = False
                        'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                        Rpt.XrLabel125.Visible = False
                        'Rpt.XrLabel29.Text = "(P) " + DTCompany.Rows(0).Item("Phone1").ToString + "     Mail Id: " + DTCompany.Rows(0).Item("EmailID1").ToString
                        'Rpt.XrLabel91.Text = "Website: " + DTCompany.Rows(0).Item("Website").ToString
                        'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                        Rpt.XrLabel89.Visible = False
                        'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                        Rpt.XrLabel42.Visible = False
                        Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                        Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                        Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                        Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                        'Rpt.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString
                        'Bank Info----
                        Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                        Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                        Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                        Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                        Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString
                        Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                        ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Proforma Invoice -")
                        End_Waiting()
                        Rpt.ShowRibbonPreviewDialog()
                    End If
                ElseIf InvoiceType = "Export Sales Invoice IGST" Then
                    If PubWithLC Then
                        If Val = 1 Then
                            'Export Print Original 
                            Dim Rpt As New XR_Invoice_Export
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.CNameXrLabel.Visible = False
                            Rpt.CINXrLabel.Visible = False
                            Rpt.CAddXrLabel.Visible = False
                            Rpt.CompanyAddXrLabel.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel6.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.CEmailXrLabel.Visible = False
                            Rpt.CPhoneXrLabel.Visible = False
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Export Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                            Dim RptCh As New XR_ExInvoice_Challan
                            RptCh.Invoice.Value = Invoice
                            RptCh.Invoice.Visible = False
                            RptCh.FillDataSource()

                            'Company Info----
                            'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                            'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                            Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                            ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                            End_Waiting()
                            RptCh.ShowRibbonPreviewDialog()
                        End If
                    Else
                        If Val = 1 Then
                            'Export Print Original 
                            Dim Rpt As New XR_Invoice_ExportWithoutLC
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            Rpt.XrPictureBox2.Visible = False
                            Rpt.CNameXrLabel.Visible = False
                            Rpt.CINXrLabel.Visible = False
                            Rpt.CAddXrLabel.Visible = False
                            Rpt.CompanyAddXrLabel.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.CGstNoXrLabel.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.CPanNoXrLabel.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel12.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.CEmailXrLabel.Visible = False
                            Rpt.CPhoneXrLabel.Visible = False
                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Export Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                            Dim RptCh As New XR_ExInvoice_Challan
                            RptCh.Invoice.Value = Invoice
                            RptCh.Invoice.Visible = False
                            RptCh.FillDataSource()

                            'Company Info----
                            'Dim newString As String = origString.Replace(vbCr, "").Replace(vbLf, "")
                            'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                            Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                            ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                            End_Waiting()
                            RptCh.ShowRibbonPreviewDialog()
                        End If
                    End If
                ElseIf InvoiceType = "SEZ Sales Invoice IGST" Then
                    If PubWithLUT Then
                        If Val = 1 Then
                            'Invoice Original
                            Dim Rpt As New XR_SEZIGSTInvoice
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            'Company Info----                     
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString

                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                            'Challan Original
                            Dim RptCh As New XR_Invoice_Challan
                            RptCh.Invoice.Value = Invoice
                            RptCh.Invoice.Visible = False
                            RptCh.FillDataSource()
                            RptCh.XrLabel145.Text = "✔"
                            'Company Info----
                            RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                            Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                            ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                            End_Waiting()
                            RptCh.ShowRibbonPreviewDialog()
                        End If
                    Else
                        If Val = 1 Then
                            'Invoice Original
                            Dim Rpt As New XR_SEZIGSTInvoiceWithoutLUT
                            Rpt.Invoice.Value = Invoice
                            Rpt.Invoice.Visible = False
                            Rpt.FillDataSource()
                            'Company Info----                     
                            'Rpt.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            Rpt.XrLabel90.Visible = False
                            Rpt.XrLabel48.Text = DTCompany.Rows(0).Item("Name").ToString

                            'Rpt.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            Rpt.XrLabel37.Visible = False
                            'Rpt.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            Rpt.XrLabel125.Visible = False
                            'Rpt.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            Rpt.XrLabel89.Visible = False
                            'Rpt.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            Rpt.XrLabel42.Visible = False
                            Rpt.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            Rpt.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            Rpt.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            Rpt.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'Bank Info----
                            Rpt.XrLabel104.Text = DT.Rows(0).Item("BankName").ToString
                            Rpt.XrLabel100.Text = DT.Rows(0).Item("BranchName").ToString
                            Rpt.XrLabel27.Text = DT.Rows(0).Item("AccountNo").ToString
                            Rpt.XrLabel98.Text = DT.Rows(0).Item("MICRCode").ToString
                            Rpt.XrLabel96.Text = DT.Rows(0).Item("IFSCCode").ToString

                            Dim ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(Rpt)
                            ReportPrintTool.PreviewRibbonForm.Text = String.Format("- Invoice -")
                            End_Waiting()
                            Rpt.ShowRibbonPreviewDialog()

                            'Challan Original
                            Dim RptCh As New XR_Invoice_Challan
                            RptCh.Invoice.Value = Invoice
                            RptCh.Invoice.Visible = False
                            RptCh.FillDataSource()
                            RptCh.XrLabel145.Text = "✔"
                            'Company Info----
                            RptCh.XrLabel90.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel26.Text = DTCompany.Rows(0).Item("Name").ToString
                            RptCh.XrLabel37.Text = "C.I.N. No. : " + DTCompany.Rows(0).Item("CINNo").ToString
                            RptCh.XrLabel125.Text = DTCompany.Rows(0).Item("Address").ToString.Replace(vbCr, "").Replace(vbLf, "")
                            RptCh.XrLabel89.Text = "E-mail: " + DTCompany.Rows(0).Item("EmailID1").ToString + ", " + DTCompany.Rows(0).Item("EmailID2").ToString + ", Website: " + DTCompany.Rows(0).Item("Website").ToString
                            RptCh.XrLabel42.Text = "Phone: " + DTCompany.Rows(0).Item("Phone1").ToString + ", " + DTCompany.Rows(0).Item("Phone2").ToString + ", " + DTCompany.Rows(0).Item("Phone3").ToString + ", " + DTCompany.Rows(0).Item("Phone4").ToString
                            RptCh.XrLabel115.Text = DTCompany.Rows(0).Item("GSTIN").ToString
                            RptCh.XrLabel116.Text = DTCompany.Rows(0).Item("PANNo").ToString
                            RptCh.XrLabel172.Text = DTCompany.Rows(0).Item("State").ToString
                            RptCh.XrLabel170.Text = DTCompany.Rows(0).Item("StateCode").ToString
                            'RptCh.XrLabel175.Text = DTCompany.Rows(0).Item("SupplyFrom").ToString

                            Dim ReportPrintToolCh = New DevExpress.XtraReports.UI.ReportPrintTool(RptCh)
                            ReportPrintToolCh.PreviewRibbonForm.Text = String.Format("- Challan -")
                            End_Waiting()
                            RptCh.ShowRibbonPreviewDialog()
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub CancelSimpleButton_Click(sender As Object, e As EventArgs) Handles CancelSimpleButton.Click
        Me.Close()
    End Sub
End Class