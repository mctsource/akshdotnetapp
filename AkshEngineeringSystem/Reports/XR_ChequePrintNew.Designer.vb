﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class XR_ChequePrintNew
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XR_ChequePrintNew))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.AmtXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.AmtWordXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.DateXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.ToXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 23.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.Detail.Visible = False
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1, Me.AmtXrLabel, Me.AmtWordXrLabel, Me.DateXrLabel, Me.ToXrLabel})
        Me.TopMargin.HeightF = 386.125!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.StylePriority.UseTextAlignment = False
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'AmtXrLabel
        '
        Me.AmtXrLabel.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AmtXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(671.875!, 120.1017!)
        Me.AmtXrLabel.Name = "AmtXrLabel"
        Me.AmtXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.AmtXrLabel.SizeF = New System.Drawing.SizeF(134.1805!, 34.36362!)
        Me.AmtXrLabel.StylePriority.UseFont = False
        Me.AmtXrLabel.StylePriority.UseTextAlignment = False
        Me.AmtXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'AmtWordXrLabel
        '
        Me.AmtWordXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AmtWordXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(153.0!, 115.0!)
        Me.AmtWordXrLabel.Name = "AmtWordXrLabel"
        Me.AmtWordXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.AmtWordXrLabel.SizeF = New System.Drawing.SizeF(557.097!, 54.0!)
        Me.AmtWordXrLabel.StylePriority.UseFont = False
        Me.AmtWordXrLabel.StylePriority.UsePadding = False
        Me.AmtWordXrLabel.StylePriority.UseTextAlignment = False
        Me.AmtWordXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DateXrLabel
        '
        Me.DateXrLabel.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DateXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 24.21756!)
        Me.DateXrLabel.Name = "DateXrLabel"
        Me.DateXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.DateXrLabel.SizeF = New System.Drawing.SizeF(165.4305!, 24.6414!)
        Me.DateXrLabel.StylePriority.UseFont = False
        Me.DateXrLabel.StylePriority.UseTextAlignment = False
        Me.DateXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'ToXrLabel
        '
        Me.ToXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(168.75!, 60.37499!)
        Me.ToXrLabel.Name = "ToXrLabel"
        Me.ToXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.ToXrLabel.SizeF = New System.Drawing.SizeF(540.4304!, 35.49998!)
        Me.ToXrLabel.StylePriority.UseFont = False
        Me.ToXrLabel.StylePriority.UseTextAlignment = False
        Me.ToXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 21.00004!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.BottomMargin.Visible = False
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.Name = "XrControlStyle1"
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(30.0!, 5.0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(106.25!, 94.79166!)
        '
        'XR_ChequePrintNew
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
        Me.BorderWidth = 0!
        Me.Margins = New System.Drawing.Printing.Margins(0, 0, 386, 21)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "17.2"
        Me.Watermark.Image = CType(resources.GetObject("XR_ChequePrintNew.Watermark.Image"), System.Drawing.Image)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents ToXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents AmtWordXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DateXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents AmtXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
End Class
