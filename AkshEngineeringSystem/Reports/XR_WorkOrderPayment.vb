﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Public Class XR_WorkOrderPayment
    Private Sub XrTable1_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTable1.BeforePrint
        Dim daCgst As OleDbDataAdapter
        Dim dtCgst As New DataTable
        Dim sqlCgst As String
        sqlCgst = "SELECT InvoiceNo, InvoiceDate, JobNoPrefix, JobNo, WONo, GrandTotalAmount from WOBill Where WorkOrderID = " + Invoice.Value.ToString() + " AND JobNo = '" + JobNoXrLabel.Text.Remove(0, 2) + "'"
        Try
            daCgst = New OleDbDataAdapter(sqlCgst, ConStr)
            daCgst.Fill(dtCgst)

            For i = 0 To dtCgst.Rows.Count - 1
                Dim RefVal As Integer
                RefVal = i + 1
                Dim row As New DevExpress.XtraReports.UI.XRTableRow()
                Dim Ref As New DevExpress.XtraReports.UI.XRTableCell()
                Dim BillNo As New DevExpress.XtraReports.UI.XRTableCell()
                Dim BillDt As New DevExpress.XtraReports.UI.XRTableCell()
                Dim PoNo As New DevExpress.XtraReports.UI.XRTableCell()
                Dim JobNo As New DevExpress.XtraReports.UI.XRTableCell()
                Dim GTAmt As New DevExpress.XtraReports.UI.XRTableCell()

                Ref.Text = RefVal.ToString()
                Ref.Borders = BorderSide.Left Or BorderSide.Bottom Or BorderSide.Top
                Ref.TextAlignment = TextAlignment.MiddleCenter
                Ref.Padding = 4
                Ref.WidthF = 37

                BillNo.Text = dtCgst.Rows(i).Item(0).ToString()
                BillNo.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                BillNo.TextAlignment = TextAlignment.MiddleCenter
                BillNo.Padding = 4
                BillNo.WidthF = 125.25


                Dim dt As Date = Date.Parse(dtCgst.Rows(i).Item(1).ToString())
                Dim dateString = dt.ToShortDateString()
                BillDt.Text = dateString
                BillDt.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                BillDt.TextAlignment = TextAlignment.MiddleCenter
                BillDt.Padding = 4
                BillDt.WidthF = 85.62

                PoNo.Text = dtCgst.Rows(i).Item(4).ToString()
                PoNo.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                PoNo.TextAlignment = TextAlignment.MiddleCenter
                PoNo.Padding = 4
                PoNo.WidthF = 146.15

                JobNo.Text = dtCgst.Rows(i).Item(2).ToString() + dtCgst.Rows(i).Item(3).ToString()
                JobNo.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                JobNo.TextAlignment = TextAlignment.MiddleCenter
                JobNo.Padding = 4
                JobNo.WidthF = 149.82

                GTAmt.Text = dtCgst.Rows(i).Item(5).ToString()
                GTAmt.Borders = BorderSide.All
                GTAmt.TextAlignment = TextAlignment.MiddleRight
                GTAmt.Padding = 4
                GTAmt.WidthF = 234.15


                row.Cells.Add(Ref)
                row.Cells.Add(BillNo)
                row.Cells.Add(BillDt)
                row.Cells.Add(PoNo)
                row.Cells.Add(JobNo)
                row.Cells.Add(GTAmt)

                XrTable1.Rows.Add(row)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub XrTable2_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTable2.BeforePrint
        Dim daCgst As OleDbDataAdapter
        Dim dtCgst As New DataTable
        Dim sqlCgst As String
        sqlCgst = "SELECT JobNo,WONo,CheckNo,ChkDate,PaidAmount,TDSPer,TDS,Remarks,RemainAmount,IsPrinted From WorkOrderPayment Where WONo='" + PONoXrLabel.Text + "' AND JobNo='" + JobNoXrLabel.Text + "'"
        'sqlCgst = "SELECT BillNo, BillDate, JobNoPrefix, JobNo, PONo, GrandTotalAmount from BillNew Where PurchaseID = " + Invoice.Value.ToString() + " AND JobNo = '" + JobNoXrLabel.Text.Remove(0, 2) + "'"
        Try
            daCgst = New OleDbDataAdapter(sqlCgst, ConStr)
            daCgst.Fill(dtCgst)

            For i = 0 To dtCgst.Rows.Count - 1
                Dim RefVal As Integer
                RefVal = i + 1
                Dim row As New DevExpress.XtraReports.UI.XRTableRow()
                Dim Ref As New DevExpress.XtraReports.UI.XRTableCell()
                Dim JobNo As New DevExpress.XtraReports.UI.XRTableCell()
                Dim PoNo As New DevExpress.XtraReports.UI.XRTableCell()
                Dim ChkNo As New DevExpress.XtraReports.UI.XRTableCell()
                Dim ChkDt As New DevExpress.XtraReports.UI.XRTableCell()
                Dim PaidAmt As New DevExpress.XtraReports.UI.XRTableCell()
                Dim tdsPer As New DevExpress.XtraReports.UI.XRTableCell()
                Dim tds As New DevExpress.XtraReports.UI.XRTableCell()
                Dim rmarks As New DevExpress.XtraReports.UI.XRTableCell()
                Dim RemAmt As New DevExpress.XtraReports.UI.XRTableCell()
                Dim IsPrint As New DevExpress.XtraReports.UI.XRTableCell()

                Ref.Text = RefVal.ToString()
                Ref.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Ref.TextAlignment = TextAlignment.MiddleCenter
                Ref.Padding = 4
                Ref.WidthF = 37

                JobNo.Text = dtCgst.Rows(i).Item(0).ToString()
                JobNo.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                JobNo.TextAlignment = TextAlignment.MiddleLeft
                JobNo.Padding = 4
                JobNo.WidthF = 45.99

                PoNo.Text = dtCgst.Rows(i).Item(1).ToString()
                PoNo.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                PoNo.TextAlignment = TextAlignment.MiddleLeft
                PoNo.Padding = 4
                PoNo.WidthF = 104.56

                ChkNo.Text = dtCgst.Rows(i).Item(2).ToString()
                ChkNo.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                ChkNo.TextAlignment = TextAlignment.MiddleLeft
                ChkNo.Padding = 4
                ChkNo.WidthF = 60.33

                Dim dt As Date = Date.Parse(dtCgst.Rows(i).Item(3).ToString())
                Dim dateString = dt.ToShortDateString()
                ChkDt.Text = dateString
                ChkDt.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                ChkDt.TextAlignment = TextAlignment.MiddleCenter
                ChkDt.Padding = 4
                ChkDt.WidthF = 66.39

                PaidAmt.Text = dtCgst.Rows(i).Item(4).ToString()
                PaidAmt.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                PaidAmt.TextAlignment = TextAlignment.MiddleRight
                PaidAmt.Padding = 4
                PaidAmt.WidthF = 68.0137

                tdsPer.Text = dtCgst.Rows(i).Item(5).ToString()
                tdsPer.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                tdsPer.TextAlignment = TextAlignment.MiddleRight
                tdsPer.Padding = 4
                tdsPer.WidthF = 44.0594177

                tds.Text = dtCgst.Rows(i).Item(6).ToString()
                tds.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                tds.TextAlignment = TextAlignment.MiddleRight
                tds.Padding = 4
                tds.WidthF = 50

                rmarks.Text = dtCgst.Rows(i).Item(7).ToString()
                rmarks.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                rmarks.TextAlignment = TextAlignment.MiddleLeft
                rmarks.Padding = 4
                rmarks.WidthF = 164.56

                RemAmt.Text = dtCgst.Rows(i).Item(8).ToString()
                RemAmt.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                RemAmt.TextAlignment = TextAlignment.MiddleRight
                RemAmt.Padding = 4
                RemAmt.WidthF = 71.84

                Dim Res As String = dtCgst.Rows(i).Item(9).ToString()
                If Res = "True" Then
                    IsPrint.Text = "Yes"
                Else
                    IsPrint.Text = "No"
                End If
                'IsPrint.Text = dtCgst.Rows(i).Item(9).ToString()
                IsPrint.Borders = BorderSide.All
                IsPrint.TextAlignment = TextAlignment.MiddleCenter
                IsPrint.Padding = 4
                IsPrint.WidthF = 65.25

                row.Cells.Add(Ref)
                row.Cells.Add(JobNo)
                row.Cells.Add(PoNo)
                row.Cells.Add(ChkNo)
                row.Cells.Add(ChkDt)
                row.Cells.Add(PaidAmt)
                row.Cells.Add(tdsPer)
                row.Cells.Add(tds)
                row.Cells.Add(rmarks)
                row.Cells.Add(RemAmt)
                row.Cells.Add(IsPrint)

                XrTable2.Rows.Add(row)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class