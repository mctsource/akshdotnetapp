﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Public Class XR_Invoice_ExportWithoutLC
    Dim daCgst As OleDbDataAdapter
    Dim dtCgst As New DataTable
    Dim sqlCgst As String
    'Private Sub XrLabel21_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles GtotalXrLabel.BeforePrint
    '    sqlCgst = "SELECT GrandTotalAmount from Invoice Where InvoiceID = " + Invoice.Value.ToString()

    '    daCgst = New OleDbDataAdapter(sqlCgst, ConStr)
    '    daCgst.Fill(dtCgst)

    '    Dim amount As Decimal
    '    amount = Convert.ToDecimal(dtCgst.Rows(0).Item(0))
    '    GtotalXrLabel.Text = PubCurrency.NumberFormat.CurrencySymbol + " " + Math.Round(amount, 2).ToString("F2")
    'End Sub

    Private Sub XR_Invoice_Export_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles MyBase.BeforePrint
        RateXrLabel.Text = "Rate in " + PubCurrency.NumberFormat.CurrencySymbol
        'RateXrLabel.Text = "Rate in " + PubCurrency.NativeName
        sqlCgst = "SELECT TotalAmtBeforeTax,TotalAmtAfterTax,GrandTotalAmount,RoundOff,PackingCharge from Invoice Where InvoiceID = " + Invoice.Value.ToString()

        daCgst = New OleDbDataAdapter(sqlCgst, ConStr)
        daCgst.Fill(dtCgst)
        TotalXrLabel.Text = PubCurrency.NumberFormat.CurrencySymbol + " " + Math.Round(Convert.ToDecimal(dtCgst.Rows(0).Item(0)), 2).ToString("F2")
        TotalAfterTaxXrLabel.Text = PubCurrency.NumberFormat.CurrencySymbol + " " + Math.Round(Convert.ToDecimal(dtCgst.Rows(0).Item(1)), 2).ToString("F2")
        GrandTotalXrLabel.Text = PubCurrency.NumberFormat.CurrencySymbol + " " + Math.Round(Convert.ToDecimal(dtCgst.Rows(0).Item(2)), 2).ToString("F2")
        RoundOffXrLabel.Text = PubCurrency.NumberFormat.CurrencySymbol + " " + Math.Round(Convert.ToDecimal(dtCgst.Rows(0).Item(3)), 2).ToString("F2")
        PackingXrLabel.Text = PubCurrency.NumberFormat.CurrencySymbol + " " + Math.Round(Convert.ToDecimal(dtCgst.Rows(0).Item(4)), 2).ToString("F2")
    End Sub

    Private Sub LUTXrLabel_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles LUTXrLabel.BeforePrint
        Dim daTerms As OleDbDataAdapter
        Dim dtTerms As New DataTable
        Dim sqlTerms As String
        sqlTerms = "Select IsLUT From Invoice Where InvoiceID = " + Invoice.Value.ToString()

        daTerms = New OleDbDataAdapter(sqlTerms, ConStr)
        daTerms.Fill(dtTerms)

        Dim chkGST = Convert.ToBoolean(dtTerms.Rows(0).Item("IsLUT"))
        If chkGST = True Then
            LUTXrLabel.Text = "Supply meant for export under Bond or Letter of Undertaking without payment of Integrated Tax (IGST)"
        Else
            LUTXrLabel.Text = ""
        End If
    End Sub
End Class