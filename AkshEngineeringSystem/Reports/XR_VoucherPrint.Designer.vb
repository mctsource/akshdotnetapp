﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class XR_VoucherPrint
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.UserXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DtXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.SumDrXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.SumTDSXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.TDSAmtXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.RemarksXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ToXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PONoXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.JobNoXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.BankNameXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.DrAmtXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.DateXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.CmpXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        'Me.DS_VendorPO1 = New AkshEngineeringSystem.DS_VendorPO()
        'Me.VendorPOTableAdapter = New AkshEngineeringSystem.DS_VendorPOTableAdapters.VendorPOTableAdapter()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ChqNoXrLabel = New DevExpress.XtraReports.UI.XRLabel()
        'SCType(Me.DS_VendorPO1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 23.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.Detail.Visible = False
        '
        'TopMargin
        '
        Me.TopMargin.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.ChqNoXrLabel, Me.XrLabel21, Me.XrLabel20, Me.XrLabel19, Me.UserXrLabel, Me.XrLabel15, Me.DtXrLabel, Me.XrLabel16, Me.XrLabel14, Me.XrLabel13, Me.SumDrXrLabel, Me.SumTDSXrLabel, Me.TDSAmtXrLabel, Me.RemarksXrLabel, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.ToXrLabel, Me.XrLabel8, Me.PONoXrLabel, Me.JobNoXrLabel, Me.XrLabel7, Me.XrLabel5, Me.XrLabel1, Me.XrLabel3, Me.XrLabel2, Me.BankNameXrLabel, Me.DrAmtXrLabel, Me.DateXrLabel, Me.CmpXrLabel})
        Me.TopMargin.HeightF = 555.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.StylePriority.UseBorderDashStyle = False
        Me.TopMargin.StylePriority.UseTextAlignment = False
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'XrLabel21
        '
        Me.XrLabel21.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel21.ForeColor = System.Drawing.Color.Black
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(680.6279!, 423.7708!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(95.02124!, 21.0!)
        Me.XrLabel21.StylePriority.UseBorderColor = False
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseForeColor = False
        Me.XrLabel21.StylePriority.UsePadding = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "Received By"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrLabel20
        '
        Me.XrLabel20.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel20.ForeColor = System.Drawing.Color.Black
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(465.758!, 423.7708!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(95.02124!, 21.0!)
        Me.XrLabel20.StylePriority.UseBorderColor = False
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseForeColor = False
        Me.XrLabel20.StylePriority.UsePadding = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "Checked By"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrLabel19
        '
        Me.XrLabel19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel19.ForeColor = System.Drawing.Color.Black
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(263.2114!, 423.7708!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(95.02124!, 21.0!)
        Me.XrLabel19.StylePriority.UseBorderColor = False
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseForeColor = False
        Me.XrLabel19.StylePriority.UsePadding = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "Approved By"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'UserXrLabel
        '
        Me.UserXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UserXrLabel.ForeColor = System.Drawing.Color.Black
        Me.UserXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(4.000122!, 444.7708!)
        Me.UserXrLabel.Name = "UserXrLabel"
        Me.UserXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.UserXrLabel.SizeF = New System.Drawing.SizeF(147.484!, 25.99994!)
        Me.UserXrLabel.StylePriority.UseBorderColor = False
        Me.UserXrLabel.StylePriority.UseFont = False
        Me.UserXrLabel.StylePriority.UseForeColor = False
        Me.UserXrLabel.StylePriority.UsePadding = False
        Me.UserXrLabel.StylePriority.UseTextAlignment = False
        Me.UserXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel15
        '
        Me.XrLabel15.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel15.ForeColor = System.Drawing.Color.Black
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(4.000122!, 470.7708!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(51.77549!, 27.50003!)
        Me.XrLabel15.StylePriority.UseBorderColor = False
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseForeColor = False
        Me.XrLabel15.StylePriority.UsePadding = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "Date :"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'DtXrLabel
        '
        Me.DtXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.DtXrLabel.ForeColor = System.Drawing.Color.Black
        Me.DtXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(55.77561!, 470.7708!)
        Me.DtXrLabel.Name = "DtXrLabel"
        Me.DtXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.DtXrLabel.SizeF = New System.Drawing.SizeF(174.5686!, 27.49997!)
        Me.DtXrLabel.StylePriority.UseBorderColor = False
        Me.DtXrLabel.StylePriority.UseFont = False
        Me.DtXrLabel.StylePriority.UseForeColor = False
        Me.DtXrLabel.StylePriority.UseTextAlignment = False
        Me.DtXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel16
        '
        Me.XrLabel16.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel16.ForeColor = System.Drawing.Color.Black
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(580.0!, 103.47!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(88.07849!, 27.50001!)
        Me.XrLabel16.StylePriority.UseBorderColor = False
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseForeColor = False
        Me.XrLabel16.StylePriority.UsePadding = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "Date :"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel14
        '
        Me.XrLabel14.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel14.ForeColor = System.Drawing.Color.Black
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(4.000124!, 423.7708!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(83.56355!, 21.0!)
        Me.XrLabel14.StylePriority.UseBorderColor = False
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseForeColor = False
        Me.XrLabel14.StylePriority.UsePadding = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Entered By"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel13
        '
        Me.XrLabel13.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel13.BorderWidth = 1.0!
        Me.XrLabel13.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel13.ForeColor = System.Drawing.Color.Black
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(4.000158!, 351.9757!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 30, 4, 4, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(461.7578!, 31.63599!)
        Me.XrLabel13.StylePriority.UseBorderColor = False
        Me.XrLabel13.StylePriority.UseBorders = False
        Me.XrLabel13.StylePriority.UseBorderWidth = False
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseForeColor = False
        Me.XrLabel13.StylePriority.UsePadding = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Total"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'SumDrXrLabel
        '
        Me.SumDrXrLabel.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.SumDrXrLabel.BorderWidth = 1.0!
        Me.SumDrXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SumDrXrLabel.ForeColor = System.Drawing.Color.Black
        Me.SumDrXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(465.758!, 351.9757!)
        Me.SumDrXrLabel.Multiline = True
        Me.SumDrXrLabel.Name = "SumDrXrLabel"
        Me.SumDrXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.SumDrXrLabel.SizeF = New System.Drawing.SizeF(163.443!, 31.63599!)
        Me.SumDrXrLabel.StylePriority.UseBorderColor = False
        Me.SumDrXrLabel.StylePriority.UseBorders = False
        Me.SumDrXrLabel.StylePriority.UseBorderWidth = False
        Me.SumDrXrLabel.StylePriority.UseFont = False
        Me.SumDrXrLabel.StylePriority.UseForeColor = False
        Me.SumDrXrLabel.StylePriority.UsePadding = False
        Me.SumDrXrLabel.StylePriority.UseTextAlignment = False
        Me.SumDrXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.SumDrXrLabel.TextFormatString = "{0:#,##0.00;(#,##0.00)}"
        '
        'SumTDSXrLabel
        '
        Me.SumTDSXrLabel.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.SumTDSXrLabel.BorderWidth = 1.0!
        Me.SumTDSXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SumTDSXrLabel.ForeColor = System.Drawing.Color.Black
        Me.SumTDSXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(629.2012!, 351.9757!)
        Me.SumTDSXrLabel.Multiline = True
        Me.SumTDSXrLabel.Name = "SumTDSXrLabel"
        Me.SumTDSXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.SumTDSXrLabel.SizeF = New System.Drawing.SizeF(146.4479!, 31.63599!)
        Me.SumTDSXrLabel.StylePriority.UseBorderColor = False
        Me.SumTDSXrLabel.StylePriority.UseBorders = False
        Me.SumTDSXrLabel.StylePriority.UseBorderWidth = False
        Me.SumTDSXrLabel.StylePriority.UseFont = False
        Me.SumTDSXrLabel.StylePriority.UseForeColor = False
        Me.SumTDSXrLabel.StylePriority.UsePadding = False
        Me.SumTDSXrLabel.StylePriority.UseTextAlignment = False
        Me.SumTDSXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.SumTDSXrLabel.TextFormatString = "{0:#,##0.00;(#,##0.00)}"
        '
        'TDSAmtXrLabel
        '
        Me.TDSAmtXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TDSAmtXrLabel.ForeColor = System.Drawing.Color.Black
        Me.TDSAmtXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(629.2009!, 198.1863!)
        Me.TDSAmtXrLabel.Name = "TDSAmtXrLabel"
        Me.TDSAmtXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.TDSAmtXrLabel.SizeF = New System.Drawing.SizeF(146.4482!, 153.7894!)
        Me.TDSAmtXrLabel.StylePriority.UseBorderColor = False
        Me.TDSAmtXrLabel.StylePriority.UseFont = False
        Me.TDSAmtXrLabel.StylePriority.UseForeColor = False
        Me.TDSAmtXrLabel.StylePriority.UsePadding = False
        Me.TDSAmtXrLabel.StylePriority.UseTextAlignment = False
        Me.TDSAmtXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'RemarksXrLabel
        '
        Me.RemarksXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RemarksXrLabel.ForeColor = System.Drawing.Color.Black
        Me.RemarksXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(4.000151!, 198.1863!)
        Me.RemarksXrLabel.Name = "RemarksXrLabel"
        Me.RemarksXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.RemarksXrLabel.SizeF = New System.Drawing.SizeF(461.7578!, 153.7894!)
        Me.RemarksXrLabel.StylePriority.UseBorderColor = False
        Me.RemarksXrLabel.StylePriority.UseFont = False
        Me.RemarksXrLabel.StylePriority.UseForeColor = False
        Me.RemarksXrLabel.StylePriority.UsePadding = False
        Me.RemarksXrLabel.StylePriority.UseTextAlignment = False
        Me.RemarksXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel12.BorderWidth = 1.0!
        Me.XrLabel12.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel12.ForeColor = System.Drawing.Color.Black
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(629.2009!, 151.967!)
        Me.XrLabel12.Multiline = True
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(146.4482!, 46.2193!)
        Me.XrLabel12.StylePriority.UseBorderColor = False
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseBorderWidth = False
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseForeColor = False
        Me.XrLabel12.StylePriority.UsePadding = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "TDS Value"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel11
        '
        Me.XrLabel11.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel11.BorderWidth = 1.0!
        Me.XrLabel11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel11.ForeColor = System.Drawing.Color.Black
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(465.758!, 151.967!)
        Me.XrLabel11.Multiline = True
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(163.443!, 46.21931!)
        Me.XrLabel11.StylePriority.UseBorderColor = False
        Me.XrLabel11.StylePriority.UseBorders = False
        Me.XrLabel11.StylePriority.UseBorderWidth = False
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseForeColor = False
        Me.XrLabel11.StylePriority.UsePadding = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "Payment" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Debit)"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel10
        '
        Me.XrLabel10.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel10.BorderWidth = 1.0!
        Me.XrLabel10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel10.ForeColor = System.Drawing.Color.Black
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(4.000122!, 151.967!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(461.7578!, 46.2193!)
        Me.XrLabel10.StylePriority.UseBorderColor = False
        Me.XrLabel10.StylePriority.UseBorders = False
        Me.XrLabel10.StylePriority.UseBorderWidth = False
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseForeColor = False
        Me.XrLabel10.StylePriority.UsePadding = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "Particulars"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel9.ForeColor = System.Drawing.Color.Black
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(4.000004!, 130.97!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(366.294!, 20.99698!)
        Me.XrLabel9.StylePriority.UseBorderColor = False
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseForeColor = False
        Me.XrLabel9.StylePriority.UsePadding = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Being the amount paid as per the following details."
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'ToXrLabel
        '
        Me.ToXrLabel.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToXrLabel.ForeColor = System.Drawing.Color.Black
        Me.ToXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(87.21634!, 109.967!)
        Me.ToXrLabel.Name = "ToXrLabel"
        Me.ToXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.ToXrLabel.SizeF = New System.Drawing.SizeF(233.1374!, 21.0!)
        Me.ToXrLabel.StylePriority.UseBorderColor = False
        Me.ToXrLabel.StylePriority.UseFont = False
        Me.ToXrLabel.StylePriority.UseForeColor = False
        Me.ToXrLabel.StylePriority.UsePadding = False
        Me.ToXrLabel.StylePriority.UseTextAlignment = False
        Me.ToXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.ForeColor = System.Drawing.Color.Black
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(3.999993!, 109.967!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(83.21635!, 21.0!)
        Me.XrLabel8.StylePriority.UseBorderColor = False
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseForeColor = False
        Me.XrLabel8.StylePriority.UsePadding = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "Dr. A/c : "
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'PONoXrLabel
        '
        Me.PONoXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PONoXrLabel.ForeColor = System.Drawing.Color.Black
        Me.PONoXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(422.0679!, 88.96698!)
        Me.PONoXrLabel.Name = "PONoXrLabel"
        Me.PONoXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.PONoXrLabel.SizeF = New System.Drawing.SizeF(148.4106!, 21.00002!)
        Me.PONoXrLabel.StylePriority.UseBorderColor = False
        Me.PONoXrLabel.StylePriority.UseFont = False
        Me.PONoXrLabel.StylePriority.UseForeColor = False
        Me.PONoXrLabel.StylePriority.UsePadding = False
        Me.PONoXrLabel.StylePriority.UseTextAlignment = False
        Me.PONoXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'JobNoXrLabel
        '
        Me.JobNoXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JobNoXrLabel.ForeColor = System.Drawing.Color.Black
        Me.JobNoXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(422.0679!, 67.96699!)
        Me.JobNoXrLabel.Name = "JobNoXrLabel"
        Me.JobNoXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.JobNoXrLabel.SizeF = New System.Drawing.SizeF(148.4106!, 20.99999!)
        Me.JobNoXrLabel.StylePriority.UseBorderColor = False
        Me.JobNoXrLabel.StylePriority.UseFont = False
        Me.JobNoXrLabel.StylePriority.UseForeColor = False
        Me.JobNoXrLabel.StylePriority.UsePadding = False
        Me.JobNoXrLabel.StylePriority.UseTextAlignment = False
        Me.JobNoXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel7.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(413.0679!, 88.96698!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(9.000031!, 21.00001!)
        Me.XrLabel7.StylePriority.UseBorderColor = False
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseForeColor = False
        Me.XrLabel7.StylePriority.UsePadding = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = ":"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(413.0679!, 67.96699!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(8.999969!, 20.99999!)
        Me.XrLabel5.StylePriority.UseBorderColor = False
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseForeColor = False
        Me.XrLabel5.StylePriority.UsePadding = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = ":"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.ForeColor = System.Drawing.Color.Black
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(355.0679!, 67.967!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(57.99997!, 20.99998!)
        Me.XrLabel1.StylePriority.UseBorderColor = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseForeColor = False
        Me.XrLabel1.StylePriority.UsePadding = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Job No"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel3.ForeColor = System.Drawing.Color.Black
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(355.0679!, 88.967!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(57.99997!, 21.0!)
        Me.XrLabel3.StylePriority.UseBorderColor = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseForeColor = False
        Me.XrLabel3.StylePriority.UsePadding = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "PO No"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.ForeColor = System.Drawing.Color.Black
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(580.0!, 67.96699!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(195.6491!, 35.49998!)
        Me.XrLabel2.StylePriority.UseBorderColor = False
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseForeColor = False
        Me.XrLabel2.StylePriority.UsePadding = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "BANK PAYMENT VOUCHER"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'BankNameXrLabel
        '
        Me.BankNameXrLabel.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.BankNameXrLabel.ForeColor = System.Drawing.Color.Black
        Me.BankNameXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(3.999999!, 88.96698!)
        Me.BankNameXrLabel.Name = "BankNameXrLabel"
        Me.BankNameXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.BankNameXrLabel.SizeF = New System.Drawing.SizeF(316.3537!, 21.0!)
        Me.BankNameXrLabel.StylePriority.UseBorderColor = False
        Me.BankNameXrLabel.StylePriority.UseFont = False
        Me.BankNameXrLabel.StylePriority.UseForeColor = False
        Me.BankNameXrLabel.StylePriority.UsePadding = False
        Me.BankNameXrLabel.StylePriority.UseTextAlignment = False
        Me.BankNameXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'DrAmtXrLabel
        '
        Me.DrAmtXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.DrAmtXrLabel.ForeColor = System.Drawing.Color.Black
        Me.DrAmtXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(465.7578!, 198.1863!)
        Me.DrAmtXrLabel.Name = "DrAmtXrLabel"
        Me.DrAmtXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.DrAmtXrLabel.SizeF = New System.Drawing.SizeF(163.4431!, 153.7894!)
        Me.DrAmtXrLabel.StylePriority.UseBorderColor = False
        Me.DrAmtXrLabel.StylePriority.UseFont = False
        Me.DrAmtXrLabel.StylePriority.UseForeColor = False
        Me.DrAmtXrLabel.StylePriority.UsePadding = False
        Me.DrAmtXrLabel.StylePriority.UseTextAlignment = False
        Me.DrAmtXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'DateXrLabel
        '
        Me.DateXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.DateXrLabel.ForeColor = System.Drawing.Color.Black
        Me.DateXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(668.0786!, 103.467!)
        Me.DateXrLabel.Name = "DateXrLabel"
        Me.DateXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.DateXrLabel.SizeF = New System.Drawing.SizeF(107.5706!, 27.50001!)
        Me.DateXrLabel.StylePriority.UseBorderColor = False
        Me.DateXrLabel.StylePriority.UseFont = False
        Me.DateXrLabel.StylePriority.UseForeColor = False
        Me.DateXrLabel.StylePriority.UseTextAlignment = False
        Me.DateXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'CmpXrLabel
        '
        Me.CmpXrLabel.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CmpXrLabel.ForeColor = System.Drawing.Color.Black
        Me.CmpXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(3.999992!, 67.96699!)
        Me.CmpXrLabel.Name = "CmpXrLabel"
        Me.CmpXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.CmpXrLabel.SizeF = New System.Drawing.SizeF(316.3537!, 21.0!)
        Me.CmpXrLabel.StylePriority.UseBorderColor = False
        Me.CmpXrLabel.StylePriority.UseFont = False
        Me.CmpXrLabel.StylePriority.UseForeColor = False
        Me.CmpXrLabel.StylePriority.UsePadding = False
        Me.CmpXrLabel.StylePriority.UseTextAlignment = False
        Me.CmpXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 44.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.BottomMargin.Visible = False
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.Name = "XrControlStyle1"
        '
        'DS_VendorPO1
        '
        'Me.DS_VendorPO1.DataSetName = "DS_VendorPO"
        'Me.DS_VendorPO1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VendorPOTableAdapter
        '
        'Me.VendorPOTableAdapter.ClearBeforeFill = True
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel4.ForeColor = System.Drawing.Color.Black
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(355.0679!, 109.97!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(94.08334!, 21.0!)
        Me.XrLabel4.StylePriority.UseBorderColor = False
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseForeColor = False
        Me.XrLabel4.StylePriority.UsePadding = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "Cheque No :"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'ChqNoXrLabel
        '
        Me.ChqNoXrLabel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChqNoXrLabel.ForeColor = System.Drawing.Color.Black
        Me.ChqNoXrLabel.LocationFloat = New DevExpress.Utils.PointFloat(449.1513!, 109.97!)
        Me.ChqNoXrLabel.Name = "ChqNoXrLabel"
        Me.ChqNoXrLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 4, 4, 4, 100.0!)
        Me.ChqNoXrLabel.SizeF = New System.Drawing.SizeF(121.3272!, 21.00002!)
        Me.ChqNoXrLabel.StylePriority.UseBorderColor = False
        Me.ChqNoXrLabel.StylePriority.UseFont = False
        Me.ChqNoXrLabel.StylePriority.UseForeColor = False
        Me.ChqNoXrLabel.StylePriority.UsePadding = False
        Me.ChqNoXrLabel.StylePriority.UseTextAlignment = False
        Me.ChqNoXrLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XR_VoucherPrint
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
        Me.BorderWidth = 0!
        'Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.DS_VendorPO1})
        Me.Margins = New System.Drawing.Printing.Margins(24, 24, 555, 44)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "17.2"
        'CType(Me.DS_VendorPO1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents CmpXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DateXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DrAmtXrLabel As DevExpress.XtraReports.UI.XRLabel
    'Friend WithEvents DS_VendorPO1 As DS_VendorPO
    'Friend WithEvents VendorPOTableAdapter As DS_VendorPOTableAdapters.VendorPOTableAdapter
    Friend WithEvents BankNameXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents JobNoXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PONoXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ToXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents RemarksXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TDSAmtXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents SumDrXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents SumTDSXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents UserXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DtXrLabel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ChqNoXrLabel As DevExpress.XtraReports.UI.XRLabel
End Class
