﻿Imports DevExpress.XtraPrinting

Public Class XR_InvoiceOld
    Private Sub XR_Invoice_AfterPrint(sender As Object, e As EventArgs) Handles MyBase.AfterPrint
        Dim rpt As New XR_InvoiceDuplicate
        rpt.Invoice.Value = Me.Invoice.Value
        rpt.Invoice.Visible = False
        rpt.XrLabel14.Text = "Duplicate Copy"
        rpt.CreateDocument()
        Dim pages As PageList = rpt.PrintingSystem.Document.Pages
        Me.Pages.AddRange(pages)
    End Sub

End Class