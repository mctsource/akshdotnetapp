﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting

Public Class XR_ExInvoice_ChallanDynamic
    Private Sub XrTable4_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTable4.BeforePrint
        Dim TotalQty As Decimal
        Dim TotalNW As Decimal
        Dim TotalGW As Decimal

        Dim daCgst As OleDbDataAdapter
        Dim dtCgst As New DataTable
        Dim sqlCgst As String
        sqlCgst = "SELECT ProductName,Dimensions,Qty,UOM,PackingType,NetWeight,GrossWeight from ChallanDetail Where InvoiceID = " + Invoice.Value.ToString()
        Try
            daCgst = New OleDbDataAdapter(sqlCgst, ConStr)
            daCgst.Fill(dtCgst)

            For i = 0 To dtCgst.Rows.Count - 1
                Dim RefVal As Integer
                RefVal = i + 1
                Dim row As New DevExpress.XtraReports.UI.XRTableRow()
                Dim Ref As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Prod As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Dimension As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Qty As New DevExpress.XtraReports.UI.XRTableCell()
                Dim UOM As New DevExpress.XtraReports.UI.XRTableCell()
                Dim PackingType As New DevExpress.XtraReports.UI.XRTableCell()
                Dim NetWeight As New DevExpress.XtraReports.UI.XRTableCell()
                Dim GrossWeight As New DevExpress.XtraReports.UI.XRTableCell()

                Ref.Borders = BorderSide.Left Or BorderSide.Bottom Or BorderSide.Top
                Ref.TextAlignment = TextAlignment.TopCenter
                Ref.Text = RefVal.ToString()
                Ref.WidthF = 27.42053

                Prod.Borders = BorderSide.All
                Prod.TextAlignment = TextAlignment.MiddleLeft
                Prod.Padding = (2)
                Prod.Text = dtCgst.Rows(i).Item(0).ToString()
                Prod.WidthF = 223.3179

                Dimension.Borders = BorderSide.Bottom Or BorderSide.Top
                Dimension.TextAlignment = TextAlignment.MiddleCenter
                Dimension.Padding = (2)
                Dimension.Text = dtCgst.Rows(i).Item(1).ToString()
                Dimension.WidthF = 98.3179

                Qty.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Qty.TextAlignment = TextAlignment.MiddleCenter
                Qty.Text = dtCgst.Rows(i).Item(2).ToString()
                Qty.WidthF = 72.73425
                TotalQty += Convert.ToDecimal(Qty.Text)

                UOM.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                UOM.TextAlignment = TextAlignment.MiddleCenter
                UOM.Padding = (2)
                UOM.Text = dtCgst.Rows(i).Item(3).ToString()
                UOM.WidthF = 64.61572

                PackingType.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                PackingType.TextAlignment = TextAlignment.MiddleCenter
                PackingType.Text = dtCgst.Rows(i).Item(4).ToString()
                PackingType.WidthF = 89.78928

                NetWeight.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                NetWeight.TextAlignment = TextAlignment.MiddleRight
                NetWeight.Padding = (2)
                NetWeight.Text = dtCgst.Rows(i).Item(5).ToString()
                NetWeight.WidthF = 100.7295
                TotalNW += Convert.ToDecimal(NetWeight.Text)


                GrossWeight.Borders = BorderSide.All
                GrossWeight.TextAlignment = TextAlignment.MiddleRight
                GrossWeight.Padding = (2)
                GrossWeight.Text = dtCgst.Rows(i).Item(6).ToString()
                GrossWeight.WidthF = 100
                TotalGW += Convert.ToDecimal(GrossWeight.Text)

                row.Cells.Add(Ref)
                row.Cells.Add(Prod)
                row.Cells.Add(Dimension)
                row.Cells.Add(Qty)
                row.Cells.Add(UOM)
                row.Cells.Add(PackingType)
                row.Cells.Add(NetWeight)
                row.Cells.Add(GrossWeight)
                'row.Borders = BorderSide.All
                XrTable4.Rows.Add(row)
            Next
            TQtyXrLabel.Text = TotalQty.ToString
            TNwXrLabel.Text = TotalNW.ToString
            TGwXrLabel.Text = TotalGW.ToString
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class