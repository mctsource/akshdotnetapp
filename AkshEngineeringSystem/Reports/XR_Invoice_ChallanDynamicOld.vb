﻿Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Public Class XR_Invoice_ChallanDynamicOld
    Private Sub XrTable4_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTable4.BeforePrint
        Dim daCgst As OleDbDataAdapter
        Dim dtCgst As New DataTable
        Dim sqlCgst As String
        sqlCgst = "SELECT ProductName, UOM, Qty from ChallanDetail Where InvoiceID = " + Invoice.Value.ToString()
        Try
            daCgst = New OleDbDataAdapter(sqlCgst, ConStr)
            daCgst.Fill(dtCgst)

            For i = 0 To dtCgst.Rows.Count - 1
                Dim RefVal As Integer
                RefVal = i + 1
                Dim row As New DevExpress.XtraReports.UI.XRTableRow()
                Dim Ref As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Prod As New DevExpress.XtraReports.UI.XRTableCell()
                Dim UOM As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Qty As New DevExpress.XtraReports.UI.XRTableCell()

                Ref.Borders = BorderSide.Left Or BorderSide.Bottom Or BorderSide.Top
                Ref.TextAlignment = TextAlignment.TopCenter
                Ref.Text = RefVal.ToString()
                Ref.WidthF = 27.42053


                Prod.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Prod.TextAlignment = TextAlignment.MiddleLeft
                Prod.Padding = (2)
                Prod.Text = dtCgst.Rows(i).Item(0).ToString()
                Prod.WidthF = 599.5752

                UOM.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                UOM.TextAlignment = TextAlignment.MiddleCenter
                UOM.Padding = (2)
                UOM.Text = dtCgst.Rows(i).Item(1).ToString()
                UOM.WidthF = 77.20251

                Qty.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Qty.TextAlignment = TextAlignment.MiddleCenter
                Qty.Text = dtCgst.Rows(i).Item(2).ToString()
                Qty.WidthF = 73.8028

                row.Cells.Add(Ref)
                row.Cells.Add(Prod)
                row.Cells.Add(UOM)
                row.Cells.Add(Qty)
                'row.Borders = BorderSide.All
                XrTable4.Rows.Add(row)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class