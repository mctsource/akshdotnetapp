﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class XR_Invoice
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XR_Invoice))
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel56 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel57 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel42 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel55 = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.DirectorySearcher1 = New System.DirectoryServices.DirectorySearcher()
        Me.XrCrossBandLine1 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.Invoice = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrCrossBandLine15 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine16 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine19 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine20 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine21 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLabel60 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel109 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel58 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel59 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel124 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel116 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel115 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel114 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel112 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel111 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel110 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel108 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel107 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel106 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel105 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel104 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel103 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel101 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel100 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel99 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel98 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel97 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel96 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel95 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel94 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel92 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel45 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel46 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel47 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel48 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel51 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel52 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel53 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel54 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTable4 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel130 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel131 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel183 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel182 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel181 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel180 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel142 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel184 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel156 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel154 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel160 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel152 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel158 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel186 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel185 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel169 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel168 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel187 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel137 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel136 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel135 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel132 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel133 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel178 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel170 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel171 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel172 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel173 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel175 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel176 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel177 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel134 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel174 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel179 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel145 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel144 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel125 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel90 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel89 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DT_InvoiceTableAdapter2 = New AkshEngineeringSystem.InvoiceNewTableAdapters.DT_InvoiceTableAdapter()
        Me.DT_InvoiceTableAdapter3 = New AkshEngineeringSystem.InvoiceNewTableAdapters.DT_InvoiceTableAdapter()
        Me.ClientPOTableAdapter = New AkshEngineeringSystem.DS_ClientPOTableAdapters.ClientPOTableAdapter()
        Me.InvoiceNew1 = New AkshEngineeringSystem.InvoiceNew()
        Me.DT_InvoiceTableAdapter1 = New AkshEngineeringSystem.InvoiceNewTableAdapters.DT_InvoiceTableAdapter()
        Me.InvoiceNew2 = New AkshEngineeringSystem.InvoiceNew()
        Me.XrCrossBandLine3 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine4 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine5 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceNew1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceNew2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel56, Me.XrLabel57, Me.XrLabel42, Me.XrLabel55})
        Me.Detail.Expanded = False
        Me.Detail.HeightF = 20.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseBorders = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel56
        '
        Me.XrLabel56.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel56.CanGrow = False
        Me.XrLabel56.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TCSRate]")})
        Me.XrLabel56.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel56.LocationFloat = New DevExpress.Utils.PointFloat(254.9905!, 0!)
        Me.XrLabel56.Multiline = True
        Me.XrLabel56.Name = "XrLabel56"
        Me.XrLabel56.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel56.SizeF = New System.Drawing.SizeF(170.9989!, 20.0!)
        Me.XrLabel56.StylePriority.UseBorders = False
        Me.XrLabel56.StylePriority.UseFont = False
        Me.XrLabel56.StylePriority.UseTextAlignment = False
        Me.XrLabel56.Text = "TCS"
        Me.XrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel56.TextFormatString = "(TCS @{0}%)"
        '
        'XrLabel57
        '
        Me.XrLabel57.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel57.CanGrow = False
        Me.XrLabel57.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TCSAmt]")})
        Me.XrLabel57.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel57.LocationFloat = New DevExpress.Utils.PointFloat(425.9894!, 0!)
        Me.XrLabel57.Multiline = True
        Me.XrLabel57.Name = "XrLabel57"
        Me.XrLabel57.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel57.SizeF = New System.Drawing.SizeF(97.02014!, 19.99998!)
        Me.XrLabel57.StylePriority.UseBorders = False
        Me.XrLabel57.StylePriority.UseFont = False
        Me.XrLabel57.StylePriority.UseTextAlignment = False
        Me.XrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel57.TextFormatString = "{0:f}"
        '
        'XrLabel42
        '
        Me.XrLabel42.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel42.CanGrow = False
        Me.XrLabel42.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TCSRate]")})
        Me.XrLabel42.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel42.LocationFloat = New DevExpress.Utils.PointFloat(254.9905!, 0!)
        Me.XrLabel42.Multiline = True
        Me.XrLabel42.Name = "XrLabel42"
        Me.XrLabel42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel42.SizeF = New System.Drawing.SizeF(170.9989!, 20.0!)
        Me.XrLabel42.StylePriority.UseBorders = False
        Me.XrLabel42.StylePriority.UseFont = False
        Me.XrLabel42.StylePriority.UseTextAlignment = False
        Me.XrLabel42.Text = "TCS"
        Me.XrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel42.TextFormatString = "(TCS @{0}%)"
        '
        'XrLabel55
        '
        Me.XrLabel55.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel55.CanGrow = False
        Me.XrLabel55.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TCSAmt]")})
        Me.XrLabel55.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel55.LocationFloat = New DevExpress.Utils.PointFloat(425.9894!, 0!)
        Me.XrLabel55.Multiline = True
        Me.XrLabel55.Name = "XrLabel55"
        Me.XrLabel55.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel55.SizeF = New System.Drawing.SizeF(97.02014!, 19.99998!)
        Me.XrLabel55.StylePriority.UseBorders = False
        Me.XrLabel55.StylePriority.UseFont = False
        Me.XrLabel55.StylePriority.UseTextAlignment = False
        Me.XrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel55.TextFormatString = "{0:f}"
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1})
        Me.TopMargin.HeightF = 50.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine1
        '
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.007947286!, 48.39423!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(777.4193!, 2.003204!)
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 0.9999955!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DirectorySearcher1
        '
        Me.DirectorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01")
        Me.DirectorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01")
        Me.DirectorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01")
        '
        'XrCrossBandLine1
        '
        Me.XrCrossBandLine1.EndBand = Nothing
        Me.XrCrossBandLine1.EndPointFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCrossBandLine1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCrossBandLine1.Name = "XrCrossBandLine1"
        Me.XrCrossBandLine1.StartBand = Nothing
        Me.XrCrossBandLine1.StartPointFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCrossBandLine1.WidthF = 9.375!
        '
        'Invoice
        '
        Me.Invoice.Description = "Invoice"
        Me.Invoice.Name = "Invoice"
        Me.Invoice.Type = GetType(Integer)
        Me.Invoice.ValueInfo = "0"
        '
        'XrCrossBandLine15
        '
        Me.XrCrossBandLine15.EndBand = Me.TopMargin
        Me.XrCrossBandLine15.EndPointFloat = New DevExpress.Utils.PointFloat(387.5!, 883792.0!)
        Me.XrCrossBandLine15.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.XrCrossBandLine15.LocationFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine15.Name = "XrCrossBandLine15"
        Me.XrCrossBandLine15.StartBand = Me.Detail
        Me.XrCrossBandLine15.StartPointFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine15.WidthF = 4.488032!
        '
        'XrCrossBandLine16
        '
        Me.XrCrossBandLine16.EndBand = Me.TopMargin
        Me.XrCrossBandLine16.EndPointFloat = New DevExpress.Utils.PointFloat(387.5!, 418424.0!)
        Me.XrCrossBandLine16.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.XrCrossBandLine16.LocationFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine16.Name = "XrCrossBandLine16"
        Me.XrCrossBandLine16.StartBand = Me.Detail
        Me.XrCrossBandLine16.StartPointFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine16.WidthF = 4.488032!
        '
        'XrCrossBandLine19
        '
        Me.XrCrossBandLine19.EndBand = Me.TopMargin
        Me.XrCrossBandLine19.EndPointFloat = New DevExpress.Utils.PointFloat(387.5!, 70963.0!)
        Me.XrCrossBandLine19.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.XrCrossBandLine19.LocationFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine19.Name = "XrCrossBandLine19"
        Me.XrCrossBandLine19.StartBand = Me.Detail
        Me.XrCrossBandLine19.StartPointFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine19.WidthF = 9.375!
        '
        'XrCrossBandLine20
        '
        Me.XrCrossBandLine20.EndBand = Me.TopMargin
        Me.XrCrossBandLine20.EndPointFloat = New DevExpress.Utils.PointFloat(387.5!, 7739.438!)
        Me.XrCrossBandLine20.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.XrCrossBandLine20.LocationFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine20.Name = "XrCrossBandLine20"
        Me.XrCrossBandLine20.StartBand = Me.Detail
        Me.XrCrossBandLine20.StartPointFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine20.WidthF = 9.375!
        '
        'XrCrossBandLine21
        '
        Me.XrCrossBandLine21.EndBand = Me.TopMargin
        Me.XrCrossBandLine21.EndPointFloat = New DevExpress.Utils.PointFloat(387.5!, 480.1044!)
        Me.XrCrossBandLine21.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.XrCrossBandLine21.LocationFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine21.Name = "XrCrossBandLine21"
        Me.XrCrossBandLine21.StartBand = Me.Detail
        Me.XrCrossBandLine21.StartPointFloat = New DevExpress.Utils.PointFloat(387.5!, 12.5!)
        Me.XrCrossBandLine21.WidthF = 9.375!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1})
        Me.PageFooter.Expanded = False
        Me.PageFooter.HeightF = 23.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrPageInfo1.Font = New System.Drawing.Font("Times New Roman", 10.0!)
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(678.0!, 0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrPageInfo1.StylePriority.UseBorders = False
        Me.XrPageInfo1.StylePriority.UseFont = False
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel60, Me.XrLabel109, Me.XrLabel58, Me.XrLabel59, Me.XrLabel124, Me.XrLabel116, Me.XrLabel115, Me.XrLabel114, Me.XrLabel112, Me.XrLabel111, Me.XrLabel110, Me.XrLabel108, Me.XrLabel107, Me.XrLabel106, Me.XrLabel105, Me.XrLabel104, Me.XrLabel103, Me.XrLabel101, Me.XrLabel100, Me.XrLabel99, Me.XrLabel98, Me.XrLabel97, Me.XrLabel96, Me.XrLabel95, Me.XrLabel94, Me.XrLabel92, Me.XrLabel1, Me.XrLabel2, Me.XrLabel3, Me.XrLabel5, Me.XrLabel39, Me.XrLabel43, Me.XrLabel44, Me.XrLabel45, Me.XrLabel46, Me.XrLabel47, Me.XrLabel48, Me.XrLabel49, Me.XrLabel50, Me.XrLabel51, Me.XrLabel52, Me.XrLabel53, Me.XrLabel54, Me.XrTable4, Me.XrLabel25, Me.XrLabel37, Me.XrLabel41, Me.XrLabel130, Me.XrLabel131})
        Me.GroupFooter2.HeightF = 341.5699!
        Me.GroupFooter2.Name = "GroupFooter2"
        Me.GroupFooter2.StylePriority.UseBorders = False
        '
        'XrLabel60
        '
        Me.XrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel60.CanGrow = False
        Me.XrLabel60.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel60.LocationFloat = New DevExpress.Utils.PointFloat(534.4171!, 234.7263!)
        Me.XrLabel60.Multiline = True
        Me.XrLabel60.Name = "XrLabel60"
        Me.XrLabel60.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel60.SizeF = New System.Drawing.SizeF(243.5834!, 31.50188!)
        Me.XrLabel60.StylePriority.UseBorders = False
        Me.XrLabel60.StylePriority.UseFont = False
        Me.XrLabel60.StylePriority.UseTextAlignment = False
        Me.XrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel109
        '
        Me.XrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel109.CanGrow = False
        Me.XrLabel109.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel109.LocationFloat = New DevExpress.Utils.PointFloat(510.1077!, 234.7263!)
        Me.XrLabel109.Multiline = True
        Me.XrLabel109.Name = "XrLabel109"
        Me.XrLabel109.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel109.SizeF = New System.Drawing.SizeF(24.30927!, 31.50188!)
        Me.XrLabel109.StylePriority.UseBorders = False
        Me.XrLabel109.StylePriority.UseFont = False
        Me.XrLabel109.StylePriority.UseTextAlignment = False
        Me.XrLabel109.Text = "For "
        Me.XrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel58
        '
        Me.XrLabel58.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel58.CanGrow = False
        Me.XrLabel58.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TCSRate]")})
        Me.XrLabel58.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel58.LocationFloat = New DevExpress.Utils.PointFloat(510.1077!, 142.6518!)
        Me.XrLabel58.Multiline = True
        Me.XrLabel58.Name = "XrLabel58"
        Me.XrLabel58.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel58.SizeF = New System.Drawing.SizeF(170.8608!, 19.99998!)
        Me.XrLabel58.StylePriority.UseBorders = False
        Me.XrLabel58.StylePriority.UseFont = False
        Me.XrLabel58.StylePriority.UseTextAlignment = False
        Me.XrLabel58.Text = "TCS"
        Me.XrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel58.TextFormatString = "(TCS @{0}%)"
        '
        'XrLabel59
        '
        Me.XrLabel59.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel59.CanGrow = False
        Me.XrLabel59.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TCSAmt]")})
        Me.XrLabel59.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel59.LocationFloat = New DevExpress.Utils.PointFloat(680.978!, 142.6518!)
        Me.XrLabel59.Multiline = True
        Me.XrLabel59.Name = "XrLabel59"
        Me.XrLabel59.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel59.SizeF = New System.Drawing.SizeF(97.02014!, 19.99998!)
        Me.XrLabel59.StylePriority.UseBorders = False
        Me.XrLabel59.StylePriority.UseFont = False
        Me.XrLabel59.StylePriority.UseTextAlignment = False
        Me.XrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel59.TextFormatString = "{0:f}"
        '
        'XrLabel124
        '
        Me.XrLabel124.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel124.CanGrow = False
        Me.XrLabel124.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel124.LocationFloat = New DevExpress.Utils.PointFloat(0!, 102.6518!)
        Me.XrLabel124.Multiline = True
        Me.XrLabel124.Name = "XrLabel124"
        Me.XrLabel124.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel124.SizeF = New System.Drawing.SizeF(93.73447!, 20.00001!)
        Me.XrLabel124.StylePriority.UseBorders = False
        Me.XrLabel124.StylePriority.UseFont = False
        Me.XrLabel124.StylePriority.UseTextAlignment = False
        Me.XrLabel124.Text = "PAN No :"
        Me.XrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel116
        '
        Me.XrLabel116.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel116.CanGrow = False
        Me.XrLabel116.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel116.LocationFloat = New DevExpress.Utils.PointFloat(93.87112!, 102.6518!)
        Me.XrLabel116.Multiline = True
        Me.XrLabel116.Name = "XrLabel116"
        Me.XrLabel116.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel116.SizeF = New System.Drawing.SizeF(416.2355!, 20.00001!)
        Me.XrLabel116.StylePriority.UseBorders = False
        Me.XrLabel116.StylePriority.UseFont = False
        Me.XrLabel116.StylePriority.UseTextAlignment = False
        Me.XrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel115
        '
        Me.XrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel115.CanGrow = False
        Me.XrLabel115.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel115.LocationFloat = New DevExpress.Utils.PointFloat(93.73405!, 82.65191!)
        Me.XrLabel115.Multiline = True
        Me.XrLabel115.Name = "XrLabel115"
        Me.XrLabel115.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel115.SizeF = New System.Drawing.SizeF(416.2343!, 19.99997!)
        Me.XrLabel115.StylePriority.UseBorders = False
        Me.XrLabel115.StylePriority.UseFont = False
        Me.XrLabel115.StylePriority.UseTextAlignment = False
        Me.XrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel114
        '
        Me.XrLabel114.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel114.CanGrow = False
        Me.XrLabel114.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel114.LocationFloat = New DevExpress.Utils.PointFloat(0!, 82.65184!)
        Me.XrLabel114.Multiline = True
        Me.XrLabel114.Name = "XrLabel114"
        Me.XrLabel114.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel114.SizeF = New System.Drawing.SizeF(93.73412!, 19.99996!)
        Me.XrLabel114.StylePriority.UseBorders = False
        Me.XrLabel114.StylePriority.UseFont = False
        Me.XrLabel114.StylePriority.UseTextAlignment = False
        Me.XrLabel114.Text = "GSTIN    : "
        Me.XrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel112
        '
        Me.XrLabel112.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel112.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel112.CanGrow = False
        Me.XrLabel112.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel112.LocationFloat = New DevExpress.Utils.PointFloat(0!, 22.65187!)
        Me.XrLabel112.Multiline = True
        Me.XrLabel112.Name = "XrLabel112"
        Me.XrLabel112.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel112.SizeF = New System.Drawing.SizeF(509.9688!, 20.00001!)
        Me.XrLabel112.StylePriority.UseBackColor = False
        Me.XrLabel112.StylePriority.UseBorders = False
        Me.XrLabel112.StylePriority.UseFont = False
        Me.XrLabel112.StylePriority.UseTextAlignment = False
        Me.XrLabel112.Text = "Total Invoice Amount in Words"
        Me.XrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel111
        '
        Me.XrLabel111.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel111.CanGrow = False
        Me.XrLabel111.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TotalInWords]")})
        Me.XrLabel111.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel111.LocationFloat = New DevExpress.Utils.PointFloat(0!, 42.65186!)
        Me.XrLabel111.Multiline = True
        Me.XrLabel111.Name = "XrLabel111"
        Me.XrLabel111.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrLabel111.SizeF = New System.Drawing.SizeF(509.9688!, 39.99996!)
        Me.XrLabel111.StylePriority.UseBorders = False
        Me.XrLabel111.StylePriority.UseFont = False
        Me.XrLabel111.StylePriority.UsePadding = False
        Me.XrLabel111.StylePriority.UseTextAlignment = False
        Me.XrLabel111.Text = "0"
        Me.XrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel110
        '
        Me.XrLabel110.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel110.CanGrow = False
        Me.XrLabel110.Font = New System.Drawing.Font("Calibri", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.XrLabel110.LocationFloat = New DevExpress.Utils.PointFloat(0!, 196.6418!)
        Me.XrLabel110.Multiline = True
        Me.XrLabel110.Name = "XrLabel110"
        Me.XrLabel110.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel110.SizeF = New System.Drawing.SizeF(509.9688!, 18.08481!)
        Me.XrLabel110.StylePriority.UseBorders = False
        Me.XrLabel110.StylePriority.UseFont = False
        Me.XrLabel110.StylePriority.UseTextAlignment = False
        Me.XrLabel110.Text = "TERMS & CONDITIONS :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel108
        '
        Me.XrLabel108.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel108.CanGrow = False
        Me.XrLabel108.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel108.LocationFloat = New DevExpress.Utils.PointFloat(0!, 214.7265!)
        Me.XrLabel108.Multiline = True
        Me.XrLabel108.Name = "XrLabel108"
        Me.XrLabel108.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel108.SizeF = New System.Drawing.SizeF(509.9686!, 89.10367!)
        Me.XrLabel108.StylePriority.UseBorders = False
        Me.XrLabel108.StylePriority.UseFont = False
        Me.XrLabel108.StylePriority.UseTextAlignment = False
        Me.XrLabel108.Text = resources.GetString("XrLabel108.Text")
        Me.XrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel107
        '
        Me.XrLabel107.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel107.CanGrow = False
        Me.XrLabel107.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.XrLabel107.LocationFloat = New DevExpress.Utils.PointFloat(509.9696!, 266.2282!)
        Me.XrLabel107.Multiline = True
        Me.XrLabel107.Name = "XrLabel107"
        Me.XrLabel107.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel107.SizeF = New System.Drawing.SizeF(268.0307!, 19.89362!)
        Me.XrLabel107.StylePriority.UseBorders = False
        Me.XrLabel107.StylePriority.UseFont = False
        Me.XrLabel107.StylePriority.UseTextAlignment = False
        Me.XrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel106
        '
        Me.XrLabel106.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel106.CanGrow = False
        Me.XrLabel106.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.XrLabel106.LocationFloat = New DevExpress.Utils.PointFloat(509.9684!, 286.1218!)
        Me.XrLabel106.Multiline = True
        Me.XrLabel106.Name = "XrLabel106"
        Me.XrLabel106.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel106.SizeF = New System.Drawing.SizeF(268.0321!, 17.70834!)
        Me.XrLabel106.StylePriority.UseBorders = False
        Me.XrLabel106.StylePriority.UseFont = False
        Me.XrLabel106.StylePriority.UseTextAlignment = False
        Me.XrLabel106.Text = "Authorised Signatory/ DIRECTOR"
        Me.XrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel105
        '
        Me.XrLabel105.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel105.CanGrow = False
        Me.XrLabel105.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel105.LocationFloat = New DevExpress.Utils.PointFloat(0!, 142.6518!)
        Me.XrLabel105.Multiline = True
        Me.XrLabel105.Name = "XrLabel105"
        Me.XrLabel105.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel105.SizeF = New System.Drawing.SizeF(99.97672!, 17.85416!)
        Me.XrLabel105.StylePriority.UseBorders = False
        Me.XrLabel105.StylePriority.UseFont = False
        Me.XrLabel105.StylePriority.UseTextAlignment = False
        Me.XrLabel105.Text = "BANK NAME      :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel104
        '
        Me.XrLabel104.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel104.CanGrow = False
        Me.XrLabel104.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel104.LocationFloat = New DevExpress.Utils.PointFloat(99.97672!, 142.6518!)
        Me.XrLabel104.Multiline = True
        Me.XrLabel104.Name = "XrLabel104"
        Me.XrLabel104.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel104.SizeF = New System.Drawing.SizeF(177.6886!, 17.85419!)
        Me.XrLabel104.StylePriority.UseBorders = False
        Me.XrLabel104.StylePriority.UseFont = False
        Me.XrLabel104.StylePriority.UseTextAlignment = False
        Me.XrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel103
        '
        Me.XrLabel103.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel103.CanGrow = False
        Me.XrLabel103.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel103.LocationFloat = New DevExpress.Utils.PointFloat(277.8032!, 142.6518!)
        Me.XrLabel103.Multiline = True
        Me.XrLabel103.Name = "XrLabel103"
        Me.XrLabel103.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel103.SizeF = New System.Drawing.SizeF(90.13834!, 18.42732!)
        Me.XrLabel103.StylePriority.UseBorders = False
        Me.XrLabel103.StylePriority.UseFont = False
        Me.XrLabel103.StylePriority.UseTextAlignment = False
        Me.XrLabel103.Text = "ACCOUNT NO :"
        Me.XrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel101
        '
        Me.XrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel101.CanGrow = False
        Me.XrLabel101.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel101.LocationFloat = New DevExpress.Utils.PointFloat(0!, 160.506!)
        Me.XrLabel101.Multiline = True
        Me.XrLabel101.Name = "XrLabel101"
        Me.XrLabel101.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel101.SizeF = New System.Drawing.SizeF(99.97672!, 18.28662!)
        Me.XrLabel101.StylePriority.UseBorders = False
        Me.XrLabel101.StylePriority.UseFont = False
        Me.XrLabel101.StylePriority.UseTextAlignment = False
        Me.XrLabel101.Text = "BRANCH NAME :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel100
        '
        Me.XrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel100.CanGrow = False
        Me.XrLabel100.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.XrLabel100.LocationFloat = New DevExpress.Utils.PointFloat(100.1146!, 160.5059!)
        Me.XrLabel100.Multiline = True
        Me.XrLabel100.Name = "XrLabel100"
        Me.XrLabel100.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel100.SizeF = New System.Drawing.SizeF(177.6886!, 18.28668!)
        Me.XrLabel100.StylePriority.UseBorders = False
        Me.XrLabel100.StylePriority.UseFont = False
        Me.XrLabel100.StylePriority.UseTextAlignment = False
        Me.XrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel99
        '
        Me.XrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel99.CanGrow = False
        Me.XrLabel99.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel99.LocationFloat = New DevExpress.Utils.PointFloat(277.9415!, 161.079!)
        Me.XrLabel99.Multiline = True
        Me.XrLabel99.Name = "XrLabel99"
        Me.XrLabel99.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel99.SizeF = New System.Drawing.SizeF(89.99988!, 18.0!)
        Me.XrLabel99.StylePriority.UseBorders = False
        Me.XrLabel99.StylePriority.UseFont = False
        Me.XrLabel99.StylePriority.UseTextAlignment = False
        Me.XrLabel99.Text = "MICR CODE     :"
        Me.XrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel98
        '
        Me.XrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel98.CanGrow = False
        Me.XrLabel98.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel98.LocationFloat = New DevExpress.Utils.PointFloat(367.9414!, 161.079!)
        Me.XrLabel98.Multiline = True
        Me.XrLabel98.Name = "XrLabel98"
        Me.XrLabel98.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel98.SizeF = New System.Drawing.SizeF(142.1663!, 17.99992!)
        Me.XrLabel98.StylePriority.UseBorders = False
        Me.XrLabel98.StylePriority.UseFont = False
        Me.XrLabel98.StylePriority.UseTextAlignment = False
        Me.XrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel97
        '
        Me.XrLabel97.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel97.CanGrow = False
        Me.XrLabel97.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel97.LocationFloat = New DevExpress.Utils.PointFloat(0!, 178.7927!)
        Me.XrLabel97.Multiline = True
        Me.XrLabel97.Name = "XrLabel97"
        Me.XrLabel97.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel97.SizeF = New System.Drawing.SizeF(99.97672!, 17.84906!)
        Me.XrLabel97.StylePriority.UseBorders = False
        Me.XrLabel97.StylePriority.UseFont = False
        Me.XrLabel97.StylePriority.UseTextAlignment = False
        Me.XrLabel97.Text = "IFSC CODE         :"
        Me.XrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel96
        '
        Me.XrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel96.CanGrow = False
        Me.XrLabel96.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel96.LocationFloat = New DevExpress.Utils.PointFloat(100.1146!, 178.9361!)
        Me.XrLabel96.Multiline = True
        Me.XrLabel96.Name = "XrLabel96"
        Me.XrLabel96.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel96.SizeF = New System.Drawing.SizeF(177.8269!, 17.70563!)
        Me.XrLabel96.StylePriority.UseBorders = False
        Me.XrLabel96.StylePriority.UseFont = False
        Me.XrLabel96.StylePriority.UseTextAlignment = False
        Me.XrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel95
        '
        Me.XrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel95.CanGrow = False
        Me.XrLabel95.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel95.LocationFloat = New DevExpress.Utils.PointFloat(277.9414!, 179.0792!)
        Me.XrLabel95.Multiline = True
        Me.XrLabel95.Name = "XrLabel95"
        Me.XrLabel95.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel95.SizeF = New System.Drawing.SizeF(232.1652!, 17.56259!)
        Me.XrLabel95.StylePriority.UseBorders = False
        Me.XrLabel95.StylePriority.UseFont = False
        Me.XrLabel95.StylePriority.UseTextAlignment = False
        Me.XrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel94
        '
        Me.XrLabel94.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel94.CanGrow = False
        Me.XrLabel94.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Underline)
        Me.XrLabel94.LocationFloat = New DevExpress.Utils.PointFloat(510.1066!, 204.9433!)
        Me.XrLabel94.Multiline = True
        Me.XrLabel94.Name = "XrLabel94"
        Me.XrLabel94.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel94.SizeF = New System.Drawing.SizeF(267.8939!, 29.78302!)
        Me.XrLabel94.StylePriority.UseBorders = False
        Me.XrLabel94.StylePriority.UseFont = False
        Me.XrLabel94.StylePriority.UseTextAlignment = False
        Me.XrLabel94.Text = "Certified that the particulars given above are true"
        Me.XrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel92
        '
        Me.XrLabel92.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel92.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel92.CanGrow = False
        Me.XrLabel92.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel92.LocationFloat = New DevExpress.Utils.PointFloat(0!, 122.6518!)
        Me.XrLabel92.Multiline = True
        Me.XrLabel92.Name = "XrLabel92"
        Me.XrLabel92.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel92.SizeF = New System.Drawing.SizeF(509.97!, 20.00004!)
        Me.XrLabel92.StylePriority.UseBackColor = False
        Me.XrLabel92.StylePriority.UseBorders = False
        Me.XrLabel92.StylePriority.UseFont = False
        Me.XrLabel92.StylePriority.UseTextAlignment = False
        Me.XrLabel92.Text = "BANK DETAILS FOR NEFT/ RTGS PAYMENTS"
        Me.XrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel1
        '
        Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(367.9415!, 142.6518!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(142.1651!, 18.42732!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PackingCharge]")})
        Me.XrLabel2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(680.9686!, 22.65187!)
        Me.XrLabel2.Multiline = True
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(97.02594!, 20.0!)
        Me.XrLabel2.StylePriority.UseBorders = False
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel2.TextFormatString = "{0:f}"
        '
        'XrLabel3
        '
        Me.XrLabel3.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(509.9685!, 22.65187!)
        Me.XrLabel3.Multiline = True
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(171.0!, 20.0!)
        Me.XrLabel3.StylePriority.UseBorders = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Packing Charge/ Courier"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel5.CanGrow = False
        Me.XrLabel5.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TransCharge]")})
        Me.XrLabel5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(680.9697!, 42.6518!)
        Me.XrLabel5.Multiline = True
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(97.02496!, 20.0!)
        Me.XrLabel5.StylePriority.UseBorders = False
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel5.TextFormatString = "{0:f}"
        '
        'XrLabel39
        '
        Me.XrLabel39.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel39.CanGrow = False
        Me.XrLabel39.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel39.LocationFloat = New DevExpress.Utils.PointFloat(509.9696!, 42.65186!)
        Me.XrLabel39.Multiline = True
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.SizeF = New System.Drawing.SizeF(171.0!, 20.0!)
        Me.XrLabel39.StylePriority.UseBorders = False
        Me.XrLabel39.StylePriority.UseFont = False
        Me.XrLabel39.StylePriority.UseTextAlignment = False
        Me.XrLabel39.Text = "Transportation Charge"
        Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel43
        '
        Me.XrLabel43.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel43.CanGrow = False
        Me.XrLabel43.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PCGSTAmt]")})
        Me.XrLabel43.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(680.9686!, 62.65186!)
        Me.XrLabel43.Multiline = True
        Me.XrLabel43.Name = "XrLabel43"
        Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel43.SizeF = New System.Drawing.SizeF(97.02594!, 20.0!)
        Me.XrLabel43.StylePriority.UseBorders = False
        Me.XrLabel43.StylePriority.UseFont = False
        Me.XrLabel43.StylePriority.UseTextAlignment = False
        Me.XrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel43.TextFormatString = "{0:f}"
        '
        'XrLabel44
        '
        Me.XrLabel44.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel44.CanGrow = False
        Me.XrLabel44.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PCGSTRate]")})
        Me.XrLabel44.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(509.9696!, 62.65252!)
        Me.XrLabel44.Multiline = True
        Me.XrLabel44.Name = "XrLabel44"
        Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel44.SizeF = New System.Drawing.SizeF(170.9989!, 19.9993!)
        Me.XrLabel44.StylePriority.UseBorders = False
        Me.XrLabel44.StylePriority.UseFont = False
        Me.XrLabel44.StylePriority.UseTextAlignment = False
        Me.XrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel44.TextFormatString = "(CGST @{0}%)"
        '
        'XrLabel45
        '
        Me.XrLabel45.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel45.CanGrow = False
        Me.XrLabel45.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PSGSTAmt]")})
        Me.XrLabel45.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel45.LocationFloat = New DevExpress.Utils.PointFloat(680.9686!, 82.65184!)
        Me.XrLabel45.Multiline = True
        Me.XrLabel45.Name = "XrLabel45"
        Me.XrLabel45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel45.SizeF = New System.Drawing.SizeF(97.0271!, 19.99999!)
        Me.XrLabel45.StylePriority.UseBorders = False
        Me.XrLabel45.StylePriority.UseFont = False
        Me.XrLabel45.StylePriority.UseTextAlignment = False
        Me.XrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel45.TextFormatString = "{0:f}"
        '
        'XrLabel46
        '
        Me.XrLabel46.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel46.CanGrow = False
        Me.XrLabel46.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PSGSTRate]")})
        Me.XrLabel46.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel46.LocationFloat = New DevExpress.Utils.PointFloat(509.9696!, 82.65184!)
        Me.XrLabel46.Multiline = True
        Me.XrLabel46.Name = "XrLabel46"
        Me.XrLabel46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel46.SizeF = New System.Drawing.SizeF(170.9989!, 20.0!)
        Me.XrLabel46.StylePriority.UseBorders = False
        Me.XrLabel46.StylePriority.UseFont = False
        Me.XrLabel46.StylePriority.UseTextAlignment = False
        Me.XrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel46.TextFormatString = "(SGST @{0}%)"
        '
        'XrLabel47
        '
        Me.XrLabel47.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel47.CanGrow = False
        Me.XrLabel47.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TotalGSTTax]")})
        Me.XrLabel47.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel47.LocationFloat = New DevExpress.Utils.PointFloat(681.1055!, 102.6518!)
        Me.XrLabel47.Multiline = True
        Me.XrLabel47.Name = "XrLabel47"
        Me.XrLabel47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel47.SizeF = New System.Drawing.SizeF(96.89496!, 20.0!)
        Me.XrLabel47.StylePriority.UseBorders = False
        Me.XrLabel47.StylePriority.UseFont = False
        Me.XrLabel47.StylePriority.UseTextAlignment = False
        Me.XrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel47.TextFormatString = "{0:f}"
        '
        'XrLabel48
        '
        Me.XrLabel48.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel48.CanGrow = False
        Me.XrLabel48.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel48.LocationFloat = New DevExpress.Utils.PointFloat(510.1066!, 102.6518!)
        Me.XrLabel48.Multiline = True
        Me.XrLabel48.Name = "XrLabel48"
        Me.XrLabel48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel48.SizeF = New System.Drawing.SizeF(170.9989!, 19.99999!)
        Me.XrLabel48.StylePriority.UseBorders = False
        Me.XrLabel48.StylePriority.UseFont = False
        Me.XrLabel48.StylePriority.UseTextAlignment = False
        Me.XrLabel48.Text = "Total GST"
        Me.XrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel49
        '
        Me.XrLabel49.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel49.CanGrow = False
        Me.XrLabel49.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TotalAmtAfterTax]")})
        Me.XrLabel49.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel49.LocationFloat = New DevExpress.Utils.PointFloat(680.9696!, 122.6518!)
        Me.XrLabel49.Multiline = True
        Me.XrLabel49.Name = "XrLabel49"
        Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel49.SizeF = New System.Drawing.SizeF(97.02594!, 20.0!)
        Me.XrLabel49.StylePriority.UseBorders = False
        Me.XrLabel49.StylePriority.UseFont = False
        Me.XrLabel49.StylePriority.UseTextAlignment = False
        Me.XrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel49.TextFormatString = "{0:f}"
        '
        'XrLabel50
        '
        Me.XrLabel50.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel50.CanGrow = False
        Me.XrLabel50.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel50.LocationFloat = New DevExpress.Utils.PointFloat(509.9707!, 122.6518!)
        Me.XrLabel50.Multiline = True
        Me.XrLabel50.Name = "XrLabel50"
        Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel50.SizeF = New System.Drawing.SizeF(170.9989!, 20.00001!)
        Me.XrLabel50.StylePriority.UseBorders = False
        Me.XrLabel50.StylePriority.UseFont = False
        Me.XrLabel50.StylePriority.UseTextAlignment = False
        Me.XrLabel50.Text = "Sub Total"
        Me.XrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel51
        '
        Me.XrLabel51.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel51.CanGrow = False
        Me.XrLabel51.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RoundOff]")})
        Me.XrLabel51.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel51.LocationFloat = New DevExpress.Utils.PointFloat(681.1066!, 162.6518!)
        Me.XrLabel51.Multiline = True
        Me.XrLabel51.Name = "XrLabel51"
        Me.XrLabel51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel51.SizeF = New System.Drawing.SizeF(96.89392!, 19.99998!)
        Me.XrLabel51.StylePriority.UseBorders = False
        Me.XrLabel51.StylePriority.UseFont = False
        Me.XrLabel51.StylePriority.UseTextAlignment = False
        Me.XrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel51.TextFormatString = "{0:f}"
        '
        'XrLabel52
        '
        Me.XrLabel52.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel52.CanGrow = False
        Me.XrLabel52.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel52.LocationFloat = New DevExpress.Utils.PointFloat(510.1077!, 162.6518!)
        Me.XrLabel52.Multiline = True
        Me.XrLabel52.Name = "XrLabel52"
        Me.XrLabel52.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel52.SizeF = New System.Drawing.SizeF(170.9989!, 20.0!)
        Me.XrLabel52.StylePriority.UseBorders = False
        Me.XrLabel52.StylePriority.UseFont = False
        Me.XrLabel52.StylePriority.UseTextAlignment = False
        Me.XrLabel52.Text = "Round Off"
        Me.XrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel53
        '
        Me.XrLabel53.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel53.CanGrow = False
        Me.XrLabel53.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[GrandTotalAmount]")})
        Me.XrLabel53.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel53.LocationFloat = New DevExpress.Utils.PointFloat(681.1066!, 182.6517!)
        Me.XrLabel53.Multiline = True
        Me.XrLabel53.Name = "XrLabel53"
        Me.XrLabel53.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel53.SizeF = New System.Drawing.SizeF(96.89392!, 22.29156!)
        Me.XrLabel53.StylePriority.UseBorders = False
        Me.XrLabel53.StylePriority.UseFont = False
        Me.XrLabel53.StylePriority.UseTextAlignment = False
        Me.XrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel53.TextFormatString = "{0:f}"
        '
        'XrLabel54
        '
        Me.XrLabel54.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel54.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel54.CanGrow = False
        Me.XrLabel54.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel54.LocationFloat = New DevExpress.Utils.PointFloat(510.1077!, 182.6518!)
        Me.XrLabel54.Multiline = True
        Me.XrLabel54.Name = "XrLabel54"
        Me.XrLabel54.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel54.SizeF = New System.Drawing.SizeF(170.9989!, 22.29153!)
        Me.XrLabel54.StylePriority.UseBackColor = False
        Me.XrLabel54.StylePriority.UseBorders = False
        Me.XrLabel54.StylePriority.UseFont = False
        Me.XrLabel54.StylePriority.UseTextAlignment = False
        Me.XrLabel54.Text = "TOTAL INVOICE VALUE"
        Me.XrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrTable4
        '
        Me.XrTable4.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable4.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable4.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrTable4.Name = "XrTable4"
        Me.XrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
        Me.XrTable4.SizeF = New System.Drawing.SizeF(778.0005!, 5.0!)
        Me.XrTable4.StylePriority.UseBorders = False
        Me.XrTable4.StylePriority.UseFont = False
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6, Me.XrTableCell7, Me.XrTableCell9, Me.XrTableCell12, Me.XrTableCell13, Me.XrTableCell14, Me.XrTableCell15, Me.XrTableCell16, Me.XrTableCell18, Me.XrTableCell20})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.23688934326172R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell6.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell6.StylePriority.UseBorders = False
        Me.XrTableCell6.StylePriority.UseFont = False
        Me.XrTableCell6.StylePriority.UsePadding = False
        Me.XrTableCell6.Weight = 0.959191052716807R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell7.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell7.StylePriority.UseBorders = False
        Me.XrTableCell7.StylePriority.UseFont = False
        Me.XrTableCell7.StylePriority.UsePadding = False
        Me.XrTableCell7.Weight = 8.4307920137290431R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell9.StylePriority.UseBorders = False
        Me.XrTableCell9.StylePriority.UseFont = False
        Me.XrTableCell9.StylePriority.UsePadding = False
        Me.XrTableCell9.Weight = 2.5676255443470941R
        '
        'XrTableCell12
        '
        Me.XrTableCell12.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell12.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell12.Name = "XrTableCell12"
        Me.XrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell12.StylePriority.UseBorders = False
        Me.XrTableCell12.StylePriority.UseFont = False
        Me.XrTableCell12.StylePriority.UsePadding = False
        Me.XrTableCell12.Weight = 1.3839780518067251R
        '
        'XrTableCell13
        '
        Me.XrTableCell13.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell13.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell13.Name = "XrTableCell13"
        Me.XrTableCell13.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell13.StylePriority.UseBorders = False
        Me.XrTableCell13.StylePriority.UseFont = False
        Me.XrTableCell13.StylePriority.UsePadding = False
        Me.XrTableCell13.Weight = 1.5544169365500409R
        '
        'XrTableCell14
        '
        Me.XrTableCell14.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell14.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell14.Name = "XrTableCell14"
        Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell14.StylePriority.UseBorders = False
        Me.XrTableCell14.StylePriority.UseFont = False
        Me.XrTableCell14.StylePriority.UsePadding = False
        Me.XrTableCell14.Weight = 3.3938337400503578R
        '
        'XrTableCell15
        '
        Me.XrTableCell15.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell15.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell15.Name = "XrTableCell15"
        Me.XrTableCell15.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell15.StylePriority.UseBorders = False
        Me.XrTableCell15.StylePriority.UseFont = False
        Me.XrTableCell15.StylePriority.UsePadding = False
        Me.XrTableCell15.Weight = 2.2097367114580297R
        '
        'XrTableCell16
        '
        Me.XrTableCell16.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell16.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell16.Name = "XrTableCell16"
        Me.XrTableCell16.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell16.StylePriority.UseBorders = False
        Me.XrTableCell16.StylePriority.UseFont = False
        Me.XrTableCell16.StylePriority.UsePadding = False
        Me.XrTableCell16.Weight = 1.2206844737600859R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell18.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell18.StylePriority.UseBorders = False
        Me.XrTableCell18.StylePriority.UseFont = False
        Me.XrTableCell18.StylePriority.UsePadding = False
        Me.XrTableCell18.Weight = 2.105370795409661R
        '
        'XrTableCell20
        '
        Me.XrTableCell20.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell20.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell20.Name = "XrTableCell20"
        Me.XrTableCell20.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell20.StylePriority.UseBorders = False
        Me.XrTableCell20.StylePriority.UseFont = False
        Me.XrTableCell20.StylePriority.UsePadding = False
        Me.XrTableCell20.Weight = 3.3894240921260606R
        '
        'XrLabel25
        '
        Me.XrLabel25.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel25.CanGrow = False
        Me.XrLabel25.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(0!, 5.00001!)
        Me.XrLabel25.Multiline = True
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(381.3978!, 17.56264!)
        Me.XrLabel25.StylePriority.UseBackColor = False
        Me.XrLabel25.StylePriority.UseBorders = False
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "Total"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel37
        '
        Me.XrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel37.CanGrow = False
        Me.XrLabel37.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([Qty])")})
        Me.XrLabel37.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(381.3978!, 4.999965!)
        Me.XrLabel37.Multiline = True
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.SizeF = New System.Drawing.SizeF(44.43631!, 17.56259!)
        Me.XrLabel37.StylePriority.UseBorders = False
        Me.XrLabel37.StylePriority.UseFont = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel37.Summary = XrSummary1
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrLabel37.TextFormatString = "{0:#,##0.00;(#,##0.00)}"
        '
        'XrLabel41
        '
        Me.XrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel41.CanGrow = False
        Me.XrLabel41.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(425.8341!, 5.00001!)
        Me.XrLabel41.Multiline = True
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.SizeF = New System.Drawing.SizeF(16.47394!, 17.56264!)
        Me.XrLabel41.StylePriority.UseBorders = False
        Me.XrLabel41.StylePriority.UseFont = False
        Me.XrLabel41.StylePriority.UseTextAlignment = False
        Me.XrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel130
        '
        Me.XrLabel130.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel130.CanGrow = False
        Me.XrLabel130.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([DiscountVal])")})
        Me.XrLabel130.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel130.LocationFloat = New DevExpress.Utils.PointFloat(620.92!, 5.00001!)
        Me.XrLabel130.Multiline = True
        Me.XrLabel130.Name = "XrLabel130"
        Me.XrLabel130.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel130.SizeF = New System.Drawing.SizeF(60.18652!, 17.56264!)
        Me.XrLabel130.StylePriority.UseBorders = False
        Me.XrLabel130.StylePriority.UseFont = False
        Me.XrLabel130.StylePriority.UseTextAlignment = False
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel130.Summary = XrSummary2
        Me.XrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel130.TextFormatString = "{0:#,##0.00;(#,##0.00)}"
        '
        'XrLabel131
        '
        Me.XrLabel131.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel131.CanGrow = False
        Me.XrLabel131.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([TaxableValue])")})
        Me.XrLabel131.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel131.LocationFloat = New DevExpress.Utils.PointFloat(681.1066!, 4.999965!)
        Me.XrLabel131.Multiline = True
        Me.XrLabel131.Name = "XrLabel131"
        Me.XrLabel131.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel131.SizeF = New System.Drawing.SizeF(96.89392!, 17.56264!)
        Me.XrLabel131.StylePriority.UseBorders = False
        Me.XrLabel131.StylePriority.UseFont = False
        Me.XrLabel131.StylePriority.UseTextAlignment = False
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel131.Summary = XrSummary3
        Me.XrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel131.TextFormatString = "{0:#,##0.00;(#,##0.00)}"
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell8.CanGrow = False
        Me.XrTableCell8.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell8.StylePriority.UseBorders = False
        Me.XrTableCell8.StylePriority.UseFont = False
        Me.XrTableCell8.StylePriority.UsePadding = False
        XrSummary4.FormatString = "{0:#}"
        XrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber
        Me.XrTableCell8.Summary = XrSummary4
        Me.XrTableCell8.Weight = 1.75190101896473R
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel34, Me.XrLabel183, Me.XrLabel182, Me.XrLabel181, Me.XrLabel180, Me.XrLabel142, Me.XrLabel35, Me.XrLabel36, Me.XrLabel184, Me.XrLabel38, Me.XrLabel156, Me.XrLabel28, Me.XrLabel154, Me.XrLabel29, Me.XrLabel30, Me.XrLabel31, Me.XrLabel32, Me.XrLabel33, Me.XrLabel160, Me.XrLabel19, Me.XrLabel20, Me.XrLabel40, Me.XrLabel152, Me.XrLabel21, Me.XrLabel22, Me.XrLabel23, Me.XrLabel158, Me.XrLabel24, Me.XrLabel26, Me.XrLabel27, Me.XrLabel8, Me.XrLabel186, Me.XrLabel185, Me.XrLabel169, Me.XrLabel168, Me.XrLabel9, Me.XrLabel10, Me.XrLabel11, Me.XrLabel12, Me.XrLabel187, Me.XrLabel137, Me.XrLabel136, Me.XrLabel135, Me.XrLabel13, Me.XrLabel14, Me.XrLabel15, Me.XrLabel16, Me.XrLabel132, Me.XrLabel133, Me.XrLabel178, Me.XrLabel17, Me.XrLabel18, Me.XrLabel170, Me.XrLabel171, Me.XrLabel172, Me.XrLabel173, Me.XrLabel175, Me.XrLabel176, Me.XrLabel177, Me.XrLabel134, Me.XrLabel174, Me.XrLabel179, Me.XrLabel145, Me.XrLabel144, Me.XrLabel7, Me.XrLabel4, Me.XrLabel125, Me.XrLabel90, Me.XrLabel89, Me.XrPictureBox2, Me.XrLabel6})
        Me.GroupHeader1.HeightF = 396.9656!
        Me.GroupHeader1.Level = 1
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrLabel34
        '
        Me.XrLabel34.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel34.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel34.CanGrow = False
        Me.XrLabel34.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(680.9711!, 356.6737!)
        Me.XrLabel34.Multiline = True
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.SizeF = New System.Drawing.SizeF(97.02!, 40.29!)
        Me.XrLabel34.StylePriority.UseBackColor = False
        Me.XrLabel34.StylePriority.UseBorders = False
        Me.XrLabel34.StylePriority.UseFont = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "Taxable Value"
        Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel183
        '
        Me.XrLabel183.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel183.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel183.CanGrow = False
        Me.XrLabel183.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel183.LocationFloat = New DevExpress.Utils.PointFloat(425.8341!, 356.6734!)
        Me.XrLabel183.Multiline = True
        Me.XrLabel183.Name = "XrLabel183"
        Me.XrLabel183.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel183.SizeF = New System.Drawing.SizeF(97.02005!, 40.29184!)
        Me.XrLabel183.StylePriority.UseBackColor = False
        Me.XrLabel183.StylePriority.UseBorders = False
        Me.XrLabel183.StylePriority.UseFont = False
        Me.XrLabel183.StylePriority.UseTextAlignment = False
        Me.XrLabel183.Text = "Rate"
        Me.XrLabel183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel182
        '
        Me.XrLabel182.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel182.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel182.CanGrow = False
        Me.XrLabel182.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel182.LocationFloat = New DevExpress.Utils.PointFloat(381.3978!, 356.6717!)
        Me.XrLabel182.Multiline = True
        Me.XrLabel182.Name = "XrLabel182"
        Me.XrLabel182.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel182.SizeF = New System.Drawing.SizeF(44.43631!, 40.29205!)
        Me.XrLabel182.StylePriority.UseBackColor = False
        Me.XrLabel182.StylePriority.UseBorders = False
        Me.XrLabel182.StylePriority.UseFont = False
        Me.XrLabel182.StylePriority.UseTextAlignment = False
        Me.XrLabel182.Text = "Qty"
        Me.XrLabel182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel181
        '
        Me.XrLabel181.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel181.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel181.CanGrow = False
        Me.XrLabel181.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel181.LocationFloat = New DevExpress.Utils.PointFloat(0.002197266!, 356.6718!)
        Me.XrLabel181.Multiline = True
        Me.XrLabel181.Name = "XrLabel181"
        Me.XrLabel181.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel181.SizeF = New System.Drawing.SizeF(27.42053!, 40.29193!)
        Me.XrLabel181.StylePriority.UseBackColor = False
        Me.XrLabel181.StylePriority.UseBorders = False
        Me.XrLabel181.StylePriority.UseFont = False
        Me.XrLabel181.StylePriority.UseTextAlignment = False
        Me.XrLabel181.Text = "Sr. No"
        Me.XrLabel181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel180
        '
        Me.XrLabel180.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel180.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel180.CanGrow = False
        Me.XrLabel180.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel180.LocationFloat = New DevExpress.Utils.PointFloat(27.42273!, 356.6718!)
        Me.XrLabel180.Multiline = True
        Me.XrLabel180.Name = "XrLabel180"
        Me.XrLabel180.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel180.SizeF = New System.Drawing.SizeF(241.01!, 40.29!)
        Me.XrLabel180.StylePriority.UseBackColor = False
        Me.XrLabel180.StylePriority.UseBorders = False
        Me.XrLabel180.StylePriority.UseFont = False
        Me.XrLabel180.StylePriority.UseTextAlignment = False
        Me.XrLabel180.Text = "Product Description"
        Me.XrLabel180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel142
        '
        Me.XrLabel142.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel142.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel142.CanGrow = False
        Me.XrLabel142.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel142.LocationFloat = New DevExpress.Utils.PointFloat(268.4327!, 356.6718!)
        Me.XrLabel142.Multiline = True
        Me.XrLabel142.Name = "XrLabel142"
        Me.XrLabel142.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel142.SizeF = New System.Drawing.SizeF(73.40106!, 40.29196!)
        Me.XrLabel142.StylePriority.UseBackColor = False
        Me.XrLabel142.StylePriority.UseBorders = False
        Me.XrLabel142.StylePriority.UseFont = False
        Me.XrLabel142.StylePriority.UseTextAlignment = False
        Me.XrLabel142.Text = "HSN/SAC" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel35
        '
        Me.XrLabel35.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel35.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel35.CanGrow = False
        Me.XrLabel35.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(341.8338!, 356.6717!)
        Me.XrLabel35.Multiline = True
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.SizeF = New System.Drawing.SizeF(39.564!, 40.29199!)
        Me.XrLabel35.StylePriority.UseBackColor = False
        Me.XrLabel35.StylePriority.UseBorders = False
        Me.XrLabel35.StylePriority.UseFont = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        Me.XrLabel35.Text = "UOM"
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel36
        '
        Me.XrLabel36.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel36.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel36.CanGrow = False
        Me.XrLabel36.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(586.0242!, 356.6732!)
        Me.XrLabel36.Multiline = True
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.SizeF = New System.Drawing.SizeF(34.89587!, 40.29196!)
        Me.XrLabel36.StylePriority.UseBackColor = False
        Me.XrLabel36.StylePriority.UseBorders = False
        Me.XrLabel36.StylePriority.UseFont = False
        Me.XrLabel36.StylePriority.UseTextAlignment = False
        Me.XrLabel36.Text = "Dis.(%)"
        Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel184
        '
        Me.XrLabel184.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel184.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel184.CanGrow = False
        Me.XrLabel184.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel184.LocationFloat = New DevExpress.Utils.PointFloat(522.8541!, 356.6732!)
        Me.XrLabel184.Multiline = True
        Me.XrLabel184.Name = "XrLabel184"
        Me.XrLabel184.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel184.SizeF = New System.Drawing.SizeF(63.1701!, 40.29202!)
        Me.XrLabel184.StylePriority.UseBackColor = False
        Me.XrLabel184.StylePriority.UseBorders = False
        Me.XrLabel184.StylePriority.UseFont = False
        Me.XrLabel184.StylePriority.UseTextAlignment = False
        Me.XrLabel184.Text = "Amount"
        Me.XrLabel184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel38
        '
        Me.XrLabel38.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel38.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel38.CanGrow = False
        Me.XrLabel38.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(620.92!, 356.6737!)
        Me.XrLabel38.Multiline = True
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.SizeF = New System.Drawing.SizeF(60.05096!, 40.29193!)
        Me.XrLabel38.StylePriority.UseBackColor = False
        Me.XrLabel38.StylePriority.UseBorders = False
        Me.XrLabel38.StylePriority.UseFont = False
        Me.XrLabel38.StylePriority.UseTextAlignment = False
        Me.XrLabel38.Text = "Dis"
        Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel156
        '
        Me.XrLabel156.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel156.CanGrow = False
        Me.XrLabel156.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel156.LocationFloat = New DevExpress.Utils.PointFloat(0.0001831055!, 337.921!)
        Me.XrLabel156.Multiline = True
        Me.XrLabel156.Name = "XrLabel156"
        Me.XrLabel156.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel156.SizeF = New System.Drawing.SizeF(55.00764!, 18.75076!)
        Me.XrLabel156.StylePriority.UseBorders = False
        Me.XrLabel156.StylePriority.UseFont = False
        Me.XrLabel156.StylePriority.UseTextAlignment = False
        Me.XrLabel156.Text = "State      : "
        Me.XrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel28
        '
        Me.XrLabel28.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel28.CanGrow = False
        Me.XrLabel28.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverState]")})
        Me.XrLabel28.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(55.00783!, 337.921!)
        Me.XrLabel28.Multiline = True
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(195.7383!, 18.60446!)
        Me.XrLabel28.StylePriority.UseBorders = False
        Me.XrLabel28.StylePriority.UseFont = False
        Me.XrLabel28.StylePriority.UseTextAlignment = False
        Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel154
        '
        Me.XrLabel154.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel154.CanGrow = False
        Me.XrLabel154.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel154.LocationFloat = New DevExpress.Utils.PointFloat(250.7462!, 337.921!)
        Me.XrLabel154.Multiline = True
        Me.XrLabel154.Name = "XrLabel154"
        Me.XrLabel154.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel154.SizeF = New System.Drawing.SizeF(68.72404!, 18.60458!)
        Me.XrLabel154.StylePriority.UseBorders = False
        Me.XrLabel154.StylePriority.UseFont = False
        Me.XrLabel154.StylePriority.UseTextAlignment = False
        Me.XrLabel154.Text = "State Code :"
        Me.XrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel29
        '
        Me.XrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel29.CanGrow = False
        Me.XrLabel29.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverStateCode]")})
        Me.XrLabel29.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(319.4702!, 337.921!)
        Me.XrLabel29.Multiline = True
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(73.05371!, 18.60278!)
        Me.XrLabel29.StylePriority.UseBorders = False
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.StylePriority.UseTextAlignment = False
        Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel30
        '
        Me.XrLabel30.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel30.CanGrow = False
        Me.XrLabel30.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(392.5502!, 338.0672!)
        Me.XrLabel30.Multiline = True
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(63.36362!, 18.60458!)
        Me.XrLabel30.StylePriority.UseBorders = False
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "State         : "
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel31
        '
        Me.XrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel31.CanGrow = False
        Me.XrLabel31.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeState]")})
        Me.XrLabel31.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(456.5882!, 338.0689!)
        Me.XrLabel31.Multiline = True
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(171.0822!, 18.60452!)
        Me.XrLabel31.StylePriority.UseBorders = False
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel32
        '
        Me.XrLabel32.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel32.CanGrow = False
        Me.XrLabel32.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(627.6704!, 338.0692!)
        Me.XrLabel32.Multiline = True
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel32.SizeF = New System.Drawing.SizeF(77.20245!, 18.60428!)
        Me.XrLabel32.StylePriority.UseBorders = False
        Me.XrLabel32.StylePriority.UseFont = False
        Me.XrLabel32.StylePriority.UseTextAlignment = False
        Me.XrLabel32.Text = "State Code   :"
        Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel33
        '
        Me.XrLabel33.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel33.CanGrow = False
        Me.XrLabel33.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeStateCode]")})
        Me.XrLabel33.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(704.873!, 338.0694!)
        Me.XrLabel33.Multiline = True
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.SizeF = New System.Drawing.SizeF(73.12744!, 18.60428!)
        Me.XrLabel33.StylePriority.UseBorders = False
        Me.XrLabel33.StylePriority.UseFont = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel160
        '
        Me.XrLabel160.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel160.CanGrow = False
        Me.XrLabel160.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel160.LocationFloat = New DevExpress.Utils.PointFloat(0.007333569!, 264.0872!)
        Me.XrLabel160.Multiline = True
        Me.XrLabel160.Name = "XrLabel160"
        Me.XrLabel160.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel160.SizeF = New System.Drawing.SizeF(55.0!, 18.6!)
        Me.XrLabel160.StylePriority.UseBorders = False
        Me.XrLabel160.StylePriority.UseFont = False
        Me.XrLabel160.StylePriority.UseTextAlignment = False
        Me.XrLabel160.Text = "Name     : "
        Me.XrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel19
        '
        Me.XrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel19.CanGrow = False
        Me.XrLabel19.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverName]")})
        Me.XrLabel19.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(55.00733!, 264.0872!)
        Me.XrLabel19.Multiline = True
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(337.5166!, 18.60428!)
        Me.XrLabel19.StylePriority.UseBorders = False
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel20
        '
        Me.XrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel20.CanGrow = False
        Me.XrLabel20.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(0.0001220703!, 282.6915!)
        Me.XrLabel20.Multiline = True
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(55.00764!, 18.60428!)
        Me.XrLabel20.StylePriority.UseBorders = False
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "Address :"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel40
        '
        Me.XrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel40.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverAddress]")})
        Me.XrLabel40.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(55.00776!, 282.6923!)
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.SizeF = New System.Drawing.SizeF(337.5161!, 36.76965!)
        Me.XrLabel40.StylePriority.UseBorders = False
        Me.XrLabel40.StylePriority.UseFont = False
        '
        'XrLabel152
        '
        Me.XrLabel152.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel152.CanGrow = False
        Me.XrLabel152.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel152.LocationFloat = New DevExpress.Utils.PointFloat(392.5501!, 264.0872!)
        Me.XrLabel152.Multiline = True
        Me.XrLabel152.Name = "XrLabel152"
        Me.XrLabel152.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel152.SizeF = New System.Drawing.SizeF(63.36353!, 18.60428!)
        Me.XrLabel152.StylePriority.UseBorders = False
        Me.XrLabel152.StylePriority.UseFont = False
        Me.XrLabel152.StylePriority.UseTextAlignment = False
        Me.XrLabel152.Text = "Name      : "
        Me.XrLabel152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel21
        '
        Me.XrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel21.CanGrow = False
        Me.XrLabel21.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(392.5238!, 282.6915!)
        Me.XrLabel21.Multiline = True
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(63.38986!, 18.60428!)
        Me.XrLabel21.StylePriority.UseBorders = False
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "Address  :"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel22
        '
        Me.XrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel22.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeAddress]")})
        Me.XrLabel22.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(455.9137!, 282.6915!)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(321.0189!, 36.77048!)
        Me.XrLabel22.StylePriority.UseBorders = False
        Me.XrLabel22.StylePriority.UseFont = False
        '
        'XrLabel23
        '
        Me.XrLabel23.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel23.CanGrow = False
        Me.XrLabel23.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeName]")})
        Me.XrLabel23.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(455.9136!, 264.0872!)
        Me.XrLabel23.Multiline = True
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(322.0862!, 18.60428!)
        Me.XrLabel23.StylePriority.UseBorders = False
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel158
        '
        Me.XrLabel158.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel158.CanGrow = False
        Me.XrLabel158.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel158.LocationFloat = New DevExpress.Utils.PointFloat(0.0001220703!, 319.4619!)
        Me.XrLabel158.Multiline = True
        Me.XrLabel158.Name = "XrLabel158"
        Me.XrLabel158.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel158.SizeF = New System.Drawing.SizeF(55.00764!, 18.4584!)
        Me.XrLabel158.StylePriority.UseBorders = False
        Me.XrLabel158.StylePriority.UseFont = False
        Me.XrLabel158.StylePriority.UseTextAlignment = False
        Me.XrLabel158.Text = "GST No  : "
        Me.XrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel24
        '
        Me.XrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel24.CanGrow = False
        Me.XrLabel24.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverGSTIN]")})
        Me.XrLabel24.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(55.00776!, 319.4623!)
        Me.XrLabel24.Multiline = True
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(337.5913!, 18.45865!)
        Me.XrLabel24.StylePriority.UseBorders = False
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel26
        '
        Me.XrLabel26.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel26.CanGrow = False
        Me.XrLabel26.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(392.605!, 319.4619!)
        Me.XrLabel26.Multiline = True
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(63.30875!, 18.45837!)
        Me.XrLabel26.StylePriority.UseBorders = False
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "GST No     : "
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel27
        '
        Me.XrLabel27.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel27.CanGrow = False
        Me.XrLabel27.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeGSTIN]")})
        Me.XrLabel27.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(455.9238!, 319.4626!)
        Me.XrLabel27.Multiline = True
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(322.0744!, 18.60651!)
        Me.XrLabel27.StylePriority.UseBorders = False
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.StylePriority.UseTextAlignment = False
        Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel8.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel8.CanGrow = False
        Me.XrLabel8.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(392.5226!, 245.7743!)
        Me.XrLabel8.Multiline = True
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(385.4756!, 18.31287!)
        Me.XrLabel8.StylePriority.UseBackColor = False
        Me.XrLabel8.StylePriority.UseBorders = False
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "Ship to Party"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel186
        '
        Me.XrLabel186.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel186.CanGrow = False
        Me.XrLabel186.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[AgainstForm]")})
        Me.XrLabel186.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel186.LocationFloat = New DevExpress.Utils.PointFloat(78.72188!, 226.8784!)
        Me.XrLabel186.Multiline = True
        Me.XrLabel186.Name = "XrLabel186"
        Me.XrLabel186.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel186.SizeF = New System.Drawing.SizeF(84.21869!, 18.74994!)
        Me.XrLabel186.StylePriority.UseBorders = False
        Me.XrLabel186.StylePriority.UseFont = False
        Me.XrLabel186.StylePriority.UseTextAlignment = False
        Me.XrLabel186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel185
        '
        Me.XrLabel185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel185.CanGrow = False
        Me.XrLabel185.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[JobNo]")})
        Me.XrLabel185.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel185.LocationFloat = New DevExpress.Utils.PointFloat(96.59148!, 190.2528!)
        Me.XrLabel185.Multiline = True
        Me.XrLabel185.Name = "XrLabel185"
        Me.XrLabel185.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel185.SizeF = New System.Drawing.SizeF(126.0384!, 18.31354!)
        Me.XrLabel185.StylePriority.UseBorders = False
        Me.XrLabel185.StylePriority.UseFont = False
        Me.XrLabel185.StylePriority.UseTextAlignment = False
        Me.XrLabel185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel169
        '
        Me.XrLabel169.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel169.CanGrow = False
        Me.XrLabel169.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel169.LocationFloat = New DevExpress.Utils.PointFloat(162.9406!, 226.8784!)
        Me.XrLabel169.Multiline = True
        Me.XrLabel169.Name = "XrLabel169"
        Me.XrLabel169.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel169.SizeF = New System.Drawing.SizeF(59.64148!, 18.74994!)
        Me.XrLabel169.StylePriority.UseBorders = False
        Me.XrLabel169.StylePriority.UseFont = False
        Me.XrLabel169.StylePriority.UseTextAlignment = False
        Me.XrLabel169.Text = "LR NO  : "
        Me.XrLabel169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel168
        '
        Me.XrLabel168.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel168.CanGrow = False
        Me.XrLabel168.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TransportationMode]")})
        Me.XrLabel168.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel168.LocationFloat = New DevExpress.Utils.PointFloat(222.582!, 226.8784!)
        Me.XrLabel168.Multiline = True
        Me.XrLabel168.Name = "XrLabel168"
        Me.XrLabel168.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel168.SizeF = New System.Drawing.SizeF(169.9455!, 18.74991!)
        Me.XrLabel168.StylePriority.UseBorders = False
        Me.XrLabel168.StylePriority.UseFont = False
        Me.XrLabel168.StylePriority.UseTextAlignment = False
        Me.XrLabel168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel9
        '
        Me.XrLabel9.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel9.CanGrow = False
        Me.XrLabel9.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.00595665!, 208.5664!)
        Me.XrLabel9.Multiline = True
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(99.94524!, 18.31216!)
        Me.XrLabel9.StylePriority.UseBorders = False
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Transport Name  :"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel10
        '
        Me.XrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel10.CanGrow = False
        Me.XrLabel10.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SupplyTo]")})
        Me.XrLabel10.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(99.95119!, 208.5664!)
        Me.XrLabel10.Multiline = True
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(292.5783!, 18.31216!)
        Me.XrLabel10.StylePriority.UseBorders = False
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel11
        '
        Me.XrLabel11.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel11.CanGrow = False
        Me.XrLabel11.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(0.005729675!, 171.6487!)
        Me.XrLabel11.Multiline = True
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(81.05447!, 18.60414!)
        Me.XrLabel11.StylePriority.UseBorders = False
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "OFFER NO.     :"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel12
        '
        Me.XrLabel12.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel12.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel12.CanGrow = False
        Me.XrLabel12.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.00595665!, 245.7743!)
        Me.XrLabel12.Multiline = True
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(392.5166!, 18.31287!)
        Me.XrLabel12.StylePriority.UseBackColor = False
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "Bill to Party"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel187
        '
        Me.XrLabel187.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel187.CanGrow = False
        Me.XrLabel187.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel187.LocationFloat = New DevExpress.Utils.PointFloat(222.6299!, 190.2528!)
        Me.XrLabel187.Multiline = True
        Me.XrLabel187.Name = "XrLabel187"
        Me.XrLabel187.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel187.SizeF = New System.Drawing.SizeF(169.8977!, 18.31351!)
        Me.XrLabel187.StylePriority.UseBorders = False
        Me.XrLabel187.StylePriority.UseFont = False
        Me.XrLabel187.StylePriority.UseTextAlignment = False
        Me.XrLabel187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel137
        '
        Me.XrLabel137.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel137.CanGrow = False
        Me.XrLabel137.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OfferNo]")})
        Me.XrLabel137.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel137.LocationFloat = New DevExpress.Utils.PointFloat(81.0602!, 171.6486!)
        Me.XrLabel137.Multiline = True
        Me.XrLabel137.Name = "XrLabel137"
        Me.XrLabel137.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel137.SizeF = New System.Drawing.SizeF(189.4546!, 18.60417!)
        Me.XrLabel137.StylePriority.UseBorders = False
        Me.XrLabel137.StylePriority.UseFont = False
        Me.XrLabel137.StylePriority.UseTextAlignment = False
        Me.XrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel136
        '
        Me.XrLabel136.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel136.CanGrow = False
        Me.XrLabel136.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel136.LocationFloat = New DevExpress.Utils.PointFloat(270.5148!, 171.6486!)
        Me.XrLabel136.Multiline = True
        Me.XrLabel136.Name = "XrLabel136"
        Me.XrLabel136.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel136.SizeF = New System.Drawing.SizeF(37.84314!, 18.60419!)
        Me.XrLabel136.StylePriority.UseBorders = False
        Me.XrLabel136.StylePriority.UseFont = False
        Me.XrLabel136.StylePriority.UseTextAlignment = False
        Me.XrLabel136.Text = "DATE :"
        Me.XrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel135
        '
        Me.XrLabel135.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel135.CanGrow = False
        Me.XrLabel135.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OfferDate]")})
        Me.XrLabel135.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel135.LocationFloat = New DevExpress.Utils.PointFloat(308.3579!, 171.6486!)
        Me.XrLabel135.Multiline = True
        Me.XrLabel135.Name = "XrLabel135"
        Me.XrLabel135.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel135.SizeF = New System.Drawing.SizeF(84.17136!, 18.60419!)
        Me.XrLabel135.StylePriority.UseBorders = False
        Me.XrLabel135.StylePriority.UseFont = False
        Me.XrLabel135.StylePriority.UseTextAlignment = False
        Me.XrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel135.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel13
        '
        Me.XrLabel13.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel13.CanGrow = False
        Me.XrLabel13.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(0.003845215!, 226.8784!)
        Me.XrLabel13.Multiline = True
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(78.71803!, 18.74997!)
        Me.XrLabel13.StylePriority.UseBorders = False
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Vehical No.   :"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel14
        '
        Me.XrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel14.CanGrow = False
        Me.XrLabel14.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[JobNoPrefix]")})
        Me.XrLabel14.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(81.0602!, 190.2528!)
        Me.XrLabel14.Multiline = True
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(15.53128!, 18.31354!)
        Me.XrLabel14.StylePriority.UseBorders = False
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel15
        '
        Me.XrLabel15.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel15.CanGrow = False
        Me.XrLabel15.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0.003845215!, 190.2528!)
        Me.XrLabel15.Multiline = True
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(81.05635!, 18.31357!)
        Me.XrLabel15.StylePriority.UseBorders = False
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "Job Ref. No    :"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel16
        '
        Me.XrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel16.CanGrow = False
        Me.XrLabel16.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(624.5632!, 190.2527!)
        Me.XrLabel16.Multiline = True
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(80.30963!, 18.60515!)
        Me.XrLabel16.StylePriority.UseBorders = False
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "DATE              :"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel132
        '
        Me.XrLabel132.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel132.CanGrow = False
        Me.XrLabel132.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PODate]")})
        Me.XrLabel132.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel132.LocationFloat = New DevExpress.Utils.PointFloat(704.9185!, 190.2527!)
        Me.XrLabel132.Multiline = True
        Me.XrLabel132.Name = "XrLabel132"
        Me.XrLabel132.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel132.SizeF = New System.Drawing.SizeF(73.08197!, 18.60521!)
        Me.XrLabel132.StylePriority.UseBorders = False
        Me.XrLabel132.StylePriority.UseFont = False
        Me.XrLabel132.StylePriority.UseTextAlignment = False
        Me.XrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel132.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel133
        '
        Me.XrLabel133.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel133.CanGrow = False
        Me.XrLabel133.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[InvoiceDate]")})
        Me.XrLabel133.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel133.LocationFloat = New DevExpress.Utils.PointFloat(704.9194!, 171.6486!)
        Me.XrLabel133.Multiline = True
        Me.XrLabel133.Name = "XrLabel133"
        Me.XrLabel133.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel133.SizeF = New System.Drawing.SizeF(73.05365!, 18.60416!)
        Me.XrLabel133.StylePriority.UseBorders = False
        Me.XrLabel133.StylePriority.UseFont = False
        Me.XrLabel133.StylePriority.UseTextAlignment = False
        Me.XrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel133.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel178
        '
        Me.XrLabel178.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel178.CanGrow = False
        Me.XrLabel178.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel178.LocationFloat = New DevExpress.Utils.PointFloat(392.6036!, 171.9401!)
        Me.XrLabel178.Multiline = True
        Me.XrLabel178.Name = "XrLabel178"
        Me.XrLabel178.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel178.SizeF = New System.Drawing.SizeF(86.53363!, 18.31265!)
        Me.XrLabel178.StylePriority.UseBorders = False
        Me.XrLabel178.StylePriority.UseFont = False
        Me.XrLabel178.StylePriority.UseTextAlignment = False
        Me.XrLabel178.Text = "Invoice No.       : "
        Me.XrLabel178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel17
        '
        Me.XrLabel17.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel17.CanGrow = False
        Me.XrLabel17.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(392.6036!, 190.2528!)
        Me.XrLabel17.Multiline = True
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(86.5336!, 18.6051!)
        Me.XrLabel17.StylePriority.UseBorders = False
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "P. O NO              :"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel18
        '
        Me.XrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel18.CanGrow = False
        Me.XrLabel18.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PO]")})
        Me.XrLabel18.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(479.1375!, 190.2528!)
        Me.XrLabel18.Multiline = True
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(145.4258!, 18.60515!)
        Me.XrLabel18.StylePriority.UseBorders = False
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel170
        '
        Me.XrLabel170.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel170.CanGrow = False
        Me.XrLabel170.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel170.LocationFloat = New DevExpress.Utils.PointFloat(693.9264!, 227.1701!)
        Me.XrLabel170.Multiline = True
        Me.XrLabel170.Name = "XrLabel170"
        Me.XrLabel170.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel170.SizeF = New System.Drawing.SizeF(84.00043!, 18.45834!)
        Me.XrLabel170.StylePriority.UseBorders = False
        Me.XrLabel170.StylePriority.UseFont = False
        Me.XrLabel170.StylePriority.UseTextAlignment = False
        Me.XrLabel170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel171
        '
        Me.XrLabel171.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel171.CanGrow = False
        Me.XrLabel171.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel171.LocationFloat = New DevExpress.Utils.PointFloat(624.5145!, 227.1701!)
        Me.XrLabel171.Multiline = True
        Me.XrLabel171.Name = "XrLabel171"
        Me.XrLabel171.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel171.SizeF = New System.Drawing.SizeF(69.41168!, 18.45844!)
        Me.XrLabel171.StylePriority.UseBorders = False
        Me.XrLabel171.StylePriority.UseFont = False
        Me.XrLabel171.StylePriority.UseTextAlignment = False
        Me.XrLabel171.Text = "State Code :"
        Me.XrLabel171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel172
        '
        Me.XrLabel172.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel172.CanGrow = False
        Me.XrLabel172.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel172.LocationFloat = New DevExpress.Utils.PointFloat(479.1373!, 227.1701!)
        Me.XrLabel172.Multiline = True
        Me.XrLabel172.Name = "XrLabel172"
        Me.XrLabel172.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel172.SizeF = New System.Drawing.SizeF(145.3773!, 18.45834!)
        Me.XrLabel172.StylePriority.UseBorders = False
        Me.XrLabel172.StylePriority.UseFont = False
        Me.XrLabel172.StylePriority.UseTextAlignment = False
        Me.XrLabel172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel173
        '
        Me.XrLabel173.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel173.CanGrow = False
        Me.XrLabel173.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel173.LocationFloat = New DevExpress.Utils.PointFloat(392.6036!, 227.1701!)
        Me.XrLabel173.Multiline = True
        Me.XrLabel173.Name = "XrLabel173"
        Me.XrLabel173.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel173.SizeF = New System.Drawing.SizeF(86.53363!, 18.45834!)
        Me.XrLabel173.StylePriority.UseBorders = False
        Me.XrLabel173.StylePriority.UseFont = False
        Me.XrLabel173.StylePriority.UseTextAlignment = False
        Me.XrLabel173.Text = "State                  :"
        Me.XrLabel173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel175
        '
        Me.XrLabel175.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel175.CanGrow = False
        Me.XrLabel175.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[GSTReverseCharge]")})
        Me.XrLabel175.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel175.LocationFloat = New DevExpress.Utils.PointFloat(491.9062!, 208.8579!)
        Me.XrLabel175.Multiline = True
        Me.XrLabel175.Name = "XrLabel175"
        Me.XrLabel175.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel175.SizeF = New System.Drawing.SizeF(286.0206!, 18.31216!)
        Me.XrLabel175.StylePriority.UseBorders = False
        Me.XrLabel175.StylePriority.UseFont = False
        Me.XrLabel175.StylePriority.UseTextAlignment = False
        Me.XrLabel175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel176
        '
        Me.XrLabel176.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel176.CanGrow = False
        Me.XrLabel176.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[InvoiceNo]")})
        Me.XrLabel176.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel176.LocationFloat = New DevExpress.Utils.PointFloat(479.1373!, 171.9401!)
        Me.XrLabel176.Multiline = True
        Me.XrLabel176.Name = "XrLabel176"
        Me.XrLabel176.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel176.SizeF = New System.Drawing.SizeF(145.424!, 18.31265!)
        Me.XrLabel176.StylePriority.UseBorders = False
        Me.XrLabel176.StylePriority.UseFont = False
        Me.XrLabel176.StylePriority.UseTextAlignment = False
        Me.XrLabel176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel177
        '
        Me.XrLabel177.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel177.CanGrow = False
        Me.XrLabel177.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel177.LocationFloat = New DevExpress.Utils.PointFloat(392.6036!, 208.8579!)
        Me.XrLabel177.Multiline = True
        Me.XrLabel177.Name = "XrLabel177"
        Me.XrLabel177.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel177.SizeF = New System.Drawing.SizeF(99.30249!, 18.31219!)
        Me.XrLabel177.StylePriority.UseBorders = False
        Me.XrLabel177.StylePriority.UseFont = False
        Me.XrLabel177.StylePriority.UseTextAlignment = False
        Me.XrLabel177.Text = "Reverse Charge :"
        Me.XrLabel177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel134
        '
        Me.XrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel134.CanGrow = False
        Me.XrLabel134.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel134.LocationFloat = New DevExpress.Utils.PointFloat(624.5613!, 171.794!)
        Me.XrLabel134.Multiline = True
        Me.XrLabel134.Name = "XrLabel134"
        Me.XrLabel134.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel134.SizeF = New System.Drawing.SizeF(80.35828!, 18.4588!)
        Me.XrLabel134.StylePriority.UseBorders = False
        Me.XrLabel134.StylePriority.UseFont = False
        Me.XrLabel134.StylePriority.UseTextAlignment = False
        Me.XrLabel134.Text = "Invoice Date :"
        Me.XrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel174
        '
        Me.XrLabel174.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel174.CanGrow = False
        Me.XrLabel174.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel174.LocationFloat = New DevExpress.Utils.PointFloat(626.9958!, 117.6275!)
        Me.XrLabel174.Multiline = True
        Me.XrLabel174.Name = "XrLabel174"
        Me.XrLabel174.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel174.SizeF = New System.Drawing.SizeF(151.0041!, 54.02104!)
        Me.XrLabel174.StylePriority.UseBorders = False
        Me.XrLabel174.StylePriority.UseFont = False
        Me.XrLabel174.StylePriority.UseTextAlignment = False
        Me.XrLabel174.Text = "Original for Receipant" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Duplicate" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Triplicate "
        Me.XrLabel174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel179
        '
        Me.XrLabel179.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel179.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel179.CanGrow = False
        Me.XrLabel179.Font = New System.Drawing.Font("Calisto MT", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel179.LocationFloat = New DevExpress.Utils.PointFloat(0.00009155273!, 117.6275!)
        Me.XrLabel179.Multiline = True
        Me.XrLabel179.Name = "XrLabel179"
        Me.XrLabel179.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel179.SizeF = New System.Drawing.SizeF(597.5432!, 54.02103!)
        Me.XrLabel179.StylePriority.UseBackColor = False
        Me.XrLabel179.StylePriority.UseBorders = False
        Me.XrLabel179.StylePriority.UseFont = False
        Me.XrLabel179.StylePriority.UseForeColor = False
        Me.XrLabel179.StylePriority.UseTextAlignment = False
        Me.XrLabel179.Text = "            TAX  INVOICE"
        Me.XrLabel179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel145
        '
        Me.XrLabel145.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel145.CanGrow = False
        Me.XrLabel145.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel145.LocationFloat = New DevExpress.Utils.PointFloat(597.5432!, 117.6273!)
        Me.XrLabel145.Multiline = True
        Me.XrLabel145.Name = "XrLabel145"
        Me.XrLabel145.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel145.SizeF = New System.Drawing.SizeF(29.45264!, 18.45847!)
        Me.XrLabel145.StylePriority.UseBorders = False
        Me.XrLabel145.StylePriority.UseFont = False
        Me.XrLabel145.StylePriority.UseTextAlignment = False
        Me.XrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel144
        '
        Me.XrLabel144.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel144.CanGrow = False
        Me.XrLabel144.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel144.LocationFloat = New DevExpress.Utils.PointFloat(597.5433!, 136.0858!)
        Me.XrLabel144.Multiline = True
        Me.XrLabel144.Name = "XrLabel144"
        Me.XrLabel144.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel144.SizeF = New System.Drawing.SizeF(29.45264!, 18.45847!)
        Me.XrLabel144.StylePriority.UseBorders = False
        Me.XrLabel144.StylePriority.UseFont = False
        Me.XrLabel144.StylePriority.UseTextAlignment = False
        Me.XrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel7
        '
        Me.XrLabel7.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel7.CanGrow = False
        Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(597.5433!, 154.5442!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(29.45264!, 17.10432!)
        Me.XrLabel7.StylePriority.UseBorders = False
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel4
        '
        Me.XrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(119.5137!, 98.94173!)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(658.4821!, 18.6858!)
        Me.XrLabel4.StylePriority.UseBorders = False
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel125
        '
        Me.XrLabel125.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel125.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel125.LocationFloat = New DevExpress.Utils.PointFloat(119.5137!, 49.04784!)
        Me.XrLabel125.Multiline = True
        Me.XrLabel125.Name = "XrLabel125"
        Me.XrLabel125.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel125.SizeF = New System.Drawing.SizeF(658.48!, 30.0!)
        Me.XrLabel125.StylePriority.UseBorders = False
        Me.XrLabel125.StylePriority.UseFont = False
        Me.XrLabel125.StylePriority.UseTextAlignment = False
        Me.XrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel90
        '
        Me.XrLabel90.BorderColor = System.Drawing.Color.Black
        Me.XrLabel90.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel90.CanGrow = False
        Me.XrLabel90.Font = New System.Drawing.Font("Cooper Black", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel90.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.XrLabel90.LocationFloat = New DevExpress.Utils.PointFloat(119.5137!, 0!)
        Me.XrLabel90.Multiline = True
        Me.XrLabel90.Name = "XrLabel90"
        Me.XrLabel90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel90.SizeF = New System.Drawing.SizeF(658.48!, 35.04!)
        Me.XrLabel90.StylePriority.UseBorderColor = False
        Me.XrLabel90.StylePriority.UseBorders = False
        Me.XrLabel90.StylePriority.UseFont = False
        Me.XrLabel90.StylePriority.UseForeColor = False
        Me.XrLabel90.StylePriority.UseTextAlignment = False
        Me.XrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel89
        '
        Me.XrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel89.CanGrow = False
        Me.XrLabel89.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel89.LocationFloat = New DevExpress.Utils.PointFloat(119.5137!, 79.04784!)
        Me.XrLabel89.Multiline = True
        Me.XrLabel89.Name = "XrLabel89"
        Me.XrLabel89.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel89.SizeF = New System.Drawing.SizeF(658.4824!, 19.89393!)
        Me.XrLabel89.StylePriority.UseBorders = False
        Me.XrLabel89.StylePriority.UseFont = False
        Me.XrLabel89.StylePriority.UseTextAlignment = False
        Me.XrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPictureBox2
        '
        Me.XrPictureBox2.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
        Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(0.003845215!, 0!)
        Me.XrPictureBox2.Name = "XrPictureBox2"
        Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(119.5098!, 117.6276!)
        Me.XrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        Me.XrPictureBox2.StylePriority.UseBorders = False
        '
        'XrLabel6
        '
        Me.XrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel6.CanGrow = False
        Me.XrLabel6.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(119.5211!, 35.03999!)
        Me.XrLabel6.Multiline = True
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(658.47!, 14.00784!)
        Me.XrLabel6.StylePriority.UseBorders = False
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'DT_InvoiceTableAdapter2
        '
        Me.DT_InvoiceTableAdapter2.ClearBeforeFill = True
        '
        'DT_InvoiceTableAdapter3
        '
        Me.DT_InvoiceTableAdapter3.ClearBeforeFill = True
        '
        'ClientPOTableAdapter
        '
        Me.ClientPOTableAdapter.ClearBeforeFill = True
        '
        'InvoiceNew1
        '
        Me.InvoiceNew1.DataSetName = "InvoiceNew"
        Me.InvoiceNew1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DT_InvoiceTableAdapter1
        '
        Me.DT_InvoiceTableAdapter1.ClearBeforeFill = True
        '
        'InvoiceNew2
        '
        Me.InvoiceNew2.DataSetName = "InvoiceNew"
        Me.InvoiceNew2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'XrCrossBandLine3
        '
        Me.XrCrossBandLine3.EndBand = Me.GroupHeader1
        Me.XrCrossBandLine3.EndPointFloat = New DevExpress.Utils.PointFloat(0!, 320.8271!)
        Me.XrCrossBandLine3.LocationFloat = New DevExpress.Utils.PointFloat(0!, 301.2957!)
        Me.XrCrossBandLine3.Name = "XrCrossBandLine3"
        Me.XrCrossBandLine3.StartBand = Me.GroupHeader1
        Me.XrCrossBandLine3.StartPointFloat = New DevExpress.Utils.PointFloat(0!, 301.2957!)
        Me.XrCrossBandLine3.WidthF = 1.0!
        '
        'XrCrossBandLine4
        '
        Me.XrCrossBandLine4.EndBand = Me.GroupHeader1
        Me.XrCrossBandLine4.EndPointFloat = New DevExpress.Utils.PointFloat(392.605!, 320.8271!)
        Me.XrCrossBandLine4.LocationFloat = New DevExpress.Utils.PointFloat(392.605!, 301.2957!)
        Me.XrCrossBandLine4.Name = "XrCrossBandLine4"
        Me.XrCrossBandLine4.StartBand = Me.GroupHeader1
        Me.XrCrossBandLine4.StartPointFloat = New DevExpress.Utils.PointFloat(392.605!, 301.2957!)
        Me.XrCrossBandLine4.WidthF = 1.0!
        '
        'XrCrossBandLine5
        '
        Me.XrCrossBandLine5.EndBand = Me.GroupHeader1
        Me.XrCrossBandLine5.EndPointFloat = New DevExpress.Utils.PointFloat(776.4272!, 319.462!)
        Me.XrCrossBandLine5.LocationFloat = New DevExpress.Utils.PointFloat(776.4272!, 282.6923!)
        Me.XrCrossBandLine5.Name = "XrCrossBandLine5"
        Me.XrCrossBandLine5.StartBand = Me.GroupHeader1
        Me.XrCrossBandLine5.StartPointFloat = New DevExpress.Utils.PointFloat(776.4272!, 282.6923!)
        Me.XrCrossBandLine5.WidthF = 1.116089!
        '
        'XR_Invoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageFooter, Me.GroupFooter2, Me.GroupHeader1})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.InvoiceNew1, Me.InvoiceNew2})
        Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandLine3, Me.XrCrossBandLine21, Me.XrCrossBandLine20, Me.XrCrossBandLine19, Me.XrCrossBandLine16, Me.XrCrossBandLine15, Me.XrCrossBandLine1, Me.XrCrossBandLine4, Me.XrCrossBandLine5})
        Me.DataAdapter = Me.DT_InvoiceTableAdapter3
        Me.DataMember = "DT_Invoice"
        Me.DataSource = Me.InvoiceNew2
        Me.FilterString = "[InvoiceID] = ?Invoice"
        Me.Font = New System.Drawing.Font("Times New Roman", 14.0!)
        Me.Margins = New System.Drawing.Printing.Margins(22, 27, 50, 1)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Invoice})
        Me.Version = "17.2"
        CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceNew1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceNew2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents DirectorySearcher1 As DirectoryServices.DirectorySearcher

    Friend WithEvents XrCrossBandLine1 As DevExpress.XtraReports.UI.XRCrossBandLine

    Friend WithEvents Invoice As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrCrossBandLine15 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine16 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine19 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine20 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine21 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel131 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrTable4 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell13 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents DT_InvoiceTableAdapter2 As InvoiceNewTableAdapters.DT_InvoiceTableAdapter
    Friend WithEvents DT_InvoiceTableAdapter3 As InvoiceNewTableAdapters.DT_InvoiceTableAdapter
    Friend WithEvents ClientPOTableAdapter As DS_ClientPOTableAdapters.ClientPOTableAdapter
    Friend WithEvents InvoiceNew1 As InvoiceNew
    Friend WithEvents DT_InvoiceTableAdapter1 As InvoiceNewTableAdapters.DT_InvoiceTableAdapter
    Friend WithEvents InvoiceNew2 As InvoiceNew
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel125 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel90 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel89 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel174 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel179 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel145 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel144 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel186 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel185 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel169 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel168 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel187 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel137 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel136 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel135 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel132 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel133 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel178 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel170 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel171 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel172 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel173 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel175 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel176 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel177 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel134 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel160 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel152 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel158 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel156 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel154 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel183 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel182 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel181 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel180 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel142 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel184 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCrossBandLine3 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine4 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine5 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrLabel124 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel116 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel115 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel114 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel112 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel111 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel110 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel108 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel107 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel106 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel105 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel104 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel103 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel101 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel100 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel99 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel98 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel97 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel96 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel95 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel94 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel92 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel43 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel44 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel45 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel46 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel47 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel48 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel49 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel50 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel51 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel52 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel53 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel54 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel130 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel56 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel57 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel42 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel55 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel58 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel59 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel60 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel109 As DevExpress.XtraReports.UI.XRLabel
End Class
