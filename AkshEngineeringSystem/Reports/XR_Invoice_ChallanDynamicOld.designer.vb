﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class XR_Invoice_ChallanDynamicOld
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XR_Invoice_ChallanDynamicOld))
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrLabel87 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DirectorySearcher1 = New System.DirectoryServices.DirectorySearcher()
        Me.XrCrossBandLine1 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.Invoice = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.DT_InvoiceTableAdapter2 = New AkshEngineeringSystem.InvoiceNewTableAdapters.DT_InvoiceTableAdapter()
        Me.DT_InvoiceTableAdapter3 = New AkshEngineeringSystem.InvoiceNewTableAdapters.DT_InvoiceTableAdapter()
        Me.ClientPOTableAdapter = New AkshEngineeringSystem.DS_ClientPOTableAdapters.ClientPOTableAdapter()
        Me.InvoiceNew1 = New AkshEngineeringSystem.InvoiceNew()
        Me.DT_InvoiceTableAdapter1 = New AkshEngineeringSystem.InvoiceNewTableAdapters.DT_InvoiceTableAdapter()
        Me.InvoiceNew2 = New AkshEngineeringSystem.InvoiceNew()
        Me.DS_PackingList1 = New AkshEngineeringSystem.DS_PackingList()
        Me.DataTable1TableAdapter = New AkshEngineeringSystem.DS_PackingListTableAdapters.DataTable1TableAdapter()
        Me.DataTable1TableAdapter1 = New AkshEngineeringSystem.DS_PackingListTableAdapters.DataTable1TableAdapter()
        Me.DataTable1TableAdapter2 = New AkshEngineeringSystem.DS_PackingListTableAdapters.DataTable1TableAdapter()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel186 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel185 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel182 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel181 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel180 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel179 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel178 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel177 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel176 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel175 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel174 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel173 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel172 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel171 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel170 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel169 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel168 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel167 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel166 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel165 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel164 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel163 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel162 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel161 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel160 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel159 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel158 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel157 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel156 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel155 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel154 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel153 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel152 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel151 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel150 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel149 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel148 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel147 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel146 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel145 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel144 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel143 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel141 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel138 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel137 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel136 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel135 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel134 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel133 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel132 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel131 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel130 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel129 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel125 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel90 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel89 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel42 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrTable4 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell19 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell20 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel124 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel116 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel115 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel114 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel110 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel109 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel108 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel107 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel106 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel94 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrCrossBandLine15 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine16 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        Me.XrCrossBandLine17 = New DevExpress.XtraReports.UI.XRCrossBandLine()
        CType(Me.InvoiceNew1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvoiceNew2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PackingList1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.Detail.HeightF = 0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseBorders = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 10.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel87})
        Me.BottomMargin.HeightF = 311.0513!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.StylePriority.UseBorders = False
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel87
        '
        Me.XrLabel87.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel87.CanGrow = False
        Me.XrLabel87.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[GSTReverseCharge]")})
        Me.XrLabel87.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel87.LocationFloat = New DevExpress.Utils.PointFloat(1039.782!, 293.3455!)
        Me.XrLabel87.Multiline = True
        Me.XrLabel87.Name = "XrLabel87"
        Me.XrLabel87.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel87.SizeF = New System.Drawing.SizeF(106.79!, 17.70578!)
        Me.XrLabel87.StylePriority.UseBorders = False
        Me.XrLabel87.StylePriority.UseFont = False
        Me.XrLabel87.StylePriority.UseTextAlignment = False
        Me.XrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrLabel87.TextFormatString = "{0:#,##0.00;(#,##0.00)}"
        '
        'DirectorySearcher1
        '
        Me.DirectorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01")
        Me.DirectorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01")
        Me.DirectorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01")
        '
        'XrCrossBandLine1
        '
        Me.XrCrossBandLine1.EndBand = Nothing
        Me.XrCrossBandLine1.EndPointFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCrossBandLine1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCrossBandLine1.Name = "XrCrossBandLine1"
        Me.XrCrossBandLine1.StartBand = Nothing
        Me.XrCrossBandLine1.StartPointFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrCrossBandLine1.WidthF = 9.375!
        '
        'Invoice
        '
        Me.Invoice.Description = "Invoice"
        Me.Invoice.Name = "Invoice"
        Me.Invoice.Type = GetType(Integer)
        Me.Invoice.ValueInfo = "0"
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell8.CanGrow = False
        Me.XrTableCell8.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell8.StylePriority.UseBorders = False
        Me.XrTableCell8.StylePriority.UseFont = False
        Me.XrTableCell8.StylePriority.UsePadding = False
        XrSummary1.FormatString = "{0:#}"
        XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber
        Me.XrTableCell8.Summary = XrSummary1
        Me.XrTableCell8.Weight = 1.75190101896473R
        '
        'DT_InvoiceTableAdapter2
        '
        Me.DT_InvoiceTableAdapter2.ClearBeforeFill = True
        '
        'DT_InvoiceTableAdapter3
        '
        Me.DT_InvoiceTableAdapter3.ClearBeforeFill = True
        '
        'ClientPOTableAdapter
        '
        Me.ClientPOTableAdapter.ClearBeforeFill = True
        '
        'InvoiceNew1
        '
        Me.InvoiceNew1.DataSetName = "InvoiceNew"
        Me.InvoiceNew1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DT_InvoiceTableAdapter1
        '
        Me.DT_InvoiceTableAdapter1.ClearBeforeFill = True
        '
        'InvoiceNew2
        '
        Me.InvoiceNew2.DataSetName = "InvoiceNew"
        Me.InvoiceNew2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DS_PackingList1
        '
        Me.DS_PackingList1.DataSetName = "DS_PackingList"
        Me.DS_PackingList1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataTable1TableAdapter
        '
        Me.DataTable1TableAdapter.ClearBeforeFill = True
        '
        'DataTable1TableAdapter1
        '
        Me.DataTable1TableAdapter1.ClearBeforeFill = True
        '
        'DataTable1TableAdapter2
        '
        Me.DataTable1TableAdapter2.ClearBeforeFill = True
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox2, Me.XrLabel186, Me.XrLabel185, Me.XrLabel182, Me.XrLabel181, Me.XrLabel180, Me.XrLabel179, Me.XrLabel178, Me.XrLabel177, Me.XrLabel176, Me.XrLabel175, Me.XrLabel174, Me.XrLabel173, Me.XrLabel172, Me.XrLabel171, Me.XrLabel170, Me.XrLabel169, Me.XrLabel168, Me.XrLabel167, Me.XrLabel166, Me.XrLabel165, Me.XrLabel164, Me.XrLabel163, Me.XrLabel162, Me.XrLabel161, Me.XrLabel160, Me.XrLabel159, Me.XrLabel158, Me.XrLabel157, Me.XrLabel156, Me.XrLabel155, Me.XrLabel154, Me.XrLabel153, Me.XrLabel152, Me.XrLabel151, Me.XrLabel150, Me.XrLabel149, Me.XrLabel148, Me.XrLabel147, Me.XrLabel146, Me.XrLabel145, Me.XrLabel144, Me.XrLabel143, Me.XrLabel141, Me.XrLabel138, Me.XrLabel137, Me.XrLabel136, Me.XrLabel135, Me.XrLabel134, Me.XrLabel133, Me.XrLabel132, Me.XrLabel131, Me.XrLabel130, Me.XrLabel129, Me.XrLabel125, Me.XrLabel90, Me.XrLabel89, Me.XrLabel42, Me.XrLabel41, Me.XrLabel40, Me.XrLabel5, Me.XrLabel3, Me.XrLabel1, Me.XrLabel6, Me.XrLabel4, Me.XrLabel8, Me.XrLabel7, Me.XrLabel37})
        Me.GroupHeader1.HeightF = 396.9652!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrPictureBox2
        '
        Me.XrPictureBox2.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
        Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(0.007611632!, 0!)
        Me.XrPictureBox2.Name = "XrPictureBox2"
        Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(119.5098!, 117.6276!)
        Me.XrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        Me.XrPictureBox2.StylePriority.UseBorders = False
        '
        'XrLabel186
        '
        Me.XrLabel186.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel186.CanGrow = False
        Me.XrLabel186.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[AgainstForm]")})
        Me.XrLabel186.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel186.LocationFloat = New DevExpress.Utils.PointFloat(471.2605!, 227.0242!)
        Me.XrLabel186.Multiline = True
        Me.XrLabel186.Name = "XrLabel186"
        Me.XrLabel186.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel186.SizeF = New System.Drawing.SizeF(84.21869!, 18.74994!)
        Me.XrLabel186.StylePriority.UseBorders = False
        Me.XrLabel186.StylePriority.UseFont = False
        Me.XrLabel186.StylePriority.UseTextAlignment = False
        Me.XrLabel186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel185
        '
        Me.XrLabel185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel185.CanGrow = False
        Me.XrLabel185.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[JobNo]")})
        Me.XrLabel185.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel185.LocationFloat = New DevExpress.Utils.PointFloat(489.1302!, 190.3984!)
        Me.XrLabel185.Multiline = True
        Me.XrLabel185.Name = "XrLabel185"
        Me.XrLabel185.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel185.SizeF = New System.Drawing.SizeF(108.413!, 18.3136!)
        Me.XrLabel185.StylePriority.UseBorders = False
        Me.XrLabel185.StylePriority.UseFont = False
        Me.XrLabel185.StylePriority.UseTextAlignment = False
        Me.XrLabel185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel182
        '
        Me.XrLabel182.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel182.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel182.CanGrow = False
        Me.XrLabel182.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel182.LocationFloat = New DevExpress.Utils.PointFloat(704.1983!, 356.6731!)
        Me.XrLabel182.Multiline = True
        Me.XrLabel182.Name = "XrLabel182"
        Me.XrLabel182.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel182.SizeF = New System.Drawing.SizeF(73.8028!, 40.29205!)
        Me.XrLabel182.StylePriority.UseBackColor = False
        Me.XrLabel182.StylePriority.UseBorders = False
        Me.XrLabel182.StylePriority.UseFont = False
        Me.XrLabel182.StylePriority.UseTextAlignment = False
        Me.XrLabel182.Text = "Qty"
        Me.XrLabel182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel181
        '
        Me.XrLabel181.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel181.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel181.CanGrow = False
        Me.XrLabel181.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel181.LocationFloat = New DevExpress.Utils.PointFloat(0.00009155273!, 356.6732!)
        Me.XrLabel181.Multiline = True
        Me.XrLabel181.Name = "XrLabel181"
        Me.XrLabel181.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel181.SizeF = New System.Drawing.SizeF(27.42053!, 40.29193!)
        Me.XrLabel181.StylePriority.UseBackColor = False
        Me.XrLabel181.StylePriority.UseBorders = False
        Me.XrLabel181.StylePriority.UseFont = False
        Me.XrLabel181.StylePriority.UseTextAlignment = False
        Me.XrLabel181.Text = "Sr. No"
        Me.XrLabel181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel180
        '
        Me.XrLabel180.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel180.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel180.CanGrow = False
        Me.XrLabel180.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel180.LocationFloat = New DevExpress.Utils.PointFloat(27.42061!, 356.6732!)
        Me.XrLabel180.Multiline = True
        Me.XrLabel180.Name = "XrLabel180"
        Me.XrLabel180.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel180.SizeF = New System.Drawing.SizeF(599.5752!, 40.29196!)
        Me.XrLabel180.StylePriority.UseBackColor = False
        Me.XrLabel180.StylePriority.UseBorders = False
        Me.XrLabel180.StylePriority.UseFont = False
        Me.XrLabel180.StylePriority.UseTextAlignment = False
        Me.XrLabel180.Text = "Product Description"
        Me.XrLabel180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel179
        '
        Me.XrLabel179.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel179.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel179.CanGrow = False
        Me.XrLabel179.Font = New System.Drawing.Font("Calisto MT", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel179.LocationFloat = New DevExpress.Utils.PointFloat(0.05357027!, 117.6275!)
        Me.XrLabel179.Multiline = True
        Me.XrLabel179.Name = "XrLabel179"
        Me.XrLabel179.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel179.SizeF = New System.Drawing.SizeF(597.4896!, 54.16664!)
        Me.XrLabel179.StylePriority.UseBackColor = False
        Me.XrLabel179.StylePriority.UseBorders = False
        Me.XrLabel179.StylePriority.UseFont = False
        Me.XrLabel179.StylePriority.UseForeColor = False
        Me.XrLabel179.StylePriority.UseTextAlignment = False
        Me.XrLabel179.Text = "            Packing List Cum Delivery Challan"
        Me.XrLabel179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel178
        '
        Me.XrLabel178.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel178.CanGrow = False
        Me.XrLabel178.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel178.LocationFloat = New DevExpress.Utils.PointFloat(0.053949!, 171.7942!)
        Me.XrLabel178.Multiline = True
        Me.XrLabel178.Name = "XrLabel178"
        Me.XrLabel178.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel178.SizeF = New System.Drawing.SizeF(93.6806!, 18.60425!)
        Me.XrLabel178.StylePriority.UseBorders = False
        Me.XrLabel178.StylePriority.UseFont = False
        Me.XrLabel178.StylePriority.UseTextAlignment = False
        Me.XrLabel178.Text = "Invoice No.          : "
        Me.XrLabel178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel177
        '
        Me.XrLabel177.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel177.CanGrow = False
        Me.XrLabel177.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel177.LocationFloat = New DevExpress.Utils.PointFloat(597.5433!, 190.3985!)
        Me.XrLabel177.Multiline = True
        Me.XrLabel177.Name = "XrLabel177"
        Me.XrLabel177.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel177.SizeF = New System.Drawing.SizeF(96.28485!, 18.31355!)
        Me.XrLabel177.StylePriority.UseBorders = False
        Me.XrLabel177.StylePriority.UseFont = False
        Me.XrLabel177.StylePriority.UseTextAlignment = False
        Me.XrLabel177.Text = "Reverse Charge  :"
        Me.XrLabel177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel176
        '
        Me.XrLabel176.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel176.CanGrow = False
        Me.XrLabel176.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[InvoiceNo]")})
        Me.XrLabel176.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel176.LocationFloat = New DevExpress.Utils.PointFloat(93.73454!, 171.7942!)
        Me.XrLabel176.Multiline = True
        Me.XrLabel176.Name = "XrLabel176"
        Me.XrLabel176.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel176.SizeF = New System.Drawing.SizeF(145.424!, 18.60425!)
        Me.XrLabel176.StylePriority.UseBorders = False
        Me.XrLabel176.StylePriority.UseFont = False
        Me.XrLabel176.StylePriority.UseTextAlignment = False
        Me.XrLabel176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel175
        '
        Me.XrLabel175.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel175.CanGrow = False
        Me.XrLabel175.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[GSTReverseCharge]")})
        Me.XrLabel175.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel175.LocationFloat = New DevExpress.Utils.PointFloat(693.8282!, 190.3985!)
        Me.XrLabel175.Multiline = True
        Me.XrLabel175.Name = "XrLabel175"
        Me.XrLabel175.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel175.SizeF = New System.Drawing.SizeF(84.1731!, 18.31361!)
        Me.XrLabel175.StylePriority.UseBorders = False
        Me.XrLabel175.StylePriority.UseFont = False
        Me.XrLabel175.StylePriority.UseTextAlignment = False
        Me.XrLabel175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel174
        '
        Me.XrLabel174.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel174.CanGrow = False
        Me.XrLabel174.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel174.LocationFloat = New DevExpress.Utils.PointFloat(626.9958!, 117.6275!)
        Me.XrLabel174.Multiline = True
        Me.XrLabel174.Name = "XrLabel174"
        Me.XrLabel174.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel174.SizeF = New System.Drawing.SizeF(151.004!, 54.16652!)
        Me.XrLabel174.StylePriority.UseBorders = False
        Me.XrLabel174.StylePriority.UseFont = False
        Me.XrLabel174.StylePriority.UseTextAlignment = False
        Me.XrLabel174.Text = "Original for Receipant" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Duplicate" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Triplicate "
        Me.XrLabel174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel173
        '
        Me.XrLabel173.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel173.CanGrow = False
        Me.XrLabel173.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel173.LocationFloat = New DevExpress.Utils.PointFloat(0.007313371!, 227.0242!)
        Me.XrLabel173.Multiline = True
        Me.XrLabel173.Name = "XrLabel173"
        Me.XrLabel173.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel173.SizeF = New System.Drawing.SizeF(93.72723!, 18.75!)
        Me.XrLabel173.StylePriority.UseBorders = False
        Me.XrLabel173.StylePriority.UseFont = False
        Me.XrLabel173.StylePriority.UseTextAlignment = False
        Me.XrLabel173.Text = "State                     :"
        Me.XrLabel173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel172
        '
        Me.XrLabel172.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel172.CanGrow = False
        Me.XrLabel172.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel172.LocationFloat = New DevExpress.Utils.PointFloat(93.73454!, 227.0242!)
        Me.XrLabel172.Multiline = True
        Me.XrLabel172.Name = "XrLabel172"
        Me.XrLabel172.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel172.SizeF = New System.Drawing.SizeF(145.3773!, 18.75!)
        Me.XrLabel172.StylePriority.UseBorders = False
        Me.XrLabel172.StylePriority.UseFont = False
        Me.XrLabel172.StylePriority.UseTextAlignment = False
        Me.XrLabel172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel171
        '
        Me.XrLabel171.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel171.CanGrow = False
        Me.XrLabel171.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel171.LocationFloat = New DevExpress.Utils.PointFloat(239.1119!, 227.0242!)
        Me.XrLabel171.Multiline = True
        Me.XrLabel171.Name = "XrLabel171"
        Me.XrLabel171.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel171.SizeF = New System.Drawing.SizeF(69.41159!, 18.74997!)
        Me.XrLabel171.StylePriority.UseBorders = False
        Me.XrLabel171.StylePriority.UseFont = False
        Me.XrLabel171.StylePriority.UseTextAlignment = False
        Me.XrLabel171.Text = "State Code :"
        Me.XrLabel171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel170
        '
        Me.XrLabel170.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel170.CanGrow = False
        Me.XrLabel170.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel170.LocationFloat = New DevExpress.Utils.PointFloat(308.5235!, 227.0242!)
        Me.XrLabel170.Multiline = True
        Me.XrLabel170.Name = "XrLabel170"
        Me.XrLabel170.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel170.SizeF = New System.Drawing.SizeF(84.00043!, 18.74997!)
        Me.XrLabel170.StylePriority.UseBorders = False
        Me.XrLabel170.StylePriority.UseFont = False
        Me.XrLabel170.StylePriority.UseTextAlignment = False
        Me.XrLabel170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel169
        '
        Me.XrLabel169.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel169.CanGrow = False
        Me.XrLabel169.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel169.LocationFloat = New DevExpress.Utils.PointFloat(555.4792!, 227.0242!)
        Me.XrLabel169.Multiline = True
        Me.XrLabel169.Name = "XrLabel169"
        Me.XrLabel169.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel169.SizeF = New System.Drawing.SizeF(59.64148!, 18.74994!)
        Me.XrLabel169.StylePriority.UseBorders = False
        Me.XrLabel169.StylePriority.UseFont = False
        Me.XrLabel169.StylePriority.UseTextAlignment = False
        Me.XrLabel169.Text = "LR NO  : "
        Me.XrLabel169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel168
        '
        Me.XrLabel168.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel168.CanGrow = False
        Me.XrLabel168.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TransportationMode]")})
        Me.XrLabel168.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel168.LocationFloat = New DevExpress.Utils.PointFloat(615.1205!, 227.0242!)
        Me.XrLabel168.Multiline = True
        Me.XrLabel168.Name = "XrLabel168"
        Me.XrLabel168.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel168.SizeF = New System.Drawing.SizeF(162.8789!, 18.74991!)
        Me.XrLabel168.StylePriority.UseBorders = False
        Me.XrLabel168.StylePriority.UseFont = False
        Me.XrLabel168.StylePriority.UseTextAlignment = False
        Me.XrLabel168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel167
        '
        Me.XrLabel167.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel167.CanGrow = False
        Me.XrLabel167.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PO]")})
        Me.XrLabel167.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel167.LocationFloat = New DevExpress.Utils.PointFloat(93.73454!, 190.3985!)
        Me.XrLabel167.Multiline = True
        Me.XrLabel167.Name = "XrLabel167"
        Me.XrLabel167.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel167.SizeF = New System.Drawing.SizeF(145.4258!, 18.02052!)
        Me.XrLabel167.StylePriority.UseBorders = False
        Me.XrLabel167.StylePriority.UseFont = False
        Me.XrLabel167.StylePriority.UseTextAlignment = False
        Me.XrLabel167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel166
        '
        Me.XrLabel166.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel166.CanGrow = False
        Me.XrLabel166.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel166.LocationFloat = New DevExpress.Utils.PointFloat(0.04813504!, 190.3985!)
        Me.XrLabel166.Multiline = True
        Me.XrLabel166.Name = "XrLabel166"
        Me.XrLabel166.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel166.SizeF = New System.Drawing.SizeF(93.68641!, 18.02052!)
        Me.XrLabel166.StylePriority.UseBorders = False
        Me.XrLabel166.StylePriority.UseFont = False
        Me.XrLabel166.StylePriority.UseTextAlignment = False
        Me.XrLabel166.Text = "P. O NO                :"
        Me.XrLabel166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel165
        '
        Me.XrLabel165.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel165.CanGrow = False
        Me.XrLabel165.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel165.LocationFloat = New DevExpress.Utils.PointFloat(392.599!, 208.712!)
        Me.XrLabel165.Multiline = True
        Me.XrLabel165.Name = "XrLabel165"
        Me.XrLabel165.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel165.SizeF = New System.Drawing.SizeF(99.88861!, 18.31216!)
        Me.XrLabel165.StylePriority.UseBorders = False
        Me.XrLabel165.StylePriority.UseFont = False
        Me.XrLabel165.StylePriority.UseTextAlignment = False
        Me.XrLabel165.Text = "Transport Name  :"
        Me.XrLabel165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel164
        '
        Me.XrLabel164.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel164.CanGrow = False
        Me.XrLabel164.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SupplyTo]")})
        Me.XrLabel164.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel164.LocationFloat = New DevExpress.Utils.PointFloat(492.4877!, 208.712!)
        Me.XrLabel164.Multiline = True
        Me.XrLabel164.Name = "XrLabel164"
        Me.XrLabel164.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel164.SizeF = New System.Drawing.SizeF(285.5125!, 18.31216!)
        Me.XrLabel164.StylePriority.UseBorders = False
        Me.XrLabel164.StylePriority.UseFont = False
        Me.XrLabel164.StylePriority.UseTextAlignment = False
        Me.XrLabel164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel163
        '
        Me.XrLabel163.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel163.CanGrow = False
        Me.XrLabel163.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel163.LocationFloat = New DevExpress.Utils.PointFloat(392.5967!, 171.794!)
        Me.XrLabel163.Multiline = True
        Me.XrLabel163.Name = "XrLabel163"
        Me.XrLabel163.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel163.SizeF = New System.Drawing.SizeF(81.00015!, 18.60445!)
        Me.XrLabel163.StylePriority.UseBorders = False
        Me.XrLabel163.StylePriority.UseFont = False
        Me.XrLabel163.StylePriority.UseTextAlignment = False
        Me.XrLabel163.Text = "OFFER NO.     :"
        Me.XrLabel163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel162
        '
        Me.XrLabel162.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel162.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel162.CanGrow = False
        Me.XrLabel162.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel162.LocationFloat = New DevExpress.Utils.PointFloat(0.007302999!, 245.7742!)
        Me.XrLabel162.Multiline = True
        Me.XrLabel162.Name = "XrLabel162"
        Me.XrLabel162.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel162.SizeF = New System.Drawing.SizeF(392.5166!, 18.31287!)
        Me.XrLabel162.StylePriority.UseBackColor = False
        Me.XrLabel162.StylePriority.UseBorders = False
        Me.XrLabel162.StylePriority.UseFont = False
        Me.XrLabel162.StylePriority.UseTextAlignment = False
        Me.XrLabel162.Text = "Bill to Party"
        Me.XrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel161
        '
        Me.XrLabel161.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel161.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel161.CanGrow = False
        Me.XrLabel161.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel161.LocationFloat = New DevExpress.Utils.PointFloat(392.524!, 245.7742!)
        Me.XrLabel161.Multiline = True
        Me.XrLabel161.Name = "XrLabel161"
        Me.XrLabel161.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel161.SizeF = New System.Drawing.SizeF(385.4756!, 18.31287!)
        Me.XrLabel161.StylePriority.UseBackColor = False
        Me.XrLabel161.StylePriority.UseBorders = False
        Me.XrLabel161.StylePriority.UseFont = False
        Me.XrLabel161.StylePriority.UseTextAlignment = False
        Me.XrLabel161.Text = "Ship to Party"
        Me.XrLabel161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel160
        '
        Me.XrLabel160.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel160.CanGrow = False
        Me.XrLabel160.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel160.LocationFloat = New DevExpress.Utils.PointFloat(0.007302999!, 264.087!)
        Me.XrLabel160.Multiline = True
        Me.XrLabel160.Name = "XrLabel160"
        Me.XrLabel160.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel160.SizeF = New System.Drawing.SizeF(55.0!, 18.6!)
        Me.XrLabel160.StylePriority.UseBorders = False
        Me.XrLabel160.StylePriority.UseFont = False
        Me.XrLabel160.StylePriority.UseTextAlignment = False
        Me.XrLabel160.Text = "Name     : "
        Me.XrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel159
        '
        Me.XrLabel159.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel159.CanGrow = False
        Me.XrLabel159.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverName]")})
        Me.XrLabel159.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel159.LocationFloat = New DevExpress.Utils.PointFloat(55.0073!, 264.087!)
        Me.XrLabel159.Multiline = True
        Me.XrLabel159.Name = "XrLabel159"
        Me.XrLabel159.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel159.SizeF = New System.Drawing.SizeF(337.5166!, 18.60428!)
        Me.XrLabel159.StylePriority.UseBorders = False
        Me.XrLabel159.StylePriority.UseFont = False
        Me.XrLabel159.StylePriority.UseTextAlignment = False
        Me.XrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel158
        '
        Me.XrLabel158.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel158.CanGrow = False
        Me.XrLabel158.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel158.LocationFloat = New DevExpress.Utils.PointFloat(0.00009155273!, 319.4618!)
        Me.XrLabel158.Multiline = True
        Me.XrLabel158.Name = "XrLabel158"
        Me.XrLabel158.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel158.SizeF = New System.Drawing.SizeF(55.00764!, 18.4584!)
        Me.XrLabel158.StylePriority.UseBorders = False
        Me.XrLabel158.StylePriority.UseFont = False
        Me.XrLabel158.StylePriority.UseTextAlignment = False
        Me.XrLabel158.Text = "GST No  : "
        Me.XrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel157
        '
        Me.XrLabel157.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel157.CanGrow = False
        Me.XrLabel157.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverGSTIN]")})
        Me.XrLabel157.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel157.LocationFloat = New DevExpress.Utils.PointFloat(55.00773!, 319.4622!)
        Me.XrLabel157.Multiline = True
        Me.XrLabel157.Name = "XrLabel157"
        Me.XrLabel157.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel157.SizeF = New System.Drawing.SizeF(337.5913!, 18.45865!)
        Me.XrLabel157.StylePriority.UseBorders = False
        Me.XrLabel157.StylePriority.UseFont = False
        Me.XrLabel157.StylePriority.UseTextAlignment = False
        Me.XrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel156
        '
        Me.XrLabel156.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel156.CanGrow = False
        Me.XrLabel156.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel156.LocationFloat = New DevExpress.Utils.PointFloat(0.00009155273!, 337.9225!)
        Me.XrLabel156.Multiline = True
        Me.XrLabel156.Name = "XrLabel156"
        Me.XrLabel156.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel156.SizeF = New System.Drawing.SizeF(55.00764!, 18.60449!)
        Me.XrLabel156.StylePriority.UseBorders = False
        Me.XrLabel156.StylePriority.UseFont = False
        Me.XrLabel156.StylePriority.UseTextAlignment = False
        Me.XrLabel156.Text = "State      : "
        Me.XrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel155
        '
        Me.XrLabel155.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel155.CanGrow = False
        Me.XrLabel155.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverState]")})
        Me.XrLabel155.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel155.LocationFloat = New DevExpress.Utils.PointFloat(55.00774!, 337.9225!)
        Me.XrLabel155.Multiline = True
        Me.XrLabel155.Name = "XrLabel155"
        Me.XrLabel155.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel155.SizeF = New System.Drawing.SizeF(195.7383!, 18.60446!)
        Me.XrLabel155.StylePriority.UseBorders = False
        Me.XrLabel155.StylePriority.UseFont = False
        Me.XrLabel155.StylePriority.UseTextAlignment = False
        Me.XrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel154
        '
        Me.XrLabel154.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel154.CanGrow = False
        Me.XrLabel154.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel154.LocationFloat = New DevExpress.Utils.PointFloat(250.7461!, 337.9225!)
        Me.XrLabel154.Multiline = True
        Me.XrLabel154.Name = "XrLabel154"
        Me.XrLabel154.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel154.SizeF = New System.Drawing.SizeF(68.72404!, 18.60458!)
        Me.XrLabel154.StylePriority.UseBorders = False
        Me.XrLabel154.StylePriority.UseFont = False
        Me.XrLabel154.StylePriority.UseTextAlignment = False
        Me.XrLabel154.Text = "State Code :"
        Me.XrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel153
        '
        Me.XrLabel153.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel153.CanGrow = False
        Me.XrLabel153.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverStateCode]")})
        Me.XrLabel153.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel153.LocationFloat = New DevExpress.Utils.PointFloat(319.4701!, 337.9225!)
        Me.XrLabel153.Multiline = True
        Me.XrLabel153.Name = "XrLabel153"
        Me.XrLabel153.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel153.SizeF = New System.Drawing.SizeF(73.05371!, 18.60278!)
        Me.XrLabel153.StylePriority.UseBorders = False
        Me.XrLabel153.StylePriority.UseFont = False
        Me.XrLabel153.StylePriority.UseTextAlignment = False
        Me.XrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel152
        '
        Me.XrLabel152.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel152.CanGrow = False
        Me.XrLabel152.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel152.LocationFloat = New DevExpress.Utils.PointFloat(392.5501!, 264.087!)
        Me.XrLabel152.Multiline = True
        Me.XrLabel152.Name = "XrLabel152"
        Me.XrLabel152.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel152.SizeF = New System.Drawing.SizeF(63.36353!, 18.60428!)
        Me.XrLabel152.StylePriority.UseBorders = False
        Me.XrLabel152.StylePriority.UseFont = False
        Me.XrLabel152.StylePriority.UseTextAlignment = False
        Me.XrLabel152.Text = "Name      : "
        Me.XrLabel152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel151
        '
        Me.XrLabel151.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel151.CanGrow = False
        Me.XrLabel151.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeName]")})
        Me.XrLabel151.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel151.LocationFloat = New DevExpress.Utils.PointFloat(455.9136!, 264.087!)
        Me.XrLabel151.Multiline = True
        Me.XrLabel151.Name = "XrLabel151"
        Me.XrLabel151.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel151.SizeF = New System.Drawing.SizeF(322.0862!, 18.60428!)
        Me.XrLabel151.StylePriority.UseBorders = False
        Me.XrLabel151.StylePriority.UseFont = False
        Me.XrLabel151.StylePriority.UseTextAlignment = False
        Me.XrLabel151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel150
        '
        Me.XrLabel150.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel150.CanGrow = False
        Me.XrLabel150.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel150.LocationFloat = New DevExpress.Utils.PointFloat(392.605!, 319.4618!)
        Me.XrLabel150.Multiline = True
        Me.XrLabel150.Name = "XrLabel150"
        Me.XrLabel150.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel150.SizeF = New System.Drawing.SizeF(63.30875!, 18.45837!)
        Me.XrLabel150.StylePriority.UseBorders = False
        Me.XrLabel150.StylePriority.UseFont = False
        Me.XrLabel150.StylePriority.UseTextAlignment = False
        Me.XrLabel150.Text = "GST No     : "
        Me.XrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel149
        '
        Me.XrLabel149.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel149.CanGrow = False
        Me.XrLabel149.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeGSTIN]")})
        Me.XrLabel149.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel149.LocationFloat = New DevExpress.Utils.PointFloat(455.9146!, 319.4625!)
        Me.XrLabel149.Multiline = True
        Me.XrLabel149.Name = "XrLabel149"
        Me.XrLabel149.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel149.SizeF = New System.Drawing.SizeF(322.0744!, 18.60651!)
        Me.XrLabel149.StylePriority.UseBorders = False
        Me.XrLabel149.StylePriority.UseFont = False
        Me.XrLabel149.StylePriority.UseTextAlignment = False
        Me.XrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel148
        '
        Me.XrLabel148.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel148.CanGrow = False
        Me.XrLabel148.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel148.LocationFloat = New DevExpress.Utils.PointFloat(392.5501!, 338.0687!)
        Me.XrLabel148.Multiline = True
        Me.XrLabel148.Name = "XrLabel148"
        Me.XrLabel148.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel148.SizeF = New System.Drawing.SizeF(63.36362!, 18.60458!)
        Me.XrLabel148.StylePriority.UseBorders = False
        Me.XrLabel148.StylePriority.UseFont = False
        Me.XrLabel148.StylePriority.UseTextAlignment = False
        Me.XrLabel148.Text = "State         : "
        Me.XrLabel148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel147
        '
        Me.XrLabel147.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel147.CanGrow = False
        Me.XrLabel147.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel147.LocationFloat = New DevExpress.Utils.PointFloat(626.9959!, 338.069!)
        Me.XrLabel147.Multiline = True
        Me.XrLabel147.Name = "XrLabel147"
        Me.XrLabel147.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel147.SizeF = New System.Drawing.SizeF(77.20245!, 18.60428!)
        Me.XrLabel147.StylePriority.UseBorders = False
        Me.XrLabel147.StylePriority.UseFont = False
        Me.XrLabel147.StylePriority.UseTextAlignment = False
        Me.XrLabel147.Text = "State Code   :"
        Me.XrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel146
        '
        Me.XrLabel146.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel146.CanGrow = False
        Me.XrLabel146.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeStateCode]")})
        Me.XrLabel146.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel146.LocationFloat = New DevExpress.Utils.PointFloat(704.1983!, 338.069!)
        Me.XrLabel146.Multiline = True
        Me.XrLabel146.Name = "XrLabel146"
        Me.XrLabel146.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel146.SizeF = New System.Drawing.SizeF(73.80145!, 18.60428!)
        Me.XrLabel146.StylePriority.UseBorders = False
        Me.XrLabel146.StylePriority.UseFont = False
        Me.XrLabel146.StylePriority.UseTextAlignment = False
        Me.XrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel145
        '
        Me.XrLabel145.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel145.CanGrow = False
        Me.XrLabel145.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel145.LocationFloat = New DevExpress.Utils.PointFloat(597.5432!, 117.6273!)
        Me.XrLabel145.Multiline = True
        Me.XrLabel145.Name = "XrLabel145"
        Me.XrLabel145.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel145.SizeF = New System.Drawing.SizeF(29.45264!, 18.45847!)
        Me.XrLabel145.StylePriority.UseBorders = False
        Me.XrLabel145.StylePriority.UseFont = False
        Me.XrLabel145.StylePriority.UseTextAlignment = False
        Me.XrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel144
        '
        Me.XrLabel144.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel144.CanGrow = False
        Me.XrLabel144.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel144.LocationFloat = New DevExpress.Utils.PointFloat(597.5433!, 136.0858!)
        Me.XrLabel144.Multiline = True
        Me.XrLabel144.Name = "XrLabel144"
        Me.XrLabel144.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel144.SizeF = New System.Drawing.SizeF(29.45264!, 18.45847!)
        Me.XrLabel144.StylePriority.UseBorders = False
        Me.XrLabel144.StylePriority.UseFont = False
        Me.XrLabel144.StylePriority.UseTextAlignment = False
        Me.XrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel143
        '
        Me.XrLabel143.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel143.CanGrow = False
        Me.XrLabel143.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel143.LocationFloat = New DevExpress.Utils.PointFloat(597.5433!, 154.5442!)
        Me.XrLabel143.Multiline = True
        Me.XrLabel143.Name = "XrLabel143"
        Me.XrLabel143.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel143.SizeF = New System.Drawing.SizeF(29.45264!, 17.24971!)
        Me.XrLabel143.StylePriority.UseBorders = False
        Me.XrLabel143.StylePriority.UseFont = False
        Me.XrLabel143.StylePriority.UseTextAlignment = False
        Me.XrLabel143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel141
        '
        Me.XrLabel141.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel141.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel141.CanGrow = False
        Me.XrLabel141.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel141.LocationFloat = New DevExpress.Utils.PointFloat(626.9958!, 356.6731!)
        Me.XrLabel141.Multiline = True
        Me.XrLabel141.Name = "XrLabel141"
        Me.XrLabel141.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel141.SizeF = New System.Drawing.SizeF(77.20251!, 40.29199!)
        Me.XrLabel141.StylePriority.UseBackColor = False
        Me.XrLabel141.StylePriority.UseBorders = False
        Me.XrLabel141.StylePriority.UseFont = False
        Me.XrLabel141.StylePriority.UseTextAlignment = False
        Me.XrLabel141.Text = "UOM"
        Me.XrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel138
        '
        Me.XrLabel138.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel138.CanGrow = False
        Me.XrLabel138.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeState]")})
        Me.XrLabel138.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel138.LocationFloat = New DevExpress.Utils.PointFloat(455.9137!, 338.0687!)
        Me.XrLabel138.Multiline = True
        Me.XrLabel138.Name = "XrLabel138"
        Me.XrLabel138.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel138.SizeF = New System.Drawing.SizeF(171.0822!, 18.60452!)
        Me.XrLabel138.StylePriority.UseBorders = False
        Me.XrLabel138.StylePriority.UseFont = False
        Me.XrLabel138.StylePriority.UseTextAlignment = False
        Me.XrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel137
        '
        Me.XrLabel137.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel137.CanGrow = False
        Me.XrLabel137.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OfferNo]")})
        Me.XrLabel137.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel137.LocationFloat = New DevExpress.Utils.PointFloat(473.5969!, 171.7943!)
        Me.XrLabel137.Multiline = True
        Me.XrLabel137.Name = "XrLabel137"
        Me.XrLabel137.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel137.SizeF = New System.Drawing.SizeF(123.9464!, 18.60414!)
        Me.XrLabel137.StylePriority.UseBorders = False
        Me.XrLabel137.StylePriority.UseFont = False
        Me.XrLabel137.StylePriority.UseTextAlignment = False
        Me.XrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel136
        '
        Me.XrLabel136.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel136.CanGrow = False
        Me.XrLabel136.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel136.LocationFloat = New DevExpress.Utils.PointFloat(597.5433!, 171.7943!)
        Me.XrLabel136.Multiline = True
        Me.XrLabel136.Name = "XrLabel136"
        Me.XrLabel136.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel136.SizeF = New System.Drawing.SizeF(96.28485!, 18.60414!)
        Me.XrLabel136.StylePriority.UseBorders = False
        Me.XrLabel136.StylePriority.UseFont = False
        Me.XrLabel136.StylePriority.UseTextAlignment = False
        Me.XrLabel136.Text = "DATE                    :"
        Me.XrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel135
        '
        Me.XrLabel135.Borders = CType((DevExpress.XtraPrinting.BorderSide.Right Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel135.CanGrow = False
        Me.XrLabel135.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OfferDate]")})
        Me.XrLabel135.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel135.LocationFloat = New DevExpress.Utils.PointFloat(693.8282!, 171.794!)
        Me.XrLabel135.Multiline = True
        Me.XrLabel135.Name = "XrLabel135"
        Me.XrLabel135.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel135.SizeF = New System.Drawing.SizeF(84.17139!, 18.60445!)
        Me.XrLabel135.StylePriority.UseBorders = False
        Me.XrLabel135.StylePriority.UseFont = False
        Me.XrLabel135.StylePriority.UseTextAlignment = False
        Me.XrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel135.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel134
        '
        Me.XrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel134.CanGrow = False
        Me.XrLabel134.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel134.LocationFloat = New DevExpress.Utils.PointFloat(239.1585!, 171.7942!)
        Me.XrLabel134.Multiline = True
        Me.XrLabel134.Name = "XrLabel134"
        Me.XrLabel134.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel134.SizeF = New System.Drawing.SizeF(80.35829!, 18.60425!)
        Me.XrLabel134.StylePriority.UseBorders = False
        Me.XrLabel134.StylePriority.UseFont = False
        Me.XrLabel134.StylePriority.UseTextAlignment = False
        Me.XrLabel134.Text = "Invoice Date :"
        Me.XrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel133
        '
        Me.XrLabel133.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel133.CanGrow = False
        Me.XrLabel133.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[InvoiceDate]")})
        Me.XrLabel133.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel133.LocationFloat = New DevExpress.Utils.PointFloat(319.5167!, 171.794!)
        Me.XrLabel133.Multiline = True
        Me.XrLabel133.Name = "XrLabel133"
        Me.XrLabel133.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel133.SizeF = New System.Drawing.SizeF(73.05368!, 18.60446!)
        Me.XrLabel133.StylePriority.UseBorders = False
        Me.XrLabel133.StylePriority.UseFont = False
        Me.XrLabel133.StylePriority.UseTextAlignment = False
        Me.XrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel133.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel132
        '
        Me.XrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel132.CanGrow = False
        Me.XrLabel132.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PODate]")})
        Me.XrLabel132.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel132.LocationFloat = New DevExpress.Utils.PointFloat(319.5167!, 190.3984!)
        Me.XrLabel132.Multiline = True
        Me.XrLabel132.Name = "XrLabel132"
        Me.XrLabel132.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel132.SizeF = New System.Drawing.SizeF(73.08197!, 18.02057!)
        Me.XrLabel132.StylePriority.UseBorders = False
        Me.XrLabel132.StylePriority.UseFont = False
        Me.XrLabel132.StylePriority.UseTextAlignment = False
        Me.XrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel132.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel131
        '
        Me.XrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel131.CanGrow = False
        Me.XrLabel131.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel131.LocationFloat = New DevExpress.Utils.PointFloat(239.1605!, 190.3985!)
        Me.XrLabel131.Multiline = True
        Me.XrLabel131.Name = "XrLabel131"
        Me.XrLabel131.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel131.SizeF = New System.Drawing.SizeF(80.30963!, 18.02052!)
        Me.XrLabel131.StylePriority.UseBorders = False
        Me.XrLabel131.StylePriority.UseFont = False
        Me.XrLabel131.StylePriority.UseTextAlignment = False
        Me.XrLabel131.Text = "DATE              :"
        Me.XrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel130
        '
        Me.XrLabel130.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel130.CanGrow = False
        Me.XrLabel130.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel130.LocationFloat = New DevExpress.Utils.PointFloat(0.00009155273!, 282.6913!)
        Me.XrLabel130.Multiline = True
        Me.XrLabel130.Name = "XrLabel130"
        Me.XrLabel130.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel130.SizeF = New System.Drawing.SizeF(55.00764!, 18.60428!)
        Me.XrLabel130.StylePriority.UseBorders = False
        Me.XrLabel130.StylePriority.UseFont = False
        Me.XrLabel130.StylePriority.UseTextAlignment = False
        Me.XrLabel130.Text = "Address :"
        Me.XrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel129
        '
        Me.XrLabel129.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrLabel129.CanGrow = False
        Me.XrLabel129.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel129.LocationFloat = New DevExpress.Utils.PointFloat(392.605!, 282.6913!)
        Me.XrLabel129.Multiline = True
        Me.XrLabel129.Name = "XrLabel129"
        Me.XrLabel129.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel129.SizeF = New System.Drawing.SizeF(63.30865!, 18.60428!)
        Me.XrLabel129.StylePriority.UseBorders = False
        Me.XrLabel129.StylePriority.UseFont = False
        Me.XrLabel129.StylePriority.UseTextAlignment = False
        Me.XrLabel129.Text = "Address  :"
        Me.XrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel125
        '
        Me.XrLabel125.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel125.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel125.LocationFloat = New DevExpress.Utils.PointFloat(119.5175!, 49.04784!)
        Me.XrLabel125.Multiline = True
        Me.XrLabel125.Name = "XrLabel125"
        Me.XrLabel125.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel125.SizeF = New System.Drawing.SizeF(658.48!, 30.0!)
        Me.XrLabel125.StylePriority.UseBorders = False
        Me.XrLabel125.StylePriority.UseFont = False
        Me.XrLabel125.StylePriority.UseTextAlignment = False
        Me.XrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel90
        '
        Me.XrLabel90.BorderColor = System.Drawing.Color.Black
        Me.XrLabel90.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel90.CanGrow = False
        Me.XrLabel90.Font = New System.Drawing.Font("Cooper Black", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel90.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.XrLabel90.LocationFloat = New DevExpress.Utils.PointFloat(119.5175!, 0!)
        Me.XrLabel90.Multiline = True
        Me.XrLabel90.Name = "XrLabel90"
        Me.XrLabel90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel90.SizeF = New System.Drawing.SizeF(658.48!, 34.04783!)
        Me.XrLabel90.StylePriority.UseBorderColor = False
        Me.XrLabel90.StylePriority.UseBorders = False
        Me.XrLabel90.StylePriority.UseFont = False
        Me.XrLabel90.StylePriority.UseForeColor = False
        Me.XrLabel90.StylePriority.UseTextAlignment = False
        Me.XrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel89
        '
        Me.XrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel89.CanGrow = False
        Me.XrLabel89.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel89.LocationFloat = New DevExpress.Utils.PointFloat(119.5175!, 79.04784!)
        Me.XrLabel89.Multiline = True
        Me.XrLabel89.Name = "XrLabel89"
        Me.XrLabel89.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel89.SizeF = New System.Drawing.SizeF(658.4824!, 19.89393!)
        Me.XrLabel89.StylePriority.UseBorders = False
        Me.XrLabel89.StylePriority.UseFont = False
        Me.XrLabel89.StylePriority.UseTextAlignment = False
        Me.XrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel42
        '
        Me.XrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel42.CanGrow = False
        Me.XrLabel42.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel42.LocationFloat = New DevExpress.Utils.PointFloat(119.5175!, 98.94173!)
        Me.XrLabel42.Multiline = True
        Me.XrLabel42.Name = "XrLabel42"
        Me.XrLabel42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel42.SizeF = New System.Drawing.SizeF(658.4821!, 18.6858!)
        Me.XrLabel42.StylePriority.UseBorders = False
        Me.XrLabel42.StylePriority.UseFont = False
        Me.XrLabel42.StylePriority.UseTextAlignment = False
        Me.XrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel41
        '
        Me.XrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel41.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ConsigneeAddress]")})
        Me.XrLabel41.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(455.9137!, 282.6913!)
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.SizeF = New System.Drawing.SizeF(321.0189!, 36.77048!)
        Me.XrLabel41.StylePriority.UseBorders = False
        Me.XrLabel41.StylePriority.UseFont = False
        '
        'XrLabel40
        '
        Me.XrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel40.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReceiverAddress]")})
        Me.XrLabel40.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(55.00772!, 282.6913!)
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.SizeF = New System.Drawing.SizeF(337.5973!, 36.77045!)
        Me.XrLabel40.StylePriority.UseBorders = False
        Me.XrLabel40.StylePriority.UseFont = False
        '
        'XrLabel5
        '
        Me.XrLabel5.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel5.CanGrow = False
        Me.XrLabel5.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(392.599!, 227.0242!)
        Me.XrLabel5.Multiline = True
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(78.66141!, 18.74997!)
        Me.XrLabel5.StylePriority.UseBorders = False
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Vehical No.   :"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[JobNoPrefix]")})
        Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(473.5988!, 190.3984!)
        Me.XrLabel3.Multiline = True
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(15.53128!, 18.31357!)
        Me.XrLabel3.StylePriority.UseBorders = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(392.5986!, 190.3984!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(81.00015!, 18.31357!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Job Ref. No    :"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel6.CanGrow = False
        Me.XrLabel6.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ChallanNo]")})
        Me.XrLabel6.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(93.68647!, 208.419!)
        Me.XrLabel6.Multiline = True
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(145.4258!, 18.60515!)
        Me.XrLabel6.StylePriority.UseBorders = False
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.00009155273!, 208.4191!)
        Me.XrLabel4.Multiline = True
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(93.68641!, 18.6051!)
        Me.XrLabel4.StylePriority.UseBorders = False
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "Challan NO          :"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel8.CanGrow = False
        Me.XrLabel8.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ChallanDate]")})
        Me.XrLabel8.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(319.5633!, 209.0036!)
        Me.XrLabel8.Multiline = True
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(72.96048!, 18.02051!)
        Me.XrLabel8.StylePriority.UseBorders = False
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel8.TextFormatString = "{0:dd-MM-yyyy}"
        '
        'XrLabel7
        '
        Me.XrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrLabel7.CanGrow = False
        Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(239.2071!, 208.419!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(80.30962!, 18.60516!)
        Me.XrLabel7.StylePriority.UseBorders = False
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "DATE              :"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel37
        '
        Me.XrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.Right
        Me.XrLabel37.CanGrow = False
        Me.XrLabel37.Font = New System.Drawing.Font("Calibri", 11.0!)
        Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(119.5174!, 34.04783!)
        Me.XrLabel37.Multiline = True
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.SizeF = New System.Drawing.SizeF(658.4715!, 15.0!)
        Me.XrLabel37.StylePriority.UseBorders = False
        Me.XrLabel37.StylePriority.UseFont = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable4, Me.XrLabel124, Me.XrLabel116, Me.XrLabel115, Me.XrLabel114, Me.XrLabel110, Me.XrLabel109, Me.XrLabel108, Me.XrLabel107, Me.XrLabel106, Me.XrLabel94, Me.XrLabel38, Me.XrLabel35})
        Me.GroupFooter2.HeightF = 192.2315!
        Me.GroupFooter2.Level = 1
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'XrTable4
        '
        Me.XrTable4.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable4.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable4.LocationFloat = New DevExpress.Utils.PointFloat(0.0005187988!, 0!)
        Me.XrTable4.Name = "XrTable4"
        Me.XrTable4.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
        Me.XrTable4.SizeF = New System.Drawing.SizeF(778.0009!, 5.0!)
        Me.XrTable4.StylePriority.UseBorders = False
        Me.XrTable4.StylePriority.UseFont = False
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6, Me.XrTableCell18, Me.XrTableCell19, Me.XrTableCell20})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.23688934326172R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell6.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell6.StylePriority.UseBorders = False
        Me.XrTableCell6.StylePriority.UseFont = False
        Me.XrTableCell6.StylePriority.UsePadding = False
        Me.XrTableCell6.Weight = 0.959191052716807R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell18.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell18.StylePriority.UseBorders = False
        Me.XrTableCell18.StylePriority.UseFont = False
        Me.XrTableCell18.StylePriority.UsePadding = False
        Me.XrTableCell18.Weight = 20.938626850391159R
        '
        'XrTableCell19
        '
        Me.XrTableCell19.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell19.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell19.Name = "XrTableCell19"
        Me.XrTableCell19.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell19.StylePriority.UseBorders = False
        Me.XrTableCell19.StylePriority.UseFont = False
        Me.XrTableCell19.StylePriority.UsePadding = False
        Me.XrTableCell19.Weight = 2.7006070699057796R
        '
        'XrTableCell20
        '
        Me.XrTableCell20.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell20.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell20.Name = "XrTableCell20"
        Me.XrTableCell20.Padding = New DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100.0!)
        Me.XrTableCell20.StylePriority.UseBorders = False
        Me.XrTableCell20.StylePriority.UseFont = False
        Me.XrTableCell20.StylePriority.UsePadding = False
        Me.XrTableCell20.Weight = 2.6166402124349428R
        '
        'XrLabel124
        '
        Me.XrLabel124.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel124.CanGrow = False
        Me.XrLabel124.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel124.LocationFloat = New DevExpress.Utils.PointFloat(0!, 52.9793!)
        Me.XrLabel124.Multiline = True
        Me.XrLabel124.Name = "XrLabel124"
        Me.XrLabel124.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel124.SizeF = New System.Drawing.SizeF(93.72329!, 19.99999!)
        Me.XrLabel124.StylePriority.UseBorders = False
        Me.XrLabel124.StylePriority.UseFont = False
        Me.XrLabel124.StylePriority.UseTextAlignment = False
        Me.XrLabel124.Text = "PAN No :"
        Me.XrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel116
        '
        Me.XrLabel116.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel116.CanGrow = False
        Me.XrLabel116.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel116.LocationFloat = New DevExpress.Utils.PointFloat(93.72326!, 52.97929!)
        Me.XrLabel116.Multiline = True
        Me.XrLabel116.Name = "XrLabel116"
        Me.XrLabel116.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel116.SizeF = New System.Drawing.SizeF(416.2355!, 20.00001!)
        Me.XrLabel116.StylePriority.UseBorders = False
        Me.XrLabel116.StylePriority.UseFont = False
        Me.XrLabel116.StylePriority.UseTextAlignment = False
        Me.XrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel115
        '
        Me.XrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel115.CanGrow = False
        Me.XrLabel115.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel115.LocationFloat = New DevExpress.Utils.PointFloat(93.72325!, 32.97932!)
        Me.XrLabel115.Multiline = True
        Me.XrLabel115.Name = "XrLabel115"
        Me.XrLabel115.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel115.SizeF = New System.Drawing.SizeF(416.2343!, 19.99997!)
        Me.XrLabel115.StylePriority.UseBorders = False
        Me.XrLabel115.StylePriority.UseFont = False
        Me.XrLabel115.StylePriority.UseTextAlignment = False
        Me.XrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel114
        '
        Me.XrLabel114.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel114.CanGrow = False
        Me.XrLabel114.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel114.LocationFloat = New DevExpress.Utils.PointFloat(0!, 32.97933!)
        Me.XrLabel114.Multiline = True
        Me.XrLabel114.Name = "XrLabel114"
        Me.XrLabel114.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel114.SizeF = New System.Drawing.SizeF(93.72326!, 19.99997!)
        Me.XrLabel114.StylePriority.UseBorders = False
        Me.XrLabel114.StylePriority.UseFont = False
        Me.XrLabel114.StylePriority.UseTextAlignment = False
        Me.XrLabel114.Text = "GSTIN    : "
        Me.XrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel110
        '
        Me.XrLabel110.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel110.CanGrow = False
        Me.XrLabel110.Font = New System.Drawing.Font("Calibri", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.XrLabel110.LocationFloat = New DevExpress.Utils.PointFloat(0!, 72.97927!)
        Me.XrLabel110.Multiline = True
        Me.XrLabel110.Name = "XrLabel110"
        Me.XrLabel110.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel110.SizeF = New System.Drawing.SizeF(509.9576!, 18.2205!)
        Me.XrLabel110.StylePriority.UseBorders = False
        Me.XrLabel110.StylePriority.UseFont = False
        Me.XrLabel110.StylePriority.UseTextAlignment = False
        Me.XrLabel110.Text = "TERMS & CONDITIONS :" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel109
        '
        Me.XrLabel109.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel109.CanGrow = False
        Me.XrLabel109.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel109.LocationFloat = New DevExpress.Utils.PointFloat(509.9587!, 62.76236!)
        Me.XrLabel109.Multiline = True
        Me.XrLabel109.Name = "XrLabel109"
        Me.XrLabel109.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel109.SizeF = New System.Drawing.SizeF(268.019!, 16.91846!)
        Me.XrLabel109.StylePriority.UseBorders = False
        Me.XrLabel109.StylePriority.UseFont = False
        Me.XrLabel109.StylePriority.UseTextAlignment = False
        Me.XrLabel109.Text = "For AKSH ENGINEERING SYSTEMS PVT LTD,"
        Me.XrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel108
        '
        Me.XrLabel108.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel108.CanGrow = False
        Me.XrLabel108.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel108.LocationFloat = New DevExpress.Utils.PointFloat(0!, 91.19977!)
        Me.XrLabel108.Multiline = True
        Me.XrLabel108.Name = "XrLabel108"
        Me.XrLabel108.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel108.SizeF = New System.Drawing.SizeF(509.9575!, 68.96805!)
        Me.XrLabel108.StylePriority.UseBorders = False
        Me.XrLabel108.StylePriority.UseFont = False
        Me.XrLabel108.StylePriority.UseTextAlignment = False
        Me.XrLabel108.Text = resources.GetString("XrLabel108.Text")
        Me.XrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel107
        '
        Me.XrLabel107.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel107.CanGrow = False
        Me.XrLabel107.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.XrLabel107.LocationFloat = New DevExpress.Utils.PointFloat(509.9588!, 79.68081!)
        Me.XrLabel107.Multiline = True
        Me.XrLabel107.Name = "XrLabel107"
        Me.XrLabel107.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel107.SizeF = New System.Drawing.SizeF(268.0308!, 57.20432!)
        Me.XrLabel107.StylePriority.UseBorders = False
        Me.XrLabel107.StylePriority.UseFont = False
        Me.XrLabel107.StylePriority.UseTextAlignment = False
        Me.XrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel106
        '
        Me.XrLabel106.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel106.CanGrow = False
        Me.XrLabel106.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.XrLabel106.LocationFloat = New DevExpress.Utils.PointFloat(509.9575!, 136.8851!)
        Me.XrLabel106.Multiline = True
        Me.XrLabel106.Name = "XrLabel106"
        Me.XrLabel106.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel106.SizeF = New System.Drawing.SizeF(268.0321!, 23.28271!)
        Me.XrLabel106.StylePriority.UseBorders = False
        Me.XrLabel106.StylePriority.UseFont = False
        Me.XrLabel106.StylePriority.UseTextAlignment = False
        Me.XrLabel106.Text = "Authorised Signatory/ DIRECTOR"
        Me.XrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel94
        '
        Me.XrLabel94.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel94.CanGrow = False
        Me.XrLabel94.Font = New System.Drawing.Font("Calibri", 8.0!, System.Drawing.FontStyle.Underline)
        Me.XrLabel94.LocationFloat = New DevExpress.Utils.PointFloat(509.9588!, 32.97932!)
        Me.XrLabel94.Multiline = True
        Me.XrLabel94.Name = "XrLabel94"
        Me.XrLabel94.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel94.SizeF = New System.Drawing.SizeF(268.0302!, 29.78304!)
        Me.XrLabel94.StylePriority.UseBorders = False
        Me.XrLabel94.StylePriority.UseFont = False
        Me.XrLabel94.StylePriority.UseTextAlignment = False
        Me.XrLabel94.Text = "Certified that the particulars given above are true"
        Me.XrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel38
        '
        Me.XrLabel38.BackColor = System.Drawing.Color.LightGray
        Me.XrLabel38.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel38.CanGrow = False
        Me.XrLabel38.Font = New System.Drawing.Font("Calisto MT", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(0.0001829863!, 4.999992!)
        Me.XrLabel38.Multiline = True
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.SizeF = New System.Drawing.SizeF(703.1989!, 27.97932!)
        Me.XrLabel38.StylePriority.UseBackColor = False
        Me.XrLabel38.StylePriority.UseBorders = False
        Me.XrLabel38.StylePriority.UseFont = False
        Me.XrLabel38.StylePriority.UseTextAlignment = False
        Me.XrLabel38.Text = "Total"
        Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel35
        '
        Me.XrLabel35.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrLabel35.CanGrow = False
        Me.XrLabel35.ExpressionBindings.AddRange(New DevExpress.XtraReports.UI.ExpressionBinding() {New DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([Qty])")})
        Me.XrLabel35.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(703.199!, 4.999992!)
        Me.XrLabel35.Multiline = True
        Me.XrLabel35.Name = "XrLabel35"
        Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel35.SizeF = New System.Drawing.SizeF(74.80225!, 27.9793!)
        Me.XrLabel35.StylePriority.UseBorders = False
        Me.XrLabel35.StylePriority.UseFont = False
        Me.XrLabel35.StylePriority.UseTextAlignment = False
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Page
        Me.XrLabel35.Summary = XrSummary2
        Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1})
        Me.PageFooter.Expanded = False
        Me.PageFooter.HeightF = 23.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(679.9999!, 0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        '
        'XrCrossBandLine15
        '
        Me.XrCrossBandLine15.EndBand = Me.GroupHeader1
        Me.XrCrossBandLine15.EndPointFloat = New DevExpress.Utils.PointFloat(0.00009139378!, 319.4618!)
        Me.XrCrossBandLine15.LocationFloat = New DevExpress.Utils.PointFloat(0.00009139378!, 301.2956!)
        Me.XrCrossBandLine15.Name = "XrCrossBandLine15"
        Me.XrCrossBandLine15.StartBand = Me.GroupHeader1
        Me.XrCrossBandLine15.StartPointFloat = New DevExpress.Utils.PointFloat(0.00009139378!, 301.2956!)
        Me.XrCrossBandLine15.WidthF = 1.041667!
        '
        'XrCrossBandLine16
        '
        Me.XrCrossBandLine16.EndBand = Me.GroupHeader1
        Me.XrCrossBandLine16.EndPointFloat = New DevExpress.Utils.PointFloat(392.6049!, 319.4618!)
        Me.XrCrossBandLine16.LocationFloat = New DevExpress.Utils.PointFloat(392.6049!, 301.2956!)
        Me.XrCrossBandLine16.Name = "XrCrossBandLine16"
        Me.XrCrossBandLine16.StartBand = Me.GroupHeader1
        Me.XrCrossBandLine16.StartPointFloat = New DevExpress.Utils.PointFloat(392.6049!, 301.2956!)
        Me.XrCrossBandLine16.WidthF = 1.0!
        '
        'XrCrossBandLine17
        '
        Me.XrCrossBandLine17.EndBand = Me.GroupHeader1
        Me.XrCrossBandLine17.EndPointFloat = New DevExpress.Utils.PointFloat(776.9326!, 319.4618!)
        Me.XrCrossBandLine17.LocationFloat = New DevExpress.Utils.PointFloat(776.9326!, 282.6913!)
        Me.XrCrossBandLine17.Name = "XrCrossBandLine17"
        Me.XrCrossBandLine17.StartBand = Me.GroupHeader1
        Me.XrCrossBandLine17.StartPointFloat = New DevExpress.Utils.PointFloat(776.9326!, 282.6913!)
        Me.XrCrossBandLine17.WidthF = 1.0!
        '
        'XR_Invoice_ChallanDynamicOld
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.GroupHeader1, Me.GroupFooter2, Me.PageFooter})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.InvoiceNew1, Me.InvoiceNew2, Me.DS_PackingList1})
        Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandLine1, Me.XrCrossBandLine15, Me.XrCrossBandLine16, Me.XrCrossBandLine17})
        Me.DataAdapter = Me.DataTable1TableAdapter2
        Me.DataMember = "DataTable1"
        Me.DataSource = Me.DS_PackingList1
        Me.FilterString = "[InvoiceID] = ?Invoice"
        Me.Font = New System.Drawing.Font("Times New Roman", 14.0!)
        Me.Margins = New System.Drawing.Printing.Margins(23, 24, 10, 311)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Invoice})
        Me.Version = "17.2"
        CType(Me.InvoiceNew1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvoiceNew2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PackingList1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents DirectorySearcher1 As DirectoryServices.DirectorySearcher

    Friend WithEvents XrCrossBandLine1 As DevExpress.XtraReports.UI.XRCrossBandLine

    Friend WithEvents Invoice As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents DT_InvoiceTableAdapter2 As InvoiceNewTableAdapters.DT_InvoiceTableAdapter
    Friend WithEvents DT_InvoiceTableAdapter3 As InvoiceNewTableAdapters.DT_InvoiceTableAdapter
    Friend WithEvents ClientPOTableAdapter As DS_ClientPOTableAdapters.ClientPOTableAdapter
    Friend WithEvents InvoiceNew1 As InvoiceNew
    Friend WithEvents DT_InvoiceTableAdapter1 As InvoiceNewTableAdapters.DT_InvoiceTableAdapter
    Friend WithEvents InvoiceNew2 As InvoiceNew
    Friend WithEvents XrLabel87 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DS_PackingList1 As DS_PackingList
    Friend WithEvents DataTable1TableAdapter As DS_PackingListTableAdapters.DataTable1TableAdapter
    Friend WithEvents DataTable1TableAdapter1 As DS_PackingListTableAdapters.DataTable1TableAdapter
    Friend WithEvents DataTable1TableAdapter2 As DS_PackingListTableAdapters.DataTable1TableAdapter
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel186 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel185 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel182 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel181 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel180 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel179 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel178 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel177 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel176 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel175 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel174 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel173 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel172 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel171 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel170 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel169 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel168 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel167 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel166 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel165 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel164 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel163 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel162 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel161 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel160 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel159 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel158 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel157 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel156 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel155 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel154 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel153 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel152 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel151 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel150 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel149 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel148 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel147 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel146 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel145 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel144 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel143 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel141 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel138 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel137 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel136 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel135 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel134 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel133 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel132 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel131 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel130 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel129 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel125 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel90 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel89 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel42 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrCrossBandLine15 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine16 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrCrossBandLine17 As DevExpress.XtraReports.UI.XRCrossBandLine
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel124 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel116 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel115 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel114 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel110 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel109 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel108 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel107 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel106 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel94 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTable4 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell19 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell20 As DevExpress.XtraReports.UI.XRTableCell
End Class
