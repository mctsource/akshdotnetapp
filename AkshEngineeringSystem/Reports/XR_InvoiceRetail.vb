﻿Imports DevExpress.XtraPrinting

Public Class XR_InvoiceRetail
    Private Sub XR_Invoice_AfterPrint(sender As Object, e As EventArgs) Handles MyBase.AfterPrint
        Dim rpt As New XR_InvoiceRetailDuplicate
        rpt.Invoice.Value = Me.Invoice.Value
        rpt.Invoice.Visible = False
        rpt.XrLabel17.Text = "DELIVERY CHALLAN"
        rpt.XrLabel14.Text = "Original Copy"
        rpt.CreateDocument()
        Dim pages As PageList = rpt.PrintingSystem.Document.Pages
        Me.Pages.AddRange(pages)

        Dim rpt1 As New XR_InvoiceRetailDuplicate
        rpt1.Invoice.Value = Me.Invoice.Value
        rpt1.Invoice.Visible = False
        rpt1.XrLabel14.Text = "Duplicate Copy"
        rpt1.CreateDocument()
        Dim pages1 As PageList = rpt1.PrintingSystem.Document.Pages
        Me.Pages.AddRange(pages1)
    End Sub

End Class