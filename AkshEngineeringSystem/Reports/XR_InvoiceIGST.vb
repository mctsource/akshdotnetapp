﻿Imports System.Data.OleDb
Imports DevExpress.XtraPrinting

Public Class XR_InvoiceIGST

    'Private Sub XrLabel62_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel62.BeforePrint
    '    Dim lbl As DevExpress.XtraReports.UI.XRLabel
    '    lbl = CType(sender, DevExpress.XtraReports.UI.XRLabel)
    '    If (lbl.Text <> lbl.Text.ToUpper()) Then
    '        lbl.Text = lbl.Text.ToUpper()
    '    End If
    '    lbl.Text = lbl.Text.ToUpper()
    'End Sub

    'Private Sub XrLabel72_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel72.BeforePrint
    '    Dim lbl As DevExpress.XtraReports.UI.XRLabel
    '    lbl = CType(sender, DevExpress.XtraReports.UI.XRLabel)
    '    If (lbl.Text <> lbl.Text.ToUpper()) Then
    '        lbl.Text = lbl.Text.ToUpper()
    '    End If
    '    lbl.Text = lbl.Text.ToUpper()
    'End Sub

    'Private Sub XrTable1_BeforePrint(sender As Object, e As Printing.PrintEventArgs)
    '    Dim daIgst As OleDbDataAdapter
    '    Dim dtIgst As New DataTable
    '    Dim sqlIgst As String

    '    Dim daLI As OleDbDataAdapter
    '    Dim dtLI As New DataTable
    '    Dim sqlLineItems As String

    '    Dim daPacking As OleDbDataAdapter
    '    Dim dtPacking As New DataTable
    '    Dim sqlPacking As String

    '    Dim i, j As Integer
    '    sqlIgst = "SELECT DISTINCT IGSTRate from InvoiceDetail Where InvoiceID = " + Invoice.Value.ToString()
    '    sqlLineItems = "SELECT TaxableValue,IGSTRate,IGSTAmt from InvoiceDetail Where InvoiceID = " + Invoice.Value.ToString()
    '    sqlPacking = "SELECT PackingCharge,PIGSTRate,PIGSTAmt from Invoice Where InvoiceID = " + Invoice.Value.ToString()

    '    Try
    '        daIgst = New OleDbDataAdapter(sqlIgst, ConStr)
    '        daIgst.Fill(dtIgst)
    '        daLI = New OleDbDataAdapter(sqlLineItems, ConStr)
    '        daLI.Fill(dtLI)
    '        daPacking = New OleDbDataAdapter(sqlPacking, ConStr)
    '        daPacking.Fill(dtPacking)
    '        Dim list1 As New List(Of Object)
    '        Dim list2 As New List(Of Object)
    '        For i = 0 To dtIgst.Rows.Count - 1
    '            list1.Add(dtIgst.Rows(i).Item(0))
    '        Next
    '        For j = 0 To dtPacking.Rows.Count - 1
    '            Dim Pcharge As Double
    '            Pcharge = CDec(dtPacking.Rows(j).Item(0))

    '            If Not list1.Contains(dtPacking.Rows(j).Item(1)) And Not Pcharge = 0 Then
    '                list1.Add(dtPacking.Rows(j).Item(1))
    '            End If
    '        Next
    '        For i = 0 To list1.Count - 1
    '            Dim row As New DevExpress.XtraReports.UI.XRTableRow()
    '            Dim taxableVal As New DevExpress.XtraReports.UI.XRTableCell()
    '            Dim igstRate As New DevExpress.XtraReports.UI.XRTableCell()
    '            Dim igstAmt As New DevExpress.XtraReports.UI.XRTableCell()
    '            Dim taxVal As Decimal
    '            Dim igstAmnt As Decimal
    '            taxVal = 0
    '            igstAmnt = 0

    '            For j = 0 To dtLI.Rows.Count - 1
    '                If CDec(list1.Item(i).ToString()) = Convert.ToDecimal(dtLI.Rows(j).Item(1)) Then
    '                    taxVal = taxVal + Convert.ToDecimal(dtLI.Rows(j).Item(0))
    '                    igstAmnt = igstAmnt + Convert.ToDecimal(dtLI.Rows(j).Item(2))

    '                End If
    '            Next
    '            For j = 0 To dtPacking.Rows.Count - 1
    '                If CDec(list1.Item(i).ToString()) = Convert.ToDecimal(dtPacking.Rows(j).Item(1)) Then
    '                    taxVal = taxVal + Convert.ToDecimal(dtPacking.Rows(j).Item(0))
    '                    igstAmnt = igstAmnt + Convert.ToDecimal(dtPacking.Rows(j).Item(2))
    '                End If
    '            Next

    '            taxableVal.Borders = BorderSide.Left Or BorderSide.Bottom
    '            taxableVal.TextAlignment = TextAlignment.MiddleRight
    '            taxableVal.Text = Math.Round(taxVal, 2).ToString("F2")
    '            taxableVal.WidthF = 100

    '            igstRate.Text = list1.Item(i).ToString() + "%"
    '            igstRate.Borders = BorderSide.Left Or BorderSide.Bottom
    '            igstRate.TextAlignment = TextAlignment.MiddleRight
    '            igstRate.WidthF = 100

    '            igstAmt.Text = Math.Round(igstAmnt, 2).ToString("F2")
    '            igstAmt.Borders = BorderSide.Left Or BorderSide.Bottom
    '            igstAmt.TextAlignment = TextAlignment.MiddleRight
    '            igstAmt.WidthF = 100

    '            row.Cells.Add(taxableVal)
    '            row.Cells.Add(igstRate)
    '            row.Cells.Add(igstAmt)

    '            XrTable1.Rows.Add(row)
    '        Next
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub XrTable4_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTable4.BeforePrint
        Dim daIgst As OleDbDataAdapter
        Dim dtIgst As New DataTable
        Dim sqlIgst As String
        sqlIgst = "SELECT ProductName, Description, HSNACS, UOM, Qty, Rate, Amount, Discount, DiscountVal,  TaxableValue, IGSTRate, IGSTAmt, TotalAmount from InvoiceDetail Where InvoiceID = " + Invoice.Value.ToString()
        Try
            daIgst = New OleDbDataAdapter(sqlIgst, ConStr)
            daIgst.Fill(dtIgst)

            For i = 0 To dtIgst.Rows.Count - 1
                Dim RefVal As Integer
                RefVal = i + 1
                Dim row As New DevExpress.XtraReports.UI.XRTableRow()
                Dim Ref As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Prod As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Hsn As New DevExpress.XtraReports.UI.XRTableCell()
                Dim UOM As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Qty As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Rate As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Amt As New DevExpress.XtraReports.UI.XRTableCell()
                Dim Dis As New DevExpress.XtraReports.UI.XRTableCell()
                Dim DisVal As New DevExpress.XtraReports.UI.XRTableCell()
                Dim TaxVal As New DevExpress.XtraReports.UI.XRTableCell()

                Ref.Borders = BorderSide.Left Or BorderSide.Bottom Or BorderSide.Top
                Ref.TextAlignment = TextAlignment.MiddleCenter
                Ref.Text = RefVal.ToString()
                Ref.WidthF = 27.42

                If (dtIgst.Rows(i).Item(1) Is DBNull.Value) Then
                    Prod.Text = dtIgst.Rows(i).Item(0).ToString()
                Else
                    Prod.Text = dtIgst.Rows(i).Item(0).ToString() & Environment.NewLine & dtIgst.Rows(i).Item(1).ToString()
                    Prod.Multiline = True
                End If
                Prod.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Prod.TextAlignment = TextAlignment.MiddleLeft
                Prod.Padding = (2)
                Prod.WidthF = 241.01

                Hsn.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Hsn.TextAlignment = TextAlignment.MiddleCenter
                Hsn.Text = dtIgst.Rows(i).Item(2).ToString()
                Hsn.WidthF = 73.4

                UOM.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                UOM.TextAlignment = TextAlignment.MiddleCenter
                UOM.Padding = (2)
                UOM.Text = dtIgst.Rows(i).Item(3).ToString()
                UOM.WidthF = 39.56

                Qty.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Qty.TextAlignment = TextAlignment.MiddleCenter
                Qty.Text = dtIgst.Rows(i).Item(4).ToString()
                Qty.WidthF = 44.44

                Rate.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Dim rte As Decimal
                rte = Convert.ToDecimal(dtIgst.Rows(i).Item(5))
                Rate.Text = Math.Round(rte, 2).ToString("F2")
                Rate.TextAlignment = TextAlignment.MiddleRight
                Rate.WidthF = 97.02

                Amt.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Dim amount As Decimal
                amount = Convert.ToDecimal(dtIgst.Rows(i).Item(6))
                Amt.Text = Math.Round(amount, 2).ToString("F2")
                Amt.TextAlignment = TextAlignment.MiddleRight
                Amt.WidthF = 63.17

                If (dtIgst.Rows(i).Item(7) Is DBNull.Value) Then

                    Dis.Text = dtIgst.Rows(i).Item(7).ToString()
                Else
                    Dim discount As Decimal
                    discount = Convert.ToDecimal(dtIgst.Rows(i).Item(7))
                    Dis.Text = Math.Round(discount, 2).ToString("F2")
                End If
                Dis.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                Dis.TextAlignment = TextAlignment.MiddleRight
                Dis.WidthF = 34.9

                Dim discountVal As Decimal
                discountVal = Convert.ToDecimal(dtIgst.Rows(i).Item(8))
                DisVal.Text = Math.Round(discountVal, 2).ToString("F2")
                DisVal.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left
                DisVal.TextAlignment = TextAlignment.MiddleRight
                DisVal.WidthF = 60.05

                TaxVal.Borders = BorderSide.Bottom Or BorderSide.Top Or BorderSide.Left Or BorderSide.Right
                Dim taxableVal As Decimal
                taxableVal = Convert.ToDecimal(dtIgst.Rows(i).Item(9))
                TaxVal.Text = Math.Round(taxableVal, 2).ToString("F2")
                TaxVal.TextAlignment = TextAlignment.MiddleRight
                TaxVal.WidthF = 96.89

                row.Cells.Add(Ref)
                row.Cells.Add(Prod)
                row.Cells.Add(Hsn)
                row.Cells.Add(UOM)
                row.Cells.Add(Qty)
                row.Cells.Add(Rate)
                row.Cells.Add(Amt)
                row.Cells.Add(Dis)
                row.Cells.Add(DisVal)
                row.Cells.Add(TaxVal)
                'row.Borders = BorderSide.All
                XrTable4.Rows.Add(row)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class